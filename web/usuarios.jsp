<%-- 
    Copyright (C) 2014 Raúl Sempere Trujillo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Document   : usuarios
    Created on : 15-abr-2014, 00:46:00
    Author     : Raúl Sempere Trujillo
--%>

<%-- 
    Este documento muestra el formulario de gestion de usuarios del administrador,
    permitiendo añadir y quitar usuarios de la asignatura
--%>
 
<%@page import="java.util.ArrayList"%>
<%@page import="EN.AsignaturaEN"%>
<%@page import="EN.AccesoUsuarioAsignaturaEN"%>
<%@page import="CEN.AccesoUsuarioAsignaturaCEN"%>
<%@page contentType="text/html" pageEncoding="UTF-8" session="true" %>

<% String titulo = "Usuarios - Generador Inteligente de Cuestinarios"; %>

<%
    request.setCharacterEncoding("UTF-8");
    
    if (session.getAttribute("usuario") == null)
    {
       response.sendRedirect("login.jsp");
    }
    else if (session.getAttribute("asignatura") == null)
    {
       response.sendRedirect("asignaturas.jsp");
    }
    else
    {
        AccesoUsuarioAsignaturaCEN accesoUsuarioAsignaturaCEN = new AccesoUsuarioAsignaturaCEN();
        AccesoUsuarioAsignaturaEN accesoUsuarioAsignaturaEN = accesoUsuarioAsignaturaCEN.getAccesoByAsignaturaYUsuario((AsignaturaEN)session.getAttribute("asignatura"), (UsuarioEN)session.getAttribute("usuario"));
        
        if (!accesoUsuarioAsignaturaEN.isAdministrador())
            response.sendRedirect("index.jsp");
        else
        {
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><%= titulo %></title>
        <link rel="stylesheet" href="styles/main-stylesheet.css" />
        <link rel="stylesheet" href="styles/menu.css" />
        <script type="text/javascript" src="script/javascript.js"></script>
    </head>
    <body>
        <%@include file="includes/menu.jsp"%>
        <div id="contenido">
            <div id="caja">
                <h2 style="text-align: center">Agregar nuevo usuario</h2>
                <div class="borde">
                    <br />
                    <div class="texto_input">Usuario:</div>
                    <form name="agregarUsuario" action="usuarios.jsp" method="POST" accept-charset="utf-8">
                        <div id="listadoNoAcceso" style="float:right; margin-right: 350px">
                            <jsp:include page="includes/consultasAjax.jsp">
                                <jsp:param name="accion" value="usuariosSinAcceso" />
                            </jsp:include> 
                        </div>
                        <br /><br />
                        <div class="texto_input"><input name="admin" type="checkbox" /> Dar permisos para administrar la asignatura</div>
                        <div class="boton_tema" style="float: right; margin-right: 10px" onClick="agregarAcceso()">Agregar</div>
                        <br /><br />
                    </form>
                </div>
                <br />
                <div class="boton_tema" style="float: right; margin-right: 2px" onClick="location.href='index.jsp'">Volver</div>
                <br />
                <h2 style="text-align: center">Usuarios de la asignatura</h2>
                <h3 class="margen">Administradores:</h3>
                <div id="listadoAdmins">
                    <jsp:include page="includes/listadoUsuarios.jsp">
                        <jsp:param name="modo" value="admin" />
                    </jsp:include> 
                </div>
                <br />
                <h3 class="margen">Usuarios:</h3>
                <div id="listadoUsuarios">
                    <jsp:include page="includes/listadoUsuarios.jsp">
                        <jsp:param name="modo" value="usuario" />
                    </jsp:include> 
                </div>
                <br />
                <div class="boton_tema" style="float: right; margin-right: 2px" onClick="location.href='index.jsp'">Volver</div>
                <br />
            </div>
        </div>
    </body>
</html>
<%
        }
    }
%>