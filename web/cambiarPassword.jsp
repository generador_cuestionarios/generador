<%-- 
    Copyright (C) 2014 Raúl Sempere Trujillo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Document   : cambiarPassword
    Created on : 27-jul-2014, 14:32:00
    Author     : Raúl Sempere Trujillo
--%>
 
<%-- 
    Este documento se emplea cambiar la contraseña del usuario que ha
    iniciado sesión en la aplicación
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" session="true" %>

<% String titulo = "Cambiar contraseña - Generador Inteligente de Cuestinarios"; %>

<%
    request.setCharacterEncoding("UTF-8");
    String errorPass = "";
    String body = "";
    
    if (session.getAttribute("usuario") == null)
    {
       response.sendRedirect("login.jsp");
    }
    else
    {
        //Comprobamos si estamos cambiando la contraseña
        if (request.getParameter("nueva") != null)
        {
            //Intentamos cambiarla
            UsuarioCEN usuarioCEN = new UsuarioCEN();
            UsuarioEN usuarioEN = (UsuarioEN)session.getAttribute("usuario");
            
            //Si la contraseña coincide permitimos el cambio
            if (usuarioEN.getPassword().equals(request.getParameter("actual")))
            {
                body = "onLoad=\"alert('Contraseña cambiada correctamente.')\"";
                usuarioEN.setPassword(request.getParameter("nueva"));
                
                usuarioCEN.Modify(usuarioEN);
            }
            else
                errorPass = " - La contraseña actual introducida no es correcta.";
        }
        
%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><%= titulo %></title>
        <link rel="stylesheet" href="styles/main-stylesheet.css" />
        <link rel="stylesheet" href="styles/menu.css" />
        <script type="text/javascript" src="script/javascript.js"></script>
    </head>
    <body <%=body%>>
        <%@include file="includes/menu.jsp"%>
        <div id="contenido">
            <div id="caja">
                <h2 style="text-align: center">Cambio de contraseña</h2>
                <form name="cambioPassword" action="cambiarPassword.jsp" method="POST" accept-charset="utf-8">
                    <div class="texto_input">Introduce la contraseña actual:</div>
                    <div style="float:right; margin-right: 90px"><input style="width: 30em" type="password" name="actual"/></div><br /><br />
                    <div class="texto_input">Introduce la nueva contraseña:</div>
                    <div style="float:right; margin-right: 90px"><input style="width: 30em" type="password" name="nueva"/></div><br /><br />
                    <div class="texto_input">Repite la nueva contraseña:</div>
                    <div style="float:right; margin-right: 90px"><input style="width: 30em" type="password" name="repetida"/></div><br />
                    <br />
                    <div id="error"><%=errorPass%></div>
                    <br />
                    <div class="boton_tema" style="float: right; margin-right: 2px" onClick="location.href='index.jsp'">Volver</div>
                    <div class="boton_tema" style="float: right; margin-right: 10px" onClick="validarCambioPassword()">Cambiar</div>
                    <br /><br />
                </form>
            </div>
       </div>
    </body>
</html>
<%
    }
%>