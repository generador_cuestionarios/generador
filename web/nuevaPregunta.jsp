<%-- 
    Copyright (C) 2014 Raúl Sempere Trujillo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Document   : nuevaPregunta
    Created on : 08-abr-2014, 10:57:00
    Author     : Raúl Sempere Trujillo
--%>

<%-- 
    Este documento se utiliza para permitir al usuario crear una nueva pregunta
    en la asignatura.
--%>
        
<%@page import="EN.TemaEN"%>
<%@page import="java.util.ArrayList"%>
<%@page import="CEN.TemaCEN"%>
<%@page import="EN.AsignaturaEN"%>
<%@page contentType="text/html" pageEncoding="UTF-8" session="true" %>

<% String titulo = "Nueva pregunta - Generador Inteligente de Cuestinarios"; %>

<%
    String contenido = "";
    
    if (session.getAttribute("usuario") == null)
    {
       response.sendRedirect("login.jsp");
    }
    else if (session.getAttribute("asignatura") == null)
    {
       response.sendRedirect("asignaturas.jsp");
    }
    else
    {
        String body = "";
        
        if (request.getParameter("creada") != null)
            body = "onLoad=\"alert('Pregunta creada correctamente.')\"";
        
        AsignaturaEN asignaturaEN = (AsignaturaEN)session.getAttribute("asignatura");
        TemaCEN temaCEN = new TemaCEN();
        ArrayList<TemaEN> temas = temaCEN.getTemasByAsignatura(asignaturaEN);
        
        contenido += "<div id=\"caja\">";
        contenido += "<h2 style=\"text-align: center\">Crear una nueva pregunta</h2>";
        contenido += "      <div style=\"float: right; margin-right: 2px\" id=\"boton_pregunta\" onClick=\"location.href='verPreguntas.jsp'\">Volver</div>";
        contenido += "      <div style=\"float: right; margin-right: 10px\" id=\"boton_pregunta\" onClick=\"validarPregunta()\">Crear Pregunta</div>";
        contenido += "      <br />";
        contenido += "  <form name=\"nuevaPregunta\" action=\"guardarPregunta.jsp\" method=\"POST\" accept-charset=\"utf-8\">";
        contenido += "      <h3 class=\"margen\">Enunciado:</h3>";
        contenido += "      <div class=\"borde\">";
        contenido += "          <div class=\"margen\"><textarea name=\"enunciado\" cols=\"82\" rows=\"6\"></textarea></div>";
        contenido += "      </div>";
        contenido += "      <br />";
        contenido += "      <h3 class=\"margen\">Opciones:</h3>";
        contenido += "      <div class=\"borde\">";
                
        //ponemos las opciones
        for (int i = 1; i <= asignaturaEN.getNumeroOpciones(); i++)
        {
            contenido += "<div class=\"margen\">Opción " + i + ":<br /><textarea style=\"vertical-align: middle\" name=\"opc" + i + "\" cols=\"70\" rows=\"3\"></textarea>"
                    + "<input type=\"radio\" name=\"opciones\" value=\"cor" + i + "\"";
            if (i == 1)
                contenido +=  " checked";

            contenido += "/> Correcta</div>";
        }

        contenido += "      </div>";
        contenido += "      <br />";
        contenido += "      <h3 class=\"margen\">Clasificación:</h3>";
        contenido += "      <div class=\"borde\">";
        contenido += "          <div class=\"margen\"><select name=\"tema\" style=\"min-width: 300px; max-width: 700px\">";
        contenido += "              <option value=\"auto\" selected>Automática</option>";
        contenido += "              <option value=\"sin\">Sin clasificar</option>";
        for (TemaEN temaEN : temas)
        {
            contenido += "          <option value=\"" + temaEN.getCodigo() + "\">" + temaEN.getNombre() + "</option>";
        }
        contenido += "          </select></div>";
        contenido += "      </div>";
        contenido += "      <br />";
        contenido += "      <h3 class=\"margen\">Otras opciones:</h3>";
        contenido += "      <div class=\"borde\">";
        contenido += "          <div class=\"margen\"><input type=\"checkbox\" name=\"proximoUso\" checked /> Utilizar en el próximo examen</div>";
        contenido += "          <div class=\"margen\"><input type=\"checkbox\" name=\"traduccion\" /> Revisar la traducción después de crear la pregunta</div>";
        contenido += "      </div>";
        contenido += "      <br />";
        contenido += "      <div id=\"error\"></div>";
        contenido += "      <br />";
        contenido += "      <div style=\"float: right; margin-right: 2px\" id=\"boton_pregunta\" onClick=\"location.href='verPreguntas.jsp'\">Volver</div>";
        contenido += "      <div style=\"float: right; margin-right: 10px\" id=\"boton_pregunta\" onClick=\"validarPregunta()\">Crear Pregunta</div>";
        contenido += "      <br /><br />";
        contenido += "  </form>";
        contenido += "</div>";
%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><%= titulo %></title>
        <link rel="stylesheet" href="styles/main-stylesheet.css" />
        <link rel="stylesheet" href="styles/menu.css" />
        <script type="text/javascript" src="script/javascript.js"></script>
    </head>
    <body <%=body%>>
        <%@include file="includes/menu.jsp"%>
        <div id="contenido">
            <%= contenido %>
        </div>
    </body>
</html>
<%
    }
%>