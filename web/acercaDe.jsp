<%-- 
    Copyright (C) 2014 Raúl Sempere Trujillo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Document   : acercaDe
    Created on : 03-ago-2014, 15:25:00
    Author     : Raúl Sempere Trujillo
--%>
 
<%-- 
    Este documento muestra el acerca de, de todo el proyecto.
--%>

<%
    String titulo = "Acerca de - Generador de Cuestinarios";  
%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><%= titulo %></title>
        <link rel="stylesheet" href="styles/main-stylesheet.css" />
        <link rel="stylesheet" href="styles/menu.css" />
        <script type="text/javascript" src="script/javascript.js"></script>
    </head>
    <body>
        <%@include file="includes/menu.jsp"%>
        <div id="contenido">
            <div id="caja">
                <h2 class="margen">Acerca de Generador de cuestinarios</h2><br />
                <div class="borde">
                        <div class="margen">Raúl Sempere Trujillo</div>
                        <div class="margen">Trabajo de Fin de Grado - Grado en Ingeniería Informática</div>
                        <div class="margen">Escuela Politécnica Superior - Universidad de Alicante</div>
                        <div class="margen">Versión: 3.0</div>
                </div>
                <br />
                <div class="boton_tema" style="float: right; margin-right: 2px" onClick="location.href='index.jsp'">Volver</div>
                <br />
            </div>
       </div>
    </body>
</html>