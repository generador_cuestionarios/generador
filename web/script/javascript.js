/* 
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : javascript
 * Created on : 20-abr-2014, 16:20:00
 * Author     : Raúl Sempere Trujillo
 */


/*
 * Contiene todas las funciones javascript y las consultas Ajax de
 * la aplicación.
 * 
 * Nota: el parametro force=(new Date()).getTime() que aparece en los GET
 * solo sirve para compatibilizar la aplicación con Internet Explorer,
 * ya que este, si se realizan 2 consultas iguales, no realiza la segunda
 * y obtiene los datos de caché. Para forzar a que realice todas las consultas, 
 * se le pasa el tiempo como parametro para que cada consulta sea única.
 * 
 */

//Variables necesarias inicializadas a 1
var num_keywords = 1;
var num_instrucciones = 1;
var paginaSeleccionar = 1;
var paginaExamen = 1;
var paginaSeleccionadas = 1;

var itemsPorPagina = 10; //debe coincidir con itemsPorPagina del listadoPreguntas.jsp
var itemsVer = 10; //debe coincidir con itemsPorPagina del listadoPreguntas.jsp
var itemsExamen = 10; //debe coincidir con itemsPorPagina del listadoExamenes.jsp
var timerBarraXML; //para renovar el estado de carga XML.
var timerBarraReclasificar; //para renovar el estado de reclasificación.
var intercambioPregunta = -1; //Indica si se esta intercambiando alguna pregunta
var divOculto = true; //Para los tooltips de las preguntas
var tooltip = 0; //Utilizado para que no se quede el tooltip en medio de la pantalla
var procesandoAjax = false;//Solo se puede procesar 1 ajax a la vez por lo que creamos un cerrojo para la zona crítica


/**
 * Crea una instancia de Ajax y la devuelve
 * 
 * @returns {ActiveXObject|XMLHttpRequest|Boolean}
 */
function Ajax()
{
    var xmlhttp = false;
    
    try
    {
        xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    }
    catch(ex)
    {
        try 
        {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        catch(ex)
        {
            xmlhttp = false;
        }
    }
    if (!xmlhttp && typeof XMLHttpRequest !== 'undefined')
    {
        xmlhttp = new XMLHttpRequest();
    }
    
    return xmlhttp;
}


/**
 * Elimina un tema seleccionado del listado de temas.
 * 
 * @param {int} tema Codigo del tema a eliminar
 */
function eliminarTema(tema)
{
    if(confirm('¿Está seguro de que desea borrar el tema seleccionado? Las preguntas del tema se quedarán sin clasificar.'))
    {
        var contenido = document.getElementById('listadoTemas');
        var ajax=Ajax();
        var ajax2=Ajax();

        if (!procesandoAjax)
        {
            document.body.style.cursor = 'wait';
            procesandoAjax = true;
            ajax.open("GET", "includes/consultasAjax.jsp?accion=eliminarTema&tema=" + tema + "&force=" + (new Date()).getTime());
            ajax.onreadystatechange=function()
            {
                if (ajax.readyState === 4)
                {
                    if (ajax.responseText.trim() === "true")
                    {
                        ajax2.open("GET", "includes/listadoTemas.jsp?modo=ver" + "&force=" + (new Date()).getTime());
                        ajax2.onreadystatechange=function()
                        {
                            procesandoAjax = false;
                            document.body.style.cursor = 'auto';
                            if (ajax2.readyState === 4)
                            {
                                contenido.innerHTML = ajax2.responseText;
                            }
                        };

                        ajax2.send(null);
                    }
                    else
                    {
                        document.body.style.cursor = 'auto';
                        procesandoAjax = false;
                    }
                }
                else
                {
                    document.body.style.cursor = 'auto';
                    procesandoAjax = false;
                }
            };

            ajax.send(null);
        }
    }
}

/**
 * Elimina la preparacion de examen seleccionada del listado.
 * 
 * @param {int} examen Codigo del examen de la preparación a eliminar
 * @param {int} idioma Codigo del idioma de la preparación a eliminar
 */
function eliminarPreparacionExamen(examen, idioma)
{
    if(confirm('¿Está seguro de que desea borrar la traducción del examen?'))
    {
        var contenido = document.getElementById('contenido');
        var ajax=Ajax();
        var ajax2=Ajax();
        
        if (!procesandoAjax)
        {    
            procesandoAjax = true;
            document.body.style.cursor = 'wait';
            ajax.open("GET", "includes/consultasAjax.jsp?accion=eliminarPreparacion&examen=" + examen + "&idioma=" + idioma + "&force=" + (new Date()).getTime());
            ajax.onreadystatechange=function()
            {
                if (ajax.readyState === 4)
                {
                    if (ajax.responseText.trim() === "true")
                    {
                        ajax2.open("GET", "includes/listadoPreparaciones.jsp?examen=" + examen + "&force=" + (new Date()).getTime());
                        ajax2.onreadystatechange=function()
                        {
                            document.body.style.cursor = 'auto';
                            procesandoAjax = false;

                            if (ajax2.readyState === 4)
                            {
                                contenido.innerHTML = ajax2.responseText;
                            }
                        };

                        ajax2.send(null);
                    }
                    else
                    {
                        document.body.style.cursor = 'auto';
                        procesandoAjax = false;
                    }
                }
                else
                {
                    document.body.style.cursor = 'auto';
                    procesandoAjax = false;
                }
            };

            ajax.send(null);
        }
    }
}

/**
 * Revisa la clasificación de la pregunta y la marca en el formulario como revisada
 * 
 * @param {int} pregunta Codigo de la pregunta a validar.
 */
function validarClasificacion(pregunta)
{
    var imagen = document.getElementById('estado');
    var ajax=Ajax();

    if (!procesandoAjax)
    {    
        document.body.style.cursor = 'wait';
        procesandoAjax = true;
        ajax.open("GET", "includes/consultasAjax.jsp?accion=validarClasificacion&pregunta=" + pregunta + "&force=" + (new Date()).getTime());
        ajax.onreadystatechange=function()
        {
            document.body.style.cursor = 'auto';
            procesandoAjax = false;

            if (ajax.readyState === 4)
            {
                imagen.src = "images/ok.png";
                document.getElementById("link").innerHTML = "";
            }
        };
    
        ajax.send(null);
    }
}

/**
 * Revisa la clasificación de la pregunta permitiendo cambiar el tema de la
 * clasificación, el cual se coge del select de la pregunta en el listado
 * 
 * @param {int} pregunta Codigo de la pregunta a validar e indice para obtener
 * el tema y modificar la imagen y link correctos
 */
function validarClasificacionListado(pregunta)
{
    var imagen = document.getElementById('img' + pregunta);
    var link = document.getElementById('link' + pregunta);
    var form = document.forms['formPreguntas'];
    var tema = form['tema' + pregunta].value;
    
    var select =  document.getElementById('tema' + pregunta);
    
    if (tema !== '-1')
    {
        var ajax=Ajax();

        if (!procesandoAjax)
        {    
            document.body.style.cursor = 'wait';
            procesandoAjax = true;
            ajax.open("GET", "includes/consultasAjax.jsp?accion=validarClasificacionTema&pregunta=" + pregunta + "&tema=" + tema + "&force=" + (new Date()).getTime());
            ajax.onreadystatechange=function()
            {
                document.body.style.cursor = 'auto';
                procesandoAjax = false;

                if (ajax.readyState === 4)
                {
                    imagen.src = "images/ok.png";
                    link.innerHTML = "";
                    select.innerHTML = ajax.responseText.trim();
                }
            };
            ajax.send(null);
        }
    }
}

/**
 * Llama a la funcion Ajax cancelarExamen para borrar las variables de sesión
 * y vuelve al formulario de examenes
 */
function cancelarExamen()
{
    var ajax=Ajax();

    if (!procesandoAjax)
    {    
        document.body.style.cursor = 'wait';
        procesandoAjax = true;
        ajax.open("GET", "includes/consultasAjax.jsp?accion=cancelarExamen" + "&force=" + (new Date()).getTime());
        ajax.onreadystatechange=function()
        {
            document.body.style.cursor = 'auto';
            procesandoAjax = false;

            if (ajax.readyState === 4)
            {
                location.href='verExamenes.jsp';
            }
        };
    
        ajax.send(null);
    }
}

/**
 * Inicia la reclasificación de las preguntas
 */
function reclasificar()
{
    if (confirm('Se volverán a clasificar todas las preguntas sin clasificar y sin revisar.\nEsta acción puede tardar varios minutos.\n\n¿Desea continuar?'))
        location.href='reclasificar.jsp';
}

/**
 * Carga en el navegador el listado de traducciones de un examen
 * 
 * @param {int} codigo Codigo del examen a cargar.
 */
function preparacionesExamen(codigo)
{
    location.href='verTraduccionesExamen.jsp?examen=' + codigo;
}

/**
 * Inicia la creación de una traducción de un examen pasado por parámentro
 * 
 * @param {int} examen Codigo del examen a traducir.
 */
function comenzarTraduccionExamen(examen)
{
    var ajax=Ajax();
    var form = document.forms["idioma"];
    var contenido = document.getElementById('traduccionExamen');

    if (!procesandoAjax)
    {    
        document.body.style.cursor = 'wait';
        procesandoAjax = true;
        ajax.open("GET", "includes/consultasAjax.jsp?accion=traduccionExamen&examen=" + examen + "&idioma=" + form["selectIdioma"].value + "&force=" + (new Date()).getTime());
        ajax.onreadystatechange=function()
        {
            document.body.style.cursor = 'auto';
            procesandoAjax = false;

            if (ajax.readyState === 4)
            {            
                contenido.innerHTML = ajax.responseText;

                //Bloqueamos el cbo y el botón
                document.getElementById('idioma').disabled = true;
                document.getElementById('seleccionar').disabled = true;
            }
        };

        ajax.send(null);
    }
}

/**
 * Carga la traducción de una pregunta en el idioma seleccionado
 * en el select "idiomas"
 * 
 * @param {int} pregunta Código de la pregunta a cargar
 */
function cambiarIdiomaRevision(pregunta)
{
    var form = document.forms["revisarTraduccionPregunta"];

    form.action = "revisarTraduccionPregunta.jsp?pregunta=" + pregunta + "&idioma=" + form["idiomas"].value + "&guardar=false";
    
    document.body.style.cursor = 'wait';
    form.submit(); 
}

/**
 * Valida y guarda la traducción de un examen
 */
function guardarTraduccionExamen()
{
    var form = document.forms["traduccionExamen"];
    var error = "";

    if (form["nombre"].value === null || form["nombre"].value === "")
    {
        if (error !== "")
            error += "<br />";
        
        error += " - La traducción del nombre del examen no puede estar vacía.";
    }
    if (form["fecha"].value === null || form["fecha"].value === "")
    {
        if (error !== "")
            error += "<br />";
        
        error += " - La traducción de la fecha del examen en texto no puede estar vacía.";
    }    
    if (form["duracion"].value === null || form["duracion"].value === "")
    {
        if (error !== "")
            error += "<br />";
        
        error += " - La traducción de la duración del examen no puede estar vacía.";
    }

    var i = 1;
    while (form["inst"+i] !== undefined)
    {
        if (form["inst"+i].value === null || form["inst"+i].value === "")
        {
            if (error !== "")
                error += "<br />";

            error += " - Alguna de las instrucciónes no ha sido traducida.";
            
            //Solo mostramos un error de este tipo
            break;
        }
        i++;
    }

    if (error !== "")
        document.getElementById('error').innerHTML = error;
    else
    {
        document.body.style.cursor = 'wait';
        form.submit();
    }
}

/**
 * Valida y guarda la traducción de una pregunta.
 */
function guardarTraduccionPregunta()
{
    var form = document.forms["revisarTraduccionPregunta"];
    var error = "";
    if (form["traduccionEnunciado"].value === null || form["traduccionEnunciado"].value === "")
    {
        if (error !== "")
            error += "<br />";
        
        error += " - La traducción del enunciado no puede estar vacía.";
    }
    var i = 1;
    while (form["traduccionOpc"+i] !== undefined)
    {
        if (form["traduccionOpc"+i].value === null || form["traduccionOpc"+i].value === "")
        {
            if (error !== "")
                error += "<br />";

            error += " - Alguna de las opciones no ha sido traducida.";
            
            //Solo mostramos un error de este tipo
            break;
        }
        i++;
    }

    if (error !== "")
        document.getElementById('error').innerHTML = error;
    else
    {
        document.body.style.cursor = 'wait';
        form.submit();
    }
}

/**
 * Habilita el formulario para poder editar la pregunta cargada.
 */
function editarPregunta()
{
    //Habilitamos los campos editables
    document.getElementById("link").style.display="";
    document.getElementById("enunciado").disabled = false;
    document.getElementById("tema").disabled = false;
    
    if (document.getElementById("proximoUso") !== null)
    {
        document.getElementById("proximoUso").disabled = false;
        document.getElementById("traduccion").disabled = false;
        document.getElementById("habilitada").disabled = false;
        document.getElementById("revisar").style.display="";
    } 
    
    var opciones = document.getElementsByClassName("opcion");
    var correctos = document.getElementsByClassName("correcto");

    for (var i = 0; i < opciones.length; i++)
    {
        opciones[i].disabled = false;
        correctos[i].disabled = false;
    }
    
    var botones = document.getElementsByClassName("botonEditar");
    
    for (var i = 0; i < botones.length; i++)
    {
        botones[i].innerHTML = "<div id=\"boton_examen\" onClick=\"guardarEdicionPregunta()\">Guardar cambios</div>";
    }
}

/**
 * Habilita el formulario para poder editar el tema cargado.
 */
function editarTema()
{
    //Habilitamos los campos editables
    document.getElementById("nombre").disabled = false;
    document.getElementById("nuevaKeyword").disabled = false;
    var keys = document.getElementsByClassName("keyword");
    var pesos = document.getElementsByClassName("peso");

    for (var i = 0; i < keys.length; i++)
    {
        keys[i].disabled = false;
        pesos[i].disabled = false;
    }

    var botones = document.getElementsByClassName("botonEditar");
    
    for (var i = 0; i < botones.length; i++)
    {
        botones[i].innerHTML = "<div id=\"boton_examen\" onClick=\"guardarEdicionTema()\">Guardar cambios</div>";
    }
}

/**
 * Habilita el formulario para poder editar el examen cargado.
 */
function editarExamen()
{
    var contenidoSeleccionar = document.getElementById('listaSeleccionarConfirmacion');
    var contenidoSeleccionadas = document.getElementById('listaSeleccionadasConfirmacion');
    var ajax=Ajax();
    var ajax2=Ajax();
    
    if (!procesandoAjax)
    {    
        document.body.style.cursor = 'wait';
        procesandoAjax = true;
        ajax.open("GET", "includes/listadoPreguntas.jsp?modo=seleccionarEditar&pagina=" + paginaSeleccionar + "&force=" + (new Date()).getTime());
        ajax.onreadystatechange=function()
        {
            if (ajax.readyState === 4)
            {
                ajax2.open("GET", "includes/listadoPreguntas.jsp?modo=seleccionadasEditar&pagina=" + paginaSeleccionadas + "&editar=True" + "&force=" + (new Date()).getTime());

                ajax2.onreadystatechange=function()
                {
                    document.body.style.cursor = 'auto';
                    procesandoAjax = false;

                    if (ajax2.readyState === 4)
                    {
                        //Actualizamos con los nuevos listados obtenidos e información
                        document.getElementById("seleccionar").innerHTML = "Preguntas disponibles:";
                        document.getElementById("seleccionadas").innerHTML = "Preguntas seleccionadas para el examen:";
                        contenidoSeleccionar.innerHTML = ajax.responseText;
                        contenidoSeleccionadas.innerHTML = ajax2.responseText;
                        document.getElementById("ext_filtro").style.display = "";

                        //Habilitamos los campos editables
                        document.getElementById("nombre").disabled = false;
                        document.getElementById("fecha").disabled = false;
                        document.getElementById("fechaTexto").disabled = false;
                        document.getElementById("modalidades").disabled = false;
                        document.getElementById("duracion").disabled = false;
                        document.getElementById("duracionTexto").disabled = false;
                        document.getElementById("nuevaInstruccion").disabled = false;

                        var botones = document.getElementsByClassName("botonEditar");
    
                        for (var i = 0; i < botones.length; i++)
                        {
                            botones[i].innerHTML = "<div id=\"boton_examen\" onClick=\"guardarEdicionExamen()\">Guardar cambios</div>";
                        }    

                        var instrucciones = document.getElementsByClassName("instruccion");

                        for (var i = 0; i < instrucciones.length; i++)
                        {
                            instrucciones[i].disabled = false;
                        }
                    }
                };

                ajax2.send(null);
             }
             else
             {
                 document.body.style.cursor = 'auto';
                 procesandoAjax = false;
             }
        };

        ajax.send(null);
        }
}

/**
 * Elimina el examen pasado por parametro y recarga el listado
 * 
 * @param {int} codigo Código del examen a eliminar
 */
function eliminarExamen(codigo)
{
    if(confirm('¿Está seguro de que desea borrar el examen seleccionado?'))
    {
        var ajax=Ajax();
        
        if (!procesandoAjax)
        {    
            procesandoAjax = true;
            document.body.style.cursor = 'wait';

            ajax.open("GET", "includes/consultasAjax.jsp?accion=borrarExamen&examen=" + codigo + "&force=" + (new Date()).getTime());
            ajax.onreadystatechange=function()
            {
                if (ajax.readyState === 4)
                {
                    var res = ajax.responseText.trim();

                    if (res === "true")
                    {
                        //Recargar el listado
                        var ajax2=Ajax();
                        ajax2.open("GET", "includes/listadoExamenes.jsp?pagina=1" + "&force=" + (new Date()).getTime());
                        ajax2.onreadystatechange=function()
                        {
                            document.body.style.cursor = 'auto';
                            procesandoAjax = false;

                            if (ajax2.readyState === 4)
                            {
                                var contenido = document.getElementById('listadoExamenes');
                                contenido.innerHTML = ajax2.responseText;
                            }
                        };

                        ajax2.send(null);                   
                    }
                    else
                    {
                        document.body.style.cursor = 'auto';
                        procesandoAjax = false;
                        alert("No se puede borrar un examen analizado.");
                    }
                }
                else
                {
                    document.body.style.cursor = 'auto';
                    procesandoAjax = false;
                }
            };
            ajax.send(null);
        }
    }
}

/**
 * Llama a la funcion jsp que genera el paquete zip con los documentos LaTeX,
 * obtiene la ruta que devuelve e inicia la descarga
 * 
 * @param {int} examen Código del examen que se generará
 * @param {int} idioma Código del idioma en que se generará
 */
function generarLatex(examen, idioma)
{
    var ajax=Ajax();
    if (!procesandoAjax)
    {    
        document.body.style.cursor = 'wait';
        procesandoAjax = true;
        ajax.open("GET", "includes/consultasAjax.jsp?accion=generarLatex&examen=" + examen + "&idioma=" + idioma + "&force=" + (new Date()).getTime());
        ajax.onreadystatechange=function()
        {
            document.body.style.cursor = 'auto';
            procesandoAjax = false;

            if (ajax.readyState === 4)
            {
                location.href=ajax.responseText.trim();
            }
        };

        ajax.send(null);
    }
}

/**
 * Llama a la funcion jsp que genera el paquete zip con los documentos PDF,
 * obtiene la ruta que devuelve e inicia la descarga
 * 
 * @param {int} examen Código del examen que se generará
 * @param {int} idioma Código del idioma en que se generará
 */
function generarPdf(examen, idioma)
{
    var ajax=Ajax();

    if (!procesandoAjax)
    {    
        document.body.style.cursor = 'wait';
        procesandoAjax = true;
        ajax.open("GET", "includes/consultasAjax.jsp?accion=generarPdf&examen=" + examen + "&idioma=" + idioma + "&force=" + (new Date()).getTime());
        ajax.onreadystatechange=function()
        {
            document.body.style.cursor = 'auto';
            procesandoAjax = false;

            if (ajax.readyState === 4)
            {
                location.href=ajax.responseText.trim();
            }
        };

        ajax.send(null);
    }
}

/**
 * Elimina la pregunta pasada por parametro y recarga el listado de preguntas
 * 
 * @param {int} codigo Código de la pregunta a eliminar
 */
function eliminarPregunta(codigo)
{
    var ajax=Ajax();
    var ajax2=Ajax();
    
    var listado = document.getElementById('ver');

    if (!procesandoAjax)
    {    
        document.body.style.cursor = 'wait';
        procesandoAjax = true;
        ajax.open("GET", "includes/consultasAjax.jsp?accion=eliminarPregunta&pregunta=" + codigo + "&force=" + (new Date()).getTime());
        ajax.onreadystatechange=function()
        {
            if (ajax.readyState === 4)
            {
                ajax2.open("GET", "includes/listadoPreguntas.jsp?modo=ver&pagina=" + paginaSeleccionadas + "&force=" + (new Date()).getTime());
                ajax2.onreadystatechange=function()
                {
                    document.body.style.cursor = 'auto';
                    procesandoAjax = false;

                    if (ajax2.readyState === 4)
                    {
                        if (ajax.responseText.trim() !== "true")
                            alert("No se eliminará la pregunta porque se utiliza en algún examen, en su lugar se deshabilitará.");

                        listado.innerHTML = ajax2.responseText;
                    }
                };
                ajax2.send(null);
            }
            else
            {
                document.body.style.cursor = 'auto';
                procesandoAjax = false;
            }
        };

        ajax.send(null);
    }

}

/**
 * Guarda la pregunta modificada en el formulario de edición
 */
function guardarEdicionPregunta()
{
    var form = document.forms["editar"];
    var error = "";
    var i = 1;
    
    if (form["enunciado"].value === null || form["enunciado"].value === "")
    {
        error += " - El enunciado de la pregunta no puede estar en blanco";
    }
  
   while ((new String(form[("opc" + i.toString())])).trim() !== "undefined")
    {
        if (form[("opc" + i.toString())].value.toString() === "")
        {
            if (error !== "")
                error += "<br />";
            
            error += " - No se pueden dejar opciones en blanco";
            
            //Solo mostramos un error de este tipo así que salimos
            break;
        }
        
        i++;
    }
    
    if (error !== "")
        document.getElementById('error').innerHTML = error;
    else
    {
        document.body.style.cursor = 'wait';
        form.submit();
    }
}

/**
 * Guarda el tema modificado en el formulario de edición
 */
function guardarEdicionTema()
{
    var form = document.forms["editar"];
    var error = "";
    
    if (form["nombre"].value === null || form["nombre"].value === "")
    {
        error += " - El nombre del tema no puede estar en blanco";
    }
    
    if (error !== "")
        document.getElementById('error').innerHTML = error;
    else
    {
        document.body.style.cursor = 'wait';
        form.submit();
    }
}

/**
 * Guarda el examen modificado en el formulario de edición
 */
function guardarEdicionExamen()
{
    var form = document.forms["editar"];
    
    var error = "";
    
    if (form["nombre"].value === null || form["nombre"].value === "")
    {
        error += " - El nombre del examen no puede estar en blanco.";
    }
    
    if (form["fecha"].value === null || form["fecha"].value === "")
    {
        if (error !== "")
            error += "<br />";
        
        error += " - La fecha del examen no puede estar en blanco.";
    }
    
    if (form["fechaTexto"].value === null || form["fechaTexto"].value === "")
    {
        if (error !== "")
            error += "<br />";
        
        error += " - Introduce la fecha (en el idioma de la asignatura) tal y como se verá en el examen (p.e: 5 de junio de 2014)";
    }
    
    if (form["modalidades"].value === null || form["modalidades"].value === "")
    {
        if (error !== "")
            error += "<br />";
        
        error += " - Introduce el número de modalidades que tendrá el examen.";
    }
   
    if (form["duracion"].value === null || form["duracion"].value === "")
    {
        if (error !== "")
            error += "<br />";
        
        error += " - Introduce la duración que tendrá el examen.";
    }
    
    if (form["duracionTexto"].value === null || form["duracionTexto"].value === "")
    {
        if (error !== "")
            error += "<br />";
        
        error += " - Introduce la duración (en el idioma de la asignatura) tal y como se verá en el examen (p.e: 1 hora 30 min)";
    }
    
    if (error !== "")
        document.getElementById('error').innerHTML = error;
    else
    {
        document.getElementById('discriminacion').disabled = false;
        document.getElementById('dificultad').disabled = false;
        
        document.body.style.cursor = 'wait';
        form.submit();
    }
}

/**
 * Actualiza en el listado de tmas la lista de preguntas disponibles por tema
 * según la fecha de último uso de las preguntas
 */
function actualizarCantidadPreguntasTemas()
{
    //codigos de los temas
    var codigos = "";
    
    var form = document.forms["selTemas"];
    var i = 0;
    
    while (form["incluir" + i.toString()] !== undefined)
    {
        codigos += form["codigo" + i].value;

        if (form["incluir" + (i+1).toString()] !== undefined)
            codigos += ",";

        i++;
    }
    
    var ajax=Ajax();

    if (!procesandoAjax)
    {    
        document.body.style.cursor = 'wait';
        procesandoAjax = true;
        ajax.open("GET", "includes/consultasAjax.jsp?accion=cantidadPreguntas&temas=" + codigos + "&ultimoUso=" + form["fechaMax"].value + "&force=" + (new Date()).getTime());
        ajax.onreadystatechange=function()
        {
            document.body.style.cursor = 'auto';
            procesandoAjax = false;

            if (ajax.readyState === 4)
            {
                var response = ajax.responseText.trim();
                var cantidades = response.split(",");
                i = 0;
                while (form["incluir" + i.toString()] !== undefined)
                {  
                    document.getElementById("disponible" + i).innerHTML = cantidades[i];
                    i++;
                }
            }
        };

        ajax.send(null);
    }
}

/**
 * Abre el formulario para ver el detalle de un examen pasado por parámentro
 * 
 * @param {int} codigo Código del examen
 */
function detalleExamen(codigo)
{
    document.body.style.cursor = 'wait';
    location.href='detalleExamen.jsp?examen=' + codigo;
}

/**
 * Habilita un campo más para añadir otra keyword en un tema
 */
function anyadirKeyword()
{
    num_keywords++;
    var contenido = document.getElementById('keywords');
    var nuevo = document.createElement('div');
    nuevo.className = "margen";
    
    nuevo.innerHTML = "Keyword " + num_keywords + ":&nbsp;&nbsp;&nbsp;"
            + "<input type=\"text\" name=\"key" + num_keywords +"\" style=\"width: 250px\" />"
            + "&nbsp;&nbsp;&nbsp;Peso:&nbsp;&nbsp;&nbsp;"
            + "<input type=\"number\" name=\"peso" + num_keywords + "\" min=\"1\" max=\"10\" value=\"5\" />";

    contenido.appendChild(nuevo);
}

/**
 * Habilita un campo más para añadir otra instrucción a un examen
 */
function anyadirInstruccion()
{
    num_instrucciones++;
    var contenido = document.getElementById('instrucciones');
    var nuevo = document.createElement('div');
    nuevo.className = "margen";
    
    nuevo.innerHTML = "Instrucción " + num_instrucciones + ":&nbsp;&nbsp;"
            + "<textarea style=\"vertical-align: top\" class=\"instruccion\" name=\"inst" + num_instrucciones + "\" cols=\"70\" rows=\"3\"></textarea>";

    contenido.appendChild(nuevo);
}

/**
 * Actualiza el número actual de instrucciones
 * 
 * @param {int} instr Numero de instrocciónes a establecer
 */
function actualizarContadorInstrucciones(instr)
{
    num_instrucciones = instr;
}

/**
 * Actualiza el número actual de keywords
 * 
 * @param {int} keys Numero de keywords a establecer
 */
function actualizarContadorKeywords(keys)
{
    num_keywords = keys;
}

/**
 * Valida el tema y si los campos son correctos ejecuta el submit del formulario.
 */
function validarTema()
{
    var form = document.forms["nuevoTema"];
    var error = "";
    
    if (form["nombre"].value === null || form["nombre"].value === "")
    {
        error += " - El nombre del tema no puede estar en blanco";
    }
    
    if (error !== "")
        document.getElementById('error').innerHTML = error;
    else
    {
        document.body.style.cursor = 'wait';
        form.submit();
    }
}

/**
 * Valida el formulario de subida del XML, si es correcto ejecuta el submit
 */
function validarXML()
{
    var form = document.forms["xml"];
    var error = "";
    
    if (form["browser"].value === null || form["browser"].value === "")
    {
        error += " - Selecciona un fichero XML para que sea examinado.";
    }
    
    if (error !== "")
        document.getElementById('error').innerHTML = error;
    else
    {
        document.body.style.cursor = 'wait';
        form.submit();
    }
}

/**
 * Valida el formulario de subida del Excel, si es correcto ejecuta el submit
 */
function validarExcel()
{
    var form = document.forms["excel"];
    var error = "";
    
    if (form["browser"].value === null || form["browser"].value === "")
    {
        error += " - Selecciona un fichero Excel para que sea examinado.";
    }
    
    if (error !== "")
        document.getElementById('error').innerHTML = error;
    else
    {   
        document.body.style.cursor = 'wait';
        form.submit();
    }
}

/**
 * Realiza el submit dle formulario.
 */
function validarInstrucciones()
{
    //Realmente no se valida nada.
    //Se pueden poner las instrucciones que se quieran
    document.body.style.cursor = 'wait';
    
    var form = document.forms["insertarInstrucciones"];
    form.submit();
}


/**
 * Añade una pregunta al examen.
 * 
 * @param {int} codigo Código de la pregunta a añadir
 * @param {String} tipo Se utiliza para decir el listado a recargar
 * (selección, confirmación, o edición)
 */
function addPregunta(codigo, tipo)
{
    var ajax=Ajax();

    if (tipo === "Editar")
    {
        var listaSeleccionar = document.getElementById('listaSeleccionarConfirmacion');
        var listaSeleccionadas = document.getElementById('listaSeleccionadasConfirmacion');
    }
    else
    {
        var listaSeleccionar = document.getElementById('listaSeleccionar');
        var listaSeleccionadas = document.getElementById('listaSeleccionadas');
    }

    //Comprobamos el cambio de página
    var totalPreguntas = document.getElementById('totalSeleccionar').innerHTML;
    
    if (paginaSeleccionar === Math.ceil((totalPreguntas)/itemsPorPagina) && paginaSeleccionar > 1)
        paginaSeleccionar = Math.ceil((totalPreguntas-1)/itemsPorPagina);

    if (!procesandoAjax)
    {    
        document.body.style.cursor = 'wait';
        procesandoAjax = true;
        
        ajax.open("GET", "includes/consultasAjax.jsp?accion=add&codigo=" + codigo + "&tipo=" + tipo + "&force=" + (new Date()).getTime());
        ajax.onreadystatechange=function()
        {
            if (ajax.readyState === 4)
            {
                var ajax2=Ajax();
                ajax2.open("GET", "includes/listadoPreguntas.jsp?modo=seleccionar" + tipo  + "&pagina=" + paginaSeleccionar + "&force=" + (new Date()).getTime());
                ajax2.onreadystatechange=function()
                {
                    if (ajax2.readyState === 4)
                    {     
                        var ajax3=Ajax();
                        ajax3.open("GET", "includes/listadoPreguntas.jsp?modo=seleccionadas" + tipo + "&pagina=" + paginaSeleccionadas + "&editar=True&force=" + (new Date()).getTime());
                        ajax3.onreadystatechange=function()
                        {
                            document.body.style.cursor = 'auto';
                            procesandoAjax = false;

                            if (ajax3.readyState === 4)
                            {
                                listaSeleccionar.innerHTML = ajax2.responseText;
                                listaSeleccionadas.innerHTML = ajax3.responseText;

                                //Actualizamos la discriminación y la dificultad
                                if (tipo === "Editar")
                                {
                                    var form1 = document.forms["editar"];
                                    var form2 = document.forms["preguntas"];

                                    form1["discriminacion"].value = form2["discriminacionO"].value;
                                    form1["dificultad"].value = form2["dificultadO"].value;
                                }
                            }
                        };
                        ajax3.send(null);
                    }
                    else
                    {
                        document.body.style.cursor = 'auto';
                        procesandoAjax = false;
                    }
                };
                ajax2.send(null);
            }
            else
            {
                document.body.style.cursor = 'auto';
                procesandoAjax = false;
            }
        };
        ajax.send(null);
    }
}

/**
 * Elimina una pregunta del examen.
 * 
 * @param {int} codigo Código de la pregunta a eliminar
 * @param {String} tipo Se utiliza para decir el listado a recargar
 * (selección, confirmación, o edición)
 */
function delPregunta(codigo, tipo)
{
    var ajax=Ajax();

    if (tipo === "Editar")
    {
        var listaSeleccionar = document.getElementById('listaSeleccionarConfirmacion');
        var listaSeleccionadas = document.getElementById('listaSeleccionadasConfirmacion');
    }
    else
    {
        var listaSeleccionar = document.getElementById('listaSeleccionar');
        var listaSeleccionadas = document.getElementById('listaSeleccionadas');
    }

    //Comprobamos el cambio de página
    var totalPreguntas = document.getElementById('totalSeleccionadas').innerHTML;
    
    if (paginaSeleccionadas === Math.ceil((totalPreguntas)/itemsPorPagina) && paginaSeleccionadas > 1)
        paginaSeleccionadas = Math.ceil((totalPreguntas-1)/itemsPorPagina);

    if (!procesandoAjax)
    {    
        document.body.style.cursor = 'wait';
        procesandoAjax = true;
        
        ajax.open("GET", "includes/consultasAjax.jsp?accion=del&codigo=" + codigo + "&tipo=" + tipo + "&force=" + (new Date()).getTime());
        ajax.onreadystatechange=function()
        {
            if (ajax.readyState === 4)
            {
                var ajax2=Ajax();
                ajax2.open("GET", "includes/listadoPreguntas.jsp?modo=seleccionar" + tipo + "&pagina=" + paginaSeleccionar + "&force=" + (new Date()).getTime());
                ajax2.onreadystatechange=function()
                {
                    if (ajax2.readyState === 4)
                    {
                        var ajax3=Ajax();
                        ajax3.open("GET", "includes/listadoPreguntas.jsp?modo=seleccionadas" + tipo + "&pagina=" + paginaSeleccionadas + "&editar=True" + "&force=" + (new Date()).getTime());
                        ajax3.onreadystatechange=function()
                        {
                            document.body.style.cursor = 'auto';
                            procesandoAjax = false;

                            if (ajax3.readyState === 4)
                            {
                                listaSeleccionar.innerHTML = ajax2.responseText;
                                listaSeleccionadas.innerHTML = ajax3.responseText;

                                //Actualizamos la discriminación y la dificultad
                                if (tipo === "Editar")
                                {
                                    var form1 = document.forms["editar"];
                                    var form2 = document.forms["preguntas"];

                                    form1["discriminacion"].value = form2["discriminacionO"].value;
                                    form1["dificultad"].value = form2["dificultadO"].value;
                                }
                            }
                        };
                        ajax3.send(null);
                    }
                    else
                    {
                        document.body.style.cursor = 'auto';
                        procesandoAjax = false;
                    }
                };
                ajax2.send(null);
            }
            else
            {
                document.body.style.cursor = 'auto';
                procesandoAjax = false;
            }
        };
        ajax.send(null);
    }
}

/**
 * Cambia una pregunta por otra aleatoria en el examen
 * 
 * @param {int} codigo Código de la pregunta a cambiar
 * @param {String} tipo Se utiliza para decir el listado a recargar
 * (selección, confirmación, o edición)
 */
function cambiarPregunta(codigo, tipo)
{
    var ajax=Ajax();

    var listaSeleccionar = document.getElementById('listaSeleccionarConfirmacion');
    var listaSeleccionadas = document.getElementById('listaSeleccionadasConfirmacion');
    
    if (!procesandoAjax)
    {   
        document.body.style.cursor = 'wait';
        procesandoAjax = true;
        
        if (tipo === "Editar")
            ajax.open("GET", "includes/consultasAjax.jsp?accion=generarPreguntaEditar&pregunta=" + codigo + "&force=" + (new Date()).getTime());
        else
            ajax.open("GET", "includes/consultasAjax.jsp?accion=generarPregunta&pregunta=" + codigo + "&force=" + (new Date()).getTime());
      
        ajax.onreadystatechange=function()
        {
            if (ajax.readyState === 4)
            {
                if (ajax.responseText.trim() === "true")
                {
                    intercambioPregunta = -1;

                    var ajax2=Ajax();
                    ajax2.open("GET", "includes/listadoPreguntas.jsp?modo=seleccionar" + tipo + "&pagina=" + paginaSeleccionar + "&force=" + (new Date()).getTime());
                    ajax2.onreadystatechange=function()
                    {
                        if (ajax2.readyState === 4)
                        {         
                            var ajax3=Ajax();
                            ajax3.open("GET", "includes/listadoPreguntas.jsp?modo=seleccionadas" + tipo + "&pagina=" + paginaSeleccionadas + "&editar=True" + "&force=" + (new Date()).getTime());
                            ajax3.onreadystatechange=function()
                            {
                                document.body.style.cursor = 'auto';
                                procesandoAjax = false;

                                if (ajax3.readyState === 4)
                                {
                                    listaSeleccionar.innerHTML = ajax2.responseText;
                                    listaSeleccionadas.innerHTML = ajax3.responseText;

                                    //Actualizamos la discriminación y la dificultad
                                    if (tipo === "Editar")
                                    {
                                        var form1 = document.forms["editar"];
                                        var form2 = document.forms["preguntas"];

                                        form1["discriminacion"].value = form2["discriminacionO"].value;
                                        form1["dificultad"].value = form2["dificultadO"].value;
                                    }
                                }
                            };
                            ajax3.send(null);
                        }
                        else
                        {
                            procesandoAjax = false;
                            document.body.style.cursor = 'auto';
                        }
                    };
                    ajax2.send(null);
                }
                else
                {
                    procesandoAjax = false;
                    document.body.style.cursor = 'auto';
                    alert("No existen preguntas candidatas para realizar este intercambio.");
                }
            }
            else
            {
                procesandoAjax = false;
                document.body.style.cursor = 'auto';
            }
        };
        ajax.send(null);
    }
}

///Tipo indica si es intercambio al crear examen o al editar las 
//preguntas del examen ya que son listados distintos

/**
 * Intercambia la pregunta almacenada en "intercambioPregunta" por la pregunta
 * pasada por parámetro
 * 
 * @param {int} codigo Código de la pregunta nueva del intercambio
 * @param {String} tipo Se utiliza para decir el listado a recargar
 * (selección, confirmación, o edición)
 */
function intercambiar(codigo, tipo)
{
    var ajax=Ajax();
    var listaSeleccionar = document.getElementById('listaSeleccionarConfirmacion');
    var listaSeleccionadas = document.getElementById('listaSeleccionadasConfirmacion');
    
    if (!procesandoAjax)
    {   
        document.body.style.cursor = 'wait';
        procesandoAjax = true;
        ajax.open("GET", "includes/consultasAjax.jsp?accion=intercambiar&intercambio=" + intercambioPregunta + "&codigo=" + codigo + "&tipo=" + tipo + "&force=" + (new Date()).getTime());
        intercambioPregunta = -1;

        ajax.onreadystatechange=function()
        {
            if (ajax.readyState === 4)
            {
                var ajax2=Ajax();
                ajax2.open("GET", "includes/listadoPreguntas.jsp?modo=seleccionar" + tipo + "&pagina=" + paginaSeleccionar + "&force=" + (new Date()).getTime());
                ajax2.onreadystatechange=function()
                {
                    if (ajax2.readyState === 4)
                    {         
                        var ajax3=Ajax();
                        ajax3.open("GET", "includes/listadoPreguntas.jsp?modo=seleccionadas" + tipo + "&pagina=" + paginaSeleccionadas + "&editar=True" + "&force=" + (new Date()).getTime());
                        ajax3.onreadystatechange=function()
                        {
                            document.body.style.cursor = 'auto';
                            procesandoAjax = false;

                            if (ajax3.readyState === 4)
                            {
                                listaSeleccionar.innerHTML = ajax2.responseText;
                                listaSeleccionadas.innerHTML = ajax3.responseText;
                                
                                //Actualizamos la discriminación y la dificultad
                                if (tipo === "Editar")
                                {
                                    var form1 = document.forms["editar"];
                                    var form2 = document.forms["preguntas"];

                                    form1["discriminacion"].value = form2["discriminacionO"].value;
                                    form1["dificultad"].value = form2["dificultadO"].value;
                                }
                            }
                        };
                        ajax3.send(null);
                    }
                    else
                    {
                        document.body.style.cursor = 'auto';
                        procesandoAjax = false;
                    }
                };
                ajax2.send(null);
            }
            else
            {
                document.body.style.cursor = 'auto';
                procesandoAjax = false;
            }
        };
        ajax.send(null);
    }
}

/**
 * Almacena una pregunta en "intercambioPregunta para que sea intercambiada
 * despues por la funcion intercambio()
 * 
 * @param {int} codigo Código de la pregunta a intercambiar
 */
function iniciarIntercambioPregunta(codigo)
{
    //Para marcar de rojo la tabla de seleccionadas
    var tr = document.getElementById("tr" + intercambioPregunta);
    var display = "none";

    if (intercambioPregunta !== -1)
    {
        for (var i = 0; i < tr.cells.length; i++)
        {
            tr.cells[i].style.backgroundColor="white";
        }
    }
    
    if (intercambioPregunta === codigo)
    {
        intercambioPregunta = -1;
    }
    else
    {
        tr = document.getElementById("tr" + codigo);

        for (var i = 0; i < tr.cells.length; i++)
        {
            tr.cells[i].style.backgroundColor="#A8FFC7";
        }

        intercambioPregunta = codigo;
    }
    
    //Para cambiar la visibilidad de intercambio
    if (intercambioPregunta !== -1)
        display = "";
    
    //th
    document.getElementById("thIntercambio").style.display = display;
    
    //td
    var td = document.getElementsByClassName("tdIntercambio");
    
    for (var i = 0; i < td.length; i++)
    {
        td[i].style.display = display;
    }
}

/**
 * Mueve el listado pasado por parametro a su primera página
 * 
 * @param {String} listado Listado a manejar
 * @returns {undefined}
 */
function primeraPagina(listado)
{  
    var contenido;
    var ajax=Ajax();
    if (!procesandoAjax)
    {    
        document.body.style.cursor = 'wait';
        procesandoAjax = true;
        if (listado === "seleccionarExamen")
        {
            var contenido = document.getElementById('listaSeleccionar');
            paginaSeleccionar = 1;

            ajax.open("GET", "includes/listadoPreguntas.jsp?modo=seleccionarExamen&pagina=" + paginaSeleccionar + "&force=" + (new Date()).getTime());
        }
        else if (listado === "seleccionadasExamen")
        {
            var contenido = document.getElementById('listaSeleccionadas');
            paginaSeleccionadas = 1;

            ajax.open("GET", "includes/listadoPreguntas.jsp?modo=seleccionadasExamen&pagina=" + paginaSeleccionadas + "&force=" + (new Date()).getTime());
        }
        else if (listado === "seleccionarExamenConfirmacion")
        {
            var contenido = document.getElementById('listaSeleccionarConfirmacion');
            paginaSeleccionar = 1;

            ajax.open("GET", "includes/listadoPreguntas.jsp?modo=seleccionarExamenConfirmacion&pagina=" + paginaSeleccionadas + "&force=" + (new Date()).getTime());
        }
        else if (listado === "seleccionadasExamenConfirmacion")
        {
            var contenido = document.getElementById('listaSeleccionadasConfirmacion');
            paginaSeleccionadas = 1;
            intercambioPregunta = -1;

            ajax.open("GET", "includes/listadoPreguntas.jsp?modo=seleccionadasExamenConfirmacion&pagina=" + paginaSeleccionadas + "&force=" + (new Date()).getTime());
        }
        else if (listado === "seleccionarEditar")
        {
            var contenido = document.getElementById('listaSeleccionarConfirmacion');
            paginaSeleccionar = 1;

            ajax.open("GET", "includes/listadoPreguntas.jsp?modo=seleccionarEditar&pagina=" + paginaSeleccionadas + "&force=" + (new Date()).getTime());
        }
        else if (listado === "seleccionadasEditarTrue" || listado === "seleccionadasEditarFalse")
        {
            var contenido = document.getElementById('listaSeleccionadasConfirmacion');
            paginaSeleccionadas = 1;
            intercambioPregunta = -1;

            if (listado === "seleccionadasEditarTrue")
                ajax.open("GET", "includes/listadoPreguntas.jsp?modo=seleccionadasEditar&editar=True&pagina=" + paginaSeleccionadas + "&force=" + (new Date()).getTime());
            else
                ajax.open("GET", "includes/listadoPreguntas.jsp?modo=seleccionadasEditar&editar=False&pagina=" + paginaSeleccionadas + "&force=" + (new Date()).getTime());
        }
        else if (listado === "ver")
        {
            var contenido = document.getElementById('ver');
            paginaSeleccionar = 1;

            ajax.open("GET", "includes/listadoPreguntas.jsp?modo=ver&pagina=" + paginaSeleccionar + "&force=" + (new Date()).getTime());
        }

        ajax.onreadystatechange=function()
        {
            document.body.style.cursor = 'auto';
            procesandoAjax = false;

            if (ajax.readyState === 4)
            {
                contenido.innerHTML = ajax.responseText;
                
                //Para cambiar la visibilidad de intercambio
                if (intercambioPregunta !== -1)
                {
                    //th
                    document.getElementById("thIntercambio").style.display = "";

                    //td
                    var td = document.getElementsByClassName("tdIntercambio");

                    for (var i = 0; i < td.length; i++)
                    {
                        td[i].style.display = "";
                    }
                }
                else if (document.getElementById("thIntercambio") !== null)
                {
                    //th
                    document.getElementById("thIntercambio").style.display = "none";

                    //td
                    var td = document.getElementsByClassName("tdIntercambio");

                    for (var i = 0; i < td.length; i++)
                    {
                        td[i].style.display = "none";
                    } 
                }
            }
        };

        ajax.send(null);
    }
}

/**
 * Mueve el listado pasado por parametro a una página anterior
 * 
 * @param {String} listado Listado a manejar
 */
function anteriorPagina(listado)
{  
    var contenido;
    var ajax=Ajax();
    
    if (!procesandoAjax)
    {    
        document.body.style.cursor = 'wait';
        procesandoAjax = true;
        if (listado === "seleccionarExamen")
        {
            var contenido = document.getElementById('listaSeleccionar');
            if (paginaSeleccionar > 1)
            {
                paginaSeleccionar--;
            }

            ajax.open("GET", "includes/listadoPreguntas.jsp?modo=seleccionarExamen&pagina=" + paginaSeleccionar + "&force=" + (new Date()).getTime());
        }
        else if (listado === "seleccionadasExamen")
        {
            var contenido = document.getElementById('listaSeleccionadas');

            if (paginaSeleccionadas > 1)
            {
                paginaSeleccionadas--;
            }

            ajax.open("GET", "includes/listadoPreguntas.jsp?modo=seleccionadasExamen&pagina=" + paginaSeleccionadas + "&force=" + (new Date()).getTime());
        }
        else if (listado === "seleccionarExamenConfirmacion")
        {
            var contenido = document.getElementById('listaSeleccionarConfirmacion');

            if (paginaSeleccionar > 1)
            {
                paginaSeleccionar--;
            }

            ajax.open("GET", "includes/listadoPreguntas.jsp?modo=seleccionarExamenConfirmacion&pagina=" + paginaSeleccionar + "&force=" + (new Date()).getTime());
        }
        else if (listado === "seleccionadasExamenConfirmacion")
        {
            var contenido = document.getElementById('listaSeleccionadasConfirmacion');
            intercambioPregunta = -1;

            if (paginaSeleccionadas > 1)
            {
                paginaSeleccionadas--;
            }

            ajax.open("GET", "includes/listadoPreguntas.jsp?modo=seleccionadasExamenConfirmacion&pagina=" + paginaSeleccionadas + "&force=" + (new Date()).getTime());
        }
        else if (listado === "seleccionarEditar")
        {
            var contenido = document.getElementById('listaSeleccionarConfirmacion');

            if (paginaSeleccionar > 1)
            {
                paginaSeleccionar--;
            }

            ajax.open("GET", "includes/listadoPreguntas.jsp?modo=seleccionarEditar&pagina=" + paginaSeleccionar + "&force=" + (new Date()).getTime());
        }
        else if (listado === "seleccionadasEditarTrue" || listado === "seleccionadasEditarFalse")
        {
            var contenido = document.getElementById('listaSeleccionadasConfirmacion');
            intercambioPregunta = -1;

            if (paginaSeleccionadas > 1)
            {
                paginaSeleccionadas--;
            }

            if (listado === "seleccionadasEditarTrue")
                ajax.open("GET", "includes/listadoPreguntas.jsp?modo=seleccionadasEditar&editar=True&pagina=" + paginaSeleccionadas + "&force=" + (new Date()).getTime());
            else
                ajax.open("GET", "includes/listadoPreguntas.jsp?modo=seleccionadasEditar&editar=False&pagina=" + paginaSeleccionadas + "&force=" + (new Date()).getTime());
        }
        else if (listado === "ver")
        {
            var contenido = document.getElementById('ver');

            if (paginaSeleccionar > 1)
            {
                paginaSeleccionar--;
            }

            ajax.open("GET", "includes/listadoPreguntas.jsp?modo=ver&pagina=" + paginaSeleccionar + "&force=" + (new Date()).getTime());
        }

        ajax.onreadystatechange=function()
        {
            procesandoAjax = false;
            document.body.style.cursor = 'auto';
            
            if (ajax.readyState === 4)
            {
                contenido.innerHTML = ajax.responseText;

                //Para cambiar la visibilidad de intercambio
                if (intercambioPregunta !== -1)
                {
                    //th
                    document.getElementById("thIntercambio").style.display = "";

                    //td
                    var td = document.getElementsByClassName("tdIntercambio");

                    for (var i = 0; i < td.length; i++)
                    {
                        td[i].style.display = "";
                    }
                }
                else if (document.getElementById("thIntercambio") !== null)
                {
                    //th
                    document.getElementById("thIntercambio").style.display = "none";

                    //td
                    var td = document.getElementsByClassName("tdIntercambio");

                    for (var i = 0; i < td.length; i++)
                    {
                        td[i].style.display = "none";
                    } 
                }
            }
        };

        ajax.send(null);
    }
}

/**
 * Mueve el listado pasado por parametro a su siguiente página
 * 
 * @param {String} listado Listado a manejar
 */
function siguientePagina(listado)
{
    var contenido;
    var ajax=Ajax();
    
    if (!procesandoAjax)
    {    
        document.body.style.cursor = 'wait';
        procesandoAjax = true;
        if (listado === "seleccionarExamen")
        {
            var totalPreguntas = document.getElementById('totalSeleccionar').innerHTML;
            var contenido = document.getElementById('listaSeleccionar');

            if (paginaSeleccionar < Math.ceil(totalPreguntas/itemsPorPagina))
            {
                paginaSeleccionar++;
            }

            ajax.open("GET", "includes/listadoPreguntas.jsp?modo=seleccionarExamen&pagina=" + paginaSeleccionar + "&force=" + (new Date()).getTime());
        }
        else if (listado === "seleccionadasExamen")
        {
            var totalPreguntas = document.getElementById('totalSeleccionadas').innerHTML;
            var contenido = document.getElementById('listaSeleccionadas');

            if (paginaSeleccionadas < Math.ceil(totalPreguntas/itemsPorPagina))
            {
                paginaSeleccionadas++;
            }

            ajax.open("GET", "includes/listadoPreguntas.jsp?modo=seleccionadasExamen&pagina=" + paginaSeleccionadas + "&force=" + (new Date()).getTime());
        }
        if (listado === "seleccionarExamenConfirmacion")
        {
            var totalPreguntas = document.getElementById('totalSeleccionar').innerHTML;
            var contenido = document.getElementById('listaSeleccionarConfirmacion');

            if (paginaSeleccionar < Math.ceil(totalPreguntas/itemsPorPagina))
            {
                paginaSeleccionar++;
            }

            ajax.open("GET", "includes/listadoPreguntas.jsp?modo=seleccionarExamenConfirmacion&pagina=" + paginaSeleccionar + "&force=" + (new Date()).getTime());
        }
        else if (listado === "seleccionadasExamenConfirmacion")
        {
            var totalPreguntas = document.getElementById('totalSeleccionadas').innerHTML;
            var contenido = document.getElementById('listaSeleccionadasConfirmacion');
            intercambioPregunta = -1;

            if (paginaSeleccionadas < Math.ceil(totalPreguntas/itemsPorPagina))
            {
                paginaSeleccionadas++;
            }

            ajax.open("GET", "includes/listadoPreguntas.jsp?modo=seleccionadasExamenConfirmacion&pagina=" + paginaSeleccionadas + "&force=" + (new Date()).getTime());
        }
        if (listado === "seleccionarEditar")
        {
            var totalPreguntas = document.getElementById('totalSeleccionar').innerHTML;
            var contenido = document.getElementById('listaSeleccionarConfirmacion');

            if (paginaSeleccionar < Math.ceil(totalPreguntas/itemsPorPagina))
            {
                paginaSeleccionar++;
            }

            ajax.open("GET", "includes/listadoPreguntas.jsp?modo=seleccionarEditar&pagina=" + paginaSeleccionar + "&force=" + (new Date()).getTime());
        }
        else if (listado === "seleccionadasEditarTrue" || listado === "seleccionadasEditarFalse")
        {
            var totalPreguntas = document.getElementById('totalSeleccionadas').innerHTML;
            var contenido = document.getElementById('listaSeleccionadasConfirmacion');
            intercambioPregunta = -1;

            if (paginaSeleccionadas < Math.ceil(totalPreguntas/itemsPorPagina))
            {
                paginaSeleccionadas++;
            }

            if (listado === "seleccionadasEditarTrue")
                ajax.open("GET", "includes/listadoPreguntas.jsp?modo=seleccionadasEditar&editar=True&pagina=" + paginaSeleccionadas + "&force=" + (new Date()).getTime());
            else
                ajax.open("GET", "includes/listadoPreguntas.jsp?modo=seleccionadasEditar&editar=False&pagina=" + paginaSeleccionadas + "&force=" + (new Date()).getTime());
        }
        else if (listado === "ver")
        {
            var totalPreguntas = document.getElementById('totalVer').innerHTML;
            var contenido = document.getElementById('ver');

            if (paginaSeleccionar < Math.ceil(totalPreguntas/itemsVer))
            {
                paginaSeleccionar++;
            }

            ajax.open("GET", "includes/listadoPreguntas.jsp?modo=ver&pagina=" + paginaSeleccionar + "&force=" + (new Date()).getTime());
        }

        ajax.onreadystatechange=function()
        {
            document.body.style.cursor = 'auto';
            procesandoAjax = false;

            if (ajax.readyState === 4)
            {
                contenido.innerHTML = ajax.responseText;

                //Para cambiar la visibilidad de intercambio
                if (intercambioPregunta !== -1)
                {
                    //th
                    document.getElementById("thIntercambio").style.display = "";

                    //td
                    var td = document.getElementsByClassName("tdIntercambio");

                    for (var i = 0; i < td.length; i++)
                    {
                        td[i].style.display = "";
                    }
                }
                else if (document.getElementById("thIntercambio") !== null)
                {
                    //th
                    document.getElementById("thIntercambio").style.display = "none";

                    //td
                    var td = document.getElementsByClassName("tdIntercambio");

                    for (var i = 0; i < td.length; i++)
                    {
                        td[i].style.display = "none";
                    } 
                }
            }
        };

        ajax.send(null);
    }
}

/**
 * Mueve el listado pasado por parametro a su última página
 * 
 * @param {String} listado Listado a manejar
 */
function ultimaPagina(listado)
{  
    var contenido;
    var ajax=Ajax();
    
    if (!procesandoAjax)
    {    
        document.body.style.cursor = 'wait';
        procesandoAjax = true;
        if (listado === "seleccionarExamen")
        {
            var contenido = document.getElementById('listaSeleccionar');
            var totalPreguntas = document.getElementById('totalSeleccionar').innerHTML;
            paginaSeleccionar = Math.ceil(totalPreguntas/itemsPorPagina);

            ajax.open("GET", "includes/listadoPreguntas.jsp?modo=seleccionarExamen&pagina=" + paginaSeleccionar + "&force=" + (new Date()).getTime());
        }
        else if (listado === "seleccionadasExamen")
        {
            var contenido = document.getElementById('listaSeleccionadas');
            var totalPreguntas = document.getElementById('totalSeleccionadas').innerHTML;
            paginaSeleccionadas = Math.ceil(totalPreguntas/itemsPorPagina);

            ajax.open("GET", "includes/listadoPreguntas.jsp?modo=seleccionadasExamen&pagina=" + paginaSeleccionadas + "&force=" + (new Date()).getTime());
        }
        if (listado === "seleccionarExamenConfirmacion")
        {
            var contenido = document.getElementById('listaSeleccionarConfirmacion');
            var totalPreguntas = document.getElementById('totalSeleccionar').innerHTML;
            paginaSeleccionar = Math.ceil(totalPreguntas/itemsPorPagina);

            ajax.open("GET", "includes/listadoPreguntas.jsp?modo=seleccionarExamenConfirmacion&pagina=" + paginaSeleccionar + "&force=" + (new Date()).getTime());
        }
        else if (listado === "seleccionadasExamenConfirmacion")
        {
            var contenido = document.getElementById('listaSeleccionadasConfirmacion');
            var totalPreguntas = document.getElementById('totalSeleccionadas').innerHTML;
            intercambioPregunta = -1;
            paginaSeleccionadas = Math.ceil(totalPreguntas/itemsPorPagina);

            ajax.open("GET", "includes/listadoPreguntas.jsp?modo=seleccionadasExamenConfirmacion&pagina=" + paginaSeleccionadas + "&force=" + (new Date()).getTime());
        }
        if (listado === "seleccionarEditar")
        {
            var contenido = document.getElementById('listaSeleccionarConfirmacion');
            var totalPreguntas = document.getElementById('totalSeleccionar').innerHTML;
            paginaSeleccionar = Math.ceil(totalPreguntas/itemsPorPagina);

            ajax.open("GET", "includes/listadoPreguntas.jsp?modo=seleccionarEditar&pagina=" + paginaSeleccionar + "&force=" + (new Date()).getTime());
        }
        else if (listado === "seleccionadasEditarTrue" || listado === "seleccionadasEditarFalse")
        {
            var contenido = document.getElementById('listaSeleccionadasConfirmacion');
            var totalPreguntas = document.getElementById('totalSeleccionadas').innerHTML;
            intercambioPregunta = -1;
            paginaSeleccionadas = Math.ceil(totalPreguntas/itemsPorPagina);

            if (listado === "seleccionadasEditarTrue")
                ajax.open("GET", "includes/listadoPreguntas.jsp?modo=seleccionadasEditar&editar=True&pagina=" + paginaSeleccionadas + "&force=" + (new Date()).getTime());
            else
                ajax.open("GET", "includes/listadoPreguntas.jsp?modo=seleccionadasEditar&editar=False&pagina=" + paginaSeleccionadas + "&force=" + (new Date()).getTime());
        }
        else if (listado === "ver")
        {
            var contenido = document.getElementById('ver');
            var totalPreguntas = document.getElementById('totalVer').innerHTML;
            paginaSeleccionar = Math.ceil(totalPreguntas/itemsVer);

            ajax.open("GET", "includes/listadoPreguntas.jsp?modo=ver&pagina=" + paginaSeleccionar + "&force=" + (new Date()).getTime());
        }

        ajax.onreadystatechange=function()
        {
            document.body.style.cursor = 'auto';
            procesandoAjax = false;

            if (ajax.readyState === 4)
            {
                contenido.innerHTML = ajax.responseText;

                //Para cambiar la visibilidad de intercambio
                if (intercambioPregunta !== -1)
                {
                    //th
                    document.getElementById("thIntercambio").style.display = "";

                    //td
                    var td = document.getElementsByClassName("tdIntercambio");

                    for (var i = 0; i < td.length; i++)
                    {
                        td[i].style.display = "";
                    }
                }
                else if (document.getElementById("thIntercambio") !== null)
                {
                    //th
                    document.getElementById("thIntercambio").style.display = "none";

                    //td
                    var td = document.getElementsByClassName("tdIntercambio");

                    for (var i = 0; i < td.length; i++)
                    {
                        td[i].style.display = "none";
                    } 
                }
            }
        };

        ajax.send(null);
    }
}
/**
 * Valida la pregunta introducida, en caso de éxito ejecuta un submit
 */
function validarPregunta()
{
    var form = document.forms["nuevaPregunta"];
    var error = "";
    var i = 1;
    
    if (form["enunciado"].value === null || form["enunciado"].value === "")
    {
        error += " - El enunciado de la pregunta no puede estar en blanco";
    }
  
   while ((new String(form[("opc" + i.toString())])).trim() !== "undefined")
    {
        if (form[("opc" + i.toString())].value.toString() === "")
        {
            if (error !== "")
                error += "<br />";
            
            error += " - No se pueden dejar opciones en blanco";
            
            //Solo mostramos un error de este tipo así que salimos
            break;
        }
        
        i++;
    }
    
    if (error !== "")
        document.getElementById('error').innerHTML = error;
    else
    {
        document.body.style.cursor = 'wait';
        form.submit();
    }
}

/**
 * Mueve el formulario de inicio del examen. Si los campos son correctos
 * ejecuta un submit.
 */
function validarComenzarExamen()
{
    var form = document.forms["nuevoExamen"];
    var error = "";
    
    if (form["nombre"].value === null || form["nombre"].value === "")
    {
        error += " - El nombre del examen no puede estar en blanco.";
    }
    
    if (form["fecha"].value === null || form["fecha"].value === "")
    {
        if (error !== "")
            error += "<br />";
        
        error += " - La fecha del examen no puede estar en blanco.";
    }
    
    if (form["fechaTexto"].value === null || form["fechaTexto"].value === "")
    {
        if (error !== "")
            error += "<br />";
        
        error += " - Introduce la fecha (en el idioma de la asignatura) tal y como se verá en el examen (p.e: 5 de junio de 2014)";
    }
    
    if (form["preguntas"].value === null || form["preguntas"].value === "")
    {
        if (error !== "")
            error += "<br />";
        
        error += " - Introduce el número de preguntas del examen.";
    }
    
    if (form["modalidades"].value === null || form["modalidades"].value === "")
    {
        if (error !== "")
            error += "<br />";
        
        error += " - Introduce el número de modalidades que tendrá el examen.";
    }
   
    if (form["duracion"].value === null || form["duracion"].value === "")
    {
        if (error !== "")
            error += "<br />";
        
        error += " - Introduce la duración que tendrá el examen.";
    }
    
    if (form["duracionTexto"].value === null || form["duracionTexto"].value === "")
    {
        if (error !== "")
            error += "<br />";
        
        error += " - Introduce la duración (en el idioma de la asignatura) tal y como se verá en el examen (p.e: 1 hora 30 min)";
    }
    
    if (error !== "")
        document.getElementById('error').innerHTML = error;
    else
    {
        document.body.style.cursor = 'wait';
        form.submit();
    }
}

/**
 * Valida la selección de temas y los porcentajes introducidos en el tercer paso
 * de creación de un examen
 * 
 * @param {int} cantidad Cantidad de preguntas del examen
 */
function validarSeleccionTemas(cantidad)
{
    var form = document.forms["selTemas"];
    var error = "";
    var i = 0;
    var disponibles, necesaria;
    var suma_porcientos = 0.0;
    
    while (form["codigo" + i.toString()] !== undefined)
    {
        if (form['incluir' + i.toString()].checked)
        {
            //Comprobamos todos los porcentajes
            suma_porcientos += parseFloat(form['porcentaje' + i.toString()].value);
            disponibles = document.getElementById('disponible' + i.toString()).innerHTML;
            necesaria =  Math.ceil(cantidad*form['porcentaje' + i.toString()].value/100);
            if (necesaria > disponibles)
            {
                if (error !== "")
                    error += "<br />";
                
                error += ' - No existen preguntas para rellenar un ' + form['porcentaje' + i.toString()].value
                        + '% del examen con el tema "' + document.getElementById('tema' + i.toString()).innerHTML + '".';
            }
        }
        i++;
    }

    if (suma_porcientos > 100)
    {
        if (error !== "")
            error += "<br />";
        
        error += " - La suma de todas las proporciones a incluir no puede superar el 100%. Actualmente es: " + suma_porcientos + "%"; 
    }
    
   if (error !== "")
        document.getElementById('error').innerHTML = error;
    else
    {
        document.body.style.cursor = 'wait';
        form.submit();
    }
}

/**
 * Valida la selección de preguntas del segundo  paso de creación de un examen
 * 
 * @param {int} cantidad Cantidad de preguntas totales del examen
 * @param {int} seleccionadas Cantidad de preguntas elegidas para el examen
 */
function validarSeleccionPreguntas(cantidad, seleccionadas)
{
    var error = "";

    if (seleccionadas > cantidad)
        error += " - Has seleccionado demasiadas preguntas obligatorias, el examen solo constará de " + cantidad;
    
    if (error !== "")
        document.getElementById('error').innerHTML = error;
    else if (seleccionadas < cantidad)
    {
        location.href='nuevoExamen.jsp?paso=3';
        document.body.style.cursor = 'wait';
    }
    else
    {
        document.body.style.cursor = 'wait';
        location.href='nuevoExamen.jsp?paso=4';
    }
}

/**
 * Confirma las preguntas introducidas en el paso 4 de creación de un examen
 * y lleva al mismo al paso 5
 */
function confirmarPreguntas()
{
    document.body.style.cursor = 'wait';
    location.href='nuevoExamen.jsp?paso=5';
}

/**
 * Inicia la barra de progreso de carga del fichero XML e inicia un timer
 * para refrescarla
 */
function barraProgresoXML()
{   
    actualizarBarraXML();
    timerBarraXML = window.setInterval(function() {actualizarBarraXML();}, 1000);
}

/**
 * Dibuja la barra de progreso obteniendo con Ajax el estado de la carga real
 */
function actualizarBarraXML()
{
    var contenido = document.getElementById('barra');
    var respuesta;
    
    var ajax=Ajax();
    if (!procesandoAjax)
    {    
        procesandoAjax = true;
        //El tiempo se pasa pq si es la misma petición, IE no la refresca y la coge de caché
        ajax.open("GET", "includes/consultasAjax.jsp?accion=progresoXML&force=" + (new Date()).getTime());
        ajax.onreadystatechange=function()
        {
            procesandoAjax = false;

            if (ajax.readyState === 4)
            {
                respuesta = ajax.responseText.trim();
                contenido.innerHTML = "<br /><div style=\"padding: 10px\" class=\"borde_corto\">Progreso...<br /><br /><progress style=\"text-align: center\" max=\"100\""
                + "value=\"" + respuesta + "\"></progress> " + respuesta + "%</div>";

                if (respuesta === "100")
                {
                    clearTimeout(timerBarraReclasificar);
                    contenido.innerHTML += "<h3>Las preguntas se han cargado correctamente.</h3>";
                    contenido.innerHTML += "<div class=\"boton_tema\" style=\"float: right; margin-right: 2px\" onClick=\"location.href='verPreguntas.jsp'\">Volver</div>";
                    contenido.innerHTML += "<br />";
                }
            }
        };
        ajax.send(null);
    }
}

/**
 * Inicia la barra de progreso de la reclasificación e inicia un timer
 * para refrescarla
 */
function barraProgresoReclasificar()
{   
    actualizarBarraReclasificar();
    timerBarraReclasificar = window.setInterval(function() {actualizarBarraReclasificar();}, 1000);
}

/**
 * Dibuja la barra de progreso obteniendo con Ajax el estado real de
 * la reclasificación
 */
function actualizarBarraReclasificar()
{
    var contenido = document.getElementById('barra');
    var respuesta;
    
    if (!procesandoAjax)
    {    
        procesandoAjax = true;
        var ajax=Ajax();
        //El tiempo se pasa pq si es la misma petición, IE no la refresca y la coge de caché
        ajax.open("GET", "includes/consultasAjax.jsp?accion=progresoReclasificar&force=" + (new Date()).getTime());
        ajax.onreadystatechange=function()
        {
            procesandoAjax = false;

            if (ajax.readyState === 4)
            {
                respuesta = ajax.responseText.trim();

                contenido.innerHTML = "<br /><div style=\"padding: 10px\" class=\"borde_corto\">Progreso...<br /><br /><progress style=\"text-align: center\" max=\"100\""
                + "value=\"" + respuesta + "\"></progress> " + respuesta + "%</div>";
                
                if (respuesta === "100")
                {
                    clearTimeout(timerBarraReclasificar);
                    contenido.innerHTML += "<h3>Las preguntas se han reclasificado correctamente.</h3>";
                    contenido.innerHTML += "<div class=\"boton_tema\" style=\"float: right; margin-right: 2px\" onClick=\"location.href='verPreguntas.jsp'\">Volver</div>";
                    contenido.innerHTML += "<br />";
                }
            }
        };

        ajax.send(null);
    }
    
}

/**
 * Muestra el tooltip del enunciado y las opciónes de una pregunta cuando
 * se pasa el ratón por encima
 * 
 * @param {event} event Evento que lanza la acción (para la posición del tooltip)
 * @param {int} pregunta Código de la pregunta que se mostrará
 */
function showTooltip(event, pregunta)
{
    //La variable IE determina si estamos utilizando IE
    var IE = document.all?true:false;
    tooltip++;
    var token = tooltip;
    var tempX = 0;
    var tempY = 0;

    if(IE)
    { //para IE
            IE6=navigator.userAgent.toLowerCase().indexOf('msie 6');
            IE7=navigator.userAgent.toLowerCase().indexOf('msie 7');

            if(IE6>0 || IE7>0)
            {
                    tempX = event.x;
                    tempY = event.y;
                    if(window.pageYOffset){
                            tempY=(tempY+window.pageYOffset);
                            tempX=(tempX+window.pageXOffset);
                    }else{
                            tempY=(tempY+Math.max(document.body.scrollTop,document.documentElement.scrollTop));
                            tempX=(tempX+Math.max(document.body.scrollLeft,document.documentElement.scrollLeft));
                    }
            }else{
                    //IE8
                    tempX = event.x;
                    tempY = event.y;
            }
    }else{ //para netscape
            document.captureEvents(Event.MOUSEMOVE);
            tempX = event.pageX;
            tempY = event.pageY;
    }

    if (tempX < 0){tempX = 0;}
    if (tempY < 0){tempY = 0;}

    //Se ultiza token en vez de procesandoAjax
    if (token === tooltip)
    {
        var ajax=Ajax();
        ajax.open("GET", "includes/consultasAjax.jsp?accion=opcionesTooltip&pregunta=" + pregunta + "&force=" + (new Date()).getTime());
        ajax.onreadystatechange=function()
        {
            if (ajax.readyState === 4)
            {
                if (token === tooltip)
                {
                    // Modificamos el contenido de la capa
                    document.getElementById('tooltip').innerHTML = ajax.responseText;

                    // Posicionamos la capa flotante
                    document.getElementById('tooltip').style.top = (tempY+5)+"px";
                    document.getElementById('tooltip').style.left = (tempX+30)+"px";
                    document.getElementById('tooltip').style.display='block';
                }
            }
        };

        ajax.send(null);
    }
}

/**
 * Oculta el tooltip
 */
function hideTooltip()
{
    tooltip++;
    document.getElementById('tooltip').style.display='none';
}

/**
 * Maneja el texto del boton de filtrado y la visibilidad de la caja de filtros
 */
function botonFiltros()
{
    if (divOculto)
    {
        document.getElementById('filtro').style.display = '';
        document.getElementById('filtro').style.opacity = 100;
        divOculto = false;
        document.getElementById('mostrarOcultar').innerHTML = "Ocultar filtros";
    }
    else
    {
        document.getElementById('filtro').style.opacity = 0;
        setTimeout("document.getElementById('filtro').style.display = 'none'", 700);
        divOculto = true;
        document.getElementById('mostrarOcultar').innerHTML = "Mostrar filtros";
    }
}

/**
 * Se llama al pulsar el botón de filtrar, aplica los filtros a los datos
 * del listado y refresca el mismo
 * 
 * @param {String} modo Indica el formulario a refrescar.
 */
function actualizarFiltroPreguntas(modo)
{
    var filtro = "";
    
    var form = form = document.forms["filtros"];
 
    //Construimos la cadena de filtrado
    
    if (modo === "ver")
    {
        if (form['habilitadaActivo'].checked)
        {
            filtro += " AND pre_habilitada = " + form['habilitada'].value;
        }
        if (form['proximoUsoActivo'].checked)
        {
            filtro += " AND pre_proximo_uso = " + form['proximoUso'].value;
        }
    }

    if (form['ultimoUsoActivo'].checked)
    {
        if (form['ultimoUso1'].value === '' && form['ultimoUso2'].value === '')
            filtro += " AND pre_ultimo_uso is null";
        else
        {
            if (form['ultimoUso1'].value !== '' && form['ultimoUso2'].value !== '')
                filtro += " AND pre_ultimo_uso BETWEEN '" + form['ultimoUso1'].value + "' AND '" + form['ultimoUso2'].value + "'";
            else if (form['ultimoUso2'].value === '' )
                filtro += " AND pre_ultimo_uso >= '" + form['ultimoUso1'].value + "'";
            else if (form['ultimoUso1'].value === '' )
                filtro += " AND (pre_ultimo_uso is null OR pre_ultimo_uso <= '" + form['ultimoUso2'].value + "')";
        }
    }
    
    if (form['utilizadaActivo'].checked)
    {
        if (!(form['utilizada1'].value === '' && form['utilizada2'].value === ''))
        {
            if (form['utilizada1'].value !== '' && form['utilizada2'].value !== '')
                filtro += " AND pre_utilizada BETWEEN " + form['utilizada1'].value + " AND " + form['utilizada2'].value;
            else if (form['utilizada2'].value === '' )
                filtro += " AND pre_utilizada >= " + form['utilizada1'].value;
            else if (form['utilizada1'].value === '' )
                filtro += " AND (pre_utilizada is null OR pre_utilizada <= " + form['utilizada2'].value + ")";
        }

    }

    if (form['fallosActivo'].checked)
    {
        if (!(form['fallos1'].value === '' && form['fallos2'].value === ''))
        {
            if (form['fallos1'].value !== '' && form['fallos2'].value !== '')
                filtro += " AND 100-ROUND((pre_aciertos/pre_contestada)*100, 2) BETWEEN " + form['fallos1'].value + " AND " + form['fallos2'].value;
            else if (form['fallos2'].value === '' )
                filtro += " AND 100-ROUND((pre_aciertos/pre_contestada)*100, 2) >= " + form['fallos1'].value;
            else if (form['fallos1'].value === '' )
                filtro += " AND 100-ROUND((pre_aciertos/pre_contestada)*100, 2) <= " + form['fallos2'].value;
        }

    }
    
    if (form['sinContestarActivo'].checked)
    {
        if (!(form['sinContestar1'].value === '' && form['sinContestar2'].value === ''))
        {
            // ")'_"    =      "+"    Para poder enviarlo por con
            if (form['sinContestar1'].value !== '' && form['sinContestar2'].value !== '')
                filtro += " AND ROUND(pre_no_contestada /(pre_contestada)'_pre_no_contestada)*100, 2) BETWEEN " + form['sinContestar1'].value + " AND " + form['sinContestar2'].value;
            else if (form['sinContestar2'].value === '' )
                filtro += " AND ROUND(pre_contestada /(pre_contestada)'_pre_no_contestada)*100, 2) >= " + form['sinContestar1'].value;
            else if (form['sinContestar1'].value === '' )
                filtro += " AND ROUND(pre_contestada /(pre_contestada)'_pre_no_contestada)*100, 2) <= " + form['sinContestar2'].value;
        }
    }

    if (form['aciertosActivo'].checked)  
    {
        if (!(form['aciertos1'].value === '' && form['aciertos2'].value === ''))
        {
            if (form['aciertos1'].value !== '' && form['aciertos2'].value !== '')
                filtro += " AND ROUND((pre_aciertos/pre_contestada)*100, 2) BETWEEN " + form['aciertos1'].value + " AND " + form['aciertos2'].value;
            else if (form['aciertos2'].value === '' )
                filtro += " AND ROUND((pre_aciertos/pre_contestada)*100, 2) >= " + form['aciertos1'].value;
            else if (form['aciertos1'].value === '' )
                filtro += " AND ROUND((pre_aciertos/pre_contestada)*100, 2) <= " + form['aciertos2'].value;
        }
    }
    
    if (form['dificultadActivo'].checked)
    {
        if (!(form['dificultad1'].value === '' && form['dificultad2'].value === ''))
        {
            if (form['dificultad1'].value !== '' && form['dificultad2'].value !== '')
                filtro += " AND ROUND(pre_dificultad, 2) BETWEEN " + form['dificultad1'].value + " AND " + form['dificultad2'].value;
            else if (form['dificultad2'].value === '' )
                filtro += " AND ROUND(pre_dificultad, 2) >= " + form['dificultad1'].value;
            else if (form['dificultad1'].value === '' )
                filtro += " AND ROUND(pre_dificultad, 2) <= " + form['dificultad2'].value;
        }
    }
    
    if (form['discriminacionActivo'].checked)
    {
        if (!(form['discriminacion1'].value === '' && form['discriminacion2'].value === ''))
        {
            if (form['discriminacion1'].value !== '' && form['discriminacion2'].value !== '')
                filtro += " AND ROUND(pre_discriminacion, 2) BETWEEN " + form['discriminacion1'].value + " AND " + form['discriminacion2'].value;
            else if (form['discriminacion2'].value === '' )
                filtro += " AND ROUND(pre_discriminacion, 2) >= " + form['discriminacion1'].value;
            else if (form['discriminacion1'].value === '' )
                filtro += " AND ROUND(pre_discriminacion, 2) <= " + form['discriminacion2'].value;
        }
    }
    
    if (form['preguntaActivo'].checked)
    {
        //Los simbolos extraños son: el primer replace es para poder enviar el % a través de la URL (Al otro lado se deshace)
        //El segundo es para poner el caracter de escape a la coma
        filtro += " AND pid_enunciado LIKE ('_'(" + form['pregunta'].value.replace(/\%/g, "_'(").replace(/\'/g, "\\\'") + "_'(')";
    }

    if (form['temaActivo'].checked)
    {
        if (form['tema'].value > 0)
            filtro += " AND cla_revisada = true AND cla_tema = " +  form['tema'].value;
        else if (form['tema'].value === "-1")
            filtro += " AND cla_tema is null";
        else
            filtro += " AND cla_revisada = false";
    }
        
    //Luego lenzamos la actualización del filtro
    var ajax=Ajax();

    if (!procesandoAjax)
    {   
        document.body.style.cursor = 'wait';
        procesandoAjax = true;
        paginaSeleccionar = 1;
        
        ajax.open("GET", "includes/consultasAjax.jsp?accion=nuevoFiltro&filtro=" + filtro + "&force=" + (new Date()).getTime());
        ajax.onreadystatechange=function()
        {
            if (ajax.readyState === 4)
            {
                //Actualizamos el listado
                var ajax2=Ajax();
                ajax2.open("GET", "includes/listadoPreguntas.jsp?modo=" + modo + "&pagina=1" + "&force=" + (new Date()).getTime());
                ajax2.onreadystatechange=function()
                {
                    document.body.style.cursor = 'auto';
                    procesandoAjax = false;

                    if (ajax2.readyState === 4)
                    {
                        var contenido;

                        contenido = document.getElementById('ver');

                        if (contenido === null)
                        {
                            contenido = document.getElementById('listaSeleccionar');

                            if (contenido === null)
                                contenido = document.getElementById('listaSeleccionarConfirmacion');
                        }

                        contenido.innerHTML = ajax2.responseText;
                    }
                };

                ajax2.send(null);
            }
            else
            {
                document.body.style.cursor = 'auto';
                procesandoAjax = false;
            }
        };

        ajax.send(null);
    }
    
}

/**
 * Se llama al pulsar el botón de filtrar, aplica los filtros a los datos
 * del listado y refresca el mismo
 */
function actualizarFiltroExamenes()
{
    var filtro = "";
    
    var form = form = document.forms["filtros"];
    
    if (form['analizadoActivo'].checked)
    {
        filtro += " AND exa_analizado = " + form['analizado'].value;
    }
    
    if (form['fechaActivo'].checked)
    {
        if (form['fecha1'].value === '' && form['fecha2'].value === '')
            filtro += " AND exa_fecha is null";
        else
        {
            if (form['fecha1'].value !== '' && form['fecha2'].value !== '')
                filtro += " AND exa_fecha BETWEEN '" + form['fecha1'].value + "' AND '" + form['fecha2'].value + "'";
            else if (form['fecha2'].value === '' )
                filtro += " AND exa_fecha >= '" + form['fecha1'].value + "'";
            else if (form['fecha1'].value === '' )
                filtro += " AND exa_fecha <= '" + form['fecha2'].value + "'";
        }
    }
    
    if (form['dificultadActivo'].checked)
    {
        if (!(form['dificultad1'].value === '' && form['dificultad2'].value === ''))
        {
            if (form['dificultad1'].value !== '' && form['dificultad2'].value !== '')
                filtro += " AND ROUND(exa_dificultad, 2) BETWEEN " + form['dificultad1'].value + " AND " + form['dificultad2'].value;
            else if (form['dificultad2'].value === '' )
                filtro += " AND ROUND(exa_dificultad, 2) >= " + form['dificultad1'].value;
            else if (form['dificultad1'].value === '' )
                filtro += " AND ROUND(exa_dificultad, 2) <= " + form['dificultad2'].value;
        }
    }
    
    if (form['discriminacionActivo'].checked)
    {
        if (!(form['discriminacion1'].value === '' && form['discriminacion2'].value === ''))
        {
            if (form['discriminacion1'].value !== '' && form['discriminacion2'].value !== '')
                filtro += " AND ROUND(exa_discriminacion, 2) BETWEEN " + form['discriminacion1'].value + " AND " + form['discriminacion2'].value;
            else if (form['discriminacion2'].value === '' )
                filtro += " AND ROUND(exa_discriminacion, 2) >= " + form['discriminacion1'].value;
            else if (form['discriminacion1'].value === '' )
                filtro += " AND ROUND(exa_discriminacion, 2) <= " + form['discriminacion2'].value;
        }
    }
    
    if (form['fallosActivo'].checked)
    {
        if (!(form['fallos1'].value === '' && form['fallos2'].value === ''))
        {
            if (form['fallos1'].value !== '' && form['fallos2'].value !== '')
                filtro += " AND 100-ROUND((exa_aciertos/exa_contestadas)*100, 2) BETWEEN " + form['fallos1'].value + " AND " + form['fallos2'].value;
            else if (form['fallos2'].value === '' )
                filtro += " AND 100-ROUND((exa_aciertos/exa_contestadas)*100, 2) >= " + form['fallos1'].value;
            else if (form['fallos1'].value === '' )
                filtro += " AND 100-ROUND((exa_aciertos/exa_contestadas)*100, 2) <= " + form['fallos2'].value;
        }

    }
    
    if (form['sinContestarActivo'].checked)
    {
        if (!(form['sinContestar1'].value === '' && form['sinContestar2'].value === ''))
        {
            // ")'_"    =      "+"    Para poder enviarlo por con
            if (form['sinContestar1'].value !== '' && form['sinContestar2'].value !== '')
                filtro += " AND ROUND(exa_no_contestadas /(exa_contestadas)'_exa_no_contestadas)*100, 2) BETWEEN " + form['sinContestar1'].value + " AND " + form['sinContestar2'].value;
            else if (form['sinContestar2'].value === '' )
                filtro += " AND ROUND(exa_contestadas /(exa_contestadas)'_exa_no_contestadas)*100, 2) >= " + form['sinContestar1'].value;
            else if (form['sinContestar1'].value === '' )
                filtro += " AND ROUND(exa_contestadas /(exa_contestadas)'_exa_no_contestadas)*100, 2) <= " + form['sinContestar2'].value;
        }
    }

    if (form['aciertosActivo'].checked)  
    {
        if (!(form['aciertos1'].value === '' && form['aciertos2'].value === ''))
        {
            if (form['aciertos1'].value !== '' && form['aciertos2'].value !== '')
                filtro += " AND ROUND((exa_aciertos/exa_contestadas)*100, 2) BETWEEN " + form['aciertos1'].value + " AND " + form['aciertos2'].value;
            else if (form['aciertos2'].value === '' )
                filtro += " AND ROUND((exa_aciertos/exa_contestadas)*100, 2) >= " + form['aciertos1'].value;
            else if (form['aciertos1'].value === '' )
                filtro += " AND ROUND((exa_aciertos/exa_contestadas)*100, 2) <= " + form['aciertos2'].value;
        }
    }
    
    if (form['examenActivo'].checked)
    {
        //Los simbolos extraños son: el primer replace es para poder enviar el % a través de la URL (Al otro lado se deshace)
        //El segundo es para poner el caracter de escape a la coma
        filtro += " AND ide_traduccion_nombre LIKE ('_'(" + form['examen'].value.replace(/\%/g, "_'(").replace(/\'/g, "\\\'") + "_'(')";
    }
    
    
    //Luego lenzamos la actualización del filtro
    var ajax=Ajax();
    if (!procesandoAjax)
    {    
        document.body.style.cursor = 'wait';
        procesandoAjax = true; 
        paginaExamen = 1;
        
        ajax.open("GET", "includes/consultasAjax.jsp?accion=nuevoFiltro&filtro=" + filtro + "&force=" + (new Date()).getTime());
        ajax.onreadystatechange=function()
        {
            if (ajax.readyState === 4)
            {
                //Actualizamos el listado
                var ajax2=Ajax();
                ajax2.open("GET", "includes/listadoExamenes.jsp?pagina=1" + "&force=" + (new Date()).getTime());
                ajax2.onreadystatechange=function()
                {
                    document.body.style.cursor = 'auto';
                    procesandoAjax = false;

                    if (ajax2.readyState === 4)
                    {
                        var contenido;

                        contenido = document.getElementById('listadoExamenes');                                   
                        contenido.innerHTML = ajax2.responseText;
                    }
                };

                ajax2.send(null);
            }
            else
            {
                document.body.style.cursor = 'auto';
                procesandoAjax = false;
            }
        };

        ajax.send(null);
    }
}

/**
 * Lleva el listado de exámenes a la primera página
 */
function primeraPaginaExamen()
{  
    var contenido;
    var ajax=Ajax();
    

    var contenido = document.getElementById('listadoExamenes');
    paginaExamen = 1;

    if (!procesandoAjax)
    {    
        document.body.style.cursor = 'wait';
        procesandoAjax = true;
        ajax.open("GET", "includes/listadoExamenes.jsp?pagina=" + paginaExamen + "&force=" + (new Date()).getTime());

        ajax.onreadystatechange=function()
        {
            document.body.style.cursor = 'auto';
            procesandoAjax = false;

            if (ajax.readyState === 4)
            {
                contenido.innerHTML = ajax.responseText;
            }
        };

        ajax.send(null);
    }
}

/**
 * Lleva el listado de exámenes a la página anterior
 */
function anteriorPaginaExamen()
{  
    var contenido;
    var ajax=Ajax();
    
   
    var contenido = document.getElementById('listadoExamenes');

    if (paginaExamen > 1)
    {
        paginaExamen--;
    }
    if (!procesandoAjax)
    {    
        document.body.style.cursor = 'wait';
        procesandoAjax = true;
        ajax.open("GET", "includes/listadoExamenes.jsp?pagina=" + paginaExamen + "&force=" + (new Date()).getTime());

        ajax.onreadystatechange=function()
        {
            document.body.style.cursor = 'auto';
            procesandoAjax = false;

            if (ajax.readyState === 4)
            {
                contenido.innerHTML = ajax.responseText;
            }
        };

        ajax.send(null);
    }
}

/**
 * Lleva el listado de exámenes a la página siguiente
 */
function siguientePaginaExamen()
{
    var contenido;
    var ajax=Ajax();
    
    var totalPreguntas = document.getElementById('total').innerHTML;
    var contenido = document.getElementById('listadoExamenes');

    if (paginaExamen < Math.ceil(totalPreguntas/itemsExamen))
    {
        paginaExamen++;
    }
    
    if (!procesandoAjax)
    {    
        document.body.style.cursor = 'wait';
        procesandoAjax = true;
        ajax.open("GET", "includes/listadoExamenes.jsp?pagina=" + paginaExamen + "&force=" + (new Date()).getTime());

        ajax.onreadystatechange=function()
        {
            document.body.style.cursor = 'auto';
            procesandoAjax = false;

            if (ajax.readyState === 4)
            {
                contenido.innerHTML = ajax.responseText;
            }
        };

        ajax.send(null);
    }
}

/**
 * Lleva el listado de exámenes a la última página
 */
function ultimaPaginaExamen()
{  
    var contenido;
    var ajax=Ajax();
    
    var totalPreguntas = document.getElementById('total').innerHTML;
    var contenido = document.getElementById('listadoExamenes');
    
    paginaExamen = Math.ceil(totalPreguntas/itemsExamen);

    if (!procesandoAjax)
    {    
        document.body.style.cursor = 'wait';
        procesandoAjax = true;
        ajax.open("GET", "includes/listadoExamenes.jsp?pagina=" + paginaExamen + "&force=" + (new Date()).getTime());

        ajax.onreadystatechange=function()
        {
            document.body.style.cursor = 'auto';
            procesandoAjax = false;

            if (ajax.readyState === 4)
            {
                contenido.innerHTML = ajax.responseText;
            }
        };

        ajax.send(null);
    }
}

/**
 * Cambia el acceso del usuario pasado por parámetro (si era admin lo quita,
 * y si no es admin lo pone)
 * 
 * @param {int} usuario Código del usuario a modificar.
 */
function cambiarAcceso(usuario)
{
    var listadoAdmins = document.getElementById('listadoAdmins');
    var listadoUsuarios = document.getElementById('listadoUsuarios');
    var listadoNoAcceso = document.getElementById('listadoNoAcceso');
    var ajax=Ajax();
    var ajax2=Ajax();
    var ajax3=Ajax();
    var ajax4=Ajax();

    if (!procesandoAjax)
    {    
        document.body.style.cursor = 'wait';
        procesandoAjax = true;
        
        ajax.open("GET", "includes/consultasAjax.jsp?accion=cambiarAcceso&usuario=" + usuario + "&force=" + (new Date()).getTime());
        ajax.onreadystatechange=function()
        {
            if (ajax.readyState === 4)
            {
                ajax2.open("GET", "includes/listadoUsuarios.jsp?modo=admin" + "&force=" + (new Date()).getTime());
                ajax2.onreadystatechange=function()
                {
                    if (ajax2.readyState === 4)
                    {
                        ajax3.open("GET", "includes/listadoUsuarios.jsp?modo=usuario" + "&force=" + (new Date()).getTime());
                        ajax3.onreadystatechange=function()
                        {
                            if (ajax3.readyState === 4)
                            {
                                ajax4.open("GET", "includes/consultasAjax.jsp?accion=usuariosSinAcceso" + "&force=" + (new Date()).getTime());
                                ajax4.onreadystatechange=function()
                                {
                                    if (ajax4.readyState === 4)
                                    {
                                        document.body.style.cursor = 'auto';
                                        procesandoAjax = false;
                                        
                                        listadoAdmins.innerHTML = ajax2.responseText;
                                        listadoUsuarios.innerHTML = ajax3.responseText;
                                        listadoNoAcceso.innerHTML = ajax4.responseText;
                                    }

                                };

                                ajax4.send(null);

                            }
                            else
                            {
                                document.body.style.cursor = 'auto';
                                procesandoAjax = false;
                            }

                        };

                        ajax3.send(null);
                    }
                    else
                    {
                        procesandoAjax = false;
                        document.body.style.cursor = 'auto';
                    }
                };

                ajax2.send(null);
            }
            else
            {
                procesandoAjax = false;
                document.body.style.cursor = 'auto';
            }
        };

        ajax.send(null);
    }
}

/**
 * Elimina el acceso del usuario a una asignatura
 * 
 * @param {int} usuario Código del usuario a eliminar de la asignatura
 */
function eliminarAcceso(usuario)
{
    var listadoAdmins = document.getElementById('listadoAdmins');
    var listadoUsuarios = document.getElementById('listadoUsuarios');
    var listadoNoAcceso = document.getElementById('listadoNoAcceso');
    var ajax=Ajax();
    var ajax2=Ajax();
    var ajax3=Ajax();
    var ajax4=Ajax();

    if (!procesandoAjax)
    {    
        document.body.style.cursor = 'wait';
        procesandoAjax = true;
        
        ajax.open("GET", "includes/consultasAjax.jsp?accion=expulsarUsuario&usuario=" + usuario + "&force=" + (new Date()).getTime());
        ajax.onreadystatechange=function()
        {
            if (ajax.readyState === 4)
            {
                ajax2.open("GET", "includes/listadoUsuarios.jsp?modo=admin" + "&force=" + (new Date()).getTime());
                ajax2.onreadystatechange=function()
                {
                    if (ajax2.readyState === 4)
                    {
                        ajax3.open("GET", "includes/listadoUsuarios.jsp?modo=usuario" + "&force=" + (new Date()).getTime());
                        ajax3.onreadystatechange=function()
                        {
                            if (ajax3.readyState === 4)
                            {
                                ajax4.open("GET", "includes/consultasAjax.jsp?accion=usuariosSinAcceso" + "&force=" + (new Date()).getTime());
                                ajax4.onreadystatechange=function()
                                {
                                    if (ajax4.readyState === 4)
                                    {
                                        document.body.style.cursor = 'auto';
                                        procesandoAjax = false;
                                        
                                        listadoAdmins.innerHTML = ajax2.responseText;
                                        listadoUsuarios.innerHTML = ajax3.responseText;
                                        listadoNoAcceso.innerHTML = ajax4.responseText;
                                    }

                                };

                                ajax4.send(null);

                            }
                            else
                                {
                                    procesandoAjax = false;
                                    document.body.style.cursor = 'auto';
                                }
                        };

                        ajax3.send(null);
                    }
                    else
                        {
                            procesandoAjax = false;
                            document.body.style.cursor = 'auto';
                        }
                };

                ajax2.send(null);
            }
            else
            {
                procesandoAjax = false;
                document.body.style.cursor = 'auto';
            }
        };

        ajax.send(null);
    }
}

/**
 * Agrega acceso del usuario seleccionado en el formulario a una asignatura
 */
function agregarAcceso()
{
    var listadoAdmins = document.getElementById('listadoAdmins');
    var listadoUsuarios = document.getElementById('listadoUsuarios');
    var listadoNoAcceso = document.getElementById('listadoNoAcceso');
    var form = document.forms['agregarUsuario'];
    var adminChecked;
    
    if (form['admin'].checked)
        adminChecked = "true";
    else
        adminChecked = "false";
    
    var ajax=Ajax();
    var ajax2=Ajax();
    var ajax3=Ajax();
    var ajax4=Ajax();

    if (!procesandoAjax)
    {    
        document.body.style.cursor = 'wait';
        procesandoAjax = true;
        
        ajax.open("GET", "includes/consultasAjax.jsp?accion=agregarAcceso&usuario=" + form['usuarios'].value + "&admin=" + adminChecked + "&force=" + (new Date()).getTime());
        ajax.onreadystatechange=function()
        {
            if (ajax.readyState === 4)
            {
                ajax2.open("GET", "includes/listadoUsuarios.jsp?modo=admin" + "&force=" + (new Date()).getTime());
                ajax2.onreadystatechange=function()
                {
                    if (ajax2.readyState === 4)
                    {
                        ajax3.open("GET", "includes/listadoUsuarios.jsp?modo=usuario" + "&force=" + (new Date()).getTime());
                        ajax3.onreadystatechange=function()
                        {
                            if (ajax3.readyState === 4)
                            {
                                ajax4.open("GET", "includes/consultasAjax.jsp?accion=usuariosSinAcceso" + "&force=" + (new Date()).getTime());
                                ajax4.onreadystatechange=function()
                                {
                                    if (ajax4.readyState === 4)
                                    {
                                        document.body.style.cursor = 'auto';
                                    
                                        procesandoAjax = false;
                                        listadoAdmins.innerHTML = ajax2.responseText;
                                        listadoUsuarios.innerHTML = ajax3.responseText;
                                        listadoNoAcceso.innerHTML = ajax4.responseText;
                                    }

                                };

                                ajax4.send(null);

                            }
                            else
                            {
                                procesandoAjax = false;
                                document.body.style.cursor = 'auto';
                            }

                        };

                        ajax3.send(null);
                    }
                    else
                    {
                        procesandoAjax = false;
                        document.body.style.cursor = 'auto';
                    }
                };

                ajax2.send(null);
            }
            else
            {
                procesandoAjax = false;
                document.body.style.cursor = 'auto';
            }
        };

        ajax.send(null);
    }
}

/**
 * Valida el formulario de cambio de contraseña
 */
function validarCambioPassword()
{
    var form = document.forms["cambioPassword"];
    var error = "";
    
    if (form["nueva"].value === null || form["nueva"].value === "")
    {
        error += " - La nueva contraseña no puede estar en blanco.";
    }
    else if (form["nueva"].value.indexOf(" ") > -1)
    {
        error += " - La nueva contraseña no puede contener espacios.";
    }
    
    if (form["nueva"].value !== form["repetida"].value)
    {
        if (error !== "")
                error += "<br />";
            
        error += " - Las contraseñas no coinciden.";
    }
    
    if (error !== "")
        document.getElementById('error').innerHTML = error;
    else
    {
        document.body.style.cursor = 'wait';
        form.submit();
    }
}

/**
 * Carga en el formulario de instrucciones de un examen (paso 5) las instrucciones
 * de otro examen.
 */
function seleccionarInstruccionesExamen()
{
    var form = document.forms["insertarInstrucciones"];
    
    document.body.style.cursor = 'wait';
    
    if (form['examenes'].value === '-')
    {
        location.href='nuevoExamen.jsp?paso=5';
    }
    else
    {
        location.href='nuevoExamen.jsp?paso=5&instrucciones=' + form['examenes'].value;
    }
}

/**
 * Valida la agregación de un idioma a la asignatura y ejecuta submit
 */
function agregarIdioma()
{
    var form = document.forms["idioma"];
    var error = "";
    
    if (form["traductor"].value === null || form["traductor"].value === "")
    {
        if (error !== "")
            error += "<br />";
        
        error += " - No se ha seleccionado ningún traductor.";
    }
    
    if (form["nombreAsignatura"].value === null || form["nombreAsignatura"].value === "")
    {
        if (error !== "")
                error += "<br />";
            
        error += " - La traducción del nombre de la asignatura en el idioma seleccionado no puede estar en blanco.";
    }
    
    if (error !== "")
        document.getElementById('error').innerHTML = error;
    else
    {
        document.body.style.cursor = 'wait';
        form.submit();
    }
}

/**
 * Quita el idioma pasado por parámetro de la asignatura
 * 
 * @param {int} idioma Código del idioma a quitar.
 */
function quitarIdioma(idioma)
{
    var listado = document.getElementById('listadoIdiomas');
    var cbo = document.getElementById('listadoNoAcceso');
    var ajax=Ajax();
    var ajax2=Ajax();
    var ajax3=Ajax();
    
    if(confirm('¿Está seguro de realizar esta acción?\n\nLas preguntas y exámenes dejarán de mantenerse en este idioma. (Las traducciones almacenadas hasta la fecha no se eliminarán)'))
    {
        if (!procesandoAjax)
        {    
            document.body.style.cursor = 'wait';
            procesandoAjax = true;        
            ajax.open("GET", "includes/consultasAjax.jsp?accion=quitarIdioma&idioma=" + idioma + "&force=" + (new Date()).getTime());
            ajax.onreadystatechange=function()
            {
                if (ajax.readyState === 4)
                {
                    ajax2.open("GET", "includes/listadoIdiomas.jsp" + "&force=" + (new Date()).getTime());
                    ajax2.onreadystatechange=function()
                    {
                        if (ajax2.readyState === 4)
                        {
                            ajax3.open("GET", "includes/consultasAjax.jsp?accion=idiomasSinConfigurar" + "&force=" + (new Date()).getTime());
                            ajax3.onreadystatechange=function()
                            {
                                document.body.style.cursor = 'auto';
                                procesandoAjax = false;

                                if (ajax3.readyState === 4)
                                {
                                    listado.innerHTML = ajax2.responseText;
                                    cbo.innerHTML = ajax3.responseText;
                                }
                            };
                            ajax3.send(null);
                        }
                        else
                        {
                            document.body.style.cursor = 'auto';
                            procesandoAjax = false;
                        }
                    };
                    ajax2.send(null);
                }
                else
                {
                    document.body.style.cursor = 'auto';
                    procesandoAjax = false;
                }
            };
            ajax.send(null);
        }
    }
}