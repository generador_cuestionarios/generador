<%-- 
    Copyright (C) 2014 Raúl Sempere Trujillo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Document   : examinarExcel
    Created on : 22-jul-2014, 13:14:00
    Author     : Raúl Sempere Trujillo
--%>

<%-- 
    Muestra el resultado del analisis de un examen utilizando un fichero Excel
--%>

<%@page import="clases_generador.ConfigManager"%>
<%@page import="clases_generador.ExcelManager"%>
<%@page import="CEN.ExamenCEN"%>
<%@page import="EN.ExamenEN"%>
<%@page import="clases_generador.XMLManager"%>
<%@page import="EN.AsignaturaEN"%>
<%@page import="CEN.PreguntaCEN"%>
<%@page import="org.apache.commons.fileupload.FileUploadException"%>
<%@page import="java.util.List"%>
<%@page import="org.apache.commons.fileupload.FileItem"%>
<%@page import="java.io.File"%>
<%@page import="org.apache.commons.fileupload.servlet.ServletFileUpload"%>
<%@page import="org.apache.commons.fileupload.disk.DiskFileItemFactory"%>

<%
    String titulo = "Analizar respuestas del examen - Generador Inteligente de Cuestinarios";
    String contenido = "";
%>

<%
    if (session.getAttribute("usuario") == null)
    {
       response.sendRedirect("login.jsp");
    }
    else if (session.getAttribute("asignatura") == null)
    {
       response.sendRedirect("asignaturas.jsp");
    }
    else if (request.getParameter("examen") != null)
    {
        String CATALINA_HOME = System.getenv("CATALINA_HOME");
        
        if (CATALINA_HOME.endsWith("/") || CATALINA_HOME.endsWith("\\"))
            CATALINA_HOME = CATALINA_HOME.substring(0, CATALINA_HOME.length()-1);
        
        AsignaturaEN asignaturaEN = (AsignaturaEN)session.getAttribute("asignatura");
        ExamenCEN examenCEN = new ExamenCEN();
        ExamenEN examenEN = examenCEN.getExamenByCodigo(asignaturaEN, Integer.parseInt(request.getParameter("examen")));
        
        String ruta = CATALINA_HOME + "/webapps/" + ConfigManager.getWebApp() + "/temp/";
        String fichero = "";
        
        boolean isMultipart = ServletFileUpload.isMultipartContent(request);   

        if (isMultipart)
        {
            DiskFileItemFactory factory = new DiskFileItemFactory();
            factory.setSizeThreshold(1024);
            factory.setRepository(new File(ruta));

            ServletFileUpload upload = new ServletFileUpload(factory);

            //Subimos el fichero xml al servidor
            try
            {
                List<FileItem> partes = upload.parseRequest(request);
                
                for (FileItem parte: partes)
                {
                    File file = new File (ruta, parte.getName());
                    parte.write(file);
                    fichero = parte.getName();
                }

                try
                {
                    ExcelManager excel = new ExcelManager(examenEN, ruta + fichero);
                    excel.examinarExcel();

                    if (excel.isCorrecto())
                    {
                        excel.aplicarExcel();
                        contenido += "<h3 class=\"maegen\">El fichero excel ha sido analizado correctamente y la información ha sido importada al sistema.</h3>";
                        contenido += "<div class=\"boton_tema\" style=\"float: left; margin-left 10px\" onClick=\"location.href='verPreguntas.jsp'\">Volver</div><br />";

                    }
                    else
                    {
                        contenido += "<h3 class=\"maegen\">" + excel.getError() + "</h3>";
                        contenido += "<div class=\"boton_tema\" style=\"float: left\" class=\"maegen\" onClick=\"location.href='subirExcel.jsp?examen=" + examenEN.getCodigo() + "'\">Reintentar</div>";
                        contenido += "<div class=\"boton_tema\" style=\"float: left; margin-left: 10px\" onClick=\"location.href='detalleExamen.jsp?examen='" + examenEN.getCodigo() + "\">Volver</div><br />";
                    }
                }
                catch (Exception ex)
                {
                    //Borramos el fichero subido
                }
                finally
                {
                    //Borramos el fichero subido
                    File archivo = new File(ruta + fichero);
                    
                    if (!archivo.isDirectory() && archivo.exists())
                        archivo.delete(); 
                }
            }  
            catch (FileUploadException ex)
            {
                out.println("Error: " + ex.getMessage());
            }
        }
%>

<%@page contentType="text/html" pageEncoding="UTF-8" session="true" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><%= titulo %></title>
        <link rel="stylesheet" href="styles/main-stylesheet.css" />
        <link rel="stylesheet" href="styles/menu.css" />
        <script type="text/javascript" src="script/javascript.js"></script>
    </head>
    <body>
        <%@include file="includes/menu.jsp"%>
        <div id="contenido">
            <%= contenido %>
        </div>
    </body>
</html>

<%
    }
%>