<%-- 
    Copyright (C) 2014 Raúl Sempere Trujillo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Document   : errorDescargarZip
    Created on : 03-ago-2014, 14:24:39
    Author     : Raúl Sempere Trujillo
--%>

<%-- 
    Página estática que muestra la información de que ha habido un error
    al generar los ficheros PDF y crear el zip
--%>

<%
    String titulo = "Error al descargar el fichero zip - Generador Inteligente de Cuestinarios";
            
    if (session.getAttribute("usuario") == null)
    {
       response.sendRedirect("login.jsp");
    }
    else if (session.getAttribute("asignatura") == null)
    {
       response.sendRedirect("asignaturas.jsp");
    }
    else
    {
        String examenCod = request.getParameter("examen");
    
%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><%= titulo %></title>
        <link rel="stylesheet" href="styles/main-stylesheet.css" />
        <link rel="stylesheet" href="styles/menu.css" />
        <script type="text/javascript" src="script/javascript.js"></script>
    </head>
    <body>
        <%@include file="includes/menu.jsp"%>
        <div id="contenido">
            <div id="caja">
                <h2 class="margen">No se pudo generar el fichero zip.</h2>
                    <div class="margen">Alguna pregunta (en el idioma que se intentaba generar)
                    tiene un error en la sintaxis que impide que el examen LaTeX pueda ser compilado en PDF con normalidad. Para averiguar el problema,
                    se recomienda la descarga de los ficheros LaTeX y realizar una compilación manual.</div>
                    <div class="boton_tema" style="float: right; margin-right: 2px" onClick="location.href='verTraduccionesExamen.jsp?examen=<%=examenCod%>'">Volver</div>
                    <br /><br />
            </div>
       </div>
    </body>
</html>
<%
    }
%>