<%-- 
    Copyright (C) 2014 Raúl Sempere Trujillo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Document   : nuevaPreparacionExamen
    Created on : 15-jul-2014, 13:19:00
    Author     : Raúl Sempere Trujillo
--%>

<%-- 
    Este documento se utiliza para permitir al usuario realziar una nueva
    traduccion de un examen.
--%>
 
<%@page import="CEN.TraduccionInstruccionCEN"%>
<%@page import="EN.InstruccionEN"%>
<%@page import="EN.TraduccionInstruccionEN"%>
<%@page import="EN.ExamenEN"%>
<%@page import="CEN.ExamenCEN"%>
<%@page import="CEN.PreparacionExamenIdiomaCEN"%>
<%@page import="EN.PreparacionExamenIdiomaEN"%>
<%@page import="java.util.ArrayList"%>
<%@page import="EN.IdiomaEN"%>
<%@page import="CEN.IdiomaCEN"%>
<%@page import="EN.TraduccionOpcionEN"%>
<%@page import="CEN.OpcionCEN"%>
<%@page import="EN.OpcionEN"%>
<%@page import="EN.TraduccionPreguntaEN"%>
<%@page import="EN.PreguntaEN"%>
<%@page import="CEN.PreguntaCEN"%>
<%@page import="CEN.TraduccionOpcionCEN"%>
<%@page import="CEN.TraduccionPreguntaCEN"%>
<%@page import="EN.AsignaturaEN"%>

<% String titulo = "Nueva Traducción de Examen- Generador Inteligente de Cuestinarios"; %>


<%
    String contenido = "";
    
    request.setCharacterEncoding("UTF-8");
    
    if (session.getAttribute("usuario") == null)
    {
       response.sendRedirect("login.jsp");
    }
    else if (session.getAttribute("asignatura") == null)
    {
       response.sendRedirect("asignaturas.jsp");
    }
    else if (request.getParameter("examen") == null)     
    {
        response.sendRedirect("varTraduccionesExamen.jsp");
    }
    else
    {
        int codExamen = Integer.parseInt(request.getParameter("examen"));
        ExamenCEN examenCEN = new ExamenCEN();
        IdiomaCEN idiomaCEN = new IdiomaCEN();
        ConfiguracionAsignaturaIdiomaCEN configuracionAsignaturaIdiomaCEN = new ConfiguracionAsignaturaIdiomaCEN();
        PreparacionExamenIdiomaCEN preparacionExamenIdiomaCEN = new PreparacionExamenIdiomaCEN();
        TraduccionInstruccionCEN traduccionInstruccionCEN = new TraduccionInstruccionCEN();
        
        AsignaturaEN asignaturaEN = (AsignaturaEN)session.getAttribute("asignatura");
        ExamenEN examenEN = examenCEN.getExamenByCodigo(asignaturaEN, codExamen);
        
        //Comprobamos si además venimos del submit y hay que guardar
        if (request.getParameter("nombre") != null)
        {
            //recogemos los campos y los guardamos
            IdiomaEN idiomaEN = idiomaCEN.getIdiomaByCodigo(Integer.parseInt(request.getParameter("idioma")));
            examenCEN.rellenarInstrucciones(examenEN);
            
            PreparacionExamenIdiomaEN nuevaPreparacion = new PreparacionExamenIdiomaEN();
            
            nuevaPreparacion.setExamen(examenEN);
            nuevaPreparacion.setIdioma(idiomaEN);
            nuevaPreparacion.setRevisada(true);
            nuevaPreparacion.setTraduccionNombre(request.getParameter("nombre"));
            nuevaPreparacion.setTraduccionFecha(request.getParameter("fecha"));
            nuevaPreparacion.setTraduccionDuracion(request.getParameter("duracion"));
            
            preparacionExamenIdiomaCEN.New(nuevaPreparacion);
            
            TraduccionInstruccionEN traduccionInstruccionEN = null;
            
            for (int i = 0; i < examenEN.getInstrucciones().size(); i++)
            {
                traduccionInstruccionEN = new TraduccionInstruccionEN();
                
                traduccionInstruccionEN.setIdioma(idiomaEN);
                traduccionInstruccionEN.setInstruccion(examenEN.getInstrucciones().get(i));
                traduccionInstruccionEN.setEnunciado(request.getParameter("inst" + (i+1)));
                
                traduccionInstruccionCEN.New(traduccionInstruccionEN);
            }
            
            if (request.getParameter("revisarPreguntas") == null)
                response.sendRedirect("verTraduccionesExamen.jsp?examen=" + examenEN.getCodigo());
            else
            {
                //Revisamos las preguntas
                response.sendRedirect("revisarTraduccionPregunta.jsp?examen=" + examenEN.getCodigo() + "&idioma=" + idiomaEN.getCodigo());
            }
        }
        //Si no estamos guardando permitimos crear una nueva traduccion
        else
        {
            ArrayList<ConfiguracionAsignaturaIdiomaEN> confs = configuracionAsignaturaIdiomaCEN.getConfiguracionesAsignaturaIdiomaByAsignatura(asignaturaEN);
            ArrayList<PreparacionExamenIdiomaEN> preparaciones = preparacionExamenIdiomaCEN.getPreparacionesExamenByExamen(examenEN);
            PreparacionExamenIdiomaEN preparacionPrincipal = null;  

            //Creamos una lista con los idiomas sin preparar
            ArrayList<IdiomaEN> idiomasSinPreparar = new ArrayList<IdiomaEN>();

            for (ConfiguracionAsignaturaIdiomaEN conf : confs)
            {
                if (conf.isIdiomaPrincipal())
                    preparacionPrincipal = preparacionExamenIdiomaCEN.getPreparacionExamenByExamenEIdioma(examenEN, conf.getIdioma());

                if (!preparacionExamenIdiomaCEN.isIdiomaPreparado(preparaciones, conf.getIdioma()))
                    idiomasSinPreparar.add(conf.getIdioma());
            }

            //Si hay algun elemento creamos un select para que el usuario elija
            if (idiomasSinPreparar.size() > 0)
            {
                contenido += "<div id=\"caja\">";
                contenido += "<h2 style=\"text-align: center\">Nueva Traducción de \"" + preparacionPrincipal.getTraduccionNombre() + "\"</h2><br />";
                contenido += "<form name=\"idioma\">";
                contenido += "<div>Idioma: <select name=\"selectIdioma\" id=\"idioma\" style=\"min-width: 250px; max-width: 500px\">";
                String selected = "selected";

                for (IdiomaEN idiomaEN : idiomasSinPreparar)
                {
                    contenido += "<option value=\"" + idiomaEN.getCodigo() + "\" " + selected + ">" + idiomaEN.getNombre() + "</option>";
                    selected = "";
                }
                contenido += " </select> <input type=\"button\" id=\"seleccionar\" value=\"Seleccionar\" onClick=\"comenzarTraduccionExamen(" + examenEN.getCodigo() + ")\"></div><br />";
                contenido += "</form>";
            }
            //Si esta vacía avisamos de que no hay mas idiomas para preprarar
            else
            {
                contenido += "<div id=\"caja\">";
                contenido += "<h2 style=\"text-align: center\">Nueva Traducción de \"" + preparacionPrincipal.getTraduccionNombre() + "\"</h2><br />";
                contenido += "<div class=\"margen\">El examen ya ha sido traducido en todos los idiomas disponibles en la asignatura.</div><br />";
                contenido += "<div style=\"margin-right: 2px\" id=\"boton_pregunta\" onClick=\"location.href='verTraduccionesExamen.jsp?examen=" + examenEN.getCodigo() + "'\">Volver</div>";
            }
        }
    }
        
%>        
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><%= titulo %></title>
        <link rel="stylesheet" href="styles/main-stylesheet.css" />
        <link rel="stylesheet" href="styles/menu.css" />
        <script type="text/javascript" src="script/javascript.js"></script>
    </head>
        <%@include file="includes/menu.jsp"%>
        <div id="contenido">
                <%= contenido %>
                <div id="traduccionExamen">
                    <%="</div>"%>
                </div>
        </div>
        
</html>