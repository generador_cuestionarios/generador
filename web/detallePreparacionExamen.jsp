<%-- 
    Copyright (C) 2014 Raúl Sempere Trujillo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Document   : detallePreparacionExamen
    Created on : 16-jul-2014, 15:34:00
    Author     : Raúl Sempere Trujillo
--%>

<%-- 
    Muestra el formulario con la traducción de un examen, permitiendo editarla.
--%>
 
<%@page import="clases_generador.ConfigManager"%>
<%@page import="clases_generador.Utiles"%>
<%@page import="clases_generador.Traduccion"%>
<%@page import="CEN.TraduccionInstruccionCEN"%>
<%@page import="EN.InstruccionEN"%>
<%@page import="EN.TraduccionInstruccionEN"%>
<%@page import="EN.ExamenEN"%>
<%@page import="CEN.ExamenCEN"%>
<%@page import="CEN.PreparacionExamenIdiomaCEN"%>
<%@page import="EN.PreparacionExamenIdiomaEN"%>
<%@page import="java.util.ArrayList"%>
<%@page import="EN.IdiomaEN"%>
<%@page import="CEN.IdiomaCEN"%>
<%@page import="EN.TraduccionOpcionEN"%>
<%@page import="CEN.OpcionCEN"%>
<%@page import="EN.OpcionEN"%>
<%@page import="EN.TraduccionPreguntaEN"%>
<%@page import="EN.PreguntaEN"%>
<%@page import="CEN.PreguntaCEN"%>
<%@page import="CEN.TraduccionOpcionCEN"%>
<%@page import="CEN.TraduccionPreguntaCEN"%>
<%@page import="EN.AsignaturaEN"%>

<% String titulo = "Revisar traducción de Examen- Generador Inteligente de Cuestinarios"; %>


<%
    String contenido = "";
    int codExamen = 0;
    int codIdioma = 0;
        
    request.setCharacterEncoding("UTF-8");
    
    if (session.getAttribute("usuario") == null)
    {
       response.sendRedirect("login.jsp");
    }
    else if (session.getAttribute("asignatura") == null)
    {
       response.sendRedirect("asignaturas.jsp");
    }
    else if (request.getParameter("examen") == null || request.getParameter("idioma") == null)     
    {
        response.sendRedirect("varTraduccionesExamen.jsp");
    }
    else
    {
        codExamen = Integer.parseInt(request.getParameter("examen"));
        codIdioma = Integer.parseInt(request.getParameter("idioma"));
        ExamenCEN examenCEN = new ExamenCEN();
        IdiomaCEN idiomaCEN = new IdiomaCEN();
        ConfiguracionAsignaturaIdiomaCEN configuracionAsignaturaIdiomaCEN = new ConfiguracionAsignaturaIdiomaCEN();
        PreparacionExamenIdiomaCEN preparacionExamenIdiomaCEN = new PreparacionExamenIdiomaCEN();
        TraduccionInstruccionCEN traduccionInstruccionCEN = new TraduccionInstruccionCEN();
        
        AsignaturaEN asignaturaEN = (AsignaturaEN)session.getAttribute("asignatura");
        ExamenEN examenEN = examenCEN.getExamenByCodigo(asignaturaEN, codExamen);
        ConfiguracionAsignaturaIdiomaEN confPrincipal = configuracionAsignaturaIdiomaCEN.getConfiguracionIdiomaPrincipal(asignaturaEN);
        PreparacionExamenIdiomaEN preparacionPrincipal = preparacionExamenIdiomaCEN.getPreparacionExamenByExamenEIdioma(examenEN, confPrincipal.getIdioma());
        IdiomaEN idiomaEN = idiomaCEN.getIdiomaByCodigo(codIdioma);
        PreparacionExamenIdiomaEN preparacionExamenIdiomaEN = preparacionExamenIdiomaCEN.getPreparacionExamenByExamenEIdioma(examenEN, idiomaEN);
        examenCEN.rellenarInstrucciones(examenEN);
        ArrayList<TraduccionInstruccionEN> traduccionInstruccionesOtras = traduccionInstruccionCEN.getTraduccionesInstruccionByExamenEIdioma(examenEN, idiomaEN);
        ArrayList<TraduccionInstruccionEN> traduccionInstruccionesPrincipal= traduccionInstruccionCEN.getTraduccionesInstruccionByExamenEIdioma(examenEN, confPrincipal.getIdioma());
        
        //Comprobamos si además venimos del submit y hay que guardar
        if (request.getParameter("nombre") != null)
        {
            //recogemos los campos y los guardamos
            preparacionExamenIdiomaEN.setRevisada(true);
            preparacionExamenIdiomaEN.setTraduccionNombre(request.getParameter("nombre"));
            preparacionExamenIdiomaEN.setTraduccionFecha(request.getParameter("fecha"));
            preparacionExamenIdiomaEN.setTraduccionDuracion(request.getParameter("duracion"));
            
            preparacionExamenIdiomaCEN.Modify(preparacionExamenIdiomaEN);
            
            TraduccionInstruccionEN traduccionInstruccionEN = null;
            
            for (int i = 0; i < examenEN.getInstrucciones().size(); i++)
            {
                traduccionInstruccionEN = new TraduccionInstruccionEN();
                
                traduccionInstruccionEN.setIdioma(idiomaEN);
                traduccionInstruccionEN.setInstruccion(examenEN.getInstrucciones().get(i));
                traduccionInstruccionEN.setEnunciado(request.getParameter("inst" + (i+1)));
                
                //Creamos o editamos dependiendo de si ya estan traducidas
                if (traduccionInstruccionesOtras.size() > 0)
                    traduccionInstruccionCEN.Modify(traduccionInstruccionEN);
                else
                    traduccionInstruccionCEN.New(traduccionInstruccionEN);
            }
            
            //volvemos
            response.sendRedirect("verTraduccionesExamen.jsp?examen=" + codExamen);
        }
        else
        {
            contenido += "<div id=\"caja\">";
            contenido += "<h2 style=\"text-align: center\">Revisar Traducción de \"" + preparacionPrincipal.getTraduccionNombre() + "\" en \""+ idiomaEN.getNombre() + "\"</h2><br />";
            contenido += "<div>Idioma: <select name=\"selectIdioma\" id=\"idioma\" style=\"min-width: 250px; max-width: 500px\" disabled>";
            contenido += "<option value=\"" + idiomaEN.getCodigo() + "\" selected>" + idiomaEN.getNombre() + "</option>";
            contenido += " </select> <input type=\"button\" id=\"seleccionar\" value=\"Seleccionar\" disabled></div><br />";

            //Preparamos el traductor
            ConfiguracionAsignaturaIdiomaEN otroIdiomaConf = configuracionAsignaturaIdiomaCEN.getConfiguracionAsignaturaIdiomaByAsignaturaEIdioma(asignaturaEN, idiomaEN);
            Traduccion traductor = new Traduccion(ConfigManager.getApertiumPackages());
            traductor.seleccionarTraduccion(otroIdiomaConf.getTraductorApertium());

            contenido += "<form name=\"traduccionExamen\" action=\"detallePreparacionExamen.jsp?examen=" + examenEN.getCodigo() + "&idioma=" + idiomaEN.getCodigo() + "\" method=\"POST\" accept-charset=\"utf-8\">";
            
            contenido += "      <div style=\"float: right; margin-right: 2px\" id=\"boton_pregunta\" onClick=\"location.href='verTraduccionesExamen.jsp?examen=" + examenEN.getCodigo() + "'\">Volver</div>";
            contenido += "      <div style=\"float: right; margin-right: 10px\" id=\"boton_pregunta\" onClick=\"guardarTraduccionExamen()\">Guardar</div>";
            contenido += "      <br /><br />";
            
            //Datos del examen
            contenido += "<h3 class=\"margen\">Datos del examen:</h3>";
            contenido += "<div class=\"borde\">";
            contenido += "<div class=\"margen\">Nombre del examen:</div>";
            contenido += "<div class=\"margen\"><input type=\"text\" size=\"80\" value=\"" + preparacionPrincipal.getTraduccionNombre() + "\"disabled /></div>";
            contenido += "<div class=\"margen\">Traducción nombre del examen:</div>";
            contenido += "<div class=\"margen\"><input type=\"text\" id=\"nombre\" name=\"nombre\" size=\"80\" value=\"" + preparacionExamenIdiomaEN.getTraduccionNombre() + "\" /></div><br />";
            contenido += "<div class=\"margen\">Fecha:</div>";
            contenido += "<div class=\"margen\"><input type=\"text\" size=\"50\" value=\"" + preparacionPrincipal.getTraduccionFecha()+ "\"disabled /></div>";
            contenido += "<div class=\"margen\">Traducción fecha:</div>";
            contenido += "<div class=\"margen\"><input type=\"text\" id=\"fecha\" name=\"fecha\" size=\"50\" value=\"" + preparacionExamenIdiomaEN.getTraduccionFecha() + "\" /></div><br />";
            contenido += "<div class=\"margen\">Duración:</div>";
            contenido += "<div class=\"margen\"><input type=\"text\" size=\"50\" value=\"" + preparacionPrincipal.getTraduccionDuracion()+ "\"disabled /></div>";
            contenido += "<div class=\"margen\">Traducción duración:</div>";
            contenido += "<div class=\"margen\"><input type=\"text\" id=\"duracion\" name=\"duracion\" size=\"50\" value=\"" + preparacionExamenIdiomaEN.getTraduccionDuracion() + "\" /></div><br />";
            contenido += "</div><br />";

            //Datos e las instrucciones
            contenido += "<h3 class=\"margen\">Instrucciones del examen:</h3>";
            contenido += "<div class=\"borde\">";

            //Si no estan traducidas se traducen
            if (traduccionInstruccionesOtras.size() > 0)
            {
                for (int i = 0 ; i < examenEN.getInstrucciones().size(); i++)
                {
                    contenido += "<div class=\"margen\">Instruccion " + (i+1) + ":</div>";
                    contenido += "<div class=\"margen\"><textarea class=\"instruccion\" style=\"vertical-align: top\" cols=\"70\" rows=\"3\" disabled>" + Utiles.textoHTML(traduccionInstruccionesPrincipal.get(i).getEnunciado()) + "</textarea>";
                    contenido += "</div>";
                    contenido += "<div class=\"margen\">Traducción instruccion " + (i+1) + ":</div>";
                    contenido += "<div class=\"margen\"><textarea class=\"instruccion\" style=\"vertical-align: top\" id=\"inst" + (i+1) + "\" name=\"inst" + (i+1) + "\" cols=\"70\" rows=\"3\">" + Utiles.textoHTML(traduccionInstruccionesOtras.get(i).getEnunciado()) + "</textarea>";
                    contenido += "</div><br />";
                }
            }
            else
            {
                for (int i = 0 ; i < examenEN.getInstrucciones().size(); i++)
                {
                    contenido += "<div class=\"margen\">Instruccion " + (i+1) + ":</div>";
                    contenido += "<div class=\"margen\"><textarea class=\"instruccion\" style=\"vertical-align: top\" cols=\"70\" rows=\"3\" disabled>" + Utiles.textoHTML(traduccionInstruccionesPrincipal.get(i).getEnunciado()) + "</textarea>";
                    contenido += "</div>";
                    contenido += "<div class=\"margen\">Traducción instruccion " + (i+1) + ":</div>";
                    contenido += "<div class=\"margen\"><textarea class=\"instruccion\" style=\"vertical-align: top\" id=\"inst" + (i+1) + "\" name=\"inst" + (i+1) + "\" cols=\"70\" rows=\"3\">" + Utiles.textoHTML(traductor.traducir(traduccionInstruccionesPrincipal.get(i).getEnunciado())) + "</textarea>";
                    contenido += "</div><br />";
                }
            }

            contenido += "</div><br />";
            contenido += "</form>";

            //Botones y div error del final
            contenido += "      <div id=\"error\"></div>";
            contenido += "      <br />";
            contenido += "      <div style=\"float: right; margin-right: 2px\" id=\"boton_pregunta\" onClick=\"location.href='verTraduccionesExamen.jsp?examen=" + examenEN.getCodigo() + "'\">Volver</div>";
            contenido += "      <div style=\"float: right; margin-right: 10px\" id=\"boton_pregunta\" onClick=\"guardarTraduccionExamen()\">Guardar</div>";
            contenido += "      <br /><br />";
            contenido += "</div>";
        }
    }
        
%>        
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><%= titulo %></title>
        <link rel="stylesheet" href="styles/main-stylesheet.css" />
        <link rel="stylesheet" href="styles/menu.css" />
        <script type="text/javascript" src="script/javascript.js"></script>
    </head>
        <%@include file="includes/menu.jsp"%>
        <div id="contenido">
                <%= contenido %>
        </div>
</html>