<%-- 
    Copyright (C) 2014 Ra�l Sempere Trujillo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Document   : guardarTema
    Created on : 07-may-2014, 16:55:00
    Author     : Ra�l Sempere Trujillo
--%>

<%-- 
    Este documento se emplea para guardar el nuevo tema que el usuario ha
    creado.
--%>

<%@page import="EN.KeywordEN"%>
<%@page import="CEN.KeywordCEN"%>
<%@page import="EN.AsignaturaEN"%>
<%@page import="EN.TemaEN"%>
<%@page import="CEN.TemaCEN"%>
<%@page import="clases_generador.XMLManager"%>

<%
    request.setCharacterEncoding("UTF-8");
    
    if (session.getAttribute("usuario") == null)
    {
       response.sendRedirect("login.jsp");
    }
    else if (session.getAttribute("asignatura") == null)
    {
       response.sendRedirect("asignaturas.jsp");
    }
    else if (request.getParameter("nombre") == null)     
    {
        response.sendRedirect("nuevoTema.jsp");
    }
    else
    {
        TemaCEN temaCEN = new TemaCEN();
        KeywordCEN keywordCEN = new KeywordCEN();
        TemaEN temaEN = new TemaEN();
        KeywordEN keywordEN;
        
        temaEN.setAsignatura((AsignaturaEN)session.getAttribute("asignatura"));
        temaEN.setNombre(request.getParameter("nombre"));
        temaCEN.New(temaEN);  

        int key = 1;
        
        while(request.getParameter("key" + key) != null)
        {
            if (request.getParameter("key" + key) != "")
            {
                keywordEN = new KeywordEN();
                keywordEN.setPalabra(request.getParameter("key" + key).toLowerCase());
                keywordEN.setPeso(Integer.parseInt(request.getParameter("peso" + key)));
                keywordEN.setTema(temaEN);
                keywordCEN.New(keywordEN);
            }
            
            key++;
        }
        
        response.sendRedirect("nuevoTema.jsp?creado=true");
    }
%>