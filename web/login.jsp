<%-- 
    Copyright (C) 2014 Raúl Sempere Trujillo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Document   : login
    Created on : 15-abr-2014, 00:46:00
    Author     : Raúl Sempere Trujillo
--%>


<%-- 
    Este documento se emplea loguear un usuario en el sistema.
--%>

<%@page import="CEN.UsuarioCEN"%>
<%@page import="EN.UsuarioEN"%>
<%@page import="clases_generador.HiloRecolectorBasura"%>
<%@page contentType="text/html" pageEncoding="UTF-8" session="true" %>
<link rel="shortcut icon" href="images/logo.ico" type="image/x-icon"/>
<%! boolean recolecorIniciado = false; %>

<%
    //La primera vez que se accede al sistema se inicia el recolector de basura
    if (!recolecorIniciado)
    {
        HiloRecolectorBasura hiloRecolector = new HiloRecolectorBasura();
        hiloRecolector.start();
        recolecorIniciado = true;
    }
    
    request.setCharacterEncoding("UTF-8");
    boolean loginIncorrecto = false;
    String titulo = "Inicio de sesión - Generador Inteligente de Cuestinarios";

    String tam_bloque = "";
    
    //Si no está logueado
    if (session.getAttribute("usuario") == null)
    {
        //Si ha echo click en validar comprobamos el login
        if (request.getParameter("user")!= null)
        {
            UsuarioCEN usuarioCEN = new UsuarioCEN();
            UsuarioEN usuarioEN = usuarioCEN.getUsuarioByLogin(request.getParameter("user"), request.getParameter("password"));

            //Si se ha logueado correctamente
            if (usuarioEN != null)
            {
                session.setAttribute("usuario", usuarioEN);
                response.sendRedirect("asignaturas.jsp");
            }
            else
            {
                loginIncorrecto = true;
                tam_bloque = "style=\"height: 23em;\"";
            }
        }
    }
    else
        response.sendRedirect("asignaturas.jsp");
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><%= titulo %></title>
        <link rel="stylesheet" href="styles/main-stylesheet.css" />
    </head>
    <body>
        <div id="caja_login" <%= tam_bloque %>>
            <div id="caja_logo">
                <img id="logo_inicio" src="images/logo.png" alt="logo" />
                Generador de cuestionarios
            </div>
            <div id="msg_iniciar_sesion" style="text-align: center">Iniciar Sesión</div>
            <div id="caja_login_interna">
                <form id="inicio_sesion" method="POST">
                    <div>
                        Usuario:<br />
                        <input class="text" type="text" id="login" name="user" value="" />
                    </div>
                    <div id="div_password">
                        Contraseña:<br />
                        <input class="text" type="password" id="password" name="password" value="" /><br>
                    </div>
                    <div id="acceso" onClick="inicio_sesion.submit();">
                        Acceder
                        <input name="acceder" style="visibility: hidden" type="submit" />
                    </div>
                </form>
            </div>
                <% if (loginIncorrecto) 
                    out.println("<div id=\"error_inicio\">Error al iniciar sesión. Comprueba tu usuario y contraseña.</div>");
                %>
        </div>
    </body>
</html>
