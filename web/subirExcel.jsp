<%-- 
    Copyright (C) 2014 Raúl Sempere Trujillo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Document   : subirExcell
    Created on : 22-jul-2014, 13:07:00
    Author     : Raúl Sempere Trujillo
--%>

<%-- 
    Este documento permite la subida del fichero Excel para que pueda ser
    accedido por la aplicación
--%>
 
<%@page import="EN.PreparacionExamenIdiomaEN"%>
<%@page import="EN.ExamenEN"%>
<%@page import="CEN.ExamenCEN"%>
<%@page import="CEN.PreparacionExamenIdiomaCEN"%>
<%@page import="EN.AsignaturaEN"%>
<%@page contentType="text/html" pageEncoding="UTF-8" session="true" %>

<% String titulo = "Analizar respuestas del examen - Generador Inteligente de Cuestinarios"; %>

<%
    if (session.getAttribute("usuario") == null)
    {
       response.sendRedirect("login.jsp");
    }
    else if (session.getAttribute("asignatura") == null)
    {
       response.sendRedirect("asignaturas.jsp");
    }
    else if (request.getParameter("examen") == null)
    {
        response.sendRedirect("verExamenes.jsp");
    }
    else
    {
        AsignaturaEN asignaturaEN = (AsignaturaEN)session.getAttribute("asignatura");
        ConfiguracionAsignaturaIdiomaCEN configuracionAsignaturaIdiomaCEN = new ConfiguracionAsignaturaIdiomaCEN();
        ConfiguracionAsignaturaIdiomaEN confPrincipal = configuracionAsignaturaIdiomaCEN.getConfiguracionIdiomaPrincipal(asignaturaEN);
        PreparacionExamenIdiomaCEN preparacionExamenIdiomaCEN = new PreparacionExamenIdiomaCEN();
        ExamenCEN examenCEN = new ExamenCEN();
        
        ExamenEN examenEN = examenCEN.getExamenByCodigo(asignaturaEN, Integer.parseInt(request.getParameter("examen")));
        PreparacionExamenIdiomaEN preparacionPrincipal = preparacionExamenIdiomaCEN.getPreparacionExamenByExamenEIdioma(examenEN, confPrincipal.getIdioma());
        
        String nombreExamen = preparacionPrincipal.getTraduccionNombre();
%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><%= titulo %></title>
        <link rel="stylesheet" href="styles/main-stylesheet.css" />
        <link rel="stylesheet" href="styles/menu.css" />
        <script type="text/javascript" src="script/javascript.js"></script>
    </head>
    <body>
        <%@include file="includes/menu.jsp"%>
        <div id="contenido">
            <br />
            <div id="caja">
                <h2 style="text-align: center">Analizar los resultados del examen "<%= nombreExamen %>"</h2><br />
                Se examinará el fichero de excel en busca de información estadística y en caso de éxito se incorporará 
                dicha información al sistema.<br /><br />
                <form name="excel" action="examinarExcel.jsp?examen=<%=request.getParameter("examen")%>" method="POST" enctype="multipart/form-data">
                    <input type="file" name="browser" accept="application/vnd.ms-excel" style="width: 500px" />
                    <br /><br />
                    <div id="error"></div>
                    <br />
                    <div class="boton_tema" style="float: right; margin-right: 2px" onClick="location.href='verExamenes.jsp'">Volver</div>
                    <div class="boton_tema" style="float: right; margin-right: 10px" onClick="validarExcel()">Analizar</div>
                    <br /><br />
                </form>
            </div>
        </div>
    </body>
</html>
<%
    }
%>