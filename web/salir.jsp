<%-- 
    Copyright (C) 2014 Raúl Sempere Trujillo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Document   : salir
    Created on : 15-abr-2014, 00:46:00
    Author     : Raúl Sempere Trujillo
--%>

<%-- 
    Este documento sirve para cerrar la sesión. Elimina todas las variables
    de sesión y devuelve el navegador a la página de login.
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" session="true" %>

<% String titulo = "Salir - Generador Inteligente de Cuestinarios"; %>

<%
    if (session.getAttribute("usuario") != null)
        session.removeAttribute("usuario");
    
    if (session.getAttribute("asignatura") != null)
        session.removeAttribute("asignatura");
    
    if (session.getAttribute("xml") != null)
        session.removeAttribute("xml");
    
    if (session.getAttribute("reclasificar") != null)
        session.removeAttribute("reclasificar");
    
    if (session.getAttribute("examen") != null)
        session.removeAttribute("examen");
    
    if (session.getAttribute("preparacion") != null)
        session.removeAttribute("preparacion");
    
    if (session.getAttribute("listaSeleccionadas") != null)
        session.removeAttribute("listaSeleccionadas");
    
    response.sendRedirect("login.jsp");
%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><%= titulo %></title>
    </head>
    <body>

    </body>
</html>