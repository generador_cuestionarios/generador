<%-- 
    Copyright (C) 2014 Ra�l Sempere Trujillo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Document   : guardarPregunta
    Created on : 08-may-2014, 19:32:00
    Author     : Ra�l Sempere Trujillo
--%>

<%-- 
    Este documento se emplea para guardar la nueva pregunta que el usuario ha
    creado.
--%>
 
<%@page import="clases_generador.ConfigManager"%>
<%@page import="CEN.ConfiguracionAsignaturaIdiomaCEN"%>
<%@page import="EN.ConfiguracionAsignaturaIdiomaEN"%>
<%@page import="CEN.ClasificacionTemaCEN"%>
<%@page import="EN.ClasificacionTemaEN"%>
<%@page import="EN.TraduccionOpcionEN"%>
<%@page import="CEN.AsignaturaCEN"%>
<%@page import="EN.OpcionEN"%>
<%@page import="EN.TraduccionPreguntaEN"%>
<%@page import="clases_generador.Traduccion"%>
<%@page import="java.util.ArrayList"%>
<%@page import="CEN.TraduccionOpcionCEN"%>
<%@page import="CEN.TraduccionPreguntaCEN"%>
<%@page import="CEN.OpcionCEN"%>
<%@page import="CEN.PreguntaCEN"%>
<%@page import="EN.PreguntaEN"%>
<%@page import="EN.KeywordEN"%>
<%@page import="CEN.KeywordCEN"%>
<%@page import="EN.AsignaturaEN"%>
<%@page import="EN.TemaEN"%>
<%@page import="CEN.TemaCEN"%>
<%@page import="clases_generador.XMLManager"%>

<%
    request.setCharacterEncoding("UTF-8");
    
    if (session.getAttribute("usuario") == null)
    {
       response.sendRedirect("login.jsp");
    }
    else if (session.getAttribute("asignatura") == null)
    {
       response.sendRedirect("asignaturas.jsp");
    }
    else if (request.getParameter("enunciado") == null)     
    {
        response.sendRedirect("nuevaPregunta.jsp");
    }
    else
    {
        PreguntaCEN preguntaCEN = new PreguntaCEN();
        AsignaturaEN asignaturaEN = (AsignaturaEN)session.getAttribute("asignatura");
        OpcionCEN opcionCEN = new OpcionCEN();
        ClasificacionTemaCEN clasificacionTemaCEN = new ClasificacionTemaCEN();
        TemaCEN temaCEN = new TemaCEN();
        TraduccionPreguntaCEN traduccionPreguntaCEN = new TraduccionPreguntaCEN();
        TraduccionOpcionCEN traduccionOpcionCEN = new TraduccionOpcionCEN();
        ConfiguracionAsignaturaIdiomaCEN configuracionAsignaturaIdiomaCEN = new ConfiguracionAsignaturaIdiomaCEN();
        ArrayList<ConfiguracionAsignaturaIdiomaEN> configuraciones;
        configuraciones = configuracionAsignaturaIdiomaCEN.getConfiguracionesAsignaturaIdiomaByAsignatura(asignaturaEN);
        PreguntaEN preguntaEN;
        TraduccionPreguntaEN traduccionPregunta;
        TraduccionOpcionEN traduccionOpcion;
        ArrayList<TraduccionPreguntaEN> traduccionesPregunta;
        ArrayList<TraduccionOpcionEN> traduccionesOpcion;
        ArrayList<OpcionEN> opciones;
        OpcionEN opcionEN;
        Traduccion traductor = new Traduccion(ConfigManager.getApertiumPackages());
        
        //Creamos la pregunta
        preguntaEN = new PreguntaEN();
        preguntaEN.setAsignatura(asignaturaEN);
        preguntaEN.setAciertos(0);
        preguntaEN.setContestada(0);
        preguntaEN.setNoContestada(0);
        preguntaEN.setUtilizada(0);
        preguntaEN.setIndiceDificultad(0.5);
        preguntaEN.setIndiceDiscriminacion(0.0);
        preguntaEN.setHabilitada(true);
        preguntaEN.setProximoUso(request.getParameter("proximoUso") != null);
        preguntaCEN.New(preguntaEN);
        
        traduccionesPregunta = new ArrayList<TraduccionPreguntaEN>();
       
        //Traducimos la pregunta en todos los idiomas de la asignatura
        for (ConfiguracionAsignaturaIdiomaEN configuracion: configuraciones)
        {
            traduccionPregunta = new TraduccionPreguntaEN();
            traduccionPregunta.setIdioma(configuracion.getIdioma());
            traduccionPregunta.setPregunta(preguntaEN);
            
            //Si el enunciado es del idioma principal no traducimos
            if (configuracion.isIdiomaPrincipal())
            {
                traduccionPregunta.setEnunciado(request.getParameter("enunciado"));
                traduccionPregunta.setRevisada(true);
            }
            else
            {
                traductor.seleccionarTraduccion(configuracion.getTraductorApertium());
                traduccionPregunta.setEnunciado(traductor.traducir(request.getParameter("enunciado")));
                traduccionPregunta.setRevisada(false);
            }
            
            traduccionPreguntaCEN.New(traduccionPregunta);
            traduccionesPregunta.add(traduccionPregunta);
        }
        
        preguntaEN.setTraducciones(traduccionesPregunta);
        
        opciones = new ArrayList<OpcionEN>();
        
        //Creamos las opciones
        for (int i = 1; i <= preguntaEN.getAsignatura().getNumeroOpciones(); i++)
        {
            opcionEN = new OpcionEN();
            opcionEN.setPregunta(preguntaEN);
            opcionEN.setCorrecta(request.getParameter("opciones").equals("cor" + i));
            opcionCEN.New(opcionEN);
            
            traduccionesOpcion = new ArrayList<TraduccionOpcionEN>();
            
            //Traducimos la opcion en todos los idiomas de la asignatura
            for (ConfiguracionAsignaturaIdiomaEN configuracion: configuraciones)
            {
                traduccionOpcion = new TraduccionOpcionEN();
                traduccionOpcion.setIdioma(configuracion.getIdioma());
                traduccionOpcion.setOpcion(opcionEN);  

                //Si el enunciado es del idioma principal no traducimos
                if (configuracion.isIdiomaPrincipal())
                {
                    traduccionOpcion.setEnunciado(request.getParameter("opc" + i));
                }
                else
                {
                    traductor.seleccionarTraduccion(configuracion.getTraductorApertium());
                    traduccionOpcion.setEnunciado(traductor.traducir(request.getParameter("opc" + i)));
                }

                traduccionOpcionCEN.New(traduccionOpcion);
                traduccionesOpcion.add(traduccionOpcion);
            }
            opciones.add(opcionEN);
        }
        
        preguntaEN.setOpciones(opciones);
        
        //Clasifiamos la pregunta
        //Autom�ticamente
        if (request.getParameter("tema").equals("auto"))
            preguntaCEN.clasificar(preguntaEN);
        //Usuario elige
        else if (!request.getParameter("tema").equals("sin"))
        {
            ClasificacionTemaEN clasificacionTemaEN = new ClasificacionTemaEN();
            TemaEN temaEN;
            
            temaEN = temaCEN.getTemaByCodigo(asignaturaEN, Integer.parseInt(request.getParameter("tema")));
            clasificacionTemaEN.setPregunta(preguntaEN);
            clasificacionTemaEN.setTema(temaEN);
            clasificacionTemaEN.setRevisada(true);
            
            clasificacionTemaCEN.New(clasificacionTemaEN);
        }
        
        //Ahora decidimos si vamos a la p�gina de revision o volvemos
        if (request.getParameter("traduccion") != null && configuraciones.size() > 1)
            response.sendRedirect("revisarTraduccionPregunta.jsp?pregunta=" + preguntaEN.getCodigo());
        else
            response.sendRedirect("nuevaPregunta.jsp?creada=true");    
    }
%>
