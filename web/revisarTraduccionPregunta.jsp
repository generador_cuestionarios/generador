<%-- 
    Copyright (C) 2014 Raúl Sempere Trujillo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Document   : revisarTraduccionPregunta
    Created on : 11-jul-2014, 15:56:00
    Author     : Raúl Sempere Trujillo
--%>
 
<%-- 
    Este documento permite revisar la traducción de una pregunta o de una
    bateria de preguntas si se le pasa como parametro un examen.
--%>

<%@page import="EN.ExamenPreguntaEN"%>
<%@page import="CEN.ExamenPreguntaCEN"%>
<%@page import="java.util.ArrayList"%>
<%@page import="EN.ExamenEN"%>
<%@page import="CEN.ExamenCEN"%>
<%@page import="EN.IdiomaEN"%>
<%@page import="CEN.IdiomaCEN"%>
<%@page import="EN.TraduccionOpcionEN"%>
<%@page import="CEN.OpcionCEN"%>
<%@page import="EN.OpcionEN"%>
<%@page import="EN.TraduccionPreguntaEN"%>
<%@page import="EN.PreguntaEN"%>
<%@page import="CEN.PreguntaCEN"%>
<%@page import="CEN.TraduccionOpcionCEN"%>
<%@page import="CEN.TraduccionPreguntaCEN"%>
<%@page import="EN.AsignaturaEN"%>

<% String titulo = "Revisar Traduccion - Generador Inteligente de Cuestinarios"; %>


<%
    String preLista = "";
    String postLista = "";
    String pregunta = "";
    String idioma = "";
    String examen = "";
    
    request.setCharacterEncoding("UTF-8");
    
    if (session.getAttribute("usuario") == null)
    {
       response.sendRedirect("login.jsp");
    }
    else if (session.getAttribute("asignatura") == null)
    {
       response.sendRedirect("asignaturas.jsp");
    }
    else if (request.getParameter("pregunta") == null && request.getParameter("examen") == null)     
    {
        response.sendRedirect("verPreguntas.jsp");
    }
    else
    {
        AsignaturaEN asignaturaEN = (AsignaturaEN)session.getAttribute("asignatura");
        ArrayList<ExamenPreguntaEN> examenesPregunta = null;
        ExamenEN examenEN = null;
        //En el unico caso que pregunta será nulo es cuando acabemos de llegar
        //al formulario para revisar las preguntas de un examen completo
        if (request.getParameter("pregunta") != null)
                pregunta = request.getParameter("pregunta");
        
        if (request.getParameter("idioma") != null)
        {
            idioma = request.getParameter("idioma");
            IdiomaCEN idiomaCEN = new IdiomaCEN();
            IdiomaEN idiomaEN = idiomaCEN.getIdiomaByCodigo(Integer.parseInt(request.getParameter("idioma")));
            
            if (request.getParameter("guardar") != null)
            {
                //Si estamos cambiando de idioma
                if (request.getParameter("guardar").equals("false") && request.getParameter("idiomas") == null)
                {
                    response.sendRedirect("revisarTraduccionPregunta.jsp?pregunta= " + request.getParameter("pregunta") + "&idioma=" + request.getParameter("idiomas"));
                }
                //Comprobamos si venimos del submit y hay que guardar
                //En este caso se utiliza la pregunta (que puede valer nulo)
                //Pero nunca entraremos al formulario directamente con submit por lo que
                //No se dará el caso
                else if (request.getParameter("guardar").equals("true"))
                {
                    PreguntaCEN preguntaCEN = new PreguntaCEN();
                    OpcionCEN opcionCEN = new OpcionCEN();
                    TraduccionPreguntaCEN traduccionPreguntaCEN = new TraduccionPreguntaCEN();
                    TraduccionOpcionCEN traduccionOpcionCEN = new TraduccionOpcionCEN();


                    TraduccionPreguntaEN traduccionPreguntaEN;
                    TraduccionOpcionEN traduccionOpcionEN;
                    PreguntaEN preguntaEN;
                    OpcionEN opcionEN;          

                    preguntaEN = preguntaCEN.getPreguntaByCodigo(asignaturaEN, Integer.parseInt(pregunta));

                    //Guardamos el enunciado de la pregunta
                    traduccionPreguntaEN = traduccionPreguntaCEN.getTraduccionPreguntaByPreguntaEIdioma(preguntaEN, idiomaEN);
                    traduccionPreguntaEN.setEnunciado(request.getParameter("traduccionEnunciado"));
                    traduccionPreguntaEN.setRevisada(true);

                    traduccionPreguntaCEN.Modify(traduccionPreguntaEN);

                    int i = 1;

                    //Buscamos las opciones
                    while (request.getParameter("traduccionOpc" + i) != null)
                    {
                        opcionEN = opcionCEN.getOpcionByCodigo(preguntaEN, i);
                        traduccionOpcionEN = traduccionOpcionCEN.getTraduccionOpcionByOpcionEIdioma(opcionEN, idiomaEN);
                        traduccionOpcionEN.setEnunciado(request.getParameter("traduccionOpc" + i));

                        traduccionOpcionCEN.Modify(traduccionOpcionEN);

                        i++;
                    }
                }
            }
            
            //comprobamos si estamos revisando las preguntas de un examen 
            //en cuyo caso cambiamos a la siguiente pregunta
            if (request.getParameter("examen") != null)
            {
                examen = request.getParameter("examen");
                
                //Buscamos la lista de preguntas del examen que no están revisadas
                ExamenCEN examenCEN = new ExamenCEN();
                examenEN = examenCEN.getExamenByCodigo(asignaturaEN, Integer.parseInt(examen));
                ExamenPreguntaCEN examenPreguntaCEN = new ExamenPreguntaCEN();
                examenesPregunta = examenPreguntaCEN.getExamenesPreguntaSinRevisarByExamenEIdioma(examenEN, idiomaEN);
                
                //Comprobamos si hay alguna pregunta más sin revisar
                if (examenesPregunta.size() > 0)
                {
                    pregunta = String.valueOf(examenesPregunta.get(0).getPregunta().getCodigo());
                }
                else
                {
                    response.sendRedirect("verTraduccionesExamen.jsp?examen=" + examenEN.getCodigo());
                }
            }
        }
        
        if (request.getParameter("examen") != null)
        {
            postLista += "<div>Preguntas restantes: " + (examenesPregunta.size()) + "</div>";
            postLista += "      <div style=\"float: right; margin-right: 2px\" id=\"boton_pregunta\" onClick=\"location.href='verTraduccionesExamen.jsp?examen=" + examenEN.getCodigo() + "'\">Volver</div>";
        }
        else
            postLista += "      <div style=\"float: right; margin-right: 2px\" id=\"boton_pregunta\" onClick=\"location.href='verPreguntas.jsp'\">Volver</div>";
        postLista += "      <div style=\"float: right; margin-right: 10px\" id=\"boton_pregunta\" onClick=\"guardarTraduccionPregunta()\">Guardar</div>";
        postLista += "      <br /><br />";
        
        postLista += "  </form>";
        postLista += "</div>";
        
        if ((examenesPregunta != null && examenesPregunta.size() > 0) || request.getParameter("examen") == null)
        {
        
%>        
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><%= titulo %></title>
        <link rel="stylesheet" href="styles/main-stylesheet.css" />
        <link rel="stylesheet" href="styles/menu.css" />
        <script type="text/javascript" src="script/javascript.js"></script>
    </head>
        <%@include file="includes/menu.jsp"%>
        <div id="contenido">
            <div id="revisionPregunta">
                <%= preLista %>

                <jsp:include page="includes/consultasAjax.jsp">
                    <jsp:param name="accion" value="traduccionPregunta" />
                    <jsp:param name="pregunta" value="<%=pregunta%>" />
                    <jsp:param name="idioma" value="<%=idioma%>" />
                    <jsp:param name="examen" value="<%=examen%>" />
                </jsp:include>
                
            <%= postLista %>
            </div>
        </div>
</html>
<%   
        }
    }
%>

