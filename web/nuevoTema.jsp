<%-- 
    Copyright (C) 2014 Raúl Sempere Trujillo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Document   : nuevoTema
    Created on : 07-may-2014, 13:36:25
    Author     : Raúl Sempere Trujillo
--%>
 
<%-- 
    Este documento se utiliza para permitir al usuario crear un nuevo tema 
    en la asignatura.
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" session="true" %>

<% String titulo = "Nuevo tema - Generador Inteligente de Cuestinarios"; %>

<%
    if (session.getAttribute("usuario") == null)
    {
       response.sendRedirect("login.jsp");
    }
    else if (session.getAttribute("asignatura") == null)
    {
       response.sendRedirect("asignaturas.jsp");
    }
    else
    {
        String body = "";
        
        if (request.getParameter("creado") != null)
            body = "onLoad=\"alert('Tema creado correctamente.')\"";
        
%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><%= titulo %></title>
        <link rel="stylesheet" href="styles/main-stylesheet.css" />
        <link rel="stylesheet" href="styles/menu.css" />
        <script type="text/javascript" src="script/javascript.js"></script>
    </head>
    <body <%=body%>>
        <%@include file="includes/menu.jsp"%>
        <div id="contenido">
            <div id="caja">
                <h2 style="text-align: center">Crear un nuevo tema</h2>
                <form name="nuevoTema" action="guardarTema.jsp" method="POST" accept-charset="utf-8">
                    <div class="boton_tema" style="float: right; margin-right: 2px" onClick="location.href='verTemas.jsp'">Volver</div>
                    <div class="boton_tema" style="float: right; margin-right: 10px" onClick="validarTema()">Crear tema</div>
                    <br /><br />
                    <div class="margen">Nombre del tema:&nbsp;&nbsp;<input type="text" name="nombre" style="width: 400px" /></div>
                    <br />
                    <h3 class="margen">Keywords:</h3>
                    <div class="borde">
                        <div id="keywords_externo">
                            <div id="keywords">
                                    <div class="margen">Keyword 1:&nbsp;&nbsp;
                                        <input type="text" name="key1" style="width: 250px" />
                                        &nbsp;&nbsp;Peso:&nbsp;&nbsp;
                                        <input type="number" name="peso1" min="1" max="10" value="5" />
                                    </div>
                            </div>
                            <input class="margen" type="button" onClick="anyadirKeyword()" value="Añadir keyword">
                        </div>
                    </div>
                    <br />
                    <div id="error"></div>
                    <br />
                    <div class="boton_tema" style="float: right; margin-right: 2px" onClick="location.href='verTemas.jsp'">Volver</div>
                    <div class="boton_tema" style="float: right; margin-right: 10px" onClick="validarTema()">Crear tema</div>
                    <br /><br />
                </form>
            </div>
       </div>
    </body>
</html>
<%
    }
%>