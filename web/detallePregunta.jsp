<%-- 
    Copyright (C) 2014 Raúl Sempere Trujillo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Document   : detallePregunta
    Created on : 11-jul-2014, 16:44:00
    Author     : Raúl Sempere Trujillo
--%>

<%-- 
    Muestra el formulario con los detalles de una pregunta, permitiendo editarla.
--%>

<%@page import="clases_generador.Utiles"%>
<%@page import="CEN.ClasificacionTemaCEN"%>
<%@page import="EN.ClasificacionTemaEN"%>
<%@page import="CEN.AsignaturaCEN"%>
<%@page import="CEN.OpcionCEN"%>
<%@page import="EN.TraduccionPreguntaEN"%>
<%@page import="EN.TraduccionOpcionEN"%>
<%@page import="CEN.TraduccionOpcionCEN"%>
<%@page import="CEN.TraduccionPreguntaCEN"%>
<%@page import="EN.PreguntaEN"%>
<%@page import="CEN.PreguntaCEN"%>
<%@page import="EN.TemaEN"%>
<%@page import="java.util.ArrayList"%>
<%@page import="CEN.TemaCEN"%>
<%@page import="EN.AsignaturaEN"%>
<%@page contentType="text/html" pageEncoding="UTF-8" session="true" %>

<% String titulo = "Preguntas - Generador Inteligente de Cuestinarios"; %>

<%
    String contenido = "";
    request.setCharacterEncoding("UTF-8");
    
    if (session.getAttribute("usuario") == null)
    {
       response.sendRedirect("login.jsp");
    }
    else if (session.getAttribute("asignatura") == null)
    {
       response.sendRedirect("asignaturas.jsp");
    }
    else if (request.getParameter("pregunta") != null)
    {
        AsignaturaEN asignaturaEN = (AsignaturaEN)session.getAttribute("asignatura");
        AsignaturaCEN asignaturaCEN = new AsignaturaCEN();
        PreguntaCEN preguntaCEN = new PreguntaCEN();
        ClasificacionTemaCEN clasificacionTemaCEN = new ClasificacionTemaCEN();
        ConfiguracionAsignaturaIdiomaCEN configuracionAsignaturaIdiomaCEN = new ConfiguracionAsignaturaIdiomaCEN();
        TemaCEN temaCEN = new TemaCEN();
        TraduccionPreguntaCEN traduccionPreguntaCEN = new TraduccionPreguntaCEN();
        TraduccionOpcionCEN traduccionOpcionCEN = new TraduccionOpcionCEN();
        OpcionCEN opcionCEN = new OpcionCEN();
        
        ConfiguracionAsignaturaIdiomaEN confPrincipal = configuracionAsignaturaIdiomaCEN.getConfiguracionIdiomaPrincipal(asignaturaEN);
        PreguntaEN preguntaEN = preguntaCEN.getPreguntaByCodigo(asignaturaEN, Integer.parseInt(request.getParameter("pregunta")));
        TraduccionPreguntaEN traduccionPreguntaEN = traduccionPreguntaCEN.getTraduccionPreguntaByPreguntaEIdioma(preguntaEN, confPrincipal.getIdioma());
        ArrayList<TraduccionOpcionEN> traduccionOpcionesEN =  traduccionOpcionCEN.getTraduccionesOpcionByPreguntaEIdioma(preguntaEN,  confPrincipal.getIdioma());
        ArrayList<TemaEN> temas = temaCEN.getTemasByAsignatura(asignaturaEN);
        preguntaCEN.rellenarClasificacion(preguntaEN);
        ArrayList<ConfiguracionAsignaturaIdiomaEN> configuraciones;
        configuraciones = configuracionAsignaturaIdiomaCEN.getConfiguracionesAsignaturaIdiomaByAsignatura(asignaturaEN);
        boolean preguntaModificada = false;
        
        //Guardar
        if (request.getParameter("enunciado") != null)
        {
            boolean habilitada = (request.getParameter("habilitada") == null);
            boolean proximoUso = (request.getParameter("proximoUso") != null);
            
            preguntaEN.setProximoUso(proximoUso);
            preguntaEN.setHabilitada(habilitada);
            
            //Guardamos la pregunta
            preguntaCEN.Modify(preguntaEN);
            
            //Comprobamos los enunciados para saber si hay que volver a revisar las traducciones
            if (!request.getParameter("enunciado").equals(traduccionPreguntaEN.getEnunciado()))
                preguntaModificada = true;
            
            traduccionPreguntaEN.setEnunciado(request.getParameter("enunciado"));
            
            //Guardamos la traduccion de la pregunta
            traduccionPreguntaCEN.Modify(traduccionPreguntaEN);
            
            for (int i = 1; i <= asignaturaEN.getNumeroOpciones(); i++)
            {
                if (!request.getParameter("opc" + i).equals(traduccionOpcionesEN.get(i-1).getEnunciado()))
                    preguntaModificada = true;
                
                traduccionOpcionesEN.get(i-1).setEnunciado(request.getParameter("opc" + i));
                traduccionOpcionesEN.get(i-1).getOpcion().setCorrecta((request.getParameter("opciones").equals("cor" + i)));
                
                //Guardamos la opción y su traducción
                opcionCEN.Modify(traduccionOpcionesEN.get(i-1).getOpcion());
                traduccionOpcionCEN.Modify(traduccionOpcionesEN.get(i-1));
            }
            
            //Clasificamos la pregunta 
            //Automáticamente
            if (request.getParameter("tema").equals("auto"))
            {
                asignaturaCEN.rellenarTemas(preguntaEN.getAsignatura());
                preguntaCEN.clasificar(preguntaEN);
            }
            //Usuario elige
            else
            {
                //Borramos la actual si la hay
                if (preguntaEN.getClasificacion() != null)
                {
                    clasificacionTemaCEN.Delete(preguntaEN.getClasificacion());
                    preguntaEN.setClasificacion(null);
                }
                
                if (!request.getParameter("tema").equals("sin"))
                {
                    ClasificacionTemaEN clasificacionTemaEN = new ClasificacionTemaEN();
                    TemaEN temaEN;

                    temaEN = temaCEN.getTemaByCodigo(asignaturaEN, Integer.parseInt(request.getParameter("tema")));
                    clasificacionTemaEN.setPregunta(preguntaEN);
                    clasificacionTemaEN.setTema(temaEN);
                    clasificacionTemaEN.setRevisada(true);

                    clasificacionTemaCEN.New(clasificacionTemaEN);
                    preguntaEN.setClasificacion(clasificacionTemaEN);
                }
            }
            
            //Eliminamos las traducciones que ahora ya dejan de estar revisadas
            if (preguntaModificada)
            {
                ArrayList<TraduccionPreguntaEN> traduccionesPregunta = traduccionPreguntaCEN.getTraduccionesPreguntaByPregunta(preguntaEN);
                ArrayList<TraduccionOpcionEN> traduccionesOpciones;

                for (TraduccionPreguntaEN traduccionPregunta : traduccionesPregunta)
                {
                    //Si no es del idioma principal
                    if (traduccionPregunta.getIdioma().getCodigo() != confPrincipal.getIdioma().getCodigo())
                    {
                        traduccionesOpciones = traduccionOpcionCEN.getTraduccionesOpcionByPreguntaEIdioma(preguntaEN, traduccionPregunta.getIdioma());

                        for (TraduccionOpcionEN traduccionOpcionEN : traduccionesOpciones)
                        {
                            traduccionOpcionCEN.Delete(traduccionOpcionEN);
                        }

                        traduccionPreguntaCEN.Delete(traduccionPregunta);
                    }
                }
            }
            
            //Ahora que hemos guardado la pregunta vemos a donde tenemos que ir
            //Si venimos de preguntas y hay que revisar
            if (preguntaModificada && request.getParameter("traduccion") != null && configuraciones.size() > 1) 
                response.sendRedirect("revisarTraduccionPregunta.jsp?pregunta=" + preguntaEN.getCodigo());
            //Venimos del examen
            else if (request.getParameter("examen") != null)
                response.sendRedirect("nuevoExamen.jsp?paso=4");
            //Venimos de ver preguntas (sin revisar)
            else
                response.sendRedirect("verPreguntas.jsp");
        }
        else
        {
            contenido += "<div id=\"caja\">";
            contenido += "<h2 style=\"text-align: center\">Detalle de la pregunta</h2>";
            
            if (request.getParameter("examen") != null)
                contenido += "<div style=\"float: right; margin-right: 2px\" class=\"boton_pregunta\" onClick=\"location.href='nuevoExamen.jsp?paso=4'\" />Volver</div>";
            else
                contenido += "<div style=\"float: right; margin-right: 2px\" class=\"boton_pregunta\" onClick=\"location.href='verPreguntas.jsp'\" />Volver</div>";

            contenido += "      <div style=\"float: right; margin-right: 10px\" class=\"botonEditar\"><div id=\"boton_examen\" onClick=\"editarPregunta()\" />Editar Pregunta</div></div>";
            contenido += "<br />";
            
            if (request.getParameter("examen") != null)
                contenido += "  <form name=\"editar\" action=\"detallePregunta.jsp?pregunta=" + preguntaEN.getCodigo() + "&examen=true" + "\" method=\"POST\" accept-charset=\"utf-8\">";
            else
                contenido += "  <form name=\"editar\" action=\"detallePregunta.jsp?pregunta=" + preguntaEN.getCodigo() + "\" method=\"POST\" accept-charset=\"utf-8\">";
            contenido += "  <form name=\"editar\" action=\"detallePregunta.jsp?pregunta=" + preguntaEN.getCodigo() + "\" method=\"POST\" accept-charset=\"utf-8\">";
            contenido += "      <h3 class=\"margen\">Enunciado:</h3>";
            contenido += "      <div class=\"borde\">";
            contenido += "          <div class=\"margen\"><textarea name=\"enunciado\" id=\"enunciado\" cols=\"82\" rows=\"6\" disabled>" + Utiles.textoHTML(traduccionPreguntaEN.getEnunciado()) + "</textarea></div>";
            contenido += "      </div>";
            contenido += "      <br />";
            contenido += "      <h3 class=\"margen\">Opciones:</h3>";
            contenido += "      <div class=\"borde\">";

            //ponemos las opciones
            for (int i = 1; i <= asignaturaEN.getNumeroOpciones(); i++)
            {
                contenido += "<div class=\"margen\">Opción " + i + ":<br /><textarea class=\"opcion\" style=\"vertical-align: middle\" name=\"opc" + i + "\" cols=\"70\" rows=\"3\" disabled>" + Utiles.textoHTML(traduccionOpcionesEN.get(i-1).getEnunciado()) + "</textarea>"
                        + "<input class=\"correcto \" type=\"radio\" name=\"opciones\" value=\"cor" + i + "\"";
                if (traduccionOpcionesEN.get(i-1).getOpcion().isCorrecta() == true)
                    contenido +=  " checked";

                contenido += " disabled/> Correcta</div>";
            }

            contenido += "      </div>";
            contenido += "      <br />";
            contenido += "      <h3 class=\"margen\">Clasificación:</h3>";
            contenido += "      <div class=\"borde\">";
            contenido += "          <div class=\"margen\"><select name=\"tema\" id=\"tema\" style=\"min-width: 300px; max-width: 700px\" disabled>";
            contenido += "<option value=\"auto\">Automática</option>";

            if (preguntaEN.getClasificacion() == null)
            {
                contenido += "<option value=\"sin\" selected>Sin clasificar</option>";

                for (TemaEN temaEN : temas)
                {
                    contenido += "<option value=\"" + temaEN.getCodigo() + "\">" + temaEN.getNombre() + "</option>";
                }
            }
            else
            {
                contenido += "<option value=\"sin\">Sin clasificar</option>";

                for (TemaEN temaEN : temas)
                {
                    if (preguntaEN.getClasificacion().getTema().getCodigo() == temaEN.getCodigo())
                        contenido += "<option value=\"" + temaEN.getCodigo() + "\" selected>" + temaEN.getNombre() + "</option>";
                    else
                        contenido += "<option value=\"" + temaEN.getCodigo() + "\">" + temaEN.getNombre() + "</option>";
                }
            }

            if (preguntaEN.getClasificacion() != null && !preguntaEN.getClasificacion().isRevisada())
                contenido += "</select>&nbsp;<img id=\"estado\" class=\"alert\" src=\"images/alert.png\" alt=\"sin revisar\" />&nbsp;<a class=\"link\" id=\"link\" onClick=\"validarClasificacion(" + preguntaEN.getCodigo() + ")\">validar</a></div>";
            else if (preguntaEN.getClasificacion() == null)
                contenido += "</select>&nbsp;<img id=\"estado\" class=\"alert\" src=\"images/error.png\" alt=\"sin clasificar\" /><a id=\"link\"></a></div>";
            else
                contenido += "</select>&nbsp;<img id=\"estado\" class=\"alert\" src=\"images/ok.png\" alt=\"revisado\" /><a id=\"link\"></a></div>";

            contenido += "      </div>";
            contenido += "      <br />";

            if (request.getParameter("examen") == null)
            {
                contenido += "      <h3 class=\"margen\">Otras opciones:</h3>";
                contenido += "      <div class=\"borde\">";
                contenido += "          <div class=\"margen\"><input type=\"checkbox\" id=\"habilitada\" name=\"habilitada\" disabled";
                if (!preguntaEN.isHabilitada())
                    contenido += " checked";
                contenido += "> Deshabilitada (no se selecionará en más examenes)</div>";
                contenido += "          <div class=\"margen\"><input type=\"checkbox\" id=\"proximoUso\" name=\"proximoUso\" disabled";
                if (preguntaEN.isProximoUso())
                    contenido += " checked";
                contenido += "> Utilizar en el próximo examen</div>";
                contenido += "          <div id=\"revisar\" style=\"display: none\" class=\"margen\"><input type=\"checkbox\" id=\"traduccion\" name=\"traduccion\"> Revisar la traducción al finalizar la edición (si han habido cambios)</div>";
                contenido += "      </div>";
                contenido += "      <br />";
            }

            contenido += "      <div id=\"error\"></div>";
            contenido += "      <br />";

            if (request.getParameter("examen") != null)
                contenido += "<div style=\"float: right; margin-right: 2px\" class=\"boton_pregunta\" onClick=\"location.href='nuevoExamen.jsp?paso=4'\" />Volver</div>";
            else
                contenido += "<div style=\"float: right; margin-right: 2px\" class=\"boton_pregunta\" onClick=\"location.href='verPreguntas.jsp'\" />Volver</div>";

            contenido += "      <div style=\"float: right; margin-right: 10px\" class=\"botonEditar\"><div id=\"boton_examen\" onClick=\"editarPregunta()\" />Editar Pregunta</div></div>";
            contenido += "      <br /><br />";
            contenido += "  </form>";
            contenido += "</div>";
        }
    }
    else
    {
        response.sendRedirect("verPreguntas.jsp");
    }
%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><%= titulo %></title>
        <link rel="stylesheet" href="styles/main-stylesheet.css" />
        <link rel="stylesheet" href="styles/menu.css" />
        <script type="text/javascript" src="script/javascript.js"></script>
    </head>
    <%
            String body;
            if (request.getParameter("examen") != null)
                body = "<body onLoad=\"editarPregunta()\">";
            else
                body = "<body>";
    %>
    <%=body%>
        <%@include file="includes/menu.jsp"%>
        <div id="contenido">
            <%= contenido %>
        </div>
    </body>
</html>