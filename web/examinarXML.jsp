<%-- 
    Copyright (C) 2014 Raúl Sempere Trujillo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Document   : examinarXML
    Created on : 02-may-2014, 18:09:00
    Author     : Raúl Sempere Trujillo
--%>

<%-- 
    Muestra los resultados de examinar el fichero XMl en busca de preguntas
    Y permite cargar las preguntas encontradas.
--%>

<%@page import="clases_generador.ConfigManager"%>
<%@page import="clases_generador.XMLManager"%>
<%@page import="EN.AsignaturaEN"%>
<%@page import="CEN.PreguntaCEN"%>
<%@page import="org.apache.commons.fileupload.FileUploadException"%>
<%@page import="java.util.List"%>
<%@page import="org.apache.commons.fileupload.FileItem"%>
<%@page import="java.io.File"%>
<%@page import="org.apache.commons.fileupload.servlet.ServletFileUpload"%>
<%@page import="org.apache.commons.fileupload.disk.DiskFileItemFactory"%>

<%
    String titulo = "Cargar Preguntas desde XML - Generador Inteligente de Cuestinarios";
    String contenido = "";
%>

<%
    if (session.getAttribute("usuario") == null)
    {
       response.sendRedirect("login.jsp");
    }
    else if (session.getAttribute("asignatura") == null)
    {
       response.sendRedirect("asignaturas.jsp");
    }
    else
    {
        String CATALINA_HOME = System.getenv("CATALINA_HOME");
        
        if (CATALINA_HOME.endsWith("/") || CATALINA_HOME.endsWith("\\"))
            CATALINA_HOME = CATALINA_HOME.substring(0, CATALINA_HOME.length()-1);
        
        String ruta = CATALINA_HOME + "/webapps/" + ConfigManager.getWebApp() + "/temp/";
        String fichero = "";

        boolean isMultipart = ServletFileUpload.isMultipartContent(request);   

        if (isMultipart)
        {
            DiskFileItemFactory factory = new DiskFileItemFactory();
            factory.setSizeThreshold(1024);
            factory.setRepository(new File(ruta));

            ServletFileUpload upload = new ServletFileUpload(factory);

            //Subimos el fichero xml al servidor
            try
            {
                List<FileItem> partes = upload.parseRequest(request);
                
                for (FileItem parte: partes)
                {
                    File file = new File (ruta, parte.getName());
                    parte.write(file);
                    fichero = parte.getName();
                }

                try
                {
                    XMLManager xml = new XMLManager((AsignaturaEN)session.getAttribute("asignatura"), ruta + fichero);
                    xml.examinarXML();

                    if (xml.isCorrecto())
                    {
                        session.setAttribute("xml", xml);
                        
                        contenido += "<div id=\"caja\">";
                        contenido += "<h2 style=\"text-align: center\">Cargar las preguntas del XML</h2><br />";
                        contenido += "El fichero XML es correcto.<br />";
                        contenido += "Número de preguntas encontradas: " + xml.getNumPreguntas() + "<br /><br />";
                        contenido += "<form action=\"cargarPreguntas.jsp\" method=\"POST\">";
                        contenido += "<h3 class=\"margen\">Otras opciones:</h3>";
                        contenido += "<div class=\"borde\">";
                        contenido += "<div class=\"margen\"><input type=\"checkbox\" name=\"clasificar\" checked />Clasificar todas las preguntas en temas.</div>";
                        contenido += "<div class=\"margen\"><input type=\"checkbox\" name=\"proximoUso\" checked />Marcar todas las preguntas para su utilización en el próximo examen.</div>";
                        contenido += "</div><br />";
                        contenido += "<input type=\"submit\" value=\"Cargar preguntas\">";
                        contenido += "</form></div>";              
                    }
                    else
                    {
                        contenido += xml.getError();
                    }
                }
                catch (Exception ex)
                {
                    //Borramos el fichero subido
                }
                finally
                {
                    //Borramos el fichero subido
                    File archivo = new File(ruta + fichero);
                    
                    if (!archivo.isDirectory() && archivo.exists())
                        archivo.delete(); 
                }
            }  
            catch (FileUploadException ex)
            {
                out.println("Error: " + ex.getMessage());
            }
        }
%>

<%@page contentType="text/html" pageEncoding="UTF-8" session="true" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><%= titulo %></title>
        <link rel="stylesheet" href="styles/main-stylesheet.css" />
        <link rel="stylesheet" href="styles/menu.css" />
        <script type="text/javascript" src="script/javascript.js"></script>
    </head>
    <body>
        <%@include file="includes/menu.jsp"%>
        <div id="contenido">
            <%= contenido %>
        </div>
    </body>
</html>

<%
    }
%>