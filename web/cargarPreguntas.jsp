<%-- 
    Copyright (C) 2014 Raúl Sempere Trujillo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Document   : cargarPreguntas
    Created on : 05-may-2014, 16:18:52
    Author     : Raúl Sempere Trujillo
--%>
 
<%-- 
    Este documento muestra la barra de carga de las preguntas XML. Inicia el
    hilo de carga de las preguntas e inicia el temporizador para actualizar
    la barra con javascript
--%>

<%@page import="clases_generador.HiloXML"%>
<%@page import="clases_generador.XMLManager"%>

<%String titulo = "Cargar Preguntas desde XML - Generador Inteligente de Cuestinarios";%>


<%
    if (session.getAttribute("usuario") == null)
    {
       response.sendRedirect("login.jsp");
    }
    else if (session.getAttribute("asignatura") == null)
    {
       response.sendRedirect("asignaturas.jsp");
    }
    else if (session.getAttribute("xml") != null)     
    {
        boolean clasificar = request.getParameter("clasificar") != null;
        boolean proximoUso = request.getParameter("proximoUso") != null;
        
        HiloXML hiloXML = new HiloXML((XMLManager)session.getAttribute("xml"), clasificar, proximoUso);
        
        //Este hilo se enctarga de procesar toda la subida de preguntas del xml
        //Mientras se muestra la pagina
        hiloXML.start();
    }
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><%= titulo %></title>
        <link rel="stylesheet" href="styles/main-stylesheet.css" />
        <link rel="stylesheet" href="styles/menu.css" />
        <script type="text/javascript" src="script/javascript.js"></script>
    </head>
    <body onLoad="barraProgresoXML();">
        <%@include file="includes/menu.jsp"%>
        <div id="contenido">
            <div id="caja">
                <h2 class="margen">Importando preguntas</h2>
                <div id="barra"></div>
            </div>
        </div>
    </body>
</html>
