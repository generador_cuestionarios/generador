<%-- 
    Copyright (C) 2014 Raúl Sempere Trujillo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Document   : idiomas
    Created on : 25-may-jul, 16:02:00
    Author     : Raúl Sempere Trujillo
--%>

<%-- 
    Este documento muestra el formulario de gestion de idiomas del administrador,
    permitiendo añadir y quitar idiomas de la asignatura
--%>

<%@page import="clases_generador.ConfigManager"%>
<%@page import="EN.IdiomaEN"%>
<%@page import="CEN.IdiomaCEN"%>
<%@page import="clases_generador.Traduccion"%>
<%@page import="java.util.ArrayList"%>
<%@page import="EN.AsignaturaEN"%>
<%@page import="EN.AccesoUsuarioAsignaturaEN"%>
<%@page import="CEN.AccesoUsuarioAsignaturaCEN"%>
<%@page contentType="text/html" pageEncoding="UTF-8" session="true" %>

<% String titulo = "Idiomas - Generador Inteligente de Cuestinarios"; %>

<%
    request.setCharacterEncoding("UTF-8");
    String opciones = "";
    String idiomaPrincipal = "";
    
    if (session.getAttribute("usuario") == null)
    {
       response.sendRedirect("login.jsp");
    }
    else if (session.getAttribute("asignatura") == null)
    {
       response.sendRedirect("asignaturas.jsp");
    }
    else
    {
        AccesoUsuarioAsignaturaCEN accesoUsuarioAsignaturaCEN = new AccesoUsuarioAsignaturaCEN();
        AccesoUsuarioAsignaturaEN accesoUsuarioAsignaturaEN = accesoUsuarioAsignaturaCEN.getAccesoByAsignaturaYUsuario((AsignaturaEN)session.getAttribute("asignatura"), (UsuarioEN)session.getAttribute("usuario"));
        ConfiguracionAsignaturaIdiomaCEN configuracionAsignaturaIdiomaCEN = new ConfiguracionAsignaturaIdiomaCEN();
        
        if (!accesoUsuarioAsignaturaEN.isAdministrador())
            response.sendRedirect("index.jsp");
        else
        {
            AsignaturaEN asignaturaEN = (AsignaturaEN)session.getAttribute("asignatura");
            Traduccion traductor = new Traduccion(ConfigManager.getApertiumPackages());
            
            for (int i = 0; i < traductor.getTraducciones().length; i++)
            {
                opciones += "<option value=\"" + traductor.getTraducciones()[i].toString() + "\">" + traductor.getTraducciones()[i].toString() + "</option>";
            }
            
            idiomaPrincipal = configuracionAsignaturaIdiomaCEN.getConfiguracionIdiomaPrincipal(asignaturaEN).getIdioma().getNombre();
            
            //Si venimos de un submit creamos la configuración
            if (request.getParameter("nombreAsignatura") != null)
            {
                IdiomaCEN idiomaCEN = new IdiomaCEN();
                IdiomaEN idiomaEN = idiomaCEN.getIdiomaByCodigo(Integer.parseInt(request.getParameter("idiomas")));
                ConfiguracionAsignaturaIdiomaEN conf = new ConfiguracionAsignaturaIdiomaEN();
                
                conf.setAsignatura(asignaturaEN);
                conf.setIdioma(idiomaEN);
                conf.setTraductorApertium(request.getParameter("traductor"));
                conf.setNombreAsignatura(request.getParameter("nombreAsignatura"));
                
                configuracionAsignaturaIdiomaCEN.New(conf);
            }
            
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><%= titulo %></title>
        <link rel="stylesheet" href="styles/main-stylesheet.css" />
        <link rel="stylesheet" href="styles/menu.css" />
        <script type="text/javascript" src="script/javascript.js"></script>
    </head>
    <body>
        <%@include file="includes/menu.jsp"%>
        <div id="contenido">
            <div id="caja">
                <h2 style="text-align: center">Agregar nuevo idioma</h2>
                <div class="borde">
                    <br />
                    <div class="margen">Idioma a configurar:</div>
                    <form name="idioma" action="idiomas.jsp" method="POST" accept-charset="utf-8">
                        <div id="listadoNoAcceso" class="margen">
                            <jsp:include page="includes/consultasAjax.jsp">
                                <jsp:param name="accion" value="idiomasSinConfigurar" />
                            </jsp:include> 
                        </div>
                        <div class="margen">Traductor: (que traduzca del idioma principal al nuevo)</div>
                        <div class="margen">
                            <select name="traductor" style="width:40em">
                                <%=opciones%>
                            </select>
                        </div>
                        <div class="margen">Nombre de la asignatura: (traducido en el nuevo idioma)</div>
                        <div class="margen"><input style="width: 40em" type="text" name="nombreAsignatura"/>
                            <div class="boton_tema" style="float: right; margin-right: 10px" onClick="agregarIdioma()">Agregar</div>
                        </div>
                        <div class="margen" id="error"></div>
                        <br />
                    </form>
                </div>
                <br />
                <div class="boton_tema" style="float: right; margin-right: 2px" onClick="location.href='index.jsp'">Volver</div>
                <br />
                <h2 style="text-align: center">Idiomas de la asignatura</h2>
                <h3 class="margen">Idioma principal de la asignatura:</h3>
                <table class="listado">
                    <tr><td><%=idiomaPrincipal%></td></tr>
                </table>
                <br />
                <h3 class="margen">Otros idiomas configurados:</h3>
                <div id="listadoIdiomas">
                    <jsp:include page="includes/listadoIdiomas.jsp" />
                </div>
                <br />
                <div class="boton_tema" style="float: right; margin-right: 2px" onClick="location.href='index.jsp'">Volver</div>
                <br />
            </div>
        </div>
    </body>
</html>
<%
        }
    }
%>