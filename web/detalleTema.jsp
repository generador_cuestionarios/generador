<%-- 
    Copyright (C) 2014 Raúl Sempere Trujillo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Document   : detalleExamen
    Created on : 17-jul-2014, 17:48:00
    Author     : Raúl Sempere Trujillo
--%>
 
<%-- 
    Muestra el formulario con los detalles de un tema, permitiendo editarlo.
--%>

<%@page import="EN.AsignaturaEN"%>
<%@page import="EN.KeywordEN"%>
<%@page import="java.util.ArrayList"%>
<%@page import="CEN.KeywordCEN"%>
<%@page import="EN.TemaEN"%>
<%@page import="CEN.TemaCEN"%>
<%@page contentType="text/html" pageEncoding="UTF-8" session="true" %>

<% String titulo = "Detalle tema - Generador Inteligente de Cuestinarios"; %>

<%
    request.setCharacterEncoding("UTF-8");
    String contenido = "";
    int cantKeywords = 0;
    
    if (session.getAttribute("usuario") == null)
    {
       response.sendRedirect("login.jsp");
    }
    else if (session.getAttribute("asignatura") == null)
    {
       response.sendRedirect("asignaturas.jsp");
    }
    else if (request.getParameter("tema") == null)
    {
       response.sendRedirect("verTemas.jsp");
    }
    else
    {
        int temaCod = Integer.parseInt(request.getParameter("tema"));
        
        TemaCEN temaCEN = new TemaCEN();
        KeywordCEN keywordCEN = new KeywordCEN();
        
        AsignaturaEN asignaturaEN = (AsignaturaEN)session.getAttribute("asignatura");
        TemaEN temaEN;
        KeywordEN keywordEN;
        
        //Si tenemos que guardar primero guardamos
        if (request.getParameter("nombre") != null)
        {
            
            temaEN = temaCEN.getTemaByCodigo(asignaturaEN, temaCod);
            temaCEN.rellenarKeywords(temaEN);
            
            //Modificamos el tema si es necesario
            if (!temaEN.getNombre().equals(request.getParameter("nombre")))
            {
                temaEN.setNombre(request.getParameter("nombre"));
                temaCEN.Modify(temaEN);
            }
            
            //Comprobamos si las keywords se han modificado
            int i = 1;

            //Borramos las actuales
            for (KeywordEN keyword : temaEN.getKeywords())
            {
                keywordCEN.Delete(keyword);
            }

            //Las volvemos a crear
            i = 1;

            while (request.getParameter("key" + i) != null)
            {
                if (request.getParameter("key" + i) != "")
                {
                    keywordEN = new KeywordEN();

                    keywordEN.setPalabra(request.getParameter("key" + i).toLowerCase());
                    keywordEN.setPeso(Integer.parseInt(request.getParameter("peso" + i)));
                    keywordEN.setTema(temaEN);

                    keywordCEN.New(keywordEN);
                }

                i++;
            }  
        }
        
        //Y ahora cargamos los datos del tema desde la BD
        temaEN = temaCEN.getTemaByCodigo(asignaturaEN, temaCod);
        temaCEN.rellenarKeywords(temaEN);
        cantKeywords = temaEN.getKeywords().size();
        
        contenido += "<div id=\"caja\">";
        contenido += "<h2 style=\"text-align: center\">Detalle del tema</h2>";
        contenido += "<form name=\"editar\" action=\"detalleTema.jsp?tema=" + temaEN.getCodigo() + "\" method=\"POST\" accept-charset=\"utf-8\">";
        contenido += "<div class=\"boton_tema\" style=\"float: right; margin-right: 2px\" onClick=\"location.href='verTemas.jsp'\">Volver</div>";
        contenido += "<div style=\"float: right; margin-right: 10px\" class=\"botonEditar\"><div class=\"boton_examen\" onClick=\"editarTema() \">Editar Tema</div></div>";
        contenido += "<br /><br />";
        contenido += "<div class=\"margen\">Nombre del tema:&nbsp;&nbsp;<input type=\"text\" id=\"nombre\" name=\"nombre\" style=\"width: 400px\" value=\"" + temaEN.getNombre() + "\" disabled/></div>";
        contenido += "<h3 class=\"margen\">Keywords:</h3>";
        contenido += "<div class=\"borde\">";
        contenido += "<div id=\"keywords_externo\">";
        contenido += "<div id=\"keywords\">";
        
        for (int i = 0; i < temaEN.getKeywords().size(); i++)
        {
            contenido += "<div class=\"margen\">Keyword " + (i+1) + ":&nbsp;&nbsp;&nbsp;";
            contenido += "<input class=\"keyword\" type=\"text\" id=\"key" + (i+1) + "\" name=\"key" + (i+1) + "\" style=\"width: 250px\" value=\"" + temaEN.getKeywords().get(i).getPalabra() + "\" disabled/>";
            contenido += "&nbsp;&nbsp;&nbsp;Peso:&nbsp;&nbsp;&nbsp;";
            contenido += "<input class=\"peso\"  type=\"number\" id=\"peso" + (i+1) + "\" name=\"peso" + (i+1) + "\" min=\"1\" max=\"10\" value=\"" + temaEN.getKeywords().get(i).getPeso() + "\" disabled/></div>";
        }
        
        contenido += "</div>";
        contenido += "<input id=\"nuevaKeyword\"class=\"margen\" type=\"button\" onClick=\"anyadirKeyword()\" value=\"Añadir keyword\" disabled>";
        contenido += "</div></div><br />";
        contenido += "<div id=\"error\"></div><br />";
        contenido += "<div class=\"boton_tema\" style=\"float: right; margin-right: 2px\" onClick=\"location.href='verTemas.jsp'\">Volver</div>";
        contenido += "<div style=\"float: right; margin-right: 10px\" class=\"botonEditar\"><div class=\"boton_examen\" onClick=\"editarTema() \">Editar Tema</div></div>";
        contenido += "<br /><br />";
        contenido += "</form></div>"; 
    }
%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><%= titulo %></title>
        <link rel="stylesheet" href="styles/main-stylesheet.css" />
        <link rel="stylesheet" href="styles/menu.css" />
        <script type="text/javascript" src="script/javascript.js"></script>
    </head>
    <body onLoad="actualizarContadorKeywords(<%=cantKeywords%>)">
        <%@include file="includes/menu.jsp"%>
        <div id="contenido">
            <%= contenido %>
       </div>
    </body>
</html>