<%-- 
    Copyright (C) 2014 Raúl Sempere Trujillo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Document   : nuevoExamen
    Created on : 17-jun-2014, 13:14:00
    Author     : Raúl Sempere Trujillo
--%>
 
<%-- 
    Este documento se utiliza para permitir al usuario crear un nuevo examen 
    en la asignatura.
--%>

<%@page import="CEN.PreparacionExamenIdiomaCEN"%>
<%@page import="clases_generador.Utiles"%>
<%@page import="EN.TraduccionInstruccionEN"%>
<%@page import="CEN.ExamenCEN"%>
<%@page import="CEN.TraduccionInstruccionCEN"%>
<%@page import="EN.TemaEN"%>
<%@page import="CEN.TemaCEN"%>
<%@page import="CEN.PreguntaCEN"%>
<%@page import="CEN.ClasificacionTemaCEN"%>
<%@page import="EN.PreguntaEN"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Random"%>
<%@page import="java.util.Calendar"%>
<%@page import="EN.AsignaturaEN"%>
<%@page import="EN.ExamenEN"%>
<%@page import="EN.PreparacionExamenIdiomaEN"%>
<%@page contentType="text/html" pageEncoding="UTF-8" session="true" %>

<% String titulo = "Nuevo examen - Generador Inteligente de Cuestinarios"; %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><%= titulo %></title>
        <link rel="stylesheet" href="styles/main-stylesheet.css" />
        <link rel="stylesheet" href="styles/menu.css" />
        <script type="text/javascript" src="script/javascript.js"></script>
    </head>
    <body>
        <%@include file="includes/menu.jsp"%>
        <div id="tooltip"></div>
        <div id="contenido">

<%
    request.setCharacterEncoding("UTF-8");
    String preLista = "";
    String preLista1 = "";
    String preLista2 = "";
    String postLista = "";
    String contenido = "";
    
    if (session.getAttribute("filtro") != null)
            session.removeAttribute("filtro");
    
    if (session.getAttribute("usuario") == null)
    {
       response.sendRedirect("login.jsp");
    }
    else if (session.getAttribute("asignatura") == null)
    {
       response.sendRedirect("asignaturas.jsp");
    }
    //Paso 1
    else if (request.getParameter("paso").equals("1"))
    {
        contenido += "<div id=\"caja\">";
        contenido += "<h2 style=\"text-align: center\">Crear un nuevo examen</h2>";
        contenido += "  <form name=\"nuevoExamen\" action=\"nuevoExamen.jsp?paso=2\" method=\"POST\" accept-charset=\"utf-8\">";
        contenido += "      <h3 class=\"margen\">Datos del examen:</h3>";
        contenido += "      <div class=\"borde\">";
        contenido += "          <div class=\"margen\">Nombre del examen: <input type=\"text\" name=\"nombre\" style=\"width: 400px\" /></div>";
        contenido += "          <div class=\"margen\">Fecha: <input type=\"date\" name=\"fecha\" style=\"width: 11em\"/> en texto: <input type=\"text\" name=\"fechaTexto\" style=\"width: 300px\" /></div>";
        contenido += "          <div class=\"margen\">Cantidad de preguntas: <input type=\"number\" name=\"preguntas\" min=\"1\" max=\"999\" /></div>";
        contenido += "          <div class=\"margen\">Cantidad de modalidades: <input type=\"number\" name=\"modalidades\" min=\"1\" max=\"20\" /></div>";
        contenido += "          <div class=\"margen\">Duración: <input type=\"time\" name=\"duracion\"> en texto: <input type=\"text\" name=\"duracionTexto\" style=\"width: 300px\" /></div>";
        contenido += "      </div>";
        contenido += "      <br />";
        contenido += "      <div id=\"error\"></div>";
        contenido += "      <br />";
        contenido += "      <div id=\"boton_pregunta\" onClick=\"validarComenzarExamen()\">Comenzar</div>";
        contenido += "  </form>";
        contenido += "</div>";
    }
    //Paso 2
    else if (request.getParameter("paso").equals("2") && (request.getParameter("nombre") != null))
    {
        
        //Primero recogemos los valores del formulario anterior
        AsignaturaEN asignaturaEN = (AsignaturaEN)session.getAttribute("asignatura");
        PreparacionExamenIdiomaEN preparacionEN;
        ExamenEN examenEN;
        examenEN = new ExamenEN();
        preparacionEN = new PreparacionExamenIdiomaEN();
        ConfiguracionAsignaturaIdiomaCEN conf = new ConfiguracionAsignaturaIdiomaCEN();

        //Recogemos los datos del examen
        examenEN.setAnalizado(false);
        examenEN.setAsignatura(asignaturaEN);
        examenEN.setDificultad(0.0);
        examenEN.setDiscriminacion(0.0);
        String duracion[] = request.getParameter("duracion").split(":");
        examenEN.setDuracion(Integer.parseInt(duracion[0])*60 + Integer.parseInt(duracion[1]));
        String fechaStr[] = request.getParameter("fecha").split("-");
        Calendar fecha = Calendar.getInstance();
        fecha.set(Integer.parseInt(fechaStr[0]), Integer.parseInt(fechaStr[1])-1, Integer.parseInt(fechaStr[2]));
        examenEN.setFecha(fecha);
        examenEN.setModalidades(Integer.parseInt(request.getParameter("modalidades")));
        Random semilla = new Random();
        examenEN.setSemilla(semilla.nextLong()); 
        int cantPreguntas = Integer.parseInt(request.getParameter("preguntas"));

        //Recogemos los datos de la preparación
        preparacionEN.setExamen(examenEN);
        preparacionEN.setIdioma(conf.getConfiguracionIdiomaPrincipal(asignaturaEN).getIdioma());
        preparacionEN.setTraduccionDuracion(request.getParameter("duracionTexto"));
        preparacionEN.setTraduccionFecha(request.getParameter("fechaTexto"));
        preparacionEN.setTraduccionNombre(request.getParameter("nombre"));
        preparacionEN.setRevisada(true);

        //guardamos el examen
        session.setAttribute("examen", examenEN);
        
        //guardamos la preparación en el idioma
        session.setAttribute("preparacion", preparacionEN);
        
        //guardamos la cantidad de preguntas
        session.setAttribute("cantPreguntas", cantPreguntas);
        
        //guardamos las preguntas que deben aparecer en este examen
        PreguntaCEN preguntaCEN = new PreguntaCEN();
        ArrayList<PreguntaEN> seleccionadas = preguntaCEN.getPreguntasProximoExamenByAsignatura(asignaturaEN);
        session.setAttribute("listaSeleccionadas", seleccionadas);

        //Y ahora programamos la interfaz de seleccion de preguntas
        //Lista de preguntas para seleccionar
        
        preLista1 += "<h2 class=\"margen\">Forzar preguntas para que aparezcan en el examen</h2><br />";
        preLista1 += "<h3 class=\"margen\">Preguntas disponibles:</h3>";
        preLista2 += "<form name=\"formPreguntas\">";
        preLista2 += "<div id=\"listaSeleccionar\">";
        postLista += "</div>";
        
        %>

        <%= preLista1 %>
        
        <jsp:include page="includes/filtroPreguntas.jsp">
            <jsp:param name="modo" value="seleccionarExamen" />
        </jsp:include>
        
        <%= preLista2 %>
        
        <jsp:include page="includes/listadoPreguntas.jsp">
            <jsp:param name="modo" value="seleccionarExamen" />
            <jsp:param name="pagina" value="1" />
        </jsp:include>

        <%= postLista %>

        <%
        //Lista de preguntas selecciondas
        preLista = "<h3 class=\"margen\">Preguntas seleccionadas:</h3>";
        preLista += "<br />";
        preLista += "<div id=\"listaSeleccionadas\">";
        postLista += "</div><br /><br />";
        postLista += "<br /><br />";
        postLista += "</form>";
        %>

        <%= preLista %>

        <jsp:include page="includes/listadoPreguntas.jsp">
            <jsp:param name="modo" value="seleccionadasExamen" />
            <jsp:param name="pagina" value="1" />
            <jsp:param name="max" value="<%= cantPreguntas %>" />
        </jsp:include>

        <%= postLista %>

        <%
    }
    //Paso 3
    else if (request.getParameter("paso").equals("3"))
    {
        int cantPreguntas = (Integer)session.getAttribute("cantPreguntas");
        
        //Y ahora programamos la interfaz de seleccion de temas
        preLista += "<h2 class=\"margen\">Completar el examen con preguntas</h2>";
        preLista += "<br /><div class=\"boton_pregunta\" style=\"float: right; margin-right: 5px\" onClick=\"cancelarExamen() \">Cancelar</div>";
        preLista += "<div class=\"boton_pregunta\" style=\"float: right; margin-right: 5px\" onClick=\"validarSeleccionTemas(" +  cantPreguntas + ")\">Siguiente</div>";
        preLista += "<br /><br />";
        preLista += "<form name=\"selTemas\" action=\"nuevoExamen.jsp?paso=4\" method=\"POST\" accept-charset=\"utf-8\">";
        postLista += "<div class=\"margen\">Preguntas no usadas desde: <input type=\"date\" name=\"fechaMax\" style=\"width: 11em\" />"
                + "<input type=\"button\" onClick=\"actualizarCantidadPreguntasTemas()\" value=\"Actualizar\"/></div>";
        postLista += "<div align=\"right\" style=\"margin-right: 5px\">Preguntas del examen: <b>" + cantPreguntas + "</b></div>";
        postLista += "<div id=\"error\"></div>";
        postLista += "<br />";
        postLista += "<br /><div class=\"boton_pregunta\" style=\"float: right; margin-right: 5px\" onClick=\"cancelarExamen() \">Cancelar</div>";
        postLista += "<div class=\"boton_pregunta\" style=\"float: right; margin-right: 5px\" onClick=\"validarSeleccionTemas(" +  cantPreguntas + ")\">Siguiente</div>";
        postLista += "</form>";
        %>

        <%= preLista %>

        <jsp:include page="includes/listadoTemas.jsp">
            <jsp:param name="modo" value="selExamen" />
        </jsp:include>

        <%= postLista %>

        <%
    }
    //Paso 4
    else if (request.getParameter("paso").equals("4"))
    {
        int cantPreguntas = (Integer)session.getAttribute("cantPreguntas");
        ClasificacionTemaCEN clasificacionTemaCEN = new ClasificacionTemaCEN();
        ArrayList<PreguntaEN> seleccionadas = ((ArrayList<PreguntaEN>)session.getAttribute("listaSeleccionadas"));
        ArrayList<PreguntaEN> generadas = new ArrayList<PreguntaEN>();
        AsignaturaEN asignaturaEN = (AsignaturaEN)session.getAttribute("asignatura");
        TemaCEN temaCEN = new TemaCEN();
                
        //Recogemos la lista de temas y los porcentajes
        int i = 0;
        double porcen, porcenCompletado = 0;
        TemaEN temaEN;
        Calendar fecha = null;
        
        //Si hay fecha limite la recogemos
        if (request.getParameter("fechaMax") != null && request.getParameter("fechaMax") != "")
        {
            String fechaStr[] = request.getParameter("fechaMax").split("-");
            fecha = Calendar.getInstance();
            fecha.set(Integer.parseInt(fechaStr[0]), Integer.parseInt(fechaStr[1]), Integer.parseInt(fechaStr[2]));
        }
        
        try
        {
            while(request.getParameter("codigo" + i) != null)
            {
                if (request.getParameter("incluir" + i) != null)
                {
                    temaEN = temaCEN.getTemaByCodigo(asignaturaEN, Integer.parseInt(request.getParameter("codigo" + i)));
                    porcen = Double.parseDouble(request.getParameter("porcentaje" + i));

                    generadas.addAll(clasificacionTemaCEN.generarPreguntasTemaExamen(cantPreguntas, temaEN, porcen, porcenCompletado, fecha, seleccionadas, generadas));
                    porcenCompletado += porcen;
                }

                i++;
            }

            generadas.addAll(seleccionadas);

            //Una vez generadas las preguntas de los temas pasamos a rellenar las restantes con
            //Preguntas aleatorias teniendo en cuenta el ultimo uso
            ArrayList<PreguntaEN> preguntasRelleno = clasificacionTemaCEN.generarPreguntasRellenar(cantPreguntas, asignaturaEN, fecha, generadas);

            if (preguntasRelleno != null)
            {
                generadas.addAll(preguntasRelleno);
                seleccionadas.clear();
                seleccionadas.addAll(generadas);

                //Y ahora mostramos la interfaz
                preLista1 += "<h2 class=\"margen\">Confirmar las preguntas selecionadas para el examen</h2><br />";
                preLista1 += "<h3 class=\"margen\">Preguntas disponibles:</h3>";
                preLista1 += "<br />";
                preLista2 += "<form name=\"formPreguntas\">";
                preLista2 += "<div id=\"listaSeleccionarConfirmacion\">";
                postLista += "</div>";
                %>

                <%= preLista1 %>

                <jsp:include page="includes/filtroPreguntas.jsp">
                    <jsp:param name="modo" value="seleccionarExamenConfirmacion" />
                </jsp:include>

                <%= preLista2 %>

                <jsp:include page="includes/listadoPreguntas.jsp">
                    <jsp:param name="modo" value="seleccionarExamenConfirmacion" />
                    <jsp:param name="pagina" value="1" />
                </jsp:include>

                <%= postLista %>

                <%
                //Lista de preguntas selecciondas
                preLista = "<h3 class=\"margen\">Preguntas seleccionadas:</h3>";
                preLista += "<br />";
                preLista += "<div id=\"listaSeleccionadasConfirmacion\">";
                postLista += "</div><br /><br />";
                postLista += "<br /><br />";
                postLista += "</form>";
                String fechaMax = request.getParameter("fechaMax");
                %>

                <%= preLista %>

                <jsp:include page="includes/listadoPreguntas.jsp">
                    <jsp:param name="modo" value="seleccionadasExamenConfirmacion" />
                    <jsp:param name="pagina" value="1" />
                    <jsp:param name="fechaMax" value="<%= fechaMax %>"/>
                </jsp:include>

                <%= postLista %>

                <%
            }
            else
            {
                preLista += "<h2 class=\"margen\">No existen preguntas suficientes que cumplan los requisitos establecidos para rellenar el examen."
                        + " Reduce la fecha de último uso o crea un exámen con un número de preguntas inferior.</h2><br />";
                preLista += "<div class=\"boton_pregunta\" style=\"float: right; margin-right: 5px\" onClick=\"location.href='nuevoExamen.jsp?paso=3'\">Volver</div>";
                %>

                <%= preLista %>

                <%
            }
        }
        catch(Exception ex)
        {
            preLista += "<h2 class=\"margen\">Los porcentajes y preguntas obligatorias introducidas hacen que se supere el número de preguntas establecido para el examen."
                        + " Crea un examen con un número superior de preguntas o reduce los porcentajes de los temas a incluir.</h2><br />";
            preLista += "<div class=\"boton_pregunta\" style=\"float: right; margin-right: 5px\" onClick=\"location.href='nuevoExamen.jsp?paso=3'\">Volver</div>";
                
            %>

            <%= preLista %>

            <%
        }
    }
    //Paso 5
    else if (request.getParameter("paso").equals("5"))
    {
        AsignaturaEN asignaturaEN = (AsignaturaEN)session.getAttribute("asignatura");
        ExamenCEN examenCEN = new ExamenCEN();
        ConfiguracionAsignaturaIdiomaCEN configuracionAsignaturaIdiomaCEN = new ConfiguracionAsignaturaIdiomaCEN();
        PreparacionExamenIdiomaCEN preparacionExamenIdiomaCEN = new PreparacionExamenIdiomaCEN();
        ArrayList<ExamenEN> examenes = examenCEN.getExamenesByAsignatura(asignaturaEN);
        ConfiguracionAsignaturaIdiomaEN confPrincipal = configuracionAsignaturaIdiomaCEN.getConfiguracionIdiomaPrincipal(asignaturaEN);
        
        contenido += "<div id=\"caja\">";
        contenido += "<h2 style=\"text-align: center\">Instrucciones</h2>";
        contenido += "<form name=\"insertarInstrucciones\" action=\"guardarExamen.jsp\" method=\"POST\" accept-charset=\"utf-8\">";
        contenido += "<div id=\"keywords_externo\">";
        contenido += "<div id=\"instrucciones\">";
        contenido += "<h3 class=\"margen\">Instrucciones de realización del examen:</h3>";
        contenido += "<div class=\"margen\">Cargar instrucciones desde otro examen:";
        
        //Cargar las instrucciones de otro examen
        if (request.getParameter("instrucciones") != null)
        {
            
            TraduccionInstruccionCEN traduccionInstruccionCEN = new TraduccionInstruccionCEN();
            ExamenEN examenEN = examenCEN.getExamenByCodigo(asignaturaEN, Integer.parseInt(request.getParameter("instrucciones")));
            ArrayList<TraduccionInstruccionEN> instrucciones = traduccionInstruccionCEN.getTraduccionesInstruccionByExamenEIdioma(examenEN, confPrincipal.getIdioma());
            
            contenido += " <select style=\"min-width: 200px; max-width: 400px\" name=\"examenes\" onChange=\"seleccionarInstruccionesExamen()\">";
            contenido += "<option value=\"-\">-</option>";
            
            String selected = "";
            for (ExamenEN examen: examenes)
            {
                selected = "";
                
                if (examen.getCodigo() == examenEN.getCodigo())
                    selected = "selected";
                
                contenido += "<option value=\"" + examen.getCodigo() + "\" " + selected + ">";
                contenido += Utiles.rellenaConCeros(String.valueOf(examen.getFecha().get(Calendar.DAY_OF_MONTH)), 2)
                        + "/" + Utiles.rellenaConCeros(String.valueOf((examen.getFecha().get(Calendar.MONTH) + 1)), 2)
                        + "/" + examen.getFecha().get(Calendar.YEAR) + " - ";
                contenido += preparacionExamenIdiomaCEN.getPreparacionExamenByExamenEIdioma(examen, confPrincipal.getIdioma()).getTraduccionNombre();
                contenido += "</option>";
            }
            contenido += "</select><br />";
            
            contenido += "</div><br />";
            
            if (instrucciones.size() == 0)
            {
                contenido += "<div class=\"margen\">Instrucción 1:&nbsp;&nbsp;";
                contenido += "<textarea style=\"vertical-align: top\" name=\"inst1\" cols=\"70\" rows=\"3\"></textarea>";
                contenido += "</div>";
            }
            else
            {
                out.println("<script language=\"JavaScript\" type=\"text/javascript\">actualizarContadorInstrucciones(" + instrucciones.size() + ");</script>");
                for (int i = 0; i < instrucciones.size(); i++)
                {
                    contenido += "<div class=\"margen\">Instrucción " + (i+1) + ":&nbsp;&nbsp;";
                    contenido += "<textarea style=\"vertical-align: top\" name=\"inst" + (i+1) + "\" cols=\"70\" rows=\"3\">"
                            + Utiles.textoHTML(instrucciones.get(i).getEnunciado()) + "</textarea>";
                    contenido += "</div>";
                }
            }

        }
        else
        {
            contenido += " <select style=\"min-width: 250px; max-width: 400px\" name=\"examenes\" onChange=\"seleccionarInstruccionesExamen()\">";
            contenido += "<option value=\"-\" selected>-</option>";
            for (ExamenEN examen: examenes)
            {
                contenido += "<option value=\"" + examen.getCodigo() + "\">";
                contenido += Utiles.rellenaConCeros(String.valueOf(examen.getFecha().get(Calendar.DAY_OF_MONTH)), 2)
                        + "/" + Utiles.rellenaConCeros(String.valueOf((examen.getFecha().get(Calendar.MONTH) + 1)), 2)
                        + "/" + examen.getFecha().get(Calendar.YEAR) + " - ";
                contenido += preparacionExamenIdiomaCEN.getPreparacionExamenByExamenEIdioma(examen, confPrincipal.getIdioma()).getTraduccionNombre();
                contenido += "</option>";
            }
            contenido += "</select></div><br />";
            
            contenido += "<div class=\"margen\">Instrucción 1:&nbsp;&nbsp;";
            contenido += "<textarea style=\"vertical-align: top\" name=\"inst1\" cols=\"70\" rows=\"3\"></textarea>";
            contenido += "</div>";
        }
        
        contenido += "</div><input class=\"margen\" type=\"button\" onClick=\"anyadirInstruccion()\" value=\"Añadir Instrucción\">";
        contenido += "</div>";
        contenido += "<br />";
        contenido += "<div id=\"error\"></div>";
        contenido += "<br />";
        contenido += "<div id=\"boton_examen\" onClick=\"validarInstrucciones()\">Finalizar Examen</div>";
        contenido += "</form>";
        contenido += "</div>";
    }
    else
    {
        response.sendRedirect("nuevoExamen.jsp?paso=1");
    }
%>
            <%= contenido %>
        </div>
    </body>
</html>