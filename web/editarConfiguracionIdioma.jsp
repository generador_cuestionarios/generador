<%-- 
    Copyright (C) 2014 Raúl Sempere Trujillo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Document   : editarConfiguracionIdioma
    Created on : 26-jul-2014, 17:25:51
    Author     : Raúl Sempere Trujillo
--%>
 
<%-- 
    Muestra el formulario con la configuración almacenada para un idioma en
    la asignatura permitiendo editarla.
--%>

<%@page import="clases_generador.ConfigManager"%>
<%@page import="EN.IdiomaEN"%>
<%@page import="CEN.IdiomaCEN"%>
<%@page import="clases_generador.Traduccion"%>
<%@page import="java.util.ArrayList"%>
<%@page import="EN.AsignaturaEN"%>
<%@page import="EN.AccesoUsuarioAsignaturaEN"%>
<%@page import="CEN.AccesoUsuarioAsignaturaCEN"%>
<%@page contentType="text/html" pageEncoding="UTF-8" session="true" %>

<% String titulo = "Idiomas - Generador Inteligente de Cuestinarios"; %>

<%
    request.setCharacterEncoding("UTF-8");
    String opciones = "";
    String idioma = "";
    String nombreTraducido = "";
    String action = "";
    
    if (session.getAttribute("usuario") == null)
    {
       response.sendRedirect("login.jsp");
    }
    else if (session.getAttribute("asignatura") == null)
    {
       response.sendRedirect("asignaturas.jsp");
    }
    else
    {
        AccesoUsuarioAsignaturaCEN accesoUsuarioAsignaturaCEN = new AccesoUsuarioAsignaturaCEN();
        AccesoUsuarioAsignaturaEN accesoUsuarioAsignaturaEN = accesoUsuarioAsignaturaCEN.getAccesoByAsignaturaYUsuario((AsignaturaEN)session.getAttribute("asignatura"), (UsuarioEN)session.getAttribute("usuario"));
        ConfiguracionAsignaturaIdiomaCEN configuracionAsignaturaIdiomaCEN = new ConfiguracionAsignaturaIdiomaCEN();
        
        if (!accesoUsuarioAsignaturaEN.isAdministrador())
            response.sendRedirect("index.jsp");
        else
        {
            AsignaturaEN asignaturaEN = (AsignaturaEN)session.getAttribute("asignatura");
            IdiomaCEN idiomaCEN = new IdiomaCEN();
            IdiomaEN idiomaEN = idiomaCEN.getIdiomaByCodigo(Integer.parseInt(request.getParameter("idioma")));
            idioma = idiomaEN.getNombre();
            ConfiguracionAsignaturaIdiomaEN conf = configuracionAsignaturaIdiomaCEN.getConfiguracionAsignaturaIdiomaByAsignaturaEIdioma(asignaturaEN, idiomaEN);;
            action = "editarConfiguracionIdioma.jsp?idioma=" + request.getParameter("idioma");
            
            //Si venimos de un submit editamos la configuración
            if (request.getParameter("nombreAsignatura") != null)
            {
                conf.setAsignatura(asignaturaEN);
                conf.setIdioma(idiomaEN);
                conf.setTraductorApertium(request.getParameter("traductor"));
                conf.setNombreAsignatura(request.getParameter("nombreAsignatura"));
                
                configuracionAsignaturaIdiomaCEN.Modify(conf);
                
                response.sendRedirect("idiomas.jsp");
            }
            
            
            //Cargamos los datos del formulario
            Traduccion traductor = new Traduccion(ConfigManager.getApertiumPackages());
            String selected;
            
            for (int i = 0; i < traductor.getTraducciones().length; i++)
            {
                selected = "";
                
                if (conf.getTraductorApertium().equals(traductor.getTraducciones()[i].toString()))
                    selected = "selected";
                
                opciones += "<option value=\"" + traductor.getTraducciones()[i].toString() + "\"" + selected + ">" + traductor.getTraducciones()[i].toString() + "</option>";
            }
            
            nombreTraducido = conf.getNombreAsignatura();
            
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><%= titulo %></title>
        <link rel="stylesheet" href="styles/main-stylesheet.css" />
        <link rel="stylesheet" href="styles/menu.css" />
        <script type="text/javascript" src="script/javascript.js"></script>
    </head>
    <body>
        <%@include file="includes/menu.jsp"%>
        <div id="contenido">
            <div id="caja">
                <h2 style="text-align: center">Editar la configuración del "<%=idioma%>"</h2>
                <div class="borde">
                    <br />
                    <form name="idioma" action="<%=action%>" method="POST" accept-charset="utf-8">
                        <div class="margen">Traductor: (que traduzca del idioma principal al nuevo)</div>
                        <div class="margen">
                            <select name="traductor" style="width:40em">
                                <%=opciones%>
                            </select>
                        </div>
                        <div class="margen">Nombre de la asignatura: (traducido en el nuevo idioma)</div>
                        <div class="margen"><input style="width: 40em" type="text" name="nombreAsignatura" value="<%=nombreTraducido%>"/></div>
                        <div class="boton_tema" style="float: right; margin-right: 10px" onClick="location.href='idiomas.jsp'">Volver</div>
                        <div class="boton_tema" style="float: right; margin-right: 10px" onClick="agregarIdioma()">Guardar</div>
                        <br/>
                        <div class="margen" id="error"></div>
                        <br />
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
<%
        }
    }
%>