<%-- 
    Copyright (C) 2014 Raúl Sempere Trujillo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Document   : subirXML
    Created on : 02-abr-2014, 18:42:00
    Author     : Raúl Sempere Trujillo
--%>
 
<%-- 
    Este documento permite la subida del fichero XML para que pueda ser
    accedido por la aplicación
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" session="true" %>

<% String titulo = "Cargar Preguntas desde XML - Generador Inteligente de Cuestinarios"; %>

<%
    if (session.getAttribute("usuario") == null)
    {
       response.sendRedirect("login.jsp");
    }
    else if (session.getAttribute("asignatura") == null)
    {
       response.sendRedirect("asignaturas.jsp");
    }
    else
    {
%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><%= titulo %></title>
        <link rel="stylesheet" href="styles/main-stylesheet.css" />
        <link rel="stylesheet" href="styles/menu.css" />
        <script type="text/javascript" src="script/javascript.js"></script>
    </head>
    <body>
        <%@include file="includes/menu.jsp"%>
        <div id="contenido">
            <br />
            <div id="caja">
                <h2 style="text-align: center">Cargar preguntas desde un fichero XML</h2><br />
                Se comprobará la sintaxis del fichero XML y se examinará
                en busca de preguntas.<br /><br />
                <form name="xml" action="examinarXML.jsp" method="POST" enctype="multipart/form-data">
                    <input type="file" name="browser" accept="text/xml" style="width: 500px"/>
                    <br /><br />
                    <div id="error"></div>
                    <br />
                    <div class="boton_tema" style="float: right; margin-right: 2px" onClick="location.href='verPreguntas.jsp'">Volver</div>
                    <div class="boton_tema" style="float: right; margin-right: 10px" onClick="validarXML()">Examinar</div>
                    <br /><br />
                </form>
            </div>
        </div>
    </body>
</html>
<%
    }
%>