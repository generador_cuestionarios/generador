<%-- 
    Copyright (C) 2014 Raúl Sempere Trujillo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Document   : index
    Created on : 15-abr-2014, 00:46:00
    Author     : Raúl Sempere Trujillo
--%>

<%@page import="clases_generador.ConfigManager"%>
<%-- 
    Este documento es el indice de la herramienta, donde se muestra cierta
    información de la asignatura seleccionada.
--%>
 
<%@page import="EN.ExamenEN"%>
<%@page import="EN.TemaEN"%>
<%@page import="CEN.ClasificacionTemaCEN"%>
<%@page import="CEN.AsignaturaCEN"%>
<%@page import="CEN.PreguntaCEN"%>
<%@page import="EN.AsignaturaEN"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8" session="true" %>

<% String titulo = "Home - Generador Inteligente de Cuestinarios"; %>

<%  
    String contenido = "";
    
    if (session.getAttribute("usuario") == null)
    {
       response.sendRedirect("login.jsp");
    }
    else if (session.getAttribute("asignatura") == null)
    {
       response.sendRedirect("asignaturas.jsp");
    }
    else
    {
        AsignaturaEN asignaturaEN = (AsignaturaEN)session.getAttribute("asignatura");
        AsignaturaCEN asignaturaCEN = new AsignaturaCEN();
        ConfiguracionAsignaturaIdiomaCEN configuracionAsignaturaIdiomaCEN = new ConfiguracionAsignaturaIdiomaCEN();
        ConfiguracionAsignaturaIdiomaEN confPrincipal = configuracionAsignaturaIdiomaCEN.getConfiguracionIdiomaPrincipal(asignaturaEN);
        ClasificacionTemaCEN clasificacionTemaCEN = new ClasificacionTemaCEN();
        asignaturaCEN.rellenarPreguntas(asignaturaEN);
        asignaturaCEN.rellenarConfiguracionesIdioma(asignaturaEN);
        asignaturaCEN.rellenarExamenes(asignaturaEN);
        asignaturaCEN.rellenarTemas(asignaturaEN);
        ArrayList<ConfiguracionAsignaturaIdiomaEN> confs = configuracionAsignaturaIdiomaCEN.getConfiguracionesAsignaturaIdiomaByAsignatura(asignaturaEN);

        contenido += "<div id=\"caja_corta\" style=\"text-align: center\">";
        contenido += "<h1 style=\"text-align: center\">" + confPrincipal.getNombreAsignatura() + "</h1><br />";
        contenido += "<div class=\"borde_corto\">";
        contenido += "<h2 class=\"margen\">Idiomas de la asignatura:</h2>";
        contenido +="<div style=\"text-align: left\" class=\"margen_doble\">" + confPrincipal.getIdioma().getNombre() + " (principal)</div>";
        for (ConfiguracionAsignaturaIdiomaEN conf: confs)
        {
            if (!conf.isIdiomaPrincipal())
                contenido +="<div style=\"text-align: left\" class=\"margen_doble\">" + conf.getIdioma().getNombre() + "</div>";
        }
        contenido += "</div><br />";
        
        contenido += "<div class=\"borde_corto\">";
        contenido += "<h2 class=\"margen\">Temas:</h2>";
        contenido += "<div style=\"text-align: left\" class=\"margen_doble\">Total temas: <b>" + asignaturaEN.getTemas().size() + "</b></div>";
        contenido += "</div><br />";
        
        contenido += "<div class=\"borde_corto\">";
        contenido += "<h2 class=\"margen\">Preguntas:</h2>";
        contenido += "<div style=\"text-align: left\" class=\"margen_doble\">Total preguntas: <b>" + asignaturaEN.getPreguntas().size() + "</b></div>";
        contenido += "<div style=\"text-align: left\" class=\"margen_doble\">Total clasificadas: <b>" + clasificacionTemaCEN.getCantClasificacionesByAsignatura(asignaturaEN) + "</b></div>";
        contenido += "</div><br />";
        
        contenido += "<div class=\"borde_corto\">";
        contenido += "<h2 class=\"margen\">Examenes:</h2>";
        contenido += "<div style=\"text-align: left\" class=\"margen_doble\">Total exámenes: <b>" + asignaturaEN.getExamenes().size() + "</b></div>";
        int analizados = 0;
        for (ExamenEN examenEN : asignaturaEN.getExamenes())
        {
            if (examenEN.isAnalizado())
                analizados++;
        }
        contenido += "<div style=\"text-align: left\" class=\"margen_doble\">Total analizados: <b>" + analizados + "</b></div>";
        contenido += "</div><br />";
        contenido += "</div>";
    }
        
%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><%= titulo %></title>
        <link rel="stylesheet" href="styles/main-stylesheet.css" />
        <link rel="stylesheet" href="styles/menu.css" />
        <script type="text/javascript" src="script/javascript.js"></script>
    </head>
    <body>
        <%@include file="includes/menu.jsp"%>
        <div id="contenido">
            <%= contenido %>
        </div>
    </body>
</html>