<%-- 
    Copyright (C) 2014 Raúl Sempere Trujillo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Document   : verTraduccionesExamen
    Created on : 11-jul-2014, 15:56:00
    Author     : Raúl Sempere Trujillo
--%>
 
<%-- 
    Este documento muestra el listado de traducciones de un examen
--%>

<%@page import="CEN.InstruccionCEN"%>
<%@page import="EN.ExamenPreguntaEN"%>
<%@page import="CEN.ExamenPreguntaCEN"%>
<%@page import="java.util.ArrayList"%>
<%@page import="EN.TraduccionInstruccionEN"%>
<%@page import="CEN.TraduccionInstruccionCEN"%>
<%@page import="EN.InstruccionEN"%>
<%@page import="clases_generador.Utiles"%>
<%@page import="java.util.Calendar"%>
<%@page import="EN.PreparacionExamenIdiomaEN"%>
<%@page import="CEN.PreparacionExamenIdiomaCEN"%>
<%@page import="java.io.OutputStreamWriter"%>
<%@page import="java.io.BufferedWriter"%>
<%@page import="java.io.FileOutputStream"%>
<%@page import="java.io.PrintWriter"%>
<%@page import="java.io.FileWriter"%>
<%@page import="EN.IdiomaEN"%>
<%@page import="CEN.ConfiguracionAsignaturaIdiomaCEN"%>
<%@page import="clases_generador.ExamenLaTeX"%>
<%@page import="EN.AsignaturaEN"%>
<%@page import="CEN.ExamenCEN"%>
<%@page import="EN.ExamenEN"%>

<% String titulo = "Traducciones del examen - Generador Inteligente de Cuestinarios"; %>

<%
    request.setCharacterEncoding("UTF-8");

    String codigoExamen = "";
    
    if (session.getAttribute("usuario") == null)
    {
       response.sendRedirect("login.jsp");
    }
    else if (session.getAttribute("asignatura") == null)
    {
       response.sendRedirect("asignaturas.jsp");
    }
    else if (request.getParameter("examen") != null)     
    {
        codigoExamen = request.getParameter("examen");
    }
    else
        response.sendRedirect("verExamenes.jsp");
    
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><%= titulo %></title>
        <link rel="stylesheet" href="styles/main-stylesheet.css" />
        <link rel="stylesheet" href="styles/menu.css" /> 
        <script type="text/javascript" src="script/javascript.js"></script>
    </head>
    <body>
        <%@include file="includes/menu.jsp"%>
        <div id="contenido">              
            <jsp:include page="includes/listadoPreparaciones.jsp">
                <jsp:param name="examen" value="<%= codigoExamen %>" />
            </jsp:include>
        </div>
    </body>
</html>
