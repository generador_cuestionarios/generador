<%-- 
    Copyright (C) 2014 Raúl Sempere Trujillo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Document   : verPreguntas
    Created on : 15-abr-2014, 00:46:00
    Author     : Raúl Sempere Trujillo
--%>

<%-- 
    Este documento muestra el listado de preguntas
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" session="true" %>

<% String titulo = "Preguntas - Generador Inteligente de Cuestinarios"; %>

<%
    if (session.getAttribute("usuario") == null)
    {
       response.sendRedirect("login.jsp");
    }
    else if (session.getAttribute("asignatura") == null)
    {
       response.sendRedirect("asignaturas.jsp");
    }
    else
    {
        if (session.getAttribute("filtro") != null)
            session.removeAttribute("filtro");
%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><%= titulo %></title>
        <link rel="stylesheet" href="styles/main-stylesheet.css" />
        <link rel="stylesheet" href="styles/menu.css" />
        <script type="text/javascript" src="script/javascript.js"></script>
    </head>
    <body>
        <%@include file="includes/menu.jsp"%>
        <div id="contenido" >
            <h2 style="text-align: center">Listado de preguntas</h2>
                <jsp:include page="includes/filtroPreguntas.jsp">
                    <jsp:param name="modo" value="ver" />
                </jsp:include> 
            <form name="formPreguntas">
            <div id="ver">
               
                    <jsp:include page="includes/listadoPreguntas.jsp">
                        <jsp:param name="modo" value="ver" />
                        <jsp:param name="pagina" value="1" />
                    </jsp:include>  
            </div>
            </form>
        </div>
        <div id="tooltip"></div>
    </body>
</html>
<%
    }
%>