<%-- 
    Copyright (C) 2014 Raúl Sempere Trujillo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Document   : asignaturas
    Created on : 19-abr-2014, 15:48:00
    Author     : Raúl Sempere Trujillo
--%>

<%-- 
    Este documento se emplea cambiar de una asignatura a otra
    dentro de la aplicación
--%>

<%@page import="clases_generador.Traduccion"%>
<%@page import="CEN.*"%>
<%@page import="EN.*"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8" session="true" %>

<% String titulo = "Selección de asignatura - Generador Inteligente de Cuestinarios"; %>

<%
    String contenido = "";
    UsuarioEN usuarioEN;
    UsuarioCEN usuarioCEN = null;

    contenido = "<div class=\"margen\" style=\"color: red\"> - No se ha seleccionado ninguna asignatura.</div>";
    
    //Si no hay usuario se vuelve al login
    if (session.getAttribute("usuario") == null)
    {
       response.sendRedirect("login.jsp");
    }
    //Si hay usuario pero no asignatura se continua para que la seleccione
    else
    {
        usuarioCEN = new UsuarioCEN();
        usuarioEN = (UsuarioEN)session.getAttribute("usuario");
        usuarioCEN.rellenarAccesos(usuarioEN);   
        
        //Si se seleccionó ya una asignatura
        if (request.getParameter("asignatura")!= null)
        {         
            //comprobamos si realmente el usuario tiene permisos en la asignatura
            //y la aplicamos
            for (int i = 0; i < usuarioEN.getAccesosUsuarioAsignatura().size(); i++)
            {
                if (usuarioEN.getAccesosUsuarioAsignatura().get(i).getAsignatura().getCodigo() == Integer.parseInt(request.getParameter("asignatura")))
                    session.setAttribute("asignatura", usuarioEN.getAccesosUsuarioAsignatura().get(i).getAsignatura());
            }
            
            response.sendRedirect("index.jsp");
        }
    }
        
%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><%= titulo %></title>
        <link rel="stylesheet" href="styles/main-stylesheet.css" />
        <link rel="stylesheet" href="styles/menu.css" /> 
    </head>
    <body>
        <%@include file="includes/menu.jsp"%>
        <div id="contenido">
            <%=contenido%>
        </div>
    </body>
</html>