<%-- 
    Copyright (C) 2014 Ra�l Sempere Trujillo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Document   : listadoPreparaciones
    Created on : 11-jul-2014, 15:56:00
    Author     : Ra�l Sempere Trujillo
--%>

<%-- 
    Este documento se emplea para el listado de preparaciones de un examen
    dentro de la opci�n traducciones de un examen.
--%>

<%@page import="EN.TraduccionPreguntaEN"%>
<%@page import="CEN.TraduccionPreguntaCEN"%>
<%@page import="CEN.ExamenPreguntaCEN"%>
<%@page import="EN.ExamenPreguntaEN"%>
<%@page import="clases_generador.Utiles"%>
<%@page import="java.util.Calendar"%>
<%@page import="EN.PreparacionExamenIdiomaEN"%>
<%@page import="CEN.PreparacionExamenIdiomaCEN"%>
<%@page import="EN.IdiomaEN"%>
<%@page import="EN.ConfiguracionAsignaturaIdiomaEN"%>
<%@page import="CEN.ConfiguracionAsignaturaIdiomaCEN"%>
<%@page import="EN.ExamenEN"%>
<%@page import="CEN.ExamenCEN"%>
<%@page import="java.util.ArrayList"%>
<%@page import="EN.AsignaturaEN"%>

<%
    String contenido = "";
    
    AsignaturaEN asignaturaEN = (AsignaturaEN)session.getAttribute("asignatura");
    ExamenCEN examenCEN = new ExamenCEN();
    ExamenPreguntaCEN examenPreguntaCEN = new ExamenPreguntaCEN();
    ConfiguracionAsignaturaIdiomaCEN configuracionAsignaturaIdiomaCEN = new ConfiguracionAsignaturaIdiomaCEN();
    PreparacionExamenIdiomaCEN preparacionExamenIdiomaCEN = new PreparacionExamenIdiomaCEN();
    ConfiguracionAsignaturaIdiomaEN conf = configuracionAsignaturaIdiomaCEN.getConfiguracionIdiomaPrincipal(asignaturaEN);
    IdiomaEN idiomaPrincipal = conf.getIdioma();
    
    
    if (request.getParameter("examen") != null)
    {
        ExamenEN examenEN = examenCEN.getExamenByCodigo(asignaturaEN, Integer.parseInt((request.getParameter("examen"))));
        ArrayList<PreparacionExamenIdiomaEN> preparaciones = preparacionExamenIdiomaCEN.getPreparacionesDisponiblesByExamen(examenEN);
        PreparacionExamenIdiomaEN preparacionPrincipal = preparacionExamenIdiomaCEN.getPreparacionExamenByExamenEIdioma(examenEN, idiomaPrincipal);
        ArrayList<ExamenPreguntaEN> preguntasExamen;
        
        
        contenido += "<div id=\"caja\">";
        contenido += "<h2 style=\"text-align: center\">Traducciones de \"" + preparacionPrincipal.getTraduccionNombre() + "\"</h2>";
        contenido += "<table id=\"preparaciones\" style=\"text-align: center\">";
        contenido += "<tr><th style=\"width: 60px\">Eliminar</th><th>Idioma</th><th style=\"width: 80px\">Traducci�n Revisada</th><th style=\"width: 80px\">Preguntas Revisadas</th><th style=\"width: 60px\">Generar LaTeX</th><th style=\"width: 60px\">Generar PDF</th></tr>";

        for (PreparacionExamenIdiomaEN preparacionEN : preparaciones)
        {
            contenido += "<tr>";
            if (preparacionPrincipal.getIdioma().getCodigo() == preparacionEN.getIdioma().getCodigo())
                contenido += "<td style=\"text-align: center\"><img class=\"add\" src=\"images/borrar_tachado.png\" alt=\"eliminar\" onClick=\"alert('Esta traducci�n no se puede eliminar.')\" /></td>";
            else
                contenido += "<td style=\"text-align: center\"><img class=\"add\" src=\"images/borrar.png\" alt=\"eliminar\" onClick=\"eliminarPreparacionExamen(" + preparacionEN.getExamen().getCodigo() + ", " + preparacionEN.getIdioma().getCodigo() + ")\" /></td>";

            contenido += "<td>" + preparacionEN.getIdioma().getNombre() + "</td>";
            
            preguntasExamen = examenPreguntaCEN.getExamenesPreguntaSinRevisarByExamenEIdioma(examenEN, preparacionEN.getIdioma());
 
            if (preparacionPrincipal.getIdioma().getCodigo() == preparacionEN.getIdioma().getCodigo())
            {
                contenido += "<td style=\"text-align: center\"><img class=\"preparado\" src=\"images/preparacion_ok.png\" alt=\"preparacion ok\" /></td>";
            }
            else if (preparacionEN.isRevisada())
                contenido += "<td style=\"text-align: center\"><img class=\"add\" src=\"images/preparacion_ok.png\" alt=\"preparacion ok\" onClick=\"document.location='detallePreparacionExamen.jsp?examen=" + examenEN.getCodigo() + "&idioma=" + preparacionEN.getIdioma().getCodigo() + "'\" /></td>";
            else
                contenido += "<td style=\"text-align: center\"><img class=\"add\" src=\"images/preparacion_error.png\" alt=\"preparacion error\" onClick=\"document.location='detallePreparacionExamen.jsp?examen=" + examenEN.getCodigo() + "&idioma=" + preparacionEN.getIdioma().getCodigo() + "'\" /></td>";
            
            if (preguntasExamen.size() == 0)
                contenido += "<td style=\"text-align: center\"><img class=\"preparado\" src=\"images/preparacion_ok.png\" alt=\"preparacion ok\" /></td>";
            else
                contenido += "<td style=\"text-align: center\"><img class=\"add\" src=\"images/preparacion_error.png\" alt=\"preparacion error\" onClick=\"document.location='revisarTraduccionPregunta.jsp?examen=" + examenEN.getCodigo() + "&idioma=" + preparacionEN.getIdioma().getCodigo() + "'\" /></td>";
            
            if (preparacionEN.isRevisada() && (preguntasExamen.size() == 0))
            {
                contenido += "<td style=\"text-align: center\"><img class=\"add\" src=\"images/tex.png\" alt=\"tex\" onClick=\"generarLatex("+ examenEN.getCodigo() + ", " + preparacionEN.getIdioma().getCodigo() + ")\"></td>";
                contenido += "<td style=\"text-align: center\"><img class=\"add\" src=\"images/pdf.png\" alt=\"pdf\" onClick=\"generarPdf("+ examenEN.getCodigo() + ", " + preparacionEN.getIdioma().getCodigo() + ")\"></td>";
            }
            else
            {
                contenido += "<td style=\"text-align: center\"><img class=\"add\" src=\"images/tex_tachado.png\" alt=\"tex\" onClick=\"alert('Alguna traducci�n necesita ser comprobada.')\"></td>";
                contenido += "<td style=\"text-align: center\"><img class=\"add\" src=\"images/pdf_tachado.png\" alt=\"pdf\" onClick=\"alert('Alguna traducci�n necesita ser comprobada.')\"></td>";
            }
            
            contenido += "</tr>";
        }

        contenido += "</table><br />";
        contenido += "<div class=\"boton_examen\" style=\"float: right; margin-right: 2px\" onClick=\"document.location='verExamenes.jsp'\">Volver</div>";
        contenido += "<div class=\"boton_examen\" style=\"float: right; margin-right: 10px\" onClick=\"document.location='nuevaPreparacionExamen.jsp?examen=" + examenEN.getCodigo() + "'\">Nueva Traducci�n</div>";
        contenido += "<br /></div>";
    }
%>

<%= contenido %>