<%-- 
    Copyright (C) 2014 Ra�l Sempere Trujillo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Document   : listadoPreguntas
    Created on : 20-mar-2014, 13:40:18
    Author     : Ra�l Sempere Trujillo
--%>
 
<%-- 
    Este documento se emplea para mostrar los diferentes listados de preguntas
    que existen en la aplicaci�n (ver preguntas, seleccionar preguntas en el examen,
    lista de preguntas seleccionadas, confirmaci�n de preguntas, edici�n de preguntas
    del examen, etc)
--%>

<%@page import="EN.TemaEN"%>
<%@page import="CEN.TemaCEN"%>
<%@page import="EN.ClasificacionTemaEN"%>
<%@page import="clases_generador.Utiles"%>
<%@page import="EN.ConfiguracionAsignaturaIdiomaEN"%>
<%@page import="EN.TraduccionPreguntaEN"%>
<%@page import="EN.ExamenPreguntaEN"%>
<%@page import="CEN.ExamenCEN"%>
<%@page import="java.util.Calendar"%>
<%@page import="CEN.ClasificacionTemaCEN"%>
<%@page import="CEN.ConfiguracionAsignaturaIdiomaCEN"%>
<%@page import="CEN.TraduccionPreguntaCEN"%>
<%@page import="EN.PreguntaEN"%>
<%@page import="CEN.PreguntaCEN"%>
<%@page import="java.util.ArrayList"%>
<%@page import="EN.AsignaturaEN"%>

<%
    int itemsPorPagina = 10;
    int itemsVer = 10;
    int pagina = Integer.parseInt(request.getParameter("pagina"));
    String colorDeshabilitada = "#FFD0D0"; //Rojo muy claro

    AsignaturaEN asignatura = (AsignaturaEN)session.getAttribute("asignatura");
    PreguntaCEN preguntaCEN = new PreguntaCEN();
    TraduccionPreguntaCEN traduccionPreguntaCEN = new TraduccionPreguntaCEN();
    ConfiguracionAsignaturaIdiomaCEN configuracionAsignaturaIdiomaCEN = new ConfiguracionAsignaturaIdiomaCEN();
    String contenido = "";
    String enunciado = "";
    
    //------------------------------------------------------------------------------------------------------------------------------------------//
    //Listado de selecci�n de preguntas para el examen
    if (request.getParameter("modo").equals("seleccionarExamen"))
    {   
        //Obtenemos las preguntas ya seleccionadas
        ArrayList<PreguntaEN> seleccionadas = ((ArrayList<PreguntaEN>)session.getAttribute("listaSeleccionadas"));
        
        TemaCEN temaCEN = new TemaCEN();
        ArrayList<TemaEN> temas = temaCEN.getTemasByAsignatura(asignatura);

        String filtro = " AND pre_habilitada = true";
        
        if (session.getAttribute("filtro") != null)
            filtro = (String)session.getAttribute("filtro") +  filtro;
        
        ArrayList<PreguntaEN> preguntas = preguntaCEN.getPreguntasByAsignaturaYFiltro(asignatura, filtro);
        
        //Quitamos las preguntas ya seleccionadas de la lista de preguntas para seleccionar
        for (PreguntaEN pregunta : seleccionadas)
        {
            preguntaCEN.borrarDeLista(preguntas, pregunta);
        }
        
        int cantPreguntas = (Integer)session.getAttribute("cantPreguntas");

        contenido += "<div class=\"boton_pregunta\" style=\"float: right; margin-right: 5px\" onClick=\"cancelarExamen() \">Cancelar</div>";
        contenido += "<div class=\"boton_pregunta\" style=\"float: right; margin-right: 5px\" onClick=\"validarSeleccionPreguntas(" + cantPreguntas + ", " + seleccionadas.size() + ") \">Siguiente</div><br /><br />";
        
        contenido += "<table id=\"listado\">";
        contenido += "<tr><th style=\"width: 60px\">A�adir</th><th>Pregunta</th><th>Tema</th>";
        contenido += "<th style=\"width: 40px\">Veces usada</th><th style=\"width: 80px\">�ltimo uso</th><th style=\"width: 60px\">Aciertos</th><th style=\"width:60px\">Fallos</th><th style=\"width: 60px\">Sin contestar</th>";
        contenido += "<th style=\"width: 60px\">�ndice Dificul.</th><th style=\"width: 60px\"> �ndice Discrim.</th></tr>";
        
        //Recorremos los items de la pagina
        for (int i = (pagina - 1) * itemsPorPagina; i < pagina * itemsPorPagina && i < preguntas.size(); i++)
        {
            preguntaCEN.rellenarClasificacion(preguntas.get(i));

            contenido += "<tr>";
            contenido += "<td style=\"text-align: center\"><img class=\"add\" src=\"images/add.png\" alt=\"add\" onClick=\"addPregunta("+ preguntas.get(i).getCodigo() +", 'Examen')\"></td>";

            enunciado = traduccionPreguntaCEN.getTraduccionPreguntaByPreguntaEIdioma(preguntas.get(i), configuracionAsignaturaIdiomaCEN.getConfiguracionIdiomaPrincipal(asignatura).getIdioma()).getEnunciado();

            if (enunciado.length() > 100)
                enunciado = enunciado.substring(0, 100) + "...";
            
            contenido += "<td style=\"cursor:help\" onMouseOver=\"showTooltip(event, " + preguntas.get(i).getCodigo() +")\" onMouseOut=\"hideTooltip()\">" + Utiles.textoHTML(enunciado) + "</td>";

            if (preguntas.get(i).getClasificacion() != null)
            {
                if (preguntas.get(i).getClasificacion().isRevisada())
                {
                    contenido += "<td><img class=\"alert\" src=\"images/ok.png\" alt=\"revisada\" /> ";
                    contenido += preguntas.get(i).getClasificacion().getTema().getNombre() + "</td>";
                }
                else
                {
                    contenido += "<td><img id=\"img" + preguntas.get(i).getCodigo() + "\" class=\"alert\" src=\"images/alert.png\" alt=\"sin revisar\" /> ";
                    contenido += "<a id=\"tema" + preguntas.get(i).getCodigo() + "\"><select name=\"tema" + preguntas.get(i).getCodigo() + "\" name=\"tema" + preguntas.get(i).getCodigo() + "\">";
                    String selected;
                    for (TemaEN tema : temas)
                    {
                        selected = "";

                        if (preguntas.get(i).getClasificacion().getTema().getCodigo() == tema.getCodigo())
                            selected = "selected";

                        contenido += "<option value=\"" + tema.getCodigo() + "\"" + selected + ">";
                        contenido += tema.getNombre();
                        contenido += "</option>";
                    }
                    contenido += "</select></a> <a class=\"link\" id=\"link" + preguntas.get(i).getCodigo() + "\" onClick=\"validarClasificacionListado(" + preguntas.get(i).getCodigo() + ")\">(validar)</a></div></td>";
                }
            }
            else
            {
                contenido += "<td><img id=\"img" + preguntas.get(i).getCodigo() + "\" class=\"alert\" src=\"images/error.png\" alt=\"sin clasificar\" /> ";
                contenido += "<a id=\"tema" + preguntas.get(i).getCodigo() + "\"><select name=\"tema" + preguntas.get(i).getCodigo() + "\" name=\"tema" + preguntas.get(i).getCodigo() + "\">";
                contenido += "<option value=\"-1\" selected>";
                contenido += " Sin clasificar</option>";

                for (TemaEN tema : temas)
                {
                    contenido += "<option value=\"" + tema.getCodigo() + "\">";
                    contenido += tema.getNombre();
                    contenido += "</option>";
                }
                contenido += "</select></a> <a class=\"link\" id=\"link" + preguntas.get(i).getCodigo() + "\" onClick=\"validarClasificacionListado(" + preguntas.get(i).getCodigo() + ")\">(validar)</a></div></td>";
            }

            contenido += "<td>" + preguntas.get(i).getUtilizada() + "</td>";
            
            //La pregunta se ha utilizado alguna vez
            if (preguntas.get(i).getUltimoUso() != null)
            {
                contenido += "<td>" + Utiles.rellenaConCeros(preguntas.get(i).getUltimoUso().get(Calendar.DAY_OF_MONTH), 2) + "/" + Utiles.rellenaConCeros((preguntas.get(i).getUltimoUso().get(Calendar.MONTH)+1), 2) + "/" + preguntas.get(i).getUltimoUso().get(Calendar.YEAR) + "</td>";
            }
            else
                contenido += "<td> - </td>";

            if (preguntas.get(i).getContestada() > 0)
            {     
                contenido += "<td>" + Utiles.redondeo((double)preguntas.get(i).getAciertos()/preguntas.get(i).getContestada()*100, 2) + "%</td>";
                contenido += "<td>" + Utiles.redondeo((double)preguntas.get(i).getFallos()/preguntas.get(i).getContestada()*100, 2) + "%</td>";
                contenido += "<td>" + Utiles.redondeo(((double)preguntas.get(i).getNoContestada()/(preguntas.get(i).getContestada() + preguntas.get(i).getNoContestada()))*100, 2) + "%</td>";
                contenido += "<td>" + preguntas.get(i).getIndiceDificultad() + "</td>";
                contenido += "<td>" + preguntas.get(i).getIndiceDiscriminacion() + "</td>";
            }
            else
            {
                contenido += "<td> - </td>";
                contenido += "<td> - </td>";
                contenido += "<td> - </td>";
                contenido += "<td> - </td>";
                contenido += "<td> - </td>";
            }
            contenido += "</tr>";
        }
        
        contenido += "</table>";
        
        //Escribimos el pi� con las opciones de navegaci�n
        contenido += "<br /><div align=\"right\" style=\"margin-right: 6px\">";
        contenido += "<img class=\"boton\" src=\"images/doble_izquierda.png\" alt=\"primera\" style=\"width: 25px; height: 25px; vertical-align: middle; margin-right: 3px\" onClick=\"primeraPagina('seleccionarExamen')\" />";
        contenido += "<img class=\"boton\" src=\"images/izquierda.png\" alt=\"anterior\" style=\"width: 25px; height: 25px; vertical-align: middle; margin-right: 10px\" onClick=\"anteriorPagina('seleccionarExamen')\" />";
        contenido += "P�gina " + pagina + " de " + (int)Math.ceil(preguntas.size()/(itemsPorPagina * 1.0));
        contenido += "<img class=\"boton\" src=\"images/derecha.png\" alt=\"siguiente\" style=\"width: 25px; height: 25px; vertical-align: middle; margin-left: 10px\" onClick=\"siguientePagina('seleccionarExamen')\" />";
        contenido += "<img class=\"boton\" src=\"images/doble_derecha.png\" alt=\"ultima\" style=\"width: 25px; height: 25px; vertical-align: middle; margin-left: 3px\" onClick=\"ultimaPagina('seleccionarExamen')\" /></div>";
        contenido += "<div align=\"right\" style=\"margin-right: 70px; margin-top: 5px; font-weight: bold;\">Total: " + preguntas.size() + "</div>";
        contenido += "<div id=\"totalSeleccionar\" style=\"display: none\">" + preguntas.size() + "</div>";
    }
    //------------------------------------------------------------------------------------------------------------------------------------------//
    //Listado de preguntas para elegir en la confirmaci�n de preguntas
    else if (request.getParameter("modo").equals("seleccionarExamenConfirmacion"))
    {   
        //Obtenemos las preguntas ya seleccionadas
        ArrayList<PreguntaEN> seleccionadas = ((ArrayList<PreguntaEN>)session.getAttribute("listaSeleccionadas"));
        
        TemaCEN temaCEN = new TemaCEN();
        ArrayList<TemaEN> temas = temaCEN.getTemasByAsignatura(asignatura);

        //Obtenemos las preguntas (solo habilitadas)
        String filtro = " AND pre_habilitada = true";
        
        if (session.getAttribute("filtro") != null)
            filtro = (String)session.getAttribute("filtro") +  filtro;
        
        ArrayList<PreguntaEN> preguntas = preguntaCEN.getPreguntasByAsignaturaYFiltro(asignatura, filtro);
        
        //Quitamos las preguntas ya seleccionadas de la lista de preguntas para seleccionar
        for (PreguntaEN pregunta : seleccionadas)
        {
            preguntaCEN.borrarDeLista(preguntas, pregunta);
        }
        
        contenido += "<div class=\"boton_pregunta\" style=\"float: right; margin-right: 5px\" onClick=\"cancelarExamen() \">Cancelar</div>";
        contenido += "<div class=\"boton_pregunta\" style=\"float: right; margin-right: 5px\" onClick=\"confirmarPreguntas() \">Siguiente</div><br /><br />";
        
        contenido += "<table id=\"listado\">";
        contenido += "<tr><th id=\"thIntercambio\" style=\"display: none; width: 60px\">Sustituir</th><th style=\"width: 40px\">Editar</th><th>Pregunta</th><th>Tema</th>";
        contenido += "<th style=\"width: 40px\">Veces usada</th><th style=\"width: 80px\">�ltimo uso</th><th style=\"width: 60px\">Aciertos</th><th style=\"width:60px\">Fallos</th><th style=\"width: 60px\">Sin contestar</th>";
        contenido += "<th style=\"width: 60px\">�ndice Dificul.</th><th style=\"width: 60px\"> �ndice Discrim.</th></tr>";

        //Recorremos los items de la pagina
        for (int i = (pagina - 1) * itemsPorPagina; i < pagina * itemsPorPagina && i < preguntas.size(); i++)
        {
            preguntaCEN.rellenarClasificacion(preguntas.get(i));

            contenido += "<tr>";
            contenido += "<td class=\"tdIntercambio\" style=\"display: none; text-align: center\"><img class=\"boton alert\" src=\"images/intercambio.png\" alt=\"intercambio\" onClick=\"intercambiar("+ preguntas.get(i).getCodigo() +", 'ExamenConfirmacion')\"></td>";

            contenido += "<td style=\"text-align: center\"><img class=\"boton alert\" src=\"images/edit.png\" alt=\"intercambio\" onClick=\"location.href='detallePregunta.jsp?pregunta=" + preguntas.get(i).getCodigo() + "&examen=true" + "'\"></td>";
            
            enunciado = traduccionPreguntaCEN.getTraduccionPreguntaByPreguntaEIdioma(preguntas.get(i), configuracionAsignaturaIdiomaCEN.getConfiguracionIdiomaPrincipal(asignatura).getIdioma()).getEnunciado();

            if (enunciado.length() > 100)
                enunciado = enunciado.substring(0, 100) + "...";
            
            contenido += "<td style=\"cursor:help\" onMouseOver=\"showTooltip(event, " + preguntas.get(i).getCodigo() +")\" onMouseOut=\"hideTooltip()\">" + Utiles.textoHTML(enunciado) + "</td>";

            if (preguntas.get(i).getClasificacion() != null)
            {
                if (preguntas.get(i).getClasificacion().isRevisada())
                {
                    contenido += "<td><img class=\"alert\" src=\"images/ok.png\" alt=\"revisada\" /> ";
                    contenido += preguntas.get(i).getClasificacion().getTema().getNombre() + "</td>";
                }
                else
                {
                    contenido += "<td><img id=\"img" + preguntas.get(i).getCodigo() + "\" class=\"alert\" src=\"images/alert.png\" alt=\"sin revisar\" /> ";
                    contenido += "<a id=\"tema" + preguntas.get(i).getCodigo() + "\"><select name=\"tema" + preguntas.get(i).getCodigo() + "\" id=\"tema" + preguntas.get(i).getCodigo() + "\" name=\"tema" + preguntas.get(i).getCodigo() + "\">";
                    String selected;
                    for (TemaEN tema : temas)
                    {
                        selected = "";

                        if (preguntas.get(i).getClasificacion().getTema().getCodigo() == tema.getCodigo())
                            selected = "selected";

                        contenido += "<option value=\"" + tema.getCodigo() + "\"" + selected + ">";
                        contenido += tema.getNombre();
                        contenido += "</option>";
                    }
                    contenido += "</select></a> <a class=\"link\" id=\"link" + preguntas.get(i).getCodigo() + "\" onClick=\"validarClasificacionListado(" + preguntas.get(i).getCodigo() + ")\">(validar)</a></div></td>";
                }
            }
            else
            {
                contenido += "<td><img id=\"img" + preguntas.get(i).getCodigo() + "\" class=\"alert\" src=\"images/error.png\" alt=\"sin clasificar\" /> ";
                contenido += "<a id=\"tema" + preguntas.get(i).getCodigo() + "\"><select name=\"tema" + preguntas.get(i).getCodigo() + "\">";
                contenido += "<option value=\"-1\" selected>";
                contenido += " Sin clasificar</option>";

                for (TemaEN tema : temas)
                {
                    contenido += "<option value=\"" + tema.getCodigo() + "\">";
                    contenido += tema.getNombre();
                    contenido += "</option>";
                }
                contenido += "</select></a> <a class=\"link\" id=\"link" + preguntas.get(i).getCodigo() + "\" onClick=\"validarClasificacionListado(" + preguntas.get(i).getCodigo() + ")\">(validar)</a></div></td>";
            }

            contenido += "<td>" + preguntas.get(i).getUtilizada() + "</td>";
            
            //La pregunta se ha utilizado alguna vez
            if (preguntas.get(i).getUltimoUso() != null)
            {
                contenido += "<td>" + Utiles.rellenaConCeros(preguntas.get(i).getUltimoUso().get(Calendar.DAY_OF_MONTH), 2) + "/" + Utiles.rellenaConCeros((preguntas.get(i).getUltimoUso().get(Calendar.MONTH)+1), 2) + "/" + preguntas.get(i).getUltimoUso().get(Calendar.YEAR) + "</td>";
            }
            else
                contenido += "<td> - </td>";

            if (preguntas.get(i).getContestada() > 0)
            {     
                contenido += "<td>" + Utiles.redondeo((double)preguntas.get(i).getAciertos()/preguntas.get(i).getContestada()*100, 2) + "%</td>";
                contenido += "<td>" + Utiles.redondeo((double)preguntas.get(i).getFallos()/preguntas.get(i).getContestada()*100, 2) + "%</td>";
                contenido += "<td>" + Utiles.redondeo(((double)preguntas.get(i).getNoContestada()/(preguntas.get(i).getContestada() + preguntas.get(i).getNoContestada()))*100, 2) + "%</td>";
                contenido += "<td>" + preguntas.get(i).getIndiceDificultad() + "</td>";
                contenido += "<td>" + preguntas.get(i).getIndiceDiscriminacion() + "</td>";
            }
            else
            {
                contenido += "<td> - </td>";
                contenido += "<td> - </td>";
                contenido += "<td> - </td>";
                contenido += "<td> - </td>";
                contenido += "<td> - </td>";
            }
            
            contenido += "</tr>";
        }
        
        contenido += "</table>";
        
        //Escribimos el pi� con las opciones de navegaci�n
        contenido += "<br /><div align=\"right\" style=\"margin-right: 6px\">";
        contenido += "<img class=\"boton\" src=\"images/doble_izquierda.png\" alt=\"primera\" style=\"width: 25px; height: 25px; vertical-align: middle; margin-right: 3px\" onClick=\"primeraPagina('seleccionarExamenConfirmacion')\" />";
        contenido += "<img class=\"boton\" src=\"images/izquierda.png\" alt=\"anterior\" style=\"width: 25px; height: 25px; vertical-align: middle; margin-right: 10px\" onClick=\"anteriorPagina('seleccionarExamenConfirmacion')\" />";
        contenido += "P�gina " + pagina + " de " + (int)Math.ceil(preguntas.size()/(itemsPorPagina * 1.0));
        contenido += "<img class=\"boton\" src=\"images/derecha.png\" alt=\"siguiente\" style=\"width: 25px; height: 25px; vertical-align: middle; margin-left: 10px\" onClick=\"siguientePagina('seleccionarExamenConfirmacion')\" />";
        contenido += "<img class=\"boton\" src=\"images/doble_derecha.png\" alt=\"ultima\" style=\"width: 25px; height: 25px; vertical-align: middle; margin-left: 3px\" onClick=\"ultimaPagina('seleccionarExamenConfirmacion')\" /></div>";
        contenido += "<div align=\"right\" style=\"margin-right: 70px; margin-top: 5px; font-weight: bold;\">Total: " + preguntas.size() + "</div>";
        contenido += "<div id=\"totalSeleccionar\" style=\"display: none\">" + preguntas.size() + "</div>";
    }
    //------------------------------------------------------------------------------------------------------------------------------------------//
    //Listado de preguntas ya seleccionadas para el examen
    else if (request.getParameter("modo").equals("seleccionadasExamen"))
    {
        //Obtenemos las preguntas ya seleccionadas
        ArrayList<PreguntaEN> preguntas = ((ArrayList<PreguntaEN>)session.getAttribute("listaSeleccionadas"));
        
        TemaCEN temaCEN = new TemaCEN();
        ArrayList<TemaEN> temas = temaCEN.getTemasByAsignatura(asignatura);
        
        //Y la cantidad m�xima de preguntas que podemos seleccionar
        int cantPreguntas = (Integer)session.getAttribute("cantPreguntas");

        contenido += "<table id=\"listado\">";
        contenido += "<tr><th style=\"width: 60px\">Eliminar</th><th>Pregunta</th><th>Tema</th>";
        contenido += "<th style=\"width: 40px\">Veces usada</th><th style=\"width: 80px\">�ltimo uso</th><th style=\"width: 60px\">Aciertos</th><th style=\"width:60px\">Fallos</th><th style=\"width: 60px\">Sin contestar</th>";
        contenido += "<th style=\"width: 60px\">�ndice Dificul.</th><th style=\"width: 60px\"> �ndice Discrim.</th></tr>";
        
        //Recorremos las preguntas
        for (int i = (pagina - 1) * itemsPorPagina; i < pagina * itemsPorPagina && i < preguntas.size(); i++)
        {
            preguntaCEN.rellenarClasificacion(preguntas.get(i));

            contenido += "<tr>";
            contenido += "<td style=\"text-align: center\"><img class=\"add\" src=\"images/delete.png\" alt=\"borrar\" onClick=\"delPregunta("+ preguntas.get(i).getCodigo() +", 'Examen')\"></td>";

            enunciado = traduccionPreguntaCEN.getTraduccionPreguntaByPreguntaEIdioma(preguntas.get(i), configuracionAsignaturaIdiomaCEN.getConfiguracionIdiomaPrincipal(asignatura).getIdioma()).getEnunciado();

            if (enunciado.length() > 100)
                enunciado = enunciado.substring(0, 100) + "...";
            
            contenido += "<td style=\"cursor:help\" onMouseOver=\"showTooltip(event, " + preguntas.get(i).getCodigo() +")\" onMouseOut=\"hideTooltip()\">" + Utiles.textoHTML(enunciado) + "</td>";

            if (preguntas.get(i).getClasificacion() != null)
            {
                if (preguntas.get(i).getClasificacion().isRevisada())
                {
                    contenido += "<td><img class=\"alert\" src=\"images/ok.png\" alt=\"revisada\" /> ";
                    contenido += preguntas.get(i).getClasificacion().getTema().getNombre() + "</td>";
                }
                else
                {
                    contenido += "<td><img id=\"img" + preguntas.get(i).getCodigo() + "\" class=\"alert\" src=\"images/alert.png\" alt=\"sin revisar\" /> ";
                    contenido += "<a id=\"tema" + preguntas.get(i).getCodigo() + "\"><select name=\"tema" + preguntas.get(i).getCodigo() + "\">";
                    String selected;
                    for (TemaEN tema : temas)
                    {
                        selected = "";

                        if (preguntas.get(i).getClasificacion().getTema().getCodigo() == tema.getCodigo())
                            selected = "selected";

                        contenido += "<option value=\"" + tema.getCodigo() + "\"" + selected + ">";
                        contenido += tema.getNombre();
                        contenido += "</option>";
                    }
                    contenido += "</select></a> <a class=\"link\" id=\"link" + preguntas.get(i).getCodigo() + "\" onClick=\"validarClasificacionListado(" + preguntas.get(i).getCodigo() + ")\">(validar)</a></div></td>";
                }
            }
            else
            {
                contenido += "<td><img id=\"img" + preguntas.get(i).getCodigo() + "\" class=\"alert\" src=\"images/error.png\" alt=\"sin clasificar\" /> ";
                contenido += "<a id=\"tema" + preguntas.get(i).getCodigo() + "\"><select name=\"tema" + preguntas.get(i).getCodigo() + "\">";
                contenido += "<option value=\"-1\" selected>";
                contenido += " Sin clasificar</option>";

                for (TemaEN tema : temas)
                {
                    contenido += "<option value=\"" + tema.getCodigo() + "\">";
                    contenido += tema.getNombre();
                    contenido += "</option>";
                }
                contenido += "</select></a> <a class=\"link\" id=\"link" + preguntas.get(i).getCodigo() + "\" onClick=\"validarClasificacionListado(" + preguntas.get(i).getCodigo() + ")\">(validar)</a></div></td>";
            }

            contenido += "<td>" + preguntas.get(i).getUtilizada() + "</td>";
            
            //La pregunta se ha utilizado alguna vez
            if (preguntas.get(i).getUltimoUso() != null)
            {
                contenido += "<td>" + Utiles.rellenaConCeros(preguntas.get(i).getUltimoUso().get(Calendar.DAY_OF_MONTH), 2) + "/" + Utiles.rellenaConCeros((preguntas.get(i).getUltimoUso().get(Calendar.MONTH)+1), 2) + "/" + preguntas.get(i).getUltimoUso().get(Calendar.YEAR) + "</td>";
            }
            else
                contenido += "<td> - </td>";

            if (preguntas.get(i).getContestada() > 0)
            {     
                contenido += "<td>" + Utiles.redondeo((double)preguntas.get(i).getAciertos()/preguntas.get(i).getContestada()*100, 2) + "%</td>";
                contenido += "<td>" + Utiles.redondeo((double)preguntas.get(i).getFallos()/preguntas.get(i).getContestada()*100, 2) + "%</td>";
                contenido += "<td>" + Utiles.redondeo(((double)preguntas.get(i).getNoContestada()/(preguntas.get(i).getContestada() + preguntas.get(i).getNoContestada()))*100, 2) + "%</td>";
                contenido += "<td>" + preguntas.get(i).getIndiceDificultad() + "</td>";
                contenido += "<td>" + preguntas.get(i).getIndiceDiscriminacion() + "</td>";
            }
            else
            {
                contenido += "<td> - </td>";
                contenido += "<td> - </td>";
                contenido += "<td> - </td>";
                contenido += "<td> - </td>";
                contenido += "<td> - </td>";
            }
            contenido += "</tr>";
        }

        contenido += "</table>";

        //Escribimos el pi� con las opciones de navegaci�n
        contenido += "<br /><div align=\"right\" style=\"margin-right: 6px\">";
        contenido += "<img class=\"boton\" src=\"images/doble_izquierda.png\" alt=\"primera\" style=\"width: 25px; height: 25px; vertical-align: middle; margin-right: 3px\" onClick=\"primeraPagina('seleccionadasExamen')\" />";
        contenido += "<img class=\"boton\" src=\"images/izquierda.png\" alt=\"anterior\" style=\"width: 25px; height: 25px; vertical-align: middle; margin-right: 10px\" onClick=\"anteriorPagina('seleccionadasExamen')\" />";
        contenido += "P�gina " + pagina + " de " + (int)Math.ceil(preguntas.size()/(itemsPorPagina * 1.0));
        contenido += "<img class=\"boton\" src=\"images/derecha.png\" alt=\"siguiente\" style=\"width: 25px; height: 25px; vertical-align: middle; margin-left: 10px\" onClick=\"siguientePagina('seleccionadasExamen')\" />";
        contenido += "<img class=\"boton\" src=\"images/doble_derecha.png\" alt=\"ultima\" style=\"width: 25px; height: 25px; vertical-align: middle; margin-left: 3px\" onClick=\"ultimaPagina('seleccionadasExamen')\" /></div>";
        contenido += "<div align=\"right\" style=\"margin-right: 70px; margin-top: 5px; font-weight: bold;\">Total: " + preguntas.size() + "</div>";
        contenido += "<div id=\"totalSeleccionadas\" style=\"display: none\">" + preguntas.size() + "</div>";
        contenido += "<div id=\"error\"></div>";
        contenido += "<br /><div class=\"boton_pregunta\" style=\"float: right; margin-right: 5px\" onClick=\"cancelarExamen() \">Cancelar</div>";
        contenido += "<div class=\"boton_pregunta\" style=\"float: right; margin-right: 5px\"onClick=\"validarSeleccionPreguntas(" + cantPreguntas + ", " + preguntas.size() + ") \">Siguiente</div>";
    }
    //------------------------------------------------------------------------------------------------------------------------------------------//
    //Listado de preguntas seleccionadas para el examen durante la confirmaci�n
    else if (request.getParameter("modo").equals("seleccionadasExamenConfirmacion"))
    {
        double dificultad = 0;
        double discriminacion = 0;
        
        //Obtenemos las preguntas ya seleccionadas
        ArrayList<PreguntaEN> preguntas = ((ArrayList<PreguntaEN>)session.getAttribute("listaSeleccionadas"));
        TemaCEN temaCEN = new TemaCEN();
        ArrayList<TemaEN> temas = temaCEN.getTemasByAsignatura(asignatura);
        
        //Calculamos la discriminaci�n y dificultad actual del examen
        for (PreguntaEN pregunta : preguntas)
        {
            dificultad += pregunta.getIndiceDificultad();
            discriminacion += pregunta.getIndiceDiscriminacion();
        }
        
        dificultad = Utiles.redondeo(dificultad/preguntas.size(), 2);
        discriminacion = Utiles.redondeo(discriminacion/preguntas.size(), 2);
        
        contenido += "<div class=\"borde_corto\">";
        contenido += "<h4 class=\"margen\" style=\"margin-top:5px\">�ndice de Dificultad actual: " + dificultad + "</h4>";
        contenido += "<h4 class=\"margen\" style=\"margin-top:0px\">�ndice de Discriminaci�n actual: " + discriminacion + "</h4>";
        contenido += "</div><br />";
        contenido += "<table id=\"listado\">";
        contenido += "<tr><th style=\"width: 60px\">Sust. Auto</th><th style=\"width: 60px\">Sust. Manual</th><th style=\"width: 40px\">Editar</th><th>Pregunta</th><th>Tema</th>";
        contenido += "<th style=\"width: 40px\">Veces usada</th><th style=\"width: 80px\">�ltimo uso</th><th style=\"width: 60px\">Aciertos</th><th style=\"width:60px\">Fallos</th><th style=\"width: 60px\">Sin contestar</th>";
        contenido += "<th style=\"width: 60px\">�ndice Dificul.</th><th style=\"width: 60px\"> �ndice Discrim.</th></tr>";

        //Recorremos las preguntas
        for (int i = (pagina - 1) * itemsPorPagina; i < pagina * itemsPorPagina && i < preguntas.size(); i++)
        {
            preguntaCEN.rellenarClasificacion(preguntas.get(i));

            contenido += "<tr id=\"tr" + preguntas.get(i).getCodigo() + "\">";
            contenido += "<td style=\"text-align: center\"><img class=\"alert boton\" src=\"images/refresh.png\" alt=\"cambiar\" onClick=\"cambiarPregunta("+ preguntas.get(i).getCodigo() + ", 'ExamenConfirmacion')\"></td>";
            contenido += "<td style=\"text-align: center\"><img class=\"alert boton\" src=\"images/intercambio.png\" alt=\"intercambio\" onClick=\"iniciarIntercambioPregunta("+ preguntas.get(i).getCodigo() +")\"></td>";
            contenido += "<td style=\"text-align: center\"><img class=\"boton alert\" src=\"images/edit.png\" alt=\"intercambio\" onClick=\"location.href='detallePregunta.jsp?pregunta=" + preguntas.get(i).getCodigo() + "&examen=true" + "'\"></td>";
            
            enunciado = traduccionPreguntaCEN.getTraduccionPreguntaByPreguntaEIdioma(preguntas.get(i), configuracionAsignaturaIdiomaCEN.getConfiguracionIdiomaPrincipal(asignatura).getIdioma()).getEnunciado();

            if (enunciado.length() > 100)
                enunciado = enunciado.substring(0, 100) + "...";
            
            contenido += "<td style=\"cursor:help\" onMouseOver=\"showTooltip(event, " + preguntas.get(i).getCodigo() +")\" onMouseOut=\"hideTooltip()\">" + Utiles.textoHTML(enunciado) + "</td>";

            if (preguntas.get(i).getClasificacion() != null)
            {
                if (preguntas.get(i).getClasificacion().isRevisada())
                {
                    contenido += "<td><img class=\"alert\" src=\"images/ok.png\" alt=\"revisada\" /> ";
                    contenido += preguntas.get(i).getClasificacion().getTema().getNombre() + "</td>";
                }
                else
                {
                    contenido += "<td><img id=\"img" + preguntas.get(i).getCodigo() + "\" class=\"alert\" src=\"images/alert.png\" alt=\"sin revisar\" /> ";
                    contenido += "<a id=\"tema" + preguntas.get(i).getCodigo() + "\"><select name=\"tema" + preguntas.get(i).getCodigo() + "\">";
                    String selected;
                    for (TemaEN tema : temas)
                    {
                        selected = "";

                        if (preguntas.get(i).getClasificacion().getTema().getCodigo() == tema.getCodigo())
                            selected = "selected";

                        contenido += "<option value=\"" + tema.getCodigo() + "\"" + selected + ">";
                        contenido += tema.getNombre();
                        contenido += "</option>";
                    }
                    contenido += "</select></a> <a class=\"link\" id=\"link" + preguntas.get(i).getCodigo() + "\" onClick=\"validarClasificacionListado(" + preguntas.get(i).getCodigo() + ")\">(validar)</a></div></td>";
                }
            }
            else
            {
                contenido += "<td><img id=\"img" + preguntas.get(i).getCodigo() + "\" class=\"alert\" src=\"images/error.png\" alt=\"sin clasificar\" /> ";
                contenido += "<a id=\"tema" + preguntas.get(i).getCodigo() + "\"><select name=\"tema" + preguntas.get(i).getCodigo() + "\">";
                contenido += "<option value=\"-1\" selected>";
                contenido += " Sin clasificar</option>";

                for (TemaEN tema : temas)
                {
                    contenido += "<option value=\"" + tema.getCodigo() + "\">";
                    contenido += tema.getNombre();
                    contenido += "</option>";
                }
                contenido += "</select></a> <a class=\"link\" id=\"link" + preguntas.get(i).getCodigo() + "\" onClick=\"validarClasificacionListado(" + preguntas.get(i).getCodigo() + ")\">(validar)</a></div></td>";
            }

            contenido += "<td>" + preguntas.get(i).getUtilizada() + "</td>";
            
            //La pregunta se ha utilizado alguna vez
            if (preguntas.get(i).getUltimoUso() != null)
            {
                contenido += "<td>" + Utiles.rellenaConCeros(preguntas.get(i).getUltimoUso().get(Calendar.DAY_OF_MONTH), 2) + "/" + Utiles.rellenaConCeros((preguntas.get(i).getUltimoUso().get(Calendar.MONTH)+1), 2) + "/" + preguntas.get(i).getUltimoUso().get(Calendar.YEAR) + "</td>";
            }
            else
                contenido += "<td> - </td>";

            if (preguntas.get(i).getContestada() > 0)
            {     
                contenido += "<td>" + Utiles.redondeo((double)preguntas.get(i).getAciertos()/preguntas.get(i).getContestada()*100, 2) + "%</td>";
                contenido += "<td>" + Utiles.redondeo((double)preguntas.get(i).getFallos()/preguntas.get(i).getContestada()*100, 2) + "%</td>";
                contenido += "<td>" + Utiles.redondeo(((double)preguntas.get(i).getNoContestada()/(preguntas.get(i).getContestada() + preguntas.get(i).getNoContestada()))*100, 2) + "%</td>";
                contenido += "<td>" + preguntas.get(i).getIndiceDificultad() + "</td>";
                contenido += "<td>" + preguntas.get(i).getIndiceDiscriminacion() + "</td>";
            }
            else
            {
                contenido += "<td> - </td>";
                contenido += "<td> - </td>";
                contenido += "<td> - </td>";
                contenido += "<td> - </td>";
                contenido += "<td> - </td>";
            }
            contenido += "</tr>";
        }

        contenido += "</table>";

        //Escribimos el pi� con las opciones de navegaci�n
        contenido += "<br /><div align=\"right\" style=\"margin-right: 6px\">";
        contenido += "<img class=\"boton\" src=\"images/doble_izquierda.png\" alt=\"primera\" style=\"width: 25px; height: 25px; vertical-align: middle; margin-right: 3px\" onClick=\"primeraPagina('seleccionadasExamenConfirmacion')\" />";
        contenido += "<img class=\"boton\" src=\"images/izquierda.png\" alt=\"anterior\" style=\"width: 25px; height: 25px; vertical-align: middle; margin-right: 10px\" onClick=\"anteriorPagina('seleccionadasExamenConfirmacion')\" />";
        contenido += "P�gina " + pagina + " de " + (int)Math.ceil(preguntas.size()/(itemsPorPagina * 1.0));
        contenido += "<img class=\"boton\" src=\"images/derecha.png\" alt=\"siguiente\" style=\"width: 25px; height: 25px; vertical-align: middle; margin-left: 10px\" onClick=\"siguientePagina('seleccionadasExamenConfirmacion')\" />";
        contenido += "<img class=\"boton\" src=\"images/doble_derecha.png\" alt=\"ultima\" style=\"width: 25px; height: 25px; vertical-align: middle; margin-left: 3px\" onClick=\"ultimaPagina('seleccionadasExamenConfirmacion')\" /></div>";
        contenido += "<div align=\"right\" style=\"margin-right: 70px; margin-top: 5px; font-weight: bold;\">Total: " + preguntas.size() + "</div>";
        contenido += "<div id=\"totalSeleccionadas\" style=\"display: none\">" + preguntas.size() + "</div>";
        contenido += "<div id=\"error\"></div>";
        contenido += "<br /><div class=\"boton_pregunta\" style=\"float: right; margin-right: 5px\" onClick=\"cancelarExamen() \">Cancelar</div>";
        contenido += "<div class=\"boton_pregunta\" style=\"float: right; margin-right: 5px\" onClick=\"confirmarPreguntas() \">Siguiente</div>";
    }
    //-----------------------------------------------------------------------------------------------------------------------------------------------------//
    //Listado de preguntas seleccionadas en la edici�n de une examen
    else if (request.getParameter("modo").equals("seleccionadasEditar"))
    {
        //Obtenemos las preguntas ya seleccionadas
        ArrayList<ExamenPreguntaEN> preguntas = ((ArrayList<ExamenPreguntaEN>)session.getAttribute("preguntasExamen"));
        TemaCEN temaCEN = new TemaCEN();
        ArrayList<TemaEN> temas = temaCEN.getTemasByAsignatura(asignatura);
        
        //Tenemos que calcular el indice de discriminacion y de dificultad
        double discriminacion = 0.0;
        double dificultad = 0.0;
                
        for (ExamenPreguntaEN pregunta : preguntas)
        {
            discriminacion += pregunta.getPregunta().getIndiceDiscriminacion();
            dificultad += pregunta.getPregunta().getIndiceDificultad();
        }
        
        discriminacion = Utiles.redondeo(discriminacion/preguntas.size(), 2);
        dificultad = Utiles.redondeo(dificultad/preguntas.size(), 2);
        
        //Lo guardamos en inputs ocultos para poder recogerlos
        contenido += "<input style=\"display: none\" type=\"text\" id=\"dificultadO\" name=\"dificultadO\" value=\"" + String.valueOf(dificultad) + "\" />";
        contenido += "<input style=\"display: none\" type=\"text\" id=\"discriminacionO\" name=\"discriminacionO\" value=\"" + String.valueOf(discriminacion) + "\" />";
        
        contenido += "<table id=\"listado\"><tr>";
        
        if (request.getParameter("editar").equals("True"))
            contenido += "<th style=\"width: 60px\">Eliminar</th><th style=\"width: 60px\">Sust. Auto</th><th style=\"width: 60px\">Sust. Manual</th>";
        
        contenido += "<th>Pregunta</th><th>Tema</th><th style=\"width: 40px\">Veces usada</th><th style=\"width: 80px\">�ltimo uso</th><th style=\"width: 60px\">Aciertos</th><th style=\"width:60px\">Fallos</th><th style=\"width: 60px\">Sin contestar</th>";
        contenido += "<th style=\"width: 60px\">�ndice Dificul.</th><th style=\"width: 60px\"> �ndice Discrim.</th></tr>";
        
        //Recorremos las preguntas
        for (int i = (pagina - 1) * itemsPorPagina; i < pagina * itemsPorPagina && i < preguntas.size(); i++)
        {
            preguntaCEN.rellenarClasificacion(preguntas.get(i).getPregunta());

            contenido += "<tr id=\"tr" + preguntas.get(i).getPregunta().getCodigo() + "\">";
            
            if (request.getParameter("editar").equals("True"))
            {
                contenido += "<td style=\"text-align: center\"><img class=\"add\" src=\"images/delete.png\" alt=\"borrar\" onClick=\"delPregunta("+ preguntas.get(i).getPregunta().getCodigo() + ", 'Editar')\"></td>";
                contenido += "<td style=\"text-align: center\"><img class=\"alert boton\" src=\"images/refresh.png\" alt=\"cambiar\" onClick=\"cambiarPregunta("+ preguntas.get(i).getPregunta().getCodigo() + ", 'Editar')\"></td>";
                contenido += "<td style=\"text-align: center\"><img class=\"alert boton\" src=\"images/intercambio.png\" alt=\"intercambio\" onClick=\"iniciarIntercambioPregunta("+ preguntas.get(i).getPregunta().getCodigo() +")\"></td>";
            }
            
            enunciado = traduccionPreguntaCEN.getTraduccionPreguntaByPreguntaEIdioma(preguntas.get(i).getPregunta(), configuracionAsignaturaIdiomaCEN.getConfiguracionIdiomaPrincipal(asignatura).getIdioma()).getEnunciado();

            if (enunciado.length() > 100)
                enunciado = enunciado.substring(0, 100) + "...";
            
            contenido += "<td style=\"cursor:help\" onMouseOver=\"showTooltip(event, " + preguntas.get(i).getPregunta().getCodigo() +")\" onMouseOut=\"hideTooltip()\">" + Utiles.textoHTML(enunciado) + "</td>";

            if (preguntas.get(i).getPregunta().getClasificacion() != null)
            {
                if (preguntas.get(i).getPregunta().getClasificacion().isRevisada())
                {
                    contenido += "<td><img class=\"alert\" src=\"images/ok.png\" alt=\"revisada\" /> ";
                    contenido += preguntas.get(i).getPregunta().getClasificacion().getTema().getNombre() + "</td>";
                }
                else
                {
                    contenido += "<td><img id=\"img" + preguntas.get(i).getPregunta().getCodigo() + "\" class=\"alert\" src=\"images/alert.png\" alt=\"sin revisar\" /> ";
                    contenido += "<a id=\"tema" + preguntas.get(i).getPregunta().getCodigo() + "\"><select name=\"tema" + preguntas.get(i).getPregunta().getCodigo() + "\">";
                    String selected;
                    for (TemaEN tema : temas)
                    {
                        selected = "";

                        if (preguntas.get(i).getPregunta().getClasificacion().getTema().getCodigo() == tema.getCodigo())
                            selected = "selected";

                        contenido += "<option value=\"" + tema.getCodigo() + "\"" + selected + ">";
                        contenido += tema.getNombre();
                        contenido += "</option>";
                    }
                    contenido += "</select></a> <a class=\"link\" id=\"link" + preguntas.get(i).getPregunta().getCodigo() + "\" onClick=\"validarClasificacionListado(" + preguntas.get(i).getPregunta().getCodigo() + ")\">(validar)</a></div></td>";
                }


            }
            else
            {
                contenido += "<td><img id=\"img" + preguntas.get(i).getPregunta().getCodigo() + "\" class=\"alert\" src=\"images/error.png\" alt=\"sin clasificar\" /> ";
                contenido += "<a id=\"tema" + preguntas.get(i).getPregunta().getCodigo() + "\"><select name=\"tema" + preguntas.get(i).getPregunta().getCodigo() + "\">";
                contenido += "<option value=\"-1\" selected>";
                contenido += " Sin clasificar</option>";

                for (TemaEN tema : temas)
                {
                    contenido += "<option value=\"" + tema.getCodigo() + "\">";
                    contenido += tema.getNombre();
                    contenido += "</option>";
                }
                contenido += "</select></a> <a class=\"link\" id=\"link" + preguntas.get(i).getPregunta().getCodigo() + "\" onClick=\"validarClasificacionListado(" + preguntas.get(i).getPregunta().getCodigo() + ")\">(validar)</a></div></td>";
            }

            contenido += "<td>" + preguntas.get(i).getPregunta().getUtilizada() + "</td>";
            
            //La pregunta se ha utilizado alguna vez
            if (preguntas.get(i).getPregunta().getUltimoUso() != null)
            {
                contenido += "<td>" + Utiles.rellenaConCeros(preguntas.get(i).getPregunta().getUltimoUso().get(Calendar.DAY_OF_MONTH), 2) + "/" + Utiles.rellenaConCeros((preguntas.get(i).getPregunta().getUltimoUso().get(Calendar.MONTH)+1), 2) + "/" + preguntas.get(i).getPregunta().getUltimoUso().get(Calendar.YEAR) + "</td>";
            }
            else
                contenido += "<td> - </td>";

            if (preguntas.get(i).getPregunta().getContestada() > 0)
            {     
                contenido += "<td>" + Utiles.redondeo((double)preguntas.get(i).getPregunta().getAciertos()/preguntas.get(i).getPregunta().getContestada()*100, 2) + "%</td>";
                contenido += "<td>" + Utiles.redondeo((double)preguntas.get(i).getPregunta().getFallos()/preguntas.get(i).getPregunta().getContestada()*100, 2) + "%</td>";
                contenido += "<td>" + Utiles.redondeo(((double)preguntas.get(i).getPregunta().getNoContestada()/(preguntas.get(i).getPregunta().getContestada() + preguntas.get(i).getPregunta().getNoContestada()))*100, 2) + "%</td>";
                contenido += "<td>" + preguntas.get(i).getPregunta().getIndiceDificultad() + "</td>";
                contenido += "<td>" + preguntas.get(i).getPregunta().getIndiceDiscriminacion() + "</td>";
            }
            else
            {
                contenido += "<td> - </td>";
                contenido += "<td> - </td>";
                contenido += "<td> - </td>";
                contenido += "<td> - </td>";
                contenido += "<td> - </td>";
            }
            contenido += "</tr>";
        }

        contenido += "</table>";

        //Escribimos el pi� con las opciones de navegaci�n
        contenido += "<br /><div align=\"right\" style=\"margin-right: 6px\">";
        contenido += "<img class=\"boton\" src=\"images/doble_izquierda.png\" alt=\"primera\" style=\"width: 25px; height: 25px; vertical-align: middle; margin-right: 3px\" onClick=\"primeraPagina('seleccionadasEditar" + request.getParameter("editar") + "')\" />";
        contenido += "<img class=\"boton\" src=\"images/izquierda.png\" alt=\"anterior\" style=\"width: 25px; height: 25px; vertical-align: middle; margin-right: 10px\" onClick=\"anteriorPagina('seleccionadasEditar" + request.getParameter("editar") + "')\" />";
        contenido += "P�gina " + pagina + " de " + (int)Math.ceil(preguntas.size()/(itemsPorPagina * 1.0));
        contenido += "<img class=\"boton\" src=\"images/derecha.png\" alt=\"siguiente\" style=\"width: 25px; height: 25px; vertical-align: middle; margin-left: 10px\" onClick=\"siguientePagina('seleccionadasEditar" + request.getParameter("editar") + "')\" />";
        contenido += "<img class=\"boton\" src=\"images/doble_derecha.png\" alt=\"ultima\" style=\"width: 25px; height: 25px; vertical-align: middle; margin-left: 3px\" onClick=\"ultimaPagina('seleccionadasEditar" + request.getParameter("editar") + "')\" /></div>";
        contenido += "<div align=\"right\" style=\"margin-right: 70px; margin-top: 5px; font-weight: bold;\">Total: " + preguntas.size() + "</div>";
        contenido += "<div id=\"totalSeleccionadas\" style=\"display: none\">" + preguntas.size() + "</div>";
    }
    //-----------------------------------------------------------------------------------------------------------------------------------------------------//
    //Listado de preguntas para elegir en la edici�n de une examen
    else if (request.getParameter("modo").equals("seleccionarEditar"))
    {   
        //Obtenemos las preguntas ya seleccionadas
        ArrayList<ExamenPreguntaEN> seleccionadas = ((ArrayList<ExamenPreguntaEN>)session.getAttribute("preguntasExamen"));
        TemaCEN temaCEN = new TemaCEN();
        ArrayList<TemaEN> temas = temaCEN.getTemasByAsignatura(asignatura);
        
        //Obtenemos las preguntas (solo habilitadas)
        String filtro = " AND pre_habilitada = true";

        if (session.getAttribute("filtro") != null)
            filtro = (String)session.getAttribute("filtro") +  filtro;
        
        ArrayList<PreguntaEN> preguntas = preguntaCEN.getPreguntasByAsignaturaYFiltro(asignatura, filtro);
        
        //Quitamos las preguntas ya seleccionadas de la lista de preguntas para seleccionar
        for (ExamenPreguntaEN pregunta : seleccionadas)
        {
            preguntaCEN.borrarDeLista(preguntas, pregunta.getPregunta());
        }
        
        contenido += "<table id=\"listado\">";
        contenido += "<tr><th style=\"width: 60px\">A�adir</td><th id=\"thIntercambio\" style=\"display: none; width:60px\">Sustituir</th><th>Pregunta</th><th>Tema</th>";
        contenido += "<th style=\"width: 40px\">Veces usada</th><th style=\"width: 80px\">�ltimo uso</th><th style=\"width: 60px\">Aciertos</th><th style=\"width:60px\">Fallos</th><th style=\"width: 60px\">Sin contestar</th>";
        contenido += "<th style=\"width: 60px\">�ndice Dificul.</th><th style=\"width: 60px\"> �ndice Discrim.</th></tr>";

        //Recorremos los items de la pagina
        for (int i = (pagina - 1) * itemsPorPagina; i < pagina * itemsPorPagina && i < preguntas.size(); i++)
        {
            preguntaCEN.rellenarClasificacion(preguntas.get(i));

            contenido += "<tr>";
            contenido += "<td style=\"text-align: center\"><img class=\"add\" src=\"images/add.png\" alt=\"add\" onClick=\"addPregunta("+ preguntas.get(i).getCodigo() +", 'Editar')\"></td>";
            contenido += "<td class=\"tdIntercambio\" style=\"display: none; text-align: center\"><img class=\"boton alert\" src=\"images/intercambio.png\" alt=\"intercambio\" onClick=\"intercambiar("+ preguntas.get(i).getCodigo() +", 'Editar')\"></td>";

            enunciado = traduccionPreguntaCEN.getTraduccionPreguntaByPreguntaEIdioma(preguntas.get(i), configuracionAsignaturaIdiomaCEN.getConfiguracionIdiomaPrincipal(asignatura).getIdioma()).getEnunciado();

            if (enunciado.length() > 100)
                enunciado = enunciado.substring(0, 100) + "...";
            
            contenido += "<td style=\"cursor:help\" onMouseOver=\"showTooltip(event, " + preguntas.get(i).getCodigo() +")\" onMouseOut=\"hideTooltip()\">" + Utiles.textoHTML(enunciado) + "</td>";

            if (preguntas.get(i).getClasificacion() != null)
            {
                if (preguntas.get(i).getClasificacion().isRevisada())
                {
                    contenido += "<td><img class=\"alert\" src=\"images/ok.png\" alt=\"revisada\" /> ";
                    contenido += preguntas.get(i).getClasificacion().getTema().getNombre() + "</td>";
                }
                else
                {
                    contenido += "<td><img id=\"img" + preguntas.get(i).getCodigo() + "\" class=\"alert\" src=\"images/alert.png\" alt=\"sin revisar\" /> ";
                    contenido += "<a id=\"tema" + preguntas.get(i).getCodigo() + "\"><select name=\"tema" + preguntas.get(i).getCodigo() + "\">";
                    String selected;
                    for (TemaEN tema : temas)
                    {
                        selected = "";

                        if (preguntas.get(i).getClasificacion().getTema().getCodigo() == tema.getCodigo())
                            selected = "selected";

                        contenido += "<option value=\"" + tema.getCodigo() + "\"" + selected + ">";
                        contenido += tema.getNombre();
                        contenido += "</option>";
                    }
                    contenido += "</select></a> <a class=\"link\" id=\"link" + preguntas.get(i).getCodigo() + "\" onClick=\"validarClasificacionListado(" + preguntas.get(i).getCodigo() + ")\">(validar)</a></div></td>";
                }
            }
            else
            {
                contenido += "<td><img id=\"img" + preguntas.get(i).getCodigo() + "\" class=\"alert\" src=\"images/error.png\" alt=\"sin clasificar\" /> ";
                contenido += "<a id=\"tema" + preguntas.get(i).getCodigo() + "\"><select name=\"tema" + preguntas.get(i).getCodigo() + "\">";
                contenido += "<option value=\"-1\" selected>";
                contenido += " Sin clasificar</option>";

                for (TemaEN tema : temas)
                {
                    contenido += "<option value=\"" + tema.getCodigo() + "\">";
                    contenido += tema.getNombre();
                    contenido += "</option>";
                }
                contenido += "</select></a> <a class=\"link\" id=\"link" + preguntas.get(i).getCodigo() + "\" onClick=\"validarClasificacionListado(" + preguntas.get(i).getCodigo() + ")\">(validar)</a></div></td>";
            }
            
            contenido += "<td>" + preguntas.get(i).getUtilizada() + "</td>";

            //La pregunta se ha utilizado alguna vez
            if (preguntas.get(i).getUltimoUso() != null)
            {
                contenido += "<td>" + Utiles.rellenaConCeros(preguntas.get(i).getUltimoUso().get(Calendar.DAY_OF_MONTH), 2) + "/" + Utiles.rellenaConCeros((preguntas.get(i).getUltimoUso().get(Calendar.MONTH)+1), 2) + "/" + preguntas.get(i).getUltimoUso().get(Calendar.YEAR) + "</td>";
            }
            else
                contenido += "<td> - </td>";

            if (preguntas.get(i).getContestada() > 0)
            {     
                contenido += "<td>" + Utiles.redondeo((double)preguntas.get(i).getAciertos()/preguntas.get(i).getContestada()*100, 2) + "%</td>";
                contenido += "<td>" + Utiles.redondeo((double)preguntas.get(i).getFallos()/preguntas.get(i).getContestada()*100, 2) + "%</td>";
                contenido += "<td>" + Utiles.redondeo(((double)preguntas.get(i).getNoContestada()/(preguntas.get(i).getContestada() + preguntas.get(i).getNoContestada()))*100, 2) + "%</td>";
                contenido += "<td>" + preguntas.get(i).getIndiceDificultad() + "</td>";
                contenido += "<td>" + preguntas.get(i).getIndiceDiscriminacion() + "</td>";
            }
            else
            {
                contenido += "<td> - </td>";
                contenido += "<td> - </td>";
                contenido += "<td> - </td>";
                contenido += "<td> - </td>";
                contenido += "<td> - </td>";
            }
            
            contenido += "</tr>";
        }
        
        contenido += "</table>";
        
        //Escribimos el pi� con las opciones de navegaci�n
        contenido += "<br /><div align=\"right\" style=\"margin-right: 6px\">";
        contenido += "<img class=\"boton\" src=\"images/doble_izquierda.png\" alt=\"primera\" style=\"width: 25px; height: 25px; vertical-align: middle; margin-right: 3px\" onClick=\"primeraPagina('seleccionarEditar')\" />";
        contenido += "<img class=\"boton\" src=\"images/izquierda.png\" alt=\"anterior\" style=\"width: 25px; height: 25px; vertical-align: middle; margin-right: 10px\" onClick=\"anteriorPagina('seleccionarEditar')\" />";
        contenido += "P�gina " + pagina + " de " + (int)Math.ceil(preguntas.size()/(itemsPorPagina * 1.0));
        contenido += "<img class=\"boton\" src=\"images/derecha.png\" alt=\"siguiente\" style=\"width: 25px; height: 25px; vertical-align: middle; margin-left: 10px\" onClick=\"siguientePagina('seleccionarEditar')\" />";
        contenido += "<img class=\"boton\" src=\"images/doble_derecha.png\" alt=\"ultima\" style=\"width: 25px; height: 25px; vertical-align: middle; margin-left: 3px\" onClick=\"ultimaPagina('seleccionarEditar')\" /></div>";
        contenido += "<div align=\"right\" style=\"margin-right: 70px; margin-top: 5px; font-weight: bold;\">Total: " + preguntas.size() + "</div>";
        contenido += "<div id=\"totalSeleccionar\" style=\"display: none\">" + preguntas.size() + "</div>";
    }
    //------------------------------------------------------------------------------------------------------------------------------------------//
    //Listado para la visualizaci�n de las preguntas
    else if (request.getParameter("modo").equals("ver"))
    {
        String filtro = "";
        
        if (session.getAttribute("filtro") != null)
            filtro = (String)session.getAttribute("filtro");
        
        ArrayList<PreguntaEN> preguntas = preguntaCEN.getPreguntasByAsignaturaYFiltro(asignatura, filtro);
        TemaCEN temaCEN = new TemaCEN();
        ArrayList<TemaEN> temas = temaCEN.getTemasByAsignatura(asignatura);
        ArrayList<ConfiguracionAsignaturaIdiomaEN> confs = configuracionAsignaturaIdiomaCEN.getConfiguracionesAsignaturaIdiomaByAsignatura(asignatura);
                
        contenido += "<table id=\"listado\">";
        contenido += "<tr><th style=\"width: 60px\">Detalle</th><th style=\"width: 60px\">Eliminar</th><th style=\"width: 60px\">Traducci�n</th><th>Pregunta</th><th>Tema</th>";
        contenido += "<th style=\"width: 40px\">Veces usada</th><th style=\"width: 80px\">�ltimo uso</th><th style=\"width: 60px\">Aciertos</th><th style=\"width:60px\">Fallos</th><th style=\"width: 60px\">Sin contestar</th>";
        contenido += "<th style=\"width: 60px\">�ndice Dificul.</th><th style=\"width: 60px\"> �ndice Discrim.</th></tr>";

        for (int i = (pagina - 1) * itemsVer; i < pagina * itemsVer && i < preguntas.size(); i++)
        {
                preguntaCEN.rellenarClasificacion(preguntas.get(i));
                
                contenido += "<tr>";
                
                //Detalle
                if (!preguntas.get(i).isHabilitada())
                    contenido += "<td style=\"text-align: center; background-color: " + colorDeshabilitada + "\">";
                else
                    contenido += "<td style=\"text-align: center\">";
                
                contenido += "<img class=\"add\" src=\"images/detalle.png\" alt=\"detalle\" onClick=\"location.href='detallePregunta.jsp?pregunta=" + preguntas.get(i).getCodigo() + "'\" /></div></td>";
                
                //Eliminar
                if (!preguntas.get(i).isHabilitada())
                    contenido += "<td style=\"text-align: center; background-color: " + colorDeshabilitada + "\">";
                else
                    contenido += "<td style=\"text-align: center\">";
                
                contenido += "<img class=\"add\" src=\"images/borrar.png\" alt=\"eliminar\" onClick=\"eliminarPregunta(" + preguntas.get(i).getCodigo() + ")\" /></div></td>";
                
                //Traduccion
                if (!preguntas.get(i).isHabilitada())
                        contenido += "<td style=\"text-align: center; background-color: " + colorDeshabilitada + "\">";
                else
                    contenido += "<td style=\"text-align: center\">";
                
                boolean revisado = true;
                ArrayList<TraduccionPreguntaEN> traducciones = traduccionPreguntaCEN.getTraduccionesPreguntaByPregunta(preguntas.get(i));
                
                
                //Si no tiene idiomas aparte del principal 
                if (confs.size() == 1)
                    revisado = true;
                else
                {
                    for (ConfiguracionAsignaturaIdiomaEN conf: confs)
                    {
                        TraduccionPreguntaEN traduccion = traduccionPreguntaCEN.idiomaTraducidoEnLista(traducciones, conf.getIdioma());
                        if (traduccion == null || !traduccion.isRevisada())
                        {
                            revisado = false;
                            break; //Optimizamos saliendo del bucle al encontrarla
                        }
                    }
                }
                
                if (confs.size() == 1)
                {
                    contenido += "<img class=\"preparado\" src=\"images/preparacion_ok.png\" alt=\"revisada\" />";
                }
                else if (revisado)
                {
                    contenido += "<img class=\"add\" src=\"images/preparacion_ok.png\" alt=\"revisada\" onClick=\"location.href='revisarTraduccionPregunta.jsp?pregunta=" + preguntas.get(i).getCodigo() + "'\" />";
                }
                else
                {
                     contenido += "<img class=\"add\" src=\"images/preparacion_error.png\" alt=\"sin revisar\" onClick=\"location.href='revisarTraduccionPregunta.jsp?pregunta=" + preguntas.get(i).getCodigo() + "'\" />";
                }   
                
                contenido += "</td>";
                
                
                //Enunciado
                enunciado = traduccionPreguntaCEN.getTraduccionPreguntaByPreguntaEIdioma(preguntas.get(i), configuracionAsignaturaIdiomaCEN.getConfiguracionIdiomaPrincipal(asignatura).getIdioma()).getEnunciado();

                if (enunciado.length() > 100)
                enunciado = enunciado.substring(0, 100) + "...";
                
                if (!preguntas.get(i).isHabilitada())
                    contenido += "<td style=\"cursor:help; background-color: " + colorDeshabilitada + "\" onMouseOver=\"showTooltip(event, " + preguntas.get(i).getCodigo() +")\" onMouseOut=\"hideTooltip()\">";
                else
                    contenido += "<td style=\"cursor:help\" onMouseOver=\"showTooltip(event, " + preguntas.get(i).getCodigo() +")\" onMouseOut=\"hideTooltip()\">";
                
                contenido += Utiles.textoHTML(enunciado) + "</td>";
                
                if (preguntas.get(i).getClasificacion() != null)
                {
                    if (!preguntas.get(i).isHabilitada())
                        contenido += "<td style=\"background-color: " + colorDeshabilitada + "\">";
                    else
                        contenido += "<td>";
                    
                    if (preguntas.get(i).getClasificacion().isRevisada())
                    {
                        contenido += "<img class=\"alert\" src=\"images/ok.png\" alt=\"revisada\" /> ";
                        contenido += preguntas.get(i).getClasificacion().getTema().getNombre();
                    }
                    else
                    {
                        contenido += "<img id=\"img" + preguntas.get(i).getCodigo() + "\" class=\"alert\" src=\"images/alert.png\" alt=\"sin revisar\" /> ";
                        contenido += "<a id=\"tema" + preguntas.get(i).getCodigo() + "\"><select name=\"tema" + preguntas.get(i).getCodigo() + "\">";
                        String selected;
                        for (TemaEN tema : temas)
                        {
                            selected = "";
                            
                            if (preguntas.get(i).getClasificacion().getTema().getCodigo() == tema.getCodigo())
                                selected = "selected";
                            
                            contenido += "<option value=\"" + tema.getCodigo() + "\"" + selected + ">";
                            contenido += tema.getNombre();
                            contenido += "</option>";
                        }
                        contenido += "</select></a> <a class=\"link\" id=\"link" + preguntas.get(i).getCodigo() + "\" onClick=\"validarClasificacionListado(" + preguntas.get(i).getCodigo() + ")\">(validar)</a></div>";
                    }
                    
                    contenido += "</td>";
                }
                else
                {
                    if (!preguntas.get(i).isHabilitada())
                        contenido += "<td style=\"background-color: " + colorDeshabilitada + "\">";
                    else
                        contenido += "<td>";
                    
                    contenido += "<img id=\"img" + preguntas.get(i).getCodigo() + "\" class=\"alert\" src=\"images/error.png\" alt=\"sin clasificar\" /> ";
                    contenido += "<a id=\"tema" + preguntas.get(i).getCodigo() + "\"><select name=\"tema" + preguntas.get(i).getCodigo() + "\">";
                    contenido += "<option value=\"-1\" selected>";
                    contenido += " Sin clasificar</option>";
                    
                    for (TemaEN tema : temas)
                    {
                        contenido += "<option value=\"" + tema.getCodigo() + "\">";
                        contenido += tema.getNombre();
                        contenido += "</option>";
                    }
                    contenido += "</select></a> <a class=\"link\" id=\"link" + preguntas.get(i).getCodigo() + "\" onClick=\"validarClasificacionListado(" + preguntas.get(i).getCodigo() + ")\">(validar)</a></div></td>";
                }
                
                
                //El n�mero de veces utilizada
                if (!preguntas.get(i).isHabilitada())
                        contenido += "<td style=\"background-color: " + colorDeshabilitada + "\">";
                else
                    contenido += "<td>";
                
                    contenido += preguntas.get(i).getUtilizada() + "</td>";

                
                //El �ltimo uso de la pregunta
                if (!preguntas.get(i).isHabilitada())
                        contenido += "<td style=\"background-color: " + colorDeshabilitada + "\">";
                else
                    contenido += "<td>";
                
                
                if (preguntas.get(i).getUltimoUso() != null)
                {
                    contenido += Utiles.rellenaConCeros(preguntas.get(i).getUltimoUso().get(Calendar.DAY_OF_MONTH), 2) + "/" + Utiles.rellenaConCeros((preguntas.get(i).getUltimoUso().get(Calendar.MONTH)+1), 2) + "/" + preguntas.get(i).getUltimoUso().get(Calendar.YEAR) + "</td>";
                }
                else   
                    contenido += " - </td>";
                
                if (!preguntas.get(i).isHabilitada())
                {
                    if (preguntas.get(i).getContestada() > 0)
                    {
                        contenido += "<td style=\"background-color: " + colorDeshabilitada + "\">" + Utiles.redondeo((double)preguntas.get(i).getAciertos()/preguntas.get(i).getContestada()*100, 2) + "%</td>";
                        contenido += "<td style=\"background-color: " + colorDeshabilitada + "\">" + Utiles.redondeo((double)preguntas.get(i).getFallos()/preguntas.get(i).getContestada()*100, 2) + "%</td>";
                        contenido += "<td style=\"background-color: " + colorDeshabilitada + "\">" + Utiles.redondeo((double)preguntas.get(i).getNoContestada()/(preguntas.get(i).getContestada() + preguntas.get(i).getNoContestada())*100, 2) + "%</td>";
                        contenido += "<td style=\"background-color: " + colorDeshabilitada + "\">" + preguntas.get(i).getIndiceDificultad() + "</td>";
                        contenido += "<td style=\"background-color: " + colorDeshabilitada + "\">" + preguntas.get(i).getIndiceDiscriminacion() + "</td>";
                    }
                    else
                    {
                        contenido += "<td style=\"background-color: " + colorDeshabilitada + "\"> - </td>";
                        contenido += "<td style=\"background-color: " + colorDeshabilitada + "\"> - </td>";
                        contenido += "<td style=\"background-color: " + colorDeshabilitada + "\"> - </td>";
                        contenido += "<td style=\"background-color: " + colorDeshabilitada + "\"> - </td>";
                        contenido += "<td style=\"background-color: " + colorDeshabilitada + "\"> - </td>";
                    }
                }
                else
                {
                    if (preguntas.get(i).getContestada() > 0)
                    {   
                        contenido += "<td>" + Utiles.redondeo((double)preguntas.get(i).getAciertos()/preguntas.get(i).getContestada()*100, 2) + "%</td>";
                        contenido += "<td>" + Utiles.redondeo((double)preguntas.get(i).getFallos()/preguntas.get(i).getContestada()*100, 2) + "%</td>";
                        contenido += "<td>" + Utiles.redondeo((double)preguntas.get(i).getNoContestada()/(preguntas.get(i).getContestada() + preguntas.get(i).getNoContestada())*100, 2) + "%</td>";
                        contenido += "<td>" + preguntas.get(i).getIndiceDificultad() + "</td>";
                        contenido += "<td>" + preguntas.get(i).getIndiceDiscriminacion() + "</td>";
                    }
                    else
                    {
                        contenido += "<td> - </td>";
                        contenido += "<td> - </td>";
                        contenido += "<td> - </td>";
                        contenido += "<td> - </td>";
                        contenido += "<td> - </td>";
                    }
                }

                contenido += "</tr>";
            }
        
        contenido += "</table>";
        
        //Escribimos el pi� con las opciones de navegaci�n
        contenido += "<br /><div align=\"right\" style=\"margin-right: 6px\">";
        contenido += "<img class=\"boton\" src=\"images/doble_izquierda.png\" alt=\"primera\" style=\"width: 25px; height: 25px; vertical-align: middle; margin-right: 3px\" onClick=\"primeraPagina('ver')\" />";
        contenido += "<img class=\"boton\" src=\"images/izquierda.png\" alt=\"anterior\" style=\"width: 25px; height: 25px; vertical-align: middle; margin-right: 10px\" onClick=\"anteriorPagina('ver')\" />";
        contenido += "P�gina " + pagina + " de " + (int)Math.ceil(preguntas.size()/(itemsVer * 1.0));
        contenido += "<img class=\"boton\" src=\"images/derecha.png\" alt=\"siguiente\" style=\"width: 25px; height: 25px; vertical-align: middle; margin-left: 10px\" onClick=\"siguientePagina('ver')\" />";
        contenido += "<img class=\"boton\" src=\"images/doble_derecha.png\" alt=\"ultima\" style=\"width: 25px; height: 25px; vertical-align: middle; margin-left: 3px\" onClick=\"ultimaPagina('ver')\" /></div>";
        contenido += "<div align=\"right\" style=\"margin-right: 70px; margin-top: 5px; font-weight: bold;\">Total: " + preguntas.size() + "</div>";
        contenido += "<div id=\"totalVer\" style=\"display: none\">" + preguntas.size() + "</div>";   
    }
%>

<%= contenido %>