<%-- 
    Copyright (C) 2014 Ra�l Sempere Trujillo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Document   : menu
    Created on : 15-abr-2014, 00:46:00
    Author     : Ra�l Sempere Trujillo
--%>

<%-- 
    Este documento se emplea para mostrar la barra con el men�.
--%>

<%@page import="EN.AsignaturaEN"%>
<%@page import="CEN.AccesoUsuarioAsignaturaCEN"%>
<%@page import="EN.ConfiguracionAsignaturaIdiomaEN"%>
<%@page import="CEN.ConfiguracionAsignaturaIdiomaCEN"%>
<%@page import="CEN.UsuarioCEN"%>
<%@page import="EN.UsuarioEN"%>
<link rel="shortcut icon" href="images/logo.ico" type="image/x-icon"/>
<%
    String asignaturasMenu = "";
    String preguntasMenu = "";
    String temasMenu = "";
    String examenesMenu = "";
    String cuentaMenu = "";
    String administracionMenu = "";
    String acercaDeMenu = "";
    String salirMenu = "";

    //Si no hay usuario se vuelve al login
    if (session.getAttribute("usuario") != null)
    {
        //Asignaturas
        UsuarioEN usuario = (UsuarioEN)session.getAttribute("usuario");
        ConfiguracionAsignaturaIdiomaEN configu = null;
        ConfiguracionAsignaturaIdiomaCEN configuracionAsignaturaCEN = new ConfiguracionAsignaturaIdiomaCEN();
        UsuarioCEN usuoCEN = new UsuarioCEN();
        usuoCEN.rellenarAccesos(usuario);
        
        asignaturasMenu += "<li><a class=\"menu\">Asignaturas</a><ul>";
        
        for (int i = 0; i < usuario.getAccesosUsuarioAsignatura().size(); i++)
        {
            configu = configuracionAsignaturaCEN.getConfiguracionIdiomaPrincipal(usuario.getAccesosUsuarioAsignatura().get(i).getAsignatura());

            //Si tiene idioma principal
            if (configu != null)
                asignaturasMenu += "<li><a class=\"menu grande\" href=\"asignaturas.jsp?asignatura=" + usuario.getAccesosUsuarioAsignatura().get(i).getAsignatura().getCodigo()
                        + "\">" + configu.getNombreAsignatura() + "</a></li>";
            else
                asignaturasMenu += "<li><a class=\"menu grande\">Asignatura no configurada</a></li>";
        }     
        asignaturasMenu += "</ul></li>";
        
        //Cuenta
        cuentaMenu += "<li><a class=\"menu\">Cuenta</a><ul>";
        cuentaMenu += "<li><a class=\"menu\" href=\"cambiarPassword.jsp?paso=1\">Cambiar contrase�a</a></li>";
        cuentaMenu += "</ul></li>";
        
        if (session.getAttribute("asignatura") != null)
        {
            AccesoUsuarioAsignaturaCEN accesoUsuarioCEN = new AccesoUsuarioAsignaturaCEN();
            
            //Preguntas
            preguntasMenu += "<li><a class=\"menu\">Preguntas</a><ul>";
            preguntasMenu += "<li><a class=\"menu\" href=\"nuevaPregunta.jsp\">Nueva pregunta</a></li>";
            preguntasMenu += "<li><a class=\"menu\" href=\"verPreguntas.jsp\">Ver preguntas</a></li>";
            preguntasMenu += "<li><a class=\"menu\" href=\"subirXML.jsp\">Cargar de un fichero</a></li>";
            preguntasMenu += "<li><a class=\"menu\" onClick=\"reclasificar()\">Reclasificar preguntas</a></li>";
            preguntasMenu += "</ul></li>";
            
            //Temas
            temasMenu += "<li><a class=\"menu\">Temas</a><ul>";
            temasMenu += "<li><a class=\"menu\" href=\"nuevoTema.jsp\">Nuevo tema</a></li>";
            temasMenu += "<li><a class=\"menu\" href=\"verTemas.jsp\">Ver temas</a></li>";
            temasMenu += "</ul></li>";
            
            //Ex�menes
            examenesMenu += "<li><a class=\"menu\">Ex�menes</a><ul>";
            examenesMenu += "<li><a class=\"menu\" href=\"nuevoExamen.jsp?paso=1\">Nuevo examen</a></li>";
            examenesMenu += "<li><a class=\"menu\" href=\"verExamenes.jsp\">Ver ex�menes</a></li>";
            examenesMenu += "</ul></li>";
            
            if (accesoUsuarioCEN.getAccesoByAsignaturaYUsuario((AsignaturaEN)session.getAttribute("asignatura"), usuario).isAdministrador())
            {
                //Administraci�n
                administracionMenu += "<li><a class=\"menu\">Administraci�n</a><ul>";
                administracionMenu += "<li><a class=\"menu\" href=\"usuarios.jsp\">Usuarios</a></li>";
                administracionMenu += "<li><a class=\"menu\" href=\"idiomas.jsp\">Idiomas</a></li>";
                administracionMenu += "</ul></li>";
            } 
        }
        
        //Salir
        acercaDeMenu += "<li><a class=\"menu\" href=\"acercaDe.jsp\">Acerca de</a></li>";
        
        //Salir
        salirMenu += "<li><a class=\"menu\" href=\"salir.jsp\">Salir</a></li>";
    }
%>

<ul id="menu">
    <li><a id="a_logo" href="index.jsp"><img id="logo_menu" src="images/logo.png" alt="logo" /></a></li>  
    <%=asignaturasMenu%>
    <%=preguntasMenu%>
    <%=temasMenu%>
    <%=examenesMenu%>
    <%=cuentaMenu%>
    <%=administracionMenu%>
    <%=acercaDeMenu%>
    <%=salirMenu%>
</ul>
