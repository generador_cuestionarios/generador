<%-- 
    Copyright (C) 2014 Ra�l Sempere Trujillo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Document   : consultasAjax
    Created on : 11-jun-2014, 15:56:00
    Author     : Ra�l Sempere Trujillo
--%>

<%@page import="java.util.stream.Stream"%>
<%-- 
    Este documento se emplea para realizar diversas acciones y consultas 
    al sistema utilizando Ajax. El parametro "accion" dentro de la consulta
    determina la acci�n a realizar.
--%>
 
<%@page import="java.util.concurrent.TimeUnit"%>
<%@page import="clases_generador.*"%>
<%@page import="CEN.*"%>
<%@page import="EN.*"%>
<%@page import="java.io.*"%>
<%@page import="java.util.Random"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.ArrayList"%>


<%
    request.setCharacterEncoding("UTF-8");
    String result = "";

    /**
     *   A�ade la pregunta pasada con el parametro "codigo" al examen
     *   que se est� creando.
    */
    if (request.getParameter("accion").equals("add"))
    {
        AsignaturaEN asignaturaEN = (AsignaturaEN)session.getAttribute("asignatura");
        PreguntaCEN preguntaCEN = new PreguntaCEN();
        ExamenPreguntaCEN examenPreguntaCEN = new ExamenPreguntaCEN();
        PreguntaEN preguntaEN = preguntaCEN.getPreguntaByCodigo(asignaturaEN, Integer.parseInt(request.getParameter("codigo")));
        
        if (request.getParameter("tipo").equals("Editar"))
        {
            ExamenEN examenEN = (ExamenEN)session.getAttribute("examen");
            ArrayList<ExamenPreguntaEN> preguntas = ((ArrayList<ExamenPreguntaEN>)session.getAttribute("preguntasExamen"));
            ExamenPreguntaEN examenPreguntaEN = new ExamenPreguntaEN();
            
            examenPreguntaEN.setExamen(examenEN);
            examenPreguntaEN.setPregunta(preguntaEN);
            
            if (!examenPreguntaCEN.existeEnLista(preguntas, preguntaEN))
                preguntas.add(examenPreguntaEN);
            
        }
        else
        {
            ArrayList<PreguntaEN> seleccionadas = ((ArrayList<PreguntaEN>)session.getAttribute("listaSeleccionadas"));

            if (!preguntaCEN.existeEnLista(seleccionadas, preguntaEN))
                seleccionadas.add(preguntaEN);
        }
    }
    
    /**
     *   Quita la pregunta pasada con el parametro "codigo" del examen
     *   que se est� creando.
    */
    else if (request.getParameter("accion").equals("del"))
    {
        AsignaturaEN asignaturaEN = (AsignaturaEN)session.getAttribute("asignatura");
        PreguntaCEN preguntaCEN = new PreguntaCEN();
        ExamenPreguntaCEN examenPreguntaCEN = new ExamenPreguntaCEN();
        PreguntaEN preguntaEN = preguntaCEN.getPreguntaByCodigo(asignaturaEN, Integer.parseInt(request.getParameter("codigo")));
        
        if (request.getParameter("tipo").equals("Editar"))
        {
            ArrayList<ExamenPreguntaEN> preguntas = ((ArrayList<ExamenPreguntaEN>)session.getAttribute("preguntasExamen"));
            examenPreguntaCEN.borrarDeLista(preguntas, preguntaEN);
        }
        else
        {
            ArrayList<PreguntaEN> seleccionadas = ((ArrayList<PreguntaEN>)session.getAttribute("listaSeleccionadas"));
            preguntaCEN.borrarDeLista(seleccionadas, preguntaEN);
        }
    }
    
    /**
     *   Sustituye la pregunta del par�metro "intercambio" por la pregunta del
     *   par�metro "codigo" dentro del examen que se est� creando.
    */
    else if (request.getParameter("accion").equals("intercambiar"))
    {
        AsignaturaEN asignaturaEN = (AsignaturaEN)session.getAttribute("asignatura");
        PreguntaCEN preguntaCEN = new PreguntaCEN();
        ExamenPreguntaCEN examenPreguntaCEN = new ExamenPreguntaCEN();
        PreguntaEN preguntaEN = preguntaCEN.getPreguntaByCodigo(asignaturaEN, Integer.parseInt(request.getParameter("codigo")));
        
        if (request.getParameter("tipo").equals("Editar"))
        {
            ArrayList<ExamenPreguntaEN> preguntas = ((ArrayList<ExamenPreguntaEN>)session.getAttribute("preguntasExamen"));
            PreguntaEN preguntaIntercambio = preguntaCEN.getPreguntaByCodigo(asignaturaEN, Integer.parseInt(request.getParameter("intercambio")));
            examenPreguntaCEN.sustituirEnLista(preguntas, preguntaIntercambio, preguntaEN);
        }
        else
        {
            ArrayList<PreguntaEN> seleccionadas = ((ArrayList<PreguntaEN>)session.getAttribute("listaSeleccionadas"));
            PreguntaEN preguntaIntercambio = preguntaCEN.getPreguntaByCodigo(asignaturaEN, Integer.parseInt(request.getParameter("intercambio")));
            preguntaCEN.sustituirEnLista(seleccionadas, preguntaIntercambio, preguntaEN);
        }
    }
    
    /**
     *   Devuelve el n�mero de preguntas disponibles (aplicando una fecha de 
     *   ultimo uso para una lista de temas pasados en el par�metro "temas" 
     *   separados por comas.
    */
    if  (request.getParameter("accion").equals("cantidadPreguntas"))
    {
            AsignaturaEN asignaturaEN = (AsignaturaEN)session.getAttribute("asignatura");
            ArrayList<PreguntaEN> seleccionadas = ((ArrayList<PreguntaEN>)session.getAttribute("listaSeleccionadas"));
            ClasificacionTemaCEN clasificacionTemaCEN = new ClasificacionTemaCEN();
            TemaCEN temaCEN = new TemaCEN();
            TemaEN temaEN;
            String temas[] = request.getParameter("temas").split(",");

            if (request.getParameter("ultimoUso") != "")
            {
                String fechaStr[] = request.getParameter("ultimoUso").split("-");
                Calendar ultimoUso = Calendar.getInstance();
                ultimoUso.set(Integer.parseInt(fechaStr[0]), Integer.parseInt(fechaStr[1]), Integer.parseInt(fechaStr[2]));

                for (String temaStr : temas)
                {
                    temaEN = temaCEN.getTemaByCodigo(asignaturaEN, Integer.parseInt(temaStr));
                    ArrayList<ClasificacionTemaEN> clasificaciones = clasificacionTemaCEN.getClasificacionesDisponiblesByTemaYUltimoUso(temaEN, ultimoUso);

                    //Quitamos las seleccionadas
                    for (PreguntaEN pregunta : seleccionadas)
                        clasificacionTemaCEN.borrarDeLista(clasificaciones, pregunta);

                    result += clasificaciones.size() + ",";
                }
            }
            else
            {
                for (String temaStr : temas)
                {
                    temaEN = temaCEN.getTemaByCodigo(asignaturaEN, Integer.parseInt(temaStr));
                    ArrayList<ClasificacionTemaEN> clasificaciones = clasificacionTemaCEN.getClasificacionesDisponiblesByTema(temaEN);

                    //Quitamos las seleccionadas
                    for (PreguntaEN pregunta : seleccionadas)
                        clasificacionTemaCEN.borrarDeLista(clasificaciones, pregunta);

                    result += clasificaciones.size() + ",";
                }
            }
    }
    
    /**
     *   Sustituye la pregunta del examen pasada por en el par�metro "pregunta" 
     *   por otra obtenida del mismo tema en caso de estar clasificada, 
     *   o una aleatoria en caso de no estarlo.
    */
    else if (request.getParameter("accion").equals("generarPregunta"))
    {
        AsignaturaEN asignaturaEN = (AsignaturaEN)session.getAttribute("asignatura");
        ClasificacionTemaCEN clasificacionTemaCEN = new ClasificacionTemaCEN();
        PreguntaCEN preguntaCEN = new PreguntaCEN();
        ArrayList<PreguntaEN> listaSeleccionadas = ((ArrayList<PreguntaEN>)session.getAttribute("listaSeleccionadas"));
        PreguntaEN preguntaEN = preguntaCEN.getPreguntaByCodigo(asignaturaEN, Integer.parseInt(request.getParameter("pregunta")));
        preguntaCEN.rellenarClasificacion(preguntaEN);

        //Si la pregunta tiene la clasificaci�n revisada se coge otra pregunta del mismo tema
        if (preguntaEN.getClasificacion() != null && preguntaEN.getClasificacion().isRevisada())
        {
            TemaEN temaEN = preguntaEN.getClasificacion().getTema();
            ArrayList<ClasificacionTemaEN> clasificaciones;

            clasificaciones = clasificacionTemaCEN.getClasificacionesDisponiblesByTema(temaEN);

            //De esas clasificaciones hay que quitar las que ya han sido seleccionadas en el examen
            for (PreguntaEN pregunta : listaSeleccionadas)
                clasificacionTemaCEN.borrarDeLista(clasificaciones, pregunta);

            if (clasificaciones.size() > 0)
            {
                //Ahora debemos seleccionar una pregunta de la lista
                Random rand = new Random();
                int pos = rand.nextInt(clasificaciones.size());

                //Sustituimos la pregunta en las seleccionadas
                preguntaCEN.sustituirEnLista(listaSeleccionadas, preguntaEN, clasificaciones.get(pos).getPregunta());

                result = "true";
            }
            else
                result = "false";
        }
        //Si no tiene clasificaci�n o esta sin revisar se coge una pregunta de cualquier tema o sin revisar
        else
        {  
            ArrayList<PreguntaEN> preguntas;

            preguntas = preguntaCEN.getPreguntasHabilitadasByAsignatura(asignaturaEN);

            //De esas clasificaciones hay que quitar las que ya han sido seleccionadas en el examen
            for (PreguntaEN pregunta : listaSeleccionadas)
                preguntaCEN.borrarDeLista(preguntas, pregunta);

            if (preguntas.size() > 0)
            {
                //Ahora debemos seleccionar una pregunta de la lista
                Random rand = new Random();
                int pos = rand.nextInt(preguntas.size());

                //Sustituimos la pregunta en las seleccionadas
                preguntaCEN.sustituirEnLista(listaSeleccionadas, preguntaEN, preguntas.get(pos));

                result = "true";
            }
            else
                result = "false";
        }
    }
    
    /**
     *   Sustituye la pregunta pasada por en el par�metro "pregunta" de la edici�n 
     *   por otra obtenida del mismo tema en caso de estar clasificada,  
     *   o una aleatoria en caso de no estarlo.
    */
    else if (request.getParameter("accion").equals("generarPreguntaEditar"))
    {
        AsignaturaEN asignaturaEN = (AsignaturaEN)session.getAttribute("asignatura");
        ClasificacionTemaCEN clasificacionTemaCEN = new ClasificacionTemaCEN();
        PreguntaCEN preguntaCEN = new PreguntaCEN();
        ExamenPreguntaCEN examenPreguntaCEN = new ExamenPreguntaCEN();
        ArrayList<ExamenPreguntaEN> listaSeleccionadas = ((ArrayList<ExamenPreguntaEN>)session.getAttribute("preguntasExamen"));
        PreguntaEN preguntaEN = preguntaCEN.getPreguntaByCodigo(asignaturaEN, Integer.parseInt(request.getParameter("pregunta")));
        preguntaCEN.rellenarClasificacion(preguntaEN);

        //Si la pregunta tiene la clasificaci�n revisada se coge otra pregunta del mismo tema
        if (preguntaEN.getClasificacion() != null && preguntaEN.getClasificacion().isRevisada())
        {
            TemaEN temaEN = preguntaEN.getClasificacion().getTema();
            ArrayList<ClasificacionTemaEN> clasificaciones;

            clasificaciones = clasificacionTemaCEN.getClasificacionesDisponiblesByTema(temaEN);

            //De esas clasificaciones hay que quitar las que ya han sido seleccionadas en el examen
            for (ExamenPreguntaEN pregunta : listaSeleccionadas)
                clasificacionTemaCEN.borrarDeLista(clasificaciones, pregunta.getPregunta());

            if (clasificaciones.size() > 0)
            {
                //Ahora debemos seleccionar una pregunta de la lista
                Random rand = new Random();
                int pos = rand.nextInt(clasificaciones.size());

                //Sustituimos la pregunta en las seleccionadas
                examenPreguntaCEN.sustituirEnLista(listaSeleccionadas, preguntaEN, clasificaciones.get(pos).getPregunta());

                result = "true";
            }
            else
                result = "false";
        }
        //Si no tiene clasificaci�n o esta sin revisar se coge una pregunta de cualquier tema o sin revisar
        else
        {  
            ArrayList<PreguntaEN> preguntas;

            preguntas = preguntaCEN.getPreguntasHabilitadasByAsignatura(asignaturaEN);

            //De esas clasificaciones hay que quitar las que ya han sido seleccionadas en el examen
            for (ExamenPreguntaEN pregunta : listaSeleccionadas)
                preguntaCEN.borrarDeLista(preguntas, pregunta.getPregunta());

            if (preguntas.size() > 0)
            {
                //Ahora debemos seleccionar una pregunta de la lista
                Random rand = new Random();
                int pos = rand.nextInt(preguntas.size());

                //Sustituimos la pregunta en las seleccionadas
                examenPreguntaCEN.sustituirEnLista(listaSeleccionadas, preguntaEN, preguntas.get(pos));

                result = "true";
            }
            else
                result = "false";
        }
    }
    
    /**
     *   Elimina el examen (si no esta analizado) pasado en el par�metro "examen".
    */
    else if (request.getParameter("accion").equals("borrarExamen"))
    {
        ExamenCEN examenCEN = new ExamenCEN();
        ExamenPreguntaCEN examenPreguntaCEN = new ExamenPreguntaCEN();
        AsignaturaEN asignaturaEN = (AsignaturaEN)session.getAttribute("asignatura");
        int codigo = Integer.parseInt(request.getParameter("examen"));
        
        ExamenEN examenEN = examenCEN.getExamenByCodigo(asignaturaEN, codigo);
        
        if (!examenEN.isAnalizado())
        {
            //Eliminamos las preguntas primero para que se actualice el ultimo uso
            //y las veces que han sido utilizadas las mismas
            examenCEN.rellenarPreguntas(examenEN);
            examenPreguntaCEN.eliminarPreguntasDeExamen(examenEN);
            examenCEN.Delete(examenEN);
            result = "true";
        }
        else
            result = "false";
    }
    
    /**
     *   Genera el c�digo LaTeX para un examen e idioma pasado por par�mentro,
     *   se comprimen todas las modalidades en zip y devuelve la ruta de descarga
    */
    else if (request.getParameter("accion").equals("generarLatex"))
    {
        String CATALINA_HOME = System.getenv("CATALINA_HOME");
        
        if (CATALINA_HOME.endsWith("/") || CATALINA_HOME.endsWith("\\"))
            CATALINA_HOME = CATALINA_HOME.substring(0, CATALINA_HOME.length()-1);
        
        ExamenCEN examenCEN = new ExamenCEN();
        IdiomaCEN idiomaCEN = new IdiomaCEN();
        
        AsignaturaEN asignaturaEN = (AsignaturaEN)session.getAttribute("asignatura");
        UsuarioEN usuario = (UsuarioEN)session.getAttribute("usuario");
        ExamenEN examenEN = examenCEN.getExamenByCodigo(asignaturaEN, Integer.parseInt(request.getParameter("examen")));
        IdiomaEN idiomaEN = idiomaCEN.getIdiomaByCodigo(Integer.parseInt(request.getParameter("idioma")));

        ArrayList<String> rutasZip = new ArrayList<String>();
        ArrayList<String> nombresZip = new ArrayList<String>();
        
        String nombreFichero = "examen" + usuario.getCodigo() + idiomaEN.getCodigo() + "_" + examenEN.getCodigo();
        String ruta = CATALINA_HOME + "/webapps/" + ConfigManager.getWebApp() + "/temp/" + nombreFichero;
        
        //Generamos los LaTeX
        for (int i = 1; i <= examenEN.getModalidades(); i++)
        {
            String rutaLatex = ruta + "_" + i + ".tex";
            rutasZip.add(rutaLatex);
            nombresZip.add(nombreFichero + "_" + i + ".tex");
            
            String LaTeX = ExamenLaTeX.generar(examenEN, idiomaEN, i);

            BufferedWriter fichero = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(rutaLatex), "utf-8"));
            fichero.write(LaTeX);
            fichero.close();
        }
        
        //Lo convertimos a zip
        Utiles.crearZip(rutasZip, nombresZip, ruta + "_TEX.zip");
        
        //Borramos los ficheros latex que ya no sirven
        for (String r : rutasZip)
        {
            File archivo = new File(r);

            if (!archivo.isDirectory() && archivo.exists())
                archivo.delete();
        }
        
        result = "./temp/" + nombreFichero + "_TEX.zip";
    }
    
    /**
     *   Genera el c�digo LaTeX para un examen e idioma pasado por par�mentro,
     *   ejecuta en la m�quina el compilador de latex a pdf "pdflatex" y comprime
     *   en zip todos los objetos pdf generados. Si hay un error en la compilaci�n
     *   no se genera zip alguno y se devuelve errorDescargaZip.jsp
    */
    else if (request.getParameter("accion").equals("generarPdf"))
    {
        String CATALINA_HOME = System.getenv("CATALINA_HOME");
        
        if (CATALINA_HOME.endsWith("/") || CATALINA_HOME.endsWith("\\"))
            CATALINA_HOME = CATALINA_HOME.substring(0, CATALINA_HOME.length()-1);
        
        ExamenCEN examenCEN = new ExamenCEN();
        IdiomaCEN idiomaCEN = new IdiomaCEN();
        
        AsignaturaEN asignaturaEN = (AsignaturaEN)session.getAttribute("asignatura");
        UsuarioEN usuario = (UsuarioEN)session.getAttribute("usuario");
        ExamenEN examenEN = examenCEN.getExamenByCodigo(asignaturaEN, Integer.parseInt(request.getParameter("examen")));
        IdiomaEN idiomaEN = idiomaCEN.getIdiomaByCodigo(Integer.parseInt(request.getParameter("idioma")));
        String rutaCompletaD = CATALINA_HOME + "/webapps/" + ConfigManager.getWebApp() + "/temp";

        ArrayList<String> rutasZip = new ArrayList<String>();
        ArrayList<String> nombresZip = new ArrayList<String>();
        
        String nombreFichero = "examen" + usuario.getCodigo() + idiomaEN.getCodigo()+ examenEN.getCodigo();
        String ruta = CATALINA_HOME + "/webapps/" + ConfigManager.getWebApp() + "/temp/" + nombreFichero;
        
        //Generamos los LaTeX
        for (int i = 1; i <= examenEN.getModalidades(); i++)
        {
            String rutaLatex = ruta + "_" + i + ".tex";
            String rutaPdf = ruta + "_" + i + ".pdf";
            rutasZip.add(rutaPdf);
            nombresZip.add(nombreFichero + "_" + i + ".pdf");
            
            String LaTeX = ExamenLaTeX.generar(examenEN, idiomaEN, i);

            BufferedWriter fichero = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(rutaLatex), "utf-8"));
            fichero.write(LaTeX);
            fichero.close();
            Process p = null;
            
            try
            {  
                //Generamos los PDF
                String[] comando = new String[] {"pdflatex", "-output-directory=" + rutaCompletaD, rutaCompletaD + "/" + nombreFichero + "_" + i + ".tex"};
                //Runtime.getRuntime().exec(command);
                p = Runtime.getRuntime().exec(comando);
                BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));  

                in.lines();
  
                p.waitFor(1, TimeUnit.SECONDS);
                p.destroy();
            }
            catch (Exception e)
            {  
                if (p != null)
                    p.destroy();
                
                e.printStackTrace();  
            }
        }
        
        //Comprobamos si se han creado todos los ficheros
        for (int i = 0; i < rutasZip.size(); i++)
        {
            if (!IO.existe(rutasZip.get(i)))
            {
                rutasZip.remove(i);
                i--;
            }
        }
        
        if (rutasZip.size() > 0)
        {
            //Lo convertimos a zip
            Utiles.crearZip(rutasZip, nombresZip, ruta + "_PDF.zip");
            result = "./temp/" + nombreFichero + "_PDF.zip";

            //Borramos los ficheros pdf
            for (String r : rutasZip)
            {
                //Borramos el fichero
                File archivo = new File(r);
                archivo.delete();
            }
        }
        else
        {
            result = "errorDescargarZip.jsp?examen=" + examenEN.getCodigo();
        }
    }
    
    /**
     *   Gestiona la traducci�n de una pregunta en los formularios de revisi�n
    */
    else if (request.getParameter("accion").equals("traduccionPregunta"))
    {
        AsignaturaEN asignaturaEN = (AsignaturaEN)session.getAttribute("asignatura");
        
        TraduccionPreguntaCEN traduccionPreguntaCEN = new TraduccionPreguntaCEN();
        TraduccionOpcionCEN traduccionOpcionCEN = new TraduccionOpcionCEN();
        PreguntaCEN preguntaCEN = new PreguntaCEN();
        ConfiguracionAsignaturaIdiomaCEN configuracionAsignaturaIdiomaCEN = new ConfiguracionAsignaturaIdiomaCEN();    
        ArrayList<ConfiguracionAsignaturaIdiomaEN> confs = configuracionAsignaturaIdiomaCEN.getConfiguracionesAsignaturaIdiomaByAsignatura(asignaturaEN);
        //Separamos la principal de las demas configuraciones
        ConfiguracionAsignaturaIdiomaEN confPrincipal = null;
        
        for (int i = 0; i < confs.size(); i++)
        {
            if (confs.get(i).isIdiomaPrincipal())
            {
                confPrincipal = confs.get(i);
                confs.remove(i);
            }
        }
        ConfiguracionAsignaturaIdiomaEN otroIdiomaConf = null;
        PreguntaEN preguntaEN = preguntaCEN.getPreguntaByCodigo(asignaturaEN, Integer.parseInt(request.getParameter("pregunta")));
        TraduccionPreguntaEN traduccionPreguntaOtroIdioma;
        TraduccionPreguntaEN traduccionPreguntaPrincipal;
        ArrayList<TraduccionOpcionEN> traduccionOpcionesPrincipal;
        ArrayList<TraduccionOpcionEN> traduccionOpcionesOtroIdioma;
        
        //Si el parametro idioma est� vac�o 
        //y la lista no est� vac�a cogemos el primer idioma de la lista
        if (request.getParameter("idioma").equals("") && confs.size() > 0)
        {
            otroIdiomaConf = confs.get(0);
        }
        //Si el parametro no esta vac�o buscamos el idioma en la lista
        else if (!request.getParameter("idioma").equals(""))
        {
            for (int i = 0; i < confs.size(); i++)
            {
                if (confs.get(i).getIdioma().getCodigo() == Integer.parseInt(request.getParameter("idioma")))
                {
                     otroIdiomaConf = confs.get(i);
                }
            } 
        }
        
        //En este punto tenemos en otroIdiomaConf la conf del idioma que se va a revisar, si es nulo nos salimos
        if (otroIdiomaConf != null)
        { 
            //Obtenemos la pregunta y las opciones en el idioma principal
            traduccionPreguntaPrincipal = traduccionPreguntaCEN.getTraduccionPreguntaByPreguntaEIdioma(preguntaEN, confPrincipal.getIdioma());
            traduccionOpcionesPrincipal = traduccionOpcionCEN.getTraduccionesOpcionByPreguntaEIdioma(preguntaEN, confPrincipal.getIdioma());
            
            //Ahora tenemos que comprobar si tenemos traducci�n para el idioma que queremos 
            //y si no existe se crea
            traduccionPreguntaOtroIdioma = traduccionPreguntaCEN.getTraduccionPreguntaByPreguntaEIdioma(preguntaEN, otroIdiomaConf.getIdioma());
            
            //Comprobamos si existe traduccion
            if (traduccionPreguntaOtroIdioma != null)
            {
                //Si existe, obtenemos las opciones
                traduccionOpcionesOtroIdioma = traduccionOpcionCEN.getTraduccionesOpcionByPreguntaEIdioma(preguntaEN, traduccionPreguntaOtroIdioma.getIdioma());
            }
            //Si no existe, tenemos que traducir en este momento
            else
            {
                Traduccion traductor = new Traduccion(ConfigManager.getApertiumPackages());
                traductor.seleccionarTraduccion(otroIdiomaConf.getTraductorApertium());
                
                //Primero las preguntas
                traduccionPreguntaOtroIdioma = new TraduccionPreguntaEN();
                traduccionPreguntaOtroIdioma.setRevisada(false);
                traduccionPreguntaOtroIdioma.setIdioma(otroIdiomaConf.getIdioma());
                traduccionPreguntaOtroIdioma.setPregunta(preguntaEN);
                traduccionPreguntaOtroIdioma.setEnunciado(traductor.traducir(traduccionPreguntaPrincipal.getEnunciado()));
                
                traduccionPreguntaCEN.New(traduccionPreguntaOtroIdioma);
                
                //Luego las opciones
                traduccionOpcionesOtroIdioma = new ArrayList<TraduccionOpcionEN>();
                        
                for (TraduccionOpcionEN traduccionOpcionPrincipal : traduccionOpcionesPrincipal)
                {
                    TraduccionOpcionEN traduccionOpcionOtroIdioma = new TraduccionOpcionEN();
                    
                    traduccionOpcionOtroIdioma.setIdioma(otroIdiomaConf.getIdioma());
                    traduccionOpcionOtroIdioma.setOpcion(traduccionOpcionPrincipal.getOpcion());
                    traduccionOpcionOtroIdioma.setEnunciado(traductor.traducir(traduccionOpcionPrincipal.getEnunciado()));
                    traduccionOpcionesOtroIdioma.add(traduccionOpcionOtroIdioma);
                    
                    traduccionOpcionCEN.New(traduccionOpcionOtroIdioma);
                }
            }
            
            String examen = "";
            String disabled = "";
            
            //Si estamos procesando las preguntas de un examen escribimos un paramentro
            //Para que se pase en el submit
            if (request.getParameter("examen") != null && !request.getParameter("examen").equals(""))
            {
                examen = "&examen=" + request.getParameter("examen");
                disabled = "disabled";
            }
            
            result += "<div id=\"caja\">";
            result += "<h2 style=\"text-align: center\">Revisi�n de la pregunta en \"" + otroIdiomaConf.getIdioma().getNombre() + "\"</h2><br />";
            result += "  <form name=\"revisarTraduccionPregunta\" action=\"revisarTraduccionPregunta.jsp?pregunta=" + traduccionPreguntaOtroIdioma.getPregunta().getCodigo()
                    + "&idioma=" + traduccionPreguntaOtroIdioma.getIdioma().getCodigo() + "&guardar=true" + examen + "\" method=\"POST\" accept-charset=\"utf-8\">";
            result += "     <div>Idioma a revisar: <select onChange=\"cambiarIdiomaRevision(" + preguntaEN.getCodigo() + ")\" name=\"idiomas\" style=\"min-width: 250px; max-width: 500px\"" + disabled + ">";     
            String revisado = "";
            for (int i = 0; i < confs.size(); i++)
            {
                revisado = "";
                
                TraduccionPreguntaEN traduccionPreguntaEN = traduccionPreguntaCEN.getTraduccionPreguntaByPreguntaEIdioma(preguntaEN, confs.get(i).getIdioma());
                
                if (traduccionPreguntaEN == null || !traduccionPreguntaEN.isRevisada())
                    revisado = "(sin revisar) ";
                
                if (confs.get(i).getIdioma().getCodigo() == otroIdiomaConf.getIdioma().getCodigo())
                    result += "      <option value=\"" + confs.get(i).getIdioma().getCodigo() + "\" selected>" + revisado + confs.get(i).getIdioma().getNombre() + "</option>";
                else
                    result += "      <option value=\"" + confs.get(i).getIdioma().getCodigo() + "\">" + revisado + confs.get(i).getIdioma().getNombre() + "</option>";
            }
            
            result += " </select><br /><br />";
            if (request.getParameter("examen") != null && !request.getParameter("examen").equals(""))
            {
                result += "      <div style=\"float: right; margin-right: 2px\" id=\"boton_pregunta\" onClick=\"location.href='verTraduccionesExamen.jsp?examen=" + request.getParameter("examen") + "'\">Volver</div>";
            }
            else
                result += "      <div style=\"float: right; margin-right: 2px\" id=\"boton_pregunta\" onClick=\"location.href='verPreguntas.jsp'\">Volver</div>";
            result += "      <div style=\"float: right; margin-right: 10px\" id=\"boton_pregunta\" onClick=\"guardarTraduccionPregunta()\">Guardar</div>";
            result += "      <br />";
            
            result += "      <h3 class=\"margen\">Enunciado:</h3>";
            result += "      <div class=\"borde\">";
            result += "          <div class=\"margen\">Enunciado original:</div>";
            result += "          <div class=\"margen\"><textarea name=\"enunciado\" cols=\"82\" rows=\"6\" disabled>" + Utiles.textoHTML(traduccionPreguntaPrincipal.getEnunciado()) + "</textarea></div>";
            result += "          <div class=\"margen\">Traducci�n enunciado:</div>";
            result += "          <div class=\"margen\"><textarea name=\"traduccionEnunciado\" cols=\"82\" rows=\"6\" >" + Utiles.textoHTML(traduccionPreguntaOtroIdioma.getEnunciado()) + "</textarea></div>";
            result += "      </div>";
            result += "      <br />";
            result += "      <h3 class=\"margen\">Opciones:</h3>";
            result += "      <div class=\"borde\">";

            //ponemos las opciones
            for (int i = 1; i <= asignaturaEN.getNumeroOpciones(); i++)
            {
                result += "<div class=\"margen\">Opci�n " + i + ":<br /><textarea style=\"vertical-align: middle\" name=\"opc" + i + "\" cols=\"82\" rows=\"3\" disabled>" + Utiles.textoHTML(traduccionOpcionesPrincipal.get(i-1).getEnunciado()) + "</textarea>";
                result += "</div>";
                result += "<div class=\"margen\">Traducci�n opci�n " + i + ":<br /><textarea style=\"vertical-align: middle\" name=\"traduccionOpc" + i + "\" cols=\"82\" rows=\"3\">" + Utiles.textoHTML(traduccionOpcionesOtroIdioma.get(i-1).getEnunciado()) + "</textarea>";
                result += "</div><br />";
            }

            result += "      </div>";
            result += "      <br />";
            result += "      <div id=\"error\"></div>";
            result += "      <br />";
        }
    }
    
    /**
     *   Valida la clasificaci�n de un tema dentro del formulario de detalle
    */
    else if (request.getParameter("accion").equals("validarClasificacion"))
    {
        PreguntaCEN preguntaCEN = new PreguntaCEN();
        ClasificacionTemaCEN clasificacionTemaCEN = new ClasificacionTemaCEN();
        AsignaturaEN asignaturaEN = (AsignaturaEN)session.getAttribute("asignatura");
        PreguntaEN preguntaEN = preguntaCEN.getPreguntaByCodigo(asignaturaEN, Integer.parseInt(request.getParameter("pregunta")));
        preguntaCEN.rellenarClasificacion(preguntaEN);
        
        preguntaEN.getClasificacion().setRevisada(true);
        clasificacionTemaCEN.Modify(preguntaEN.getClasificacion());
    }
    
    /**
     *   Valida la clasificaci�n de un tema dentro de los listados donde
     *   se puede seleccionar un tema en un input select
    */
    else if (request.getParameter("accion").equals("validarClasificacionTema"))
    {
        PreguntaCEN preguntaCEN = new PreguntaCEN();
        ClasificacionTemaCEN clasificacionTemaCEN = new ClasificacionTemaCEN();
        AsignaturaEN asignaturaEN = (AsignaturaEN)session.getAttribute("asignatura");
        PreguntaEN preguntaEN = preguntaCEN.getPreguntaByCodigo(asignaturaEN, Integer.parseInt(request.getParameter("pregunta")));
        TemaCEN temaCEN = new TemaCEN();
        TemaEN temaEN = temaCEN.getTemaByCodigo(asignaturaEN, Integer.parseInt(request.getParameter("tema")));
        preguntaCEN.rellenarClasificacion(preguntaEN);
        
        ClasificacionTemaEN clasificacionTemaEN = preguntaEN.getClasificacion();
        
        if (clasificacionTemaEN != null)
        {
            clasificacionTemaEN.setTema(temaEN);
            clasificacionTemaEN.setRevisada(true);
            
            clasificacionTemaCEN.Modify(preguntaEN.getClasificacion());
        }
        else
        {
            clasificacionTemaEN = new ClasificacionTemaEN();
            
            clasificacionTemaEN.setPregunta(preguntaEN);
            clasificacionTemaEN.setTema(temaEN);
            clasificacionTemaEN.setRevisada(true);
            
            clasificacionTemaCEN.New(clasificacionTemaEN);
        }
        
        result = temaEN.getNombre();
    }
    
    /**
     *   Elimina la pregunta pasada en el par�metro "pregunta" del sistema
     */
    else if (request.getParameter("accion").equals("eliminarPregunta"))
    {
        PreguntaCEN preguntaCEN = new PreguntaCEN();
        AsignaturaEN asignaturaEN = (AsignaturaEN)session.getAttribute("asignatura");
        PreguntaEN preguntaEN = preguntaCEN.getPreguntaByCodigo(asignaturaEN, Integer.parseInt(request.getParameter("pregunta")));
        
        //Comprobamos si se utiliza en alg�n examen
        preguntaCEN.rellenarExamenes(preguntaEN);
        
        if (preguntaEN.getExamenes().size() > 0)
        {
            preguntaEN.setHabilitada(false);
            preguntaCEN.Modify(preguntaEN);
            result = "false";
        }
        else
        {
            preguntaCEN.Delete(preguntaEN);
            result = "true";
        }
    }
    
    /**
     *   Gestiona la traducci�n de un examen
    */
    else if (request.getParameter("accion").equals("traduccionExamen"))
    {
        PreparacionExamenIdiomaCEN preparacionExamenIdiomaCEN = new PreparacionExamenIdiomaCEN();
        ExamenCEN examenCEN = new ExamenCEN();
        IdiomaCEN idiomaCEN = new IdiomaCEN();
        ConfiguracionAsignaturaIdiomaCEN configuracionAsignaturaIdiomaCEN = new ConfiguracionAsignaturaIdiomaCEN();
        TraduccionInstruccionCEN traduccionInstruccionCEN = new TraduccionInstruccionCEN();
        
        AsignaturaEN asignaturaEN = (AsignaturaEN)session.getAttribute("asignatura");
        IdiomaEN idiomaEN = idiomaCEN.getIdiomaByCodigo(Integer.parseInt(request.getParameter("idioma")));
        ExamenEN examenEN = examenCEN.getExamenByCodigo(asignaturaEN, Integer.parseInt(request.getParameter("examen")));
        ConfiguracionAsignaturaIdiomaEN confPrincipal = configuracionAsignaturaIdiomaCEN.getConfiguracionIdiomaPrincipal(asignaturaEN);
        ConfiguracionAsignaturaIdiomaEN otroIdiomaConf = configuracionAsignaturaIdiomaCEN.getConfiguracionAsignaturaIdiomaByAsignaturaEIdioma(asignaturaEN, idiomaEN);
        
        PreparacionExamenIdiomaEN preparacionPrincipal = preparacionExamenIdiomaCEN.getPreparacionExamenByExamenEIdioma(examenEN, confPrincipal.getIdioma());
        
        //Preparamos el traductor
        Traduccion traductor = new Traduccion(ConfigManager.getApertiumPackages());
        traductor.seleccionarTraduccion(otroIdiomaConf.getTraductorApertium());
        examenCEN.rellenarInstrucciones(examenEN);
                
        result += "<form name=\"traduccionExamen\" action=\"nuevaPreparacionExamen.jsp?examen=" + examenEN.getCodigo() + "&idioma=" + idiomaEN.getCodigo() + "\" method=\"POST\" accept-charset=\"utf-8\">";
        
        result += "      <div style=\"float: right; margin-right: 2px\" class=\"boton_pregunta\" onClick=\"location.href='verTraduccionesExamen.jsp?examen=" + examenEN.getCodigo() + "'\">Volver</div>";
        result += "      <div style=\"float: right; margin-right: 10px\" class=\"boton_pregunta\" onClick=\"guardarTraduccionExamen()\">Guardar</div>";
        result += "      <br /><br />";
        
        //Datos del examen
        result += "<h3 class=\"margen\">Datos del examen:</h3>";
        result += "<div class=\"borde\">";
        result += "<div class=\"margen\">Nombre del examen:</div>";
        result += "<div class=\"margen\"><input type=\"text\" size=\"80\" value=\"" + preparacionPrincipal.getTraduccionNombre() + "\"disabled /></div>";
        result += "<div class=\"margen\">Traducci�n nombre del examen:</div>";
        result += "<div class=\"margen\"><input type=\"text\" id=\"nombre\" name=\"nombre\" size=\"80\" value=\"" + traductor.traducir(preparacionPrincipal.getTraduccionNombre()) + "\" /></div><br />";
        result += "<div class=\"margen\">Fecha:</div>";
        result += "<div class=\"margen\"><input type=\"text\" size=\"50\" value=\"" + preparacionPrincipal.getTraduccionFecha()+ "\"disabled /></div>";
        result += "<div class=\"margen\">Traducci�n fecha:</div>";
        result += "<div class=\"margen\"><input type=\"text\" id=\"fecha\" name=\"fecha\" size=\"50\" value=\"" + traductor.traducir(preparacionPrincipal.getTraduccionFecha()) + "\" /></div><br />";
        result += "<div class=\"margen\">Duraci�n:</div>";
        result += "<div class=\"margen\"><input type=\"text\" size=\"50\" value=\"" + preparacionPrincipal.getTraduccionDuracion()+ "\"disabled /></div>";
        result += "<div class=\"margen\">Traducci�n duraci�n:</div>";
        result += "<div class=\"margen\"><input type=\"text\" id=\"duracion\" name=\"duracion\" size=\"50\" value=\"" + traductor.traducir(preparacionPrincipal.getTraduccionDuracion()) + "\" /></div><br />";
        result += "</div><br />";
        
        //Datos e las instrucciones
        result += "<h3 class=\"margen\">Instrucciones del examen:</h3>";
        result += "<div class=\"borde\">";
 
        for (int i = 0 ; i < examenEN.getInstrucciones().size(); i++)
        {
            TraduccionInstruccionEN traduccionInstruccionEN = traduccionInstruccionCEN.getTraduccionInstruccionByInstruccionEIdioma(examenEN.getInstrucciones().get(i), confPrincipal.getIdioma());

            result += "<div class=\"margen\">Instruccion " + (i+1) + ":</div>";
            result += "<div class=\"margen\"><textarea class=\"instruccion\" style=\"vertical-align: top\" cols=\"70\" rows=\"3\" disabled>" + Utiles.textoHTML(traduccionInstruccionEN.getEnunciado()) + "</textarea>";
            result += "</div>";
            result += "<div class=\"margen\">Traducci�n instruccion " + (i+1) + ":</div>";
            result += "<div class=\"margen\"><textarea class=\"instruccion\" style=\"vertical-align: top\" id=\"inst" + (i+1) + "\" name=\"inst" + (i+1) + "\" cols=\"70\" rows=\"3\">" + Utiles.textoHTML(traductor.traducir(traduccionInstruccionEN.getEnunciado())) + "</textarea>";
            result += "</div><br />";
        }
        
        //Otras opciones
        result += "</div><br />";
        result += "<h3 class=\"margen\">Otras opciones:</h3>";
        result += "<div class=\"borde\">";
        result += "<div class=\"margen\"><input type=\"checkbox\" name=\"revisarPreguntas\" checked> Revisar las preguntas del examen sin traducir al finalizar.</div>";
        
        result += "</div><br />";
        result += "</form>";
        
        //Botones y div error del final
        result += "      <div id=\"error\"></div>";
        result += "      <br />";
        result += "      <div style=\"float: right; margin-right: 2px\" class=\"boton_pregunta\" onClick=\"location.href='verTraduccionesExamen.jsp?examen=" + examenEN.getCodigo() + "'\">Volver</div>";
        result += "      <div style=\"float: right; margin-right: 10px\" class=\"boton_pregunta\" onClick=\"guardarTraduccionExamen()\">Guardar</div>";
        result += "      <br /><br />";
    }
    
    /**
     *   Elimina la preparaci�n de un examen en un idioma utilizando los
     *   par�mentros "examen" e "idioma"
    */
    else if (request.getParameter("accion").equals("eliminarPreparacion"))  
    {
        result = "false";
        
        AsignaturaEN asignaturaEN = (AsignaturaEN)session.getAttribute("asignatura");
        
        ExamenCEN examenCEN = new ExamenCEN();
        IdiomaCEN idiomaCEN = new IdiomaCEN();
        PreparacionExamenIdiomaCEN preparacionExamenIdiomaCEN = new PreparacionExamenIdiomaCEN();
        TraduccionInstruccionCEN traduccionInstruccionCEN = new TraduccionInstruccionCEN();
        ConfiguracionAsignaturaIdiomaCEN configuracionAsignaturaIdiomaCEN = new ConfiguracionAsignaturaIdiomaCEN();
        ConfiguracionAsignaturaIdiomaEN confPrincipal = configuracionAsignaturaIdiomaCEN.getConfiguracionIdiomaPrincipal(asignaturaEN);
        
        int examenCod = Integer.parseInt(request.getParameter("examen"));
        int idiomaCod = Integer.parseInt(request.getParameter("idioma"));
        
        //Solo permitimos borrar si no es la traducci�n del idioma principal
        if (idiomaCod != confPrincipal.getIdioma().getCodigo())
        {
            ExamenEN examenEN = examenCEN.getExamenByCodigo(asignaturaEN, examenCod);
            IdiomaEN idiomaEN = idiomaCEN.getIdiomaByCodigo(idiomaCod);

            //Eliminamos la preparaci�n
            PreparacionExamenIdiomaEN preparacionEliminar = preparacionExamenIdiomaCEN.getPreparacionExamenByExamenEIdioma(examenEN, idiomaEN);
            preparacionExamenIdiomaCEN.Delete(preparacionEliminar);
            
            //Eliminamos la traducci�n de las instrucciones
            ArrayList<TraduccionInstruccionEN> traduccionInstrucciones = traduccionInstruccionCEN.getTraduccionesInstruccionByExamenEIdioma(examenEN, idiomaEN);
            
            for (TraduccionInstruccionEN traduccion : traduccionInstrucciones)
            {
                traduccionInstruccionCEN.Delete(traduccion);
            }
            
            result = "true";
        }
    }
    
    /**
     *   Elimina un tema
    */
    else if (request.getParameter("accion").equals("eliminarTema"))
    {
        result = "false";
        
        AsignaturaEN asignaturaEN = (AsignaturaEN)session.getAttribute("asignatura");
        int temaCod = Integer.parseInt(request.getParameter("tema"));
        
        TemaCEN temaCEN = new TemaCEN();
        TemaEN temaEN = temaCEN.getTemaByCodigo(asignaturaEN, temaCod);
        
        temaCEN.rellenarClasificaciones(temaEN);
        
        //Eliminamos el tema (Esto elimina las clasificaciones y keywords)
        if (temaCEN.Delete(temaEN));
             result = "true";

        result = "true";
    }
    
    /**
     *   Un hilo escribe en la variable de sesi�n "xml" durante el progreso de carga
     *   de preguntas desde un fichero XML. El cliente utiliza esta funci�n 
     *   para actualizar su barra de carga.
    */
    else if (request.getParameter("accion").equals("progresoXML"))
    {
        result = "100";

        if (session.getAttribute("xml") != null)
        {
            XMLManager xml = (XMLManager)session.getAttribute("xml");
            result = String.valueOf(xml.getPorcentajeCargadas());

            if (result.equals("100"))
            {
                session.removeAttribute("xml");
            }
        }
    }

    /**
     *   Un hilo escribe en la variable de sesi�n "reclasificar" durante el progreso 
     *   de reclasificaci�n de las preguntas. El cliente utiliza esta funci�n 
     *   para actualizar su barra de carga.
    */
    else if (request.getParameter("accion").equals("progresoReclasificar"))
    {
        result = "100";

        if (session.getAttribute("reclasificar") != null)
        {
            ClasificacionTemaCEN clasificacionTemaCEN = (ClasificacionTemaCEN)session.getAttribute("reclasificar");
            result = String.valueOf(clasificacionTemaCEN.getPorcentajeReclasificadas());
            
            if (result.equals("100"))
            {
                session.removeAttribute("reclasificar");
            }
        }
    }
    
    /**
     *   Crea una variable de sesi�n con el filtro pasado por par�mentro que
     *   que despu�s se consulta en los CAD para filtrar las consultas a la BD
     *   se transformar los caraceres en "%" y "+" ya que estos 2 caracteres 
     *   no podian enviarse a trav�s del GET de Ajax.
    */
    else if (request.getParameter("accion").equals("nuevoFiltro"))
    {
        String filtro = request.getParameter("filtro").replace("_'(", "%").replace(")'_", "+");
        session.setAttribute("filtro", filtro);
    }
    
    /**
     *   Escribe el enunciado y las opci�nes de una pregunta pasada por parametro
     *   y lo devuelve para que sea mostrado en forma de tooltip
    */
    else if (request.getParameter("accion").equals("opcionesTooltip"))
    {
        AsignaturaEN asignaturaEN = (AsignaturaEN)session.getAttribute("asignatura");
        
        TraduccionOpcionCEN traduccionOpcionCEN = new TraduccionOpcionCEN();
        TraduccionPreguntaCEN traduccionPreguntaCEN = new TraduccionPreguntaCEN();
        ConfiguracionAsignaturaIdiomaCEN configuracionAsignaturaIdiomaCEN = new ConfiguracionAsignaturaIdiomaCEN();
        PreguntaCEN preguntaCEN = new PreguntaCEN();
        
        PreguntaEN preguntaEN = preguntaCEN.getPreguntaByCodigo(asignaturaEN, Integer.parseInt(request.getParameter("pregunta")));
        ConfiguracionAsignaturaIdiomaEN confPrincipal = configuracionAsignaturaIdiomaCEN.getConfiguracionIdiomaPrincipal(asignaturaEN);
        ArrayList<TraduccionOpcionEN> opciones = traduccionOpcionCEN.getTraduccionesOpcionByPreguntaEIdioma(preguntaEN, confPrincipal.getIdioma());
        TraduccionPreguntaEN TraduccionPreguntaEN = traduccionPreguntaCEN.getTraduccionPreguntaByPreguntaEIdioma(preguntaEN, confPrincipal.getIdioma());
              
        result += "<div style=\"font-weight:bold\">" + Utiles.textoHTML(TraduccionPreguntaEN.getEnunciado()) + "</div>";
        
        for (int i = 0; i < opciones.size(); i++)
        {
            result += "<br /><br />";
            
            if (opciones.get(i).getOpcion().isCorrecta())
                result += "<div style=\"font-weight:bold\">- " + Utiles.textoHTML(opciones.get(i).getEnunciado()) + "</div>";
            else
                result += "<div>- " + Utiles.textoHTML(opciones.get(i).getEnunciado()) + "</div>";
        }
    }
    
    /**
     *   Cambia los permisos de un usuario pasado por par�metro. Si era administrador
     *   de asignatura se lo quita y si no lo era se lo pone.
    */
    else if (request.getParameter("accion").equals("cambiarAcceso"))
    {
        result = "false";
        int usuarioCod = Integer.parseInt(request.getParameter("usuario"));
        
        if (usuarioCod != ((UsuarioEN)session.getAttribute("usuario")).getCodigo())
        {
            AccesoUsuarioAsignaturaCEN accesoUsuarioAsignaturaCEN = new AccesoUsuarioAsignaturaCEN();
            UsuarioCEN usuarioCEN = new UsuarioCEN();
            AsignaturaEN asignaturaEN = (AsignaturaEN)session.getAttribute("asignatura");
            UsuarioEN usuarioEN = usuarioCEN.getUsuarioByCodigo(usuarioCod);
            AccesoUsuarioAsignaturaEN accesoUsuarioAsignaturaEN = accesoUsuarioAsignaturaCEN.getAccesoByAsignaturaYUsuario(asignaturaEN, usuarioEN);

            accesoUsuarioAsignaturaEN.setAdministrador(!accesoUsuarioAsignaturaEN.isAdministrador());
            
            if (accesoUsuarioAsignaturaCEN.Modify(accesoUsuarioAsignaturaEN))
                result = "true";
        }
    }
    
    /**
     *   Expulsa al usuario pasado por par�metro de la asignatura pasada
     *   por par�metro.
    */
    else if (request.getParameter("accion").equals("expulsarUsuario"))
    {
        result = "false";
        int usuarioCod = Integer.parseInt(request.getParameter("usuario"));
        
        if (usuarioCod != ((UsuarioEN)session.getAttribute("usuario")).getCodigo())
        {
            AccesoUsuarioAsignaturaCEN accesoUsuarioAsignaturaCEN = new AccesoUsuarioAsignaturaCEN();
            UsuarioCEN usuarioCEN = new UsuarioCEN();
            AsignaturaEN asignaturaEN = (AsignaturaEN)session.getAttribute("asignatura");
            UsuarioEN usuarioEN = usuarioCEN.getUsuarioByCodigo(usuarioCod);
            AccesoUsuarioAsignaturaEN accesoUsuarioAsignaturaEN = accesoUsuarioAsignaturaCEN.getAccesoByAsignaturaYUsuario(asignaturaEN, usuarioEN);

            if (accesoUsuarioAsignaturaCEN.Delete(accesoUsuarioAsignaturaEN))
                result = "true";
        }
    }
    
    /**
     *   Dibuja un select (comboBox) con todos los usuarios sin acceso a una
     *   asignatura pasada por par�mentro.
    */
    else if (request.getParameter("accion").equals("usuariosSinAcceso"))
    {
        AsignaturaEN asignaturaEN = (AsignaturaEN)session.getAttribute("asignatura");
        UsuarioCEN usuarioCEN = new UsuarioCEN();
        ArrayList<UsuarioEN> usuariosSinAcceso = usuarioCEN.getUsuariosSinAccesoAsignatura(asignaturaEN);

        result += "<select name=\"usuarios\" style=\"width: 300px;\">";

        for (UsuarioEN usuario : usuariosSinAcceso)
        {
            result += "<option value=\"" + usuario.getCodigo() + "\">" + usuario.getUser() + "</oprion>";
        }

        result += "</select>";
    }
    
    /**
     *   Crea el acceso de un usuario pasado por par�metro a una asignatura
     *   pasada por par�metro y lo hace o no administrador consultando el 
     *   par�metro "admin"
    */
    else if (request.getParameter("accion").equals("agregarAcceso"))
    {
        result = "false";

        AsignaturaEN asignaturaEN = (AsignaturaEN)session.getAttribute("asignatura");
        UsuarioCEN usuarioCEN = new UsuarioCEN();
        AccesoUsuarioAsignaturaCEN accesoUsuarioAsignaturaCEN = new AccesoUsuarioAsignaturaCEN();
        UsuarioEN usuarioEN = usuarioCEN.getUsuarioByCodigo(Integer.parseInt(request.getParameter("usuario")));

        AccesoUsuarioAsignaturaEN accesoUsuarioAsignaturaEN = new AccesoUsuarioAsignaturaEN();
        accesoUsuarioAsignaturaEN.setAsignatura(asignaturaEN);
        accesoUsuarioAsignaturaEN.setUsuario(usuarioEN);

        if (request.getParameter("admin").equals("true"))
            accesoUsuarioAsignaturaEN.setAdministrador(true);
        else
            accesoUsuarioAsignaturaEN.setAdministrador(false);

        accesoUsuarioAsignaturaCEN.New(accesoUsuarioAsignaturaEN);

        result = "true";
    }
    
    /**
     *   Dibuja un select (comboBox) con todos los idiomas sin configurar en
     *   asignatura pasada por par�mentro.
    */
    else if (request.getParameter("accion").equals("idiomasSinConfigurar"))
    {
        AsignaturaEN asignaturaEN = (AsignaturaEN)session.getAttribute("asignatura");
        IdiomaCEN idiomaCEN = new IdiomaCEN();
        ArrayList<IdiomaEN> idiomasSinConfigurar = idiomaCEN.getIdiomasSinConfigurarAsignatura(asignaturaEN);

        result += "<select name=\"idiomas\" style=\"width: 300px;\">";

        for (IdiomaEN idioma : idiomasSinConfigurar)
        {
            result += "<option value=\"" + idioma.getCodigo() + "\">" + idioma.getNombre() + "</oprion>";
        }

        result += "</select>";
    }
    
    /**
     *   Elimina la configuraci�n de un idioma para una asignatura
    */
    else if (request.getParameter("accion").equals("quitarIdioma"))
    {
        result = "false";
        
        AsignaturaEN asignaturaEN = (AsignaturaEN)session.getAttribute("asignatura");
        ConfiguracionAsignaturaIdiomaCEN configuracionAsignaturaIdiomaCEN = new ConfiguracionAsignaturaIdiomaCEN();
        IdiomaCEN idiomaCEN = new IdiomaCEN();
        
        IdiomaEN idiomaEN = idiomaCEN.getIdiomaByCodigo(Integer.parseInt(request.getParameter("idioma")));
        ConfiguracionAsignaturaIdiomaEN conf = configuracionAsignaturaIdiomaCEN.getConfiguracionAsignaturaIdiomaByAsignaturaEIdioma(asignaturaEN, idiomaEN);
        
        configuracionAsignaturaIdiomaCEN.Delete(conf);
        
        result = "true";
    }
    
    /**
     *   Cancela el proceso de creaci�n de un examen borrando todas las
     *   variables de sesi�n.
    */
    else if (request.getParameter("accion").equals("cancelarExamen"))
    {
        session.removeAttribute("examen");
        session.removeAttribute("preparacion");
        session.removeAttribute("listaSeleccionadas");
    }
%>

<%= result %>