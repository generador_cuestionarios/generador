<%-- 
    Copyright (C) 2014 Ra�l Sempere Trujillo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Document   : listadoIdiomas
    Created on : 25-jul-2014, 18:06:00
    Author     : Ra�l Sempere Trujillo
--%>
 
<%-- 
    Este documento se emplea para mostrar el listado de idiomas que se utiliza
    dentro de la configuraci�n de los idiomas (admin).
--%>

<%@page import="CEN.AccesoUsuarioAsignaturaCEN"%>
<%@page import="EN.AccesoUsuarioAsignaturaEN"%>
<%@page import="clases_generador.Utiles"%>
<%@page import="java.util.Calendar"%>
<%@page import="EN.PreparacionExamenIdiomaEN"%>
<%@page import="CEN.PreparacionExamenIdiomaCEN"%>
<%@page import="EN.IdiomaEN"%>
<%@page import="EN.ConfiguracionAsignaturaIdiomaEN"%>
<%@page import="CEN.ConfiguracionAsignaturaIdiomaCEN"%>
<%@page import="EN.ExamenEN"%>
<%@page import="CEN.ExamenCEN"%>
<%@page import="java.util.ArrayList"%>
<%@page import="EN.AsignaturaEN"%>

<%
    String contenido = "";
    
    AsignaturaEN asignaturaEN = (AsignaturaEN)session.getAttribute("asignatura");
    ConfiguracionAsignaturaIdiomaCEN configuracionAsignaturaIdiomaCEN = new ConfiguracionAsignaturaIdiomaCEN();

    ArrayList<ConfiguracionAsignaturaIdiomaEN> confs = configuracionAsignaturaIdiomaCEN.getConfiguracionesAsignaturaIdiomaByAsignatura(asignaturaEN);
    contenido += "<table id=\"listado\">";
    contenido += "<tr><th style=\"width: 50px\">Editar</th><th style=\"width: 50px\">Quitar</th><th>Idioma</th><th>Nombre de la asignatura</th><th>Traductor</th></tr>";

    //Recorremos los items de la pagina
    for (ConfiguracionAsignaturaIdiomaEN conf : confs)
    {
        if (!conf.isIdiomaPrincipal())
        {
            contenido += "<tr>";
            contenido += "<td style=\"text-align: center\"><img class=\"add\" src=\"images/edit.png\" alt=\"editar\" onClick=\"location.href='editarConfiguracionIdioma.jsp?idioma=" + conf.getIdioma().getCodigo() + "'\" /></td>";
            contenido += "<td style=\"text-align: center\"><img class=\"add\" src=\"images/borrar.png\" alt=\"eliminar\" onClick=\"quitarIdioma(" + conf.getIdioma().getCodigo() + ")\" /></td>";
            contenido += "<td>" + conf.getIdioma().getNombre() + "</td>";
            contenido += "<td>" + conf.getNombreAsignatura() + "</td>";
            contenido += "<td>" + conf.getTraductorApertium() + "</td>";
            contenido += "</tr>";
        }
    }
    contenido += "</table>";
%>

<%= contenido %>