<%-- 
    Copyright (C) 2014 Ra�l Sempere Trujillo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Document   : filtroPreguntas
    Created on : 19-jul-2014, 16:07:26
    Author     : Ra�l Sempere Trujillo
--%>
 
<%-- 
    Este documento se emplea para mostrar las diferentes opciones de
    filtrado en las preguntas.
--%>
 
<%@page import="EN.TemaEN"%>
<%@page import="java.util.ArrayList"%>
<%@page import="CEN.TemaCEN"%>
<%@page import="EN.AsignaturaEN"%>
<%
    String contenido = "";
    
    //Necesitamos pasar el modo para saber que formulario refrescar luego con ajax
    if (request.getParameter("modo") != null)
    {
        AsignaturaEN asignaturaEN = (AsignaturaEN)session.getAttribute("asignatura");
        String modo = request.getParameter("modo");
        TemaCEN temaCEN = new TemaCEN();
        ArrayList<TemaEN> temas = temaCEN.getTemasByAsignatura(asignaturaEN);
        
        if (modo.equals("ver"))
        {
            contenido += "<button id=\"mostrarOcultar\" type=\"button\" onCLick=\"botonFiltros()\">Mostrar filtros</button>";
            contenido += "<div id=\"filtro\" class=\"oculto  filtro_pregunta\" style=\"display: none\">";  
            contenido += "<form name=\"filtros\">";
            contenido += "<h3 style=\"margin-bottom: 5px\">Filtros:</h3>";
            contenido += "<div>";
            contenido += "<input  type=\"checkbox\" name=\"habilitadaActivo\" /> Habilitada: ";
            contenido += "<select style=\"width: 45px\" name=\"habilitada\"><option value=\"true\" selected>Si</option><option value=\"false\" selected>No</option></select> ";
            contenido += "<input style=\"margin-left: 30px\" type=\"checkbox\" name=\"ultimoUsoActivo\" /> �ltimo uso entre: ";
            contenido += "<input style=\"width: 11em\" type=\"date\" name=\"ultimoUso1\"  /> ";
            contenido += "<input style=\"width: 11em\" type=\"date\" name=\"ultimoUso2\" />";
            contenido += "</div><div>";
            contenido += "<input type=\"checkbox\" name=\"utilizadaActivo\" /> Veces utilizada entre: ";
            contenido += "<input style=\"width: 45px\" type=\"number\" name=\"utilizada1\" min=\"0\" step=\"1\" value=\"0\" /> ";
            contenido += "<input style=\"width: 45px\" type=\"number\" name=\"utilizada2\" min=\"0\" step=\"1\" value=\"1000\" />";
            contenido += "<input style=\"margin-left: 30px\" type=\"checkbox\" name=\"dificultadActivo\" /> Dificultad entre: ";
            contenido += "<input style=\"width: 50px\" type=\"number\" name=\"dificultad1\" min=\"0\" max=\"1\" step=\"0.01\" value=\"0\" /> ";
            contenido += "<input style=\"width: 50px\" type=\"number\" name=\"dificultad2\" min=\"0\" max=\"1\" step=\"0.01\" value=\"1\" />";
            contenido += "<input style=\"margin-left: 30px\" type=\"checkbox\" name=\"discriminacionActivo\" /> Discriminaci�n entre: ";
            contenido += "<input style=\"width: 50px\" type=\"number\" name=\"discriminacion1\" min=\"-1\" max=\"1\" step=\"0.01\" value=\"-1\" /> ";
            contenido += "<input style=\"width: 50px\" type=\"number\" name=\"discriminacion2\" min=\"-1\" max=\"1\" step=\"0.01\" value=\"1\" />";
            contenido += "</div><div>";
            contenido += "<input type=\"checkbox\" name=\"proximoUsoActivo\" /> Pr�ximo examen: ";
            contenido += "<select style=\"width: 45px\" name=\"proximoUso\"><option value=\"true\" selected>Si</option><option value=\"false\" selected>No</option></select> ";
            contenido += "<input style=\"margin-left: 25px\" type=\"checkbox\" name=\"preguntaActivo\" /> Pregunta: ";
            contenido += "<input type=\"text\" name=\"pregunta\" style=\"width: 190px\" />";
            contenido += "<input style=\"margin-left: 25px\" type=\"checkbox\" name=\"temaActivo\" /> Tema: ";

            //Rellenamos los temas
            contenido += "<select style=\"width: 210px\" name=\"tema\">";
            contenido += "<option value=\"-1\" selected>Sin Clasificar</option>";
            contenido += "<option value=\"0\" selected>Sin Revisar</option>";
            for (TemaEN temaEN : temas)
            {
                contenido += "<option value=\"" + temaEN.getCodigo() + "\">" + temaEN.getNombre() + "</option>";
            }
            contenido += "</select>";
            contenido += "</div><div>";
            contenido += "<input type=\"checkbox\" name=\"aciertosActivo\" /> % Aciertos entre: ";
            contenido += "<input style=\"width: 45px\" type=\"number\" name=\"aciertos1\" min=\"0\" max=\"100\" step=\"0.01\" value=\"0\" /> ";
            contenido += "<input style=\"width: 45px\" type=\"number\" name=\"aciertos2\" min=\"0\" max=\"100\" step=\"0.01\" value=\"100\" />";
            contenido += "<input style=\"margin-left: 30px\" type=\"checkbox\" name=\"fallosActivo\" /> % Fallos entre: ";
            contenido += "<input style=\"width: 50px\" type=\"number\" name=\"fallos1\" min=\"0\" max=\"100\" step=\"0.01\" value=\"0\" /> ";
            contenido += "<input style=\"width: 50px\" type=\"number\" name=\"fallos2\" min=\"0\" max=\"100\" step=\"0.01\" value=\"100\" />";
            contenido += "<input style=\"margin-left: 30px\" type=\"checkbox\" name=\"sinContestarActivo\" />  % Sin contestar entre: ";
            contenido += "<input style=\"width: 50px\" type=\"number\" name=\"sinContestar1\" min=\"0\" max=\"100\" step=\"0.01\" value=\"0\" /> ";
            contenido += "<input style=\"width: 50px\" type=\"number\" name=\"sinContestar2\" min=\"0\" max=\"100\" step=\"0.01\" value=\"100\" />";
            contenido += "</div>";
            contenido += "</form>";
            contenido += "<div style=\"margin-top: 5px\" class=\"boton_pregunta\" onClick=\"actualizarFiltroPreguntas('" + modo + "')\">Aplicar</div>";
            contenido += "</div><br />";
        }
        else
        {
                contenido += "<button id=\"mostrarOcultar\" type=\"button\" onCLick=\"botonFiltros()\">Mostrar filtros</button>";
                contenido += "<div id=\"filtro\" class=\"oculto filtro_pregunta\" style=\"display: none\">";  
                contenido += "<form name=\"filtros\">";
                contenido += "<h3 style=\"margin-bottom: 5px\">Filtros:</h3>";
                contenido += "<div>";
                contenido += "<input type=\"checkbox\" name=\"ultimoUsoActivo\" /> �ltimo uso entre: ";
                contenido += "<input style=\"width: 11em\" type=\"date\" name=\"ultimoUso1\"  /> ";
                contenido += "<input style=\"width: 11em\" type=\"date\" name=\"ultimoUso2\" />";
                contenido += "</div><div>";
                contenido += "<input type=\"checkbox\" name=\"utilizadaActivo\" /> Veces utilizada entre: ";
                contenido += "<input style=\"width: 45px\" type=\"number\" name=\"utilizada1\" min=\"0\" step=\"1\" value=\"0\" /> ";
                contenido += "<input style=\"width: 45px\" type=\"number\" name=\"utilizada2\" min=\"0\" step=\"1\" value=\"1000\" />";
                contenido += "<input style=\"margin-left: 30px\" type=\"checkbox\" name=\"dificultadActivo\" /> Dificultad entre: ";
                contenido += "<input style=\"width: 50px\" type=\"number\" name=\"dificultad1\" min=\"0\" max=\"1\" step=\"0.01\" value=\"0\" /> ";
                contenido += "<input style=\"width: 50px\" type=\"number\" name=\"dificultad2\" min=\"0\" max=\"1\" step=\"0.01\" value=\"1\" />";
                contenido += "<input style=\"margin-left: 30px\" type=\"checkbox\" name=\"discriminacionActivo\" /> Discriminaci�n entre: ";
                contenido += "<input style=\"width: 50px\" type=\"number\" name=\"discriminacion1\" min=\"-1\" max=\"1\" step=\"0.01\" value=\"-1\" /> ";
                contenido += "<input style=\"width: 50px\" type=\"number\" name=\"discriminacion2\" min=\"-1\" max=\"1\" step=\"0.01\" value=\"1\" />";
                contenido += "</div><div>";
                contenido += "<input type=\"checkbox\" name=\"preguntaActivo\" /> Pregunta: ";
                contenido += "<input type=\"text\" name=\"pregunta\" size=\"40px\" />";
                contenido += "<input style=\"margin-left: 25px\" type=\"checkbox\" name=\"temaActivo\" /> Tema: ";

                //Rellenamos los temas
                contenido += "<select style=\"width: 300px\" name=\"tema\">";
                contenido += "<option value=\"-1\" selected>Sin Clasificar</option>";
                contenido += "<option value=\"0\" selected>Sin Revisar</option>";
                for (TemaEN temaEN : temas)
                {
                    contenido += "<option value=\"" + temaEN.getCodigo() + "\">" + temaEN.getNombre() + "</option>";
                }
                contenido += "</select>";
                contenido += "</div><div>";
                contenido += "<input type=\"checkbox\" name=\"aciertosActivo\" /> % Aciertos entre: ";
                contenido += "<input style=\"width: 45px\" type=\"number\" name=\"aciertos1\" min=\"0\" max=\"100\" step=\"0.01\" value=\"0\" /> ";
                contenido += "<input style=\"width: 45px\" type=\"number\" name=\"aciertos2\" min=\"0\" max=\"100\" step=\"0.01\" value=\"100\" />";
                contenido += "<input style=\"margin-left: 30px\" type=\"checkbox\" name=\"fallosActivo\" /> % Fallos entre: ";
                contenido += "<input style=\"width: 50px\" type=\"number\" name=\"fallos1\" min=\"0\" max=\"100\" step=\"0.01\" value=\"0\" /> ";
                contenido += "<input style=\"width: 50px\" type=\"number\" name=\"fallos2\" min=\"0\" max=\"100\" step=\"0.01\" value=\"100\" />";
                contenido += "<input style=\"margin-left: 30px\" type=\"checkbox\" name=\"sinContestarActivo\" /> % Sin contestar entre: ";
                contenido += "<input style=\"width: 50px\" type=\"number\" name=\"sinContestar1\" min=\"0\" max=\"100\" step=\"0.01\" value=\"0\" /> ";
                contenido += "<input style=\"width: 50px\" type=\"number\" name=\"sinContestar2\" min=\"0\" max=\"100\" step=\"0.01\" value=\"100\" />";
                contenido += "</div>";
                contenido += "</form>";
                contenido += "<div style=\"margin-top: 5px\" class=\"boton_pregunta\" onClick=\"actualizarFiltroPreguntas('" + modo + "')\">Aplicar</div>";
                contenido += "</div><br />";
        }
    }
%>

<%=contenido%>