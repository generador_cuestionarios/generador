<%-- 
    Copyright (C) 2014 Ra�l Sempere Trujillo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Document   : filtroExamenes
    Created on : 20-jul-2014, 13:59:00
    Author     : Ra�l Sempere Trujillo
--%>
 
<%-- 
    Muestra las opciones de filtrado para un examen.
--%>
 
<%
    String contenido = "";

    contenido += "<button id=\"mostrarOcultar\" type=\"button\" onCLick=\"botonFiltros()\">Mostrar filtros</button>";
    contenido += "<div id=\"filtro\" class=\"oculto filtro_examen\" style=\"display: none\">";  
    contenido += "<form name=\"filtros\">";
    contenido += "<h3 style=\"margin-bottom: 5px\">Filtros:</h3>";
    contenido += "<div>";
    contenido += "<input  type=\"checkbox\" name=\"analizadoActivo\" /> Analizado: ";
    contenido += "<select style=\"width: 45px\" name=\"analizado\"><option value=\"true\" selected>Si</option><option value=\"false\" selected>No</option></select> ";
    contenido += "<input style=\"margin-left: 30px\" type=\"checkbox\" name=\"fechaActivo\" /> Fecha entre: ";
    contenido += "<input style=\"width: 11em\" type=\"date\" name=\"fecha1\"  /> ";
    contenido += "<input style=\"width: 11em\" type=\"date\" name=\"fecha2\" />";
    contenido += "</div><div>";
    contenido += "<input type=\"checkbox\" name=\"dificultadActivo\" /> Dificultad entre: ";
    contenido += "<input style=\"width: 50px\" type=\"number\" name=\"dificultad1\" min=\"0\" max=\"1\" step=\"0.01\" value=\"0\" /> ";
    contenido += "<input style=\"width: 50px\" type=\"number\" name=\"dificultad2\" min=\"0\" max=\"1\" step=\"0.01\" value=\"1\" />";
    contenido += "<input style=\"margin-left: 30px\" type=\"checkbox\" name=\"discriminacionActivo\" /> Discriminaci�n entre: ";
    contenido += "<input style=\"width: 50px\" type=\"number\" name=\"discriminacion1\" min=\"-1\" max=\"1\" step=\"0.01\" value=\"-1\" /> ";
    contenido += "<input style=\"width: 50px\" type=\"number\" name=\"discriminacion2\" min=\"-1\" max=\"1\" step=\"0.01\" value=\"1\" />";
    contenido += "</div><div>";
    contenido += "<input type=\"checkbox\" name=\"examenActivo\" /> Examen: ";
    contenido += "<input type=\"text\" name=\"examen\" style=\"width: 250px\" />";
    contenido += "<input style=\"margin-left: 30px\" type=\"checkbox\" name=\"aciertosActivo\" /> % Aciertos entre: ";
    contenido += "<input style=\"width: 45px\" type=\"number\" name=\"aciertos1\" min=\"0\" max=\"100\" step=\"0.01\" value=\"0\" /> ";
    contenido += "<input style=\"width: 45px\" type=\"number\" name=\"aciertos2\" min=\"0\" max=\"100\" step=\"0.01\" value=\"100\" />";
    contenido += "</div><div>";
    contenido += "<input type=\"checkbox\" name=\"fallosActivo\" /> % Fallos entre: ";
    contenido += "<input style=\"width: 50px\" type=\"number\" name=\"fallos1\" min=\"0\" max=\"100\" step=\"0.01\" value=\"0\" /> ";
    contenido += "<input style=\"width: 50px\" type=\"number\" name=\"fallos2\" min=\"0\" max=\"100\" step=\"0.01\" value=\"100\" />";
    contenido += "<input style=\"margin-left: 30px\" type=\"checkbox\" name=\"sinContestarActivo\" /> % Sin contestar entre: ";
    contenido += "<input style=\"width: 50px\" type=\"number\" name=\"sinContestar1\" min=\"0\" max=\"100\" step=\"0.01\" value=\"0\" /> ";
    contenido += "<input style=\"width: 50px\" type=\"number\" name=\"sinContestar2\" min=\"0\" max=\"100\" step=\"0.01\" value=\"100\" />";
    contenido += "</div>";
    contenido += "</form>";
    contenido += "<div style=\"margin-top: 5px\" class=\"boton_pregunta\" onClick=\"actualizarFiltroExamenes()\">Aplicar</div>";
    contenido += "</div><br />";
%>

<%=contenido%>