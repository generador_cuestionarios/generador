<%-- 
    Copyright (C) 2014 Ra�l Sempere Trujillo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Document   : listadoUsuarios
    Created on : 24-jul-2014, 14:33:00
    Author     : Ra�l Sempere Trujillo
--%>

<%-- 
    Este documento se emplea para mostrar el listado de usuarios de la asignatura
    dentro de la configuraci�n de los usuarios (admin).
--%>

<%@page import="EN.UsuarioEN"%>
<%@page import="CEN.AccesoUsuarioAsignaturaCEN"%>
<%@page import="EN.AccesoUsuarioAsignaturaEN"%>
<%@page import="clases_generador.Utiles"%>
<%@page import="java.util.Calendar"%>
<%@page import="EN.PreparacionExamenIdiomaEN"%>
<%@page import="CEN.PreparacionExamenIdiomaCEN"%>
<%@page import="EN.IdiomaEN"%>
<%@page import="EN.ConfiguracionAsignaturaIdiomaEN"%>
<%@page import="CEN.ConfiguracionAsignaturaIdiomaCEN"%>
<%@page import="EN.ExamenEN"%>
<%@page import="CEN.ExamenCEN"%>
<%@page import="java.util.ArrayList"%>
<%@page import="EN.AsignaturaEN"%>

<%
    String contenido = "";
    
    AsignaturaEN asignaturaEN = (AsignaturaEN)session.getAttribute("asignatura");
    AccesoUsuarioAsignaturaCEN accesoUsuarioAsignaturaCEN = new AccesoUsuarioAsignaturaCEN();
    UsuarioEN usuarioEN = (UsuarioEN)session.getAttribute("usuario");
    
    if (request.getParameter("modo").equals("admin"))
    {
        ArrayList<AccesoUsuarioAsignaturaEN> accesos = accesoUsuarioAsignaturaCEN.getAccesosByAsignatura(asignaturaEN);

        contenido += "<table id=\"listado\">";
        contenido += "<tr><th style=\"width: 100px\">Degradar a usuario</th><th style=\"width: 60px\">Expulsar</th><th>Nombre de usuario</th></tr>";

        //Recorremos los items de la pagina
        for (AccesoUsuarioAsignaturaEN acceso : accesos)
        {
            if (acceso.isAdministrador())
            {
                contenido += "<tr>";
                
                //No nos podemos borrar ni degradar
                if (usuarioEN.getCodigo() == acceso.getUsuario().getCodigo())
                {
                    contenido += "<td style=\"text-align: center\"><img class=\"add\" src=\"images/arrow_down.png\" alt=\"degradar\" onClick=\"alert('Por motivos de seguridad, no est� permitido degradarse uno mismo.')\" /></td>";
                    contenido += "<td style=\"text-align: center\"><img class=\"add\" src=\"images/borrar_usuario.png\" alt=\"eliminar\" onClick=\"alert('Por motivos de seguridad, no est� permitido expulsarse de la asignatura uno mismo.')\" /></td>";
                }
                else
                {
                    contenido += "<td style=\"text-align: center\"><img class=\"add\" src=\"images/arrow_down.png\" alt=\"degradar\" onClick=\"cambiarAcceso("+ acceso.getUsuario().getCodigo() +")\" /></td>";
                    contenido += "<td style=\"text-align: center\"><img class=\"add\" src=\"images/borrar_usuario.png\" alt=\"eliminar\" onClick=\"eliminarAcceso("+ acceso.getUsuario().getCodigo() +")\" /></td>";
                }
                contenido += "<td>" + acceso.getUsuario().getUser() + "</td>";
                contenido += "</tr>";
            }
        }
        contenido += "</table>";
    }
    //---------------------------------------------------------------------------------------------------------------------------------//
    else if (request.getParameter("modo").equals("usuario"))
    {
        ArrayList<AccesoUsuarioAsignaturaEN> accesos = accesoUsuarioAsignaturaCEN.getAccesosByAsignatura(asignaturaEN);

        contenido += "<table id=\"listado\">";
        contenido += "<tr><th style=\"width: 100px\">Ascender a administrador</th><th style=\"width: 60px\">Expulsar</th><th>Nombre de usuario</th></tr>";

        //Recorremos los items de la pagina
        for (AccesoUsuarioAsignaturaEN acceso : accesos)
        {
            if (!acceso.isAdministrador())
            {
                contenido += "<tr>";
                contenido += "<td style=\"text-align: center\"><img class=\"add\" src=\"images/arrow_up.png\" alt=\"promocionar\" onClick=\"cambiarAcceso("+ acceso.getUsuario().getCodigo() +")\" /></td>";
                contenido += "<td style=\"text-align: center\"><img class=\"add\" src=\"images/borrar_usuario.png\" alt=\"eliminar\" onClick=\"eliminarAcceso("+ acceso.getUsuario().getCodigo() +")\" /></td>";
                contenido += "<td>" + acceso.getUsuario().getUser() + "</td>";
                contenido += "</tr>";
            }
        }
        contenido += "</table>";
    }
%>

<%= contenido %>