<%-- 
    Copyright (C) 2014 Ra�l Sempere Trujillo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Document   : listadoExamenes
    Created on : 17-jun-2014, 13:09:00
    Author     : Ra�l Sempere Trujillo
--%>
 
<%-- 
    Este documento se emplea para mostrar el listado de ex�menes.
--%>
 

<%@page import="clases_generador.Utiles"%>
<%@page import="java.util.Calendar"%>
<%@page import="EN.PreparacionExamenIdiomaEN"%>
<%@page import="CEN.PreparacionExamenIdiomaCEN"%>
<%@page import="EN.IdiomaEN"%>
<%@page import="EN.ConfiguracionAsignaturaIdiomaEN"%>
<%@page import="CEN.ConfiguracionAsignaturaIdiomaCEN"%>
<%@page import="EN.ExamenEN"%>
<%@page import="CEN.ExamenCEN"%>
<%@page import="java.util.ArrayList"%>
<%@page import="EN.AsignaturaEN"%>

<%
    String contenido = "";
    int itemsPorPagina = 10;
    int pagina = Integer.parseInt(request.getParameter("pagina"));
    
    AsignaturaEN asignaturaEN = (AsignaturaEN)session.getAttribute("asignatura");
    ExamenCEN examenCEN = new ExamenCEN();
    ConfiguracionAsignaturaIdiomaCEN configuracionAsignaturaIdiomaCEN = new ConfiguracionAsignaturaIdiomaCEN();
    PreparacionExamenIdiomaCEN preparacionExamenIdiomaCEN = new PreparacionExamenIdiomaCEN();
    ConfiguracionAsignaturaIdiomaEN conf = configuracionAsignaturaIdiomaCEN.getConfiguracionIdiomaPrincipal(asignaturaEN);
    IdiomaEN idiomaPrincipal = conf.getIdioma();
    PreparacionExamenIdiomaEN preparacionExamenIdiomaEN;
    
    String filtro = "";

    if (session.getAttribute("filtro") != null)
        filtro = (String)session.getAttribute("filtro") +  filtro;
    
    ArrayList<ExamenEN> examenes = examenCEN.getExamenesByAsignaturaYFiltro(asignaturaEN, filtro);
    
    contenido += "<table id=\"listado\">";
    contenido += "<tr><th style=\"width: 50px\">Detalle</th><th style=\"width: 60px\">Analizar</th><th style=\"width: 60px\">Traducc.</th>"
            + "<th style=\"width: 60px\">Eliminar</th><th>Examen</th>"
            + "<th style=\"width: 80px\">Fecha</th><th style=\"width: 90px\">Preguntas</th><th style=\"width: 100px\" >Modalidades</th>"
            + "<th style=\"width: 60px\">�ndice Dificul.</th><th style=\"width: 60px\">�ndice Discrim.</th>"
            + "<th style=\"width: 60px\">Aciertos</th><th style=\"width: 60px\">Fallos</th><th style=\"width: 60px\">Sin contestar</th><th style=\"width: 70px\">Analizado</th></tr>";
    
    
    //Recorremos los items de la pagina
    for (int i = (pagina - 1) * itemsPorPagina; i < pagina * itemsPorPagina && i < examenes.size(); i++)
    {
        preparacionExamenIdiomaEN = preparacionExamenIdiomaCEN.getPreparacionExamenByExamenEIdioma(examenes.get(i), idiomaPrincipal);
        examenCEN.rellenarPreguntas(examenes.get(i));

        contenido += "<tr>";
        contenido += "<td style=\"text-align: center\"><img class=\"add\" src=\"images/detalle.png\" alt=\"detalle\" onClick=\"detalleExamen("+ examenes.get(i).getCodigo() +")\"></td>";
        if (examenes.get(i).isAnalizado())
            contenido += "<td style=\"text-align: center\"><img class=\"add\" src=\"images/analizar.png\" alt=\"analizar\" onClick=\"alert('Este examen ya ha sido analizado.')\"></td>";
        else
            contenido += "<td style=\"text-align: center\"><img class=\"add\" src=\"images/analizar.png\" alt=\"analizar\" onClick=\"location.href='subirExcel.jsp?examen=" + examenes.get(i).getCodigo() + "'\"></td>";

        contenido += "<td style=\"text-align: center\"><img class=\"add\" src=\"images/preparaciones.png\" alt=\"preapraciones\" onClick=\"preparacionesExamen("+ examenes.get(i).getCodigo() +")\"></td>";
        
        if (examenes.get(i).isAnalizado())
            contenido += "<td style=\"text-align: center\"><img class=\"add\" src=\"images/borrar.png\" alt=\"eliminar\" onClick=\"alert('No se puede eliminar un examen que ya ha sido analizado.')\"></td>";
        else
            contenido += "<td style=\"text-align: center\"><img class=\"add\" src=\"images/borrar.png\" alt=\"eliminar\" onClick=\"eliminarExamen("+ examenes.get(i).getCodigo() +")\"></td>";

        contenido += "<td>" + preparacionExamenIdiomaEN.getTraduccionNombre() + "</td>"; 
        contenido += "<td>" + Utiles.rellenaConCeros(String.valueOf(examenes.get(i).getFecha().get(Calendar.DAY_OF_MONTH)), 2) + "/" + Utiles.rellenaConCeros(String.valueOf((examenes.get(i).getFecha().get(Calendar.MONTH) + 1)), 2) + "/" + examenes.get(i).getFecha().get(Calendar.YEAR)  + "</td>";
        contenido += "<td>" + examenes.get(i).getPreguntas().size() + "</td>";
        contenido += "<td>" + examenes.get(i).getModalidades() + "</td>";
        contenido += "<td>" + examenes.get(i).getDificultad() + "</td>"; 
        contenido += "<td>" + examenes.get(i).getDiscriminacion() + "</td>";
        if (examenes.get(i).isAnalizado())
        {
            contenido += "<td>" + Utiles.redondeo(((double)examenes.get(i).getAciertos()/examenes.get(i).getContestadas())*100, 2) + "%</td>";
            contenido += "<td>" + Utiles.redondeo((100-((double)examenes.get(i).getAciertos()/examenes.get(i).getContestadas())*100), 2) + "%</td>";
            contenido += "<td>" + Utiles.redondeo(((double)examenes.get(i).getNoContestadas()/(examenes.get(i).getContestadas() + examenes.get(i).getNoContestadas()))*100, 2) + "%</td>";
            contenido += "<td style=\"text-align: center\"><img class=\"preparado\" src=\"images/verde.png\" alt=\"analizado\" /></td>";
        }
        else
        {
            contenido += "<td>-</td>";
            contenido += "<td>-</td>";
            contenido += "<td>-</td>";
            contenido += "<td style=\"text-align: center\"><img class=\"preparado\" src=\"images/rojo.png\" alt=\"sin analizar\" /></td>";
        }
        contenido += "</tr>";
    }
    
    contenido += "</table>";
    
    //Escribimos el pi� con las opciones de navegaci�n
    contenido += "<br /><div align=\"right\" style=\"margin-right: 6px\">";
    contenido += "<img class=\"boton\" src=\"images/doble_izquierda.png\" alt=\"primera\" style=\"width: 25px; height: 25px; vertical-align: middle; margin-right: 3px\" onClick=\"primeraPaginaExamen()\" />";
    contenido += "<img class=\"boton\" src=\"images/izquierda.png\" alt=\"anterior\" style=\"width: 25px; height: 25px; vertical-align: middle; margin-right: 10px\" onClick=\"anteriorPaginaExamen()\" />";
    contenido += "P�gina " + pagina + " de " + (int)Math.ceil(examenes.size()/(itemsPorPagina * 1.0));
    contenido += "<img class=\"boton\" src=\"images/derecha.png\" alt=\"siguiente\" style=\"width: 25px; height: 25px; vertical-align: middle; margin-left: 10px\" onClick=\"siguientePaginaExamen()\" />";
    contenido += "<img class=\"boton\" src=\"images/doble_derecha.png\" alt=\"ultima\" style=\"width: 25px; height: 25px; vertical-align: middle; margin-left: 3px\" onClick=\"ultimaPaginaExamen()\" /></div>";
    contenido += "<div align=\"right\" style=\"margin-right: 70px; margin-top: 5px; font-weight: bold;\">Total: " + examenes.size() + "</div>";
    contenido += "<div id=\"total\" style=\"display: none\">" + examenes.size() + "</div>";
%>

<%= contenido %>