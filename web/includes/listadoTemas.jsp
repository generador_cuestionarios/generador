<%-- 
    Copyright (C) 2014 Ra�l Sempere Trujillo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Document   : listadotemas
    Created on : 24-abr-2014, 21:54:00
    Author     : Ra�l Sempere Trujillo
--%>
 
<%-- 
    Este documento se emplea para mostrar el listado de temas.
--%>
 
<%@page import="EN.ClasificacionTemaEN"%>
<%@page import="EN.TemaEN"%>
<%@page import="CEN.KeywordCEN"%>
<%@page import="CEN.TemaCEN"%>
<%@page import="CEN.ClasificacionTemaCEN"%>
<%@page import="CEN.ConfiguracionAsignaturaIdiomaCEN"%>
<%@page import="CEN.TraduccionPreguntaCEN"%>
<%@page import="EN.PreguntaEN"%>
<%@page import="CEN.PreguntaCEN"%>
<%@page import="java.util.ArrayList"%>
<%@page import="EN.AsignaturaEN"%>


<%
    String contenido = "";
    AsignaturaEN asignaturaEN = (AsignaturaEN)session.getAttribute("asignatura");
    TemaCEN temaCEN = new TemaCEN();
    
    if (request.getParameter("modo").equals("selExamen"))
    {
        ArrayList<TemaEN> temas = temaCEN.getTemasByAsignatura(asignaturaEN);
        ArrayList<ClasificacionTemaEN> clasificaciones;
        ClasificacionTemaCEN clasificacionTemaCEN = new ClasificacionTemaCEN();

        contenido += "<table id=\"listado\">";
        contenido += "<tr><th style=\"display: none\">Codigo</th><th>Incluir</th><th>Cantidad</th><th>Tema</th><th style=\"width: 80px\">Preguntas disponibles</th></tr>";

        for (int i = 0; i < temas.size(); i++)
        {
            temaCEN.rellenarClasificaciones(temas.get(i));
            temaCEN.rellenarKeywords(temas.get(i));

            //Ponemos una columna oculta con la id de los temas para poder reconocerlos
            contenido += "<tr><td style=\"display: none\"><input type=\"text\" name=\"codigo" + i + "\" value=\"" + temas.get(i).getCodigo() + "\" /></td>";
            contenido += "<td style=\"text-align: center\"><input type=\"checkbox\"  name=\"incluir" + i + "\" /></td>";
            contenido += "<td><input type=\"number\"  name=\"porcentaje" + i + "\" min=\"0\" max=\"100\" step=\"0.01\" value=\"0\" /> %</td>";
            contenido += "<td id=\"tema" + i + "\">" + temas.get(i).getNombre() + "</td>";
            
            //Obtenemos las preguntas disponibles en cada tema
            //(preguntas habilitadas y con el tema revisado)
            clasificaciones = clasificacionTemaCEN.getClasificacionesDisponiblesByTema(temas.get(i));
          
            contenido += "<td id=\"disponible" + i + "\">" + clasificaciones.size() + "</td>";
            contenido += "</tr>";
        }

        contenido += "</table>";
    }
    else if (request.getParameter("modo").equals("ver"))
    {
        ArrayList<TemaEN> temas = temaCEN.getTemasByAsignatura(asignaturaEN);
        ClasificacionTemaCEN clasificacionTemaCEN = new ClasificacionTemaCEN();
        
        
        contenido += "<table id=\"listado\">";
        contenido += "<tr><th style=\"width: 60px\">Detalle</th><th style=\"width: 60px\">Eliminar</th><th>Tema</th><th>Keywords</th><th style=\"width: 80px\">N� Preguntas</th></tr>";
        

        for (TemaEN temaEN : temas)
        {
            temaCEN.rellenarKeywords(temaEN);
            contenido += "<td style=\"text-align: center\"><img class=\"add\" src=\"images/detalle.png\" alt=\"detalle\" onClick=\"location.href='detalleTema.jsp?tema=" + temaEN.getCodigo() + "'\" /></div></td>";
            contenido += "<td style=\"text-align: center\"><img class=\"add\" src=\"images/borrar.png\" alt=\"eliminar\" onClick=\"eliminarTema(" + temaEN.getCodigo() + ")\" /></div></td>";
            contenido += "<td>" + temaEN.getNombre() + "</td>";
            contenido += "<td>";
            for (int j = 0; j < temaEN.getKeywords().size(); j++)
            {
                contenido += temaEN.getKeywords().get(j).getPalabra();
                if (j < temaEN.getKeywords().size() - 1)
                    contenido += ", ";
            }
            contenido += "</td>";
            contenido += "<td>" + clasificacionTemaCEN.getCantidadClasificacionesRevisadasByTema(temaEN) + "</td>";
            contenido += "</tr>";
        }

        contenido += "</table>";
    }
    
%>

<%= contenido %>