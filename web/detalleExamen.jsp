<%-- 
    Copyright (C) 2014 Raúl Sempere Trujillo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Document   : detalleExamen
    Created on : 11-jul-2014, 15:03:00
    Author     : Raúl Sempere Trujillo
--%>

<%-- 
    Muestra el formulario con los detalles de un examen, permitiendo editarlo
    en caso de no estar analizado.
--%>
 
<%@page import="CEN.TraduccionPreguntaCEN"%>
<%@page import="EN.TraduccionPreguntaEN"%>
<%@page import="EN.PreguntaEN"%>
<%@page import="CEN.PreguntaCEN"%>
<%@page import="CEN.InstruccionCEN"%>
<%@page import="EN.ExamenPreguntaEN"%>
<%@page import="CEN.ExamenPreguntaCEN"%>
<%@page import="java.util.ArrayList"%>
<%@page import="EN.TraduccionInstruccionEN"%>
<%@page import="CEN.TraduccionInstruccionCEN"%>
<%@page import="EN.InstruccionEN"%>
<%@page import="clases_generador.Utiles"%>
<%@page import="java.util.Calendar"%>
<%@page import="EN.PreparacionExamenIdiomaEN"%>
<%@page import="CEN.PreparacionExamenIdiomaCEN"%>
<%@page import="EN.IdiomaEN"%>
<%@page import="CEN.ConfiguracionAsignaturaIdiomaCEN"%>
<%@page import="clases_generador.ExamenLaTeX"%>
<%@page import="EN.AsignaturaEN"%>
<%@page import="CEN.ExamenCEN"%>
<%@page import="EN.ExamenEN"%>

<% String titulo = "Detalle del examen - Generador Inteligente de Cuestinarios"; %>

<%
    request.setCharacterEncoding("UTF-8");
 
    String contenidoPre = "";
    String contenidoPost = "";
    int instrucciones = 0;
    
    if (session.getAttribute("usuario") == null)
    {
       response.sendRedirect("login.jsp");
    }
    else if (session.getAttribute("asignatura") == null)
    {
       response.sendRedirect("asignaturas.jsp");
    }
    else if (request.getParameter("examen") != null)     
    {
        if (session.getAttribute("filtro") != null)
            session.removeAttribute("filtro");
        
        ExamenCEN examenCEN = new ExamenCEN();
        ConfiguracionAsignaturaIdiomaCEN configuracionAsignaturaIdiomaCEN = new ConfiguracionAsignaturaIdiomaCEN();
        PreparacionExamenIdiomaCEN preparacionExamenIdiomaCEN = new PreparacionExamenIdiomaCEN();
        TraduccionInstruccionCEN traduccionInstruccionCEN = new TraduccionInstruccionCEN();
        ExamenPreguntaCEN examenPreguntaCEN = new ExamenPreguntaCEN();
        InstruccionCEN instruccionCEN = new InstruccionCEN();
        PreguntaCEN preguntaCEN = new PreguntaCEN();
        
        
        AsignaturaEN asignaturaEN = (AsignaturaEN)session.getAttribute("asignatura");
        ExamenEN examenEN = examenCEN.getExamenByCodigo(asignaturaEN, Integer.parseInt(request.getParameter("examen")));
        session.setAttribute("examen", examenEN);
        ConfiguracionAsignaturaIdiomaEN configuracionAsignaturaIdiomaEN = configuracionAsignaturaIdiomaCEN.getConfiguracionIdiomaPrincipal(asignaturaEN);    
        PreparacionExamenIdiomaEN preparacionPrincipal = preparacionExamenIdiomaCEN.getPreparacionExamenByExamenEIdioma(examenEN, configuracionAsignaturaIdiomaEN.getIdioma());
        
        examenCEN.rellenarInstrucciones(examenEN);
        examenCEN.rellenarPreguntas(examenEN);
        examenCEN.rellenarPreparacionIdiomas(examenEN);
        
        //Guardar cambios
        if (request.getParameter("nombre")!= null)
        {
            ArrayList<ExamenPreguntaEN> preguntas = (ArrayList<ExamenPreguntaEN>)session.getAttribute("preguntasExamen");
            boolean examenModificado = false;
            boolean preparacionModificada = false;
            boolean instruccionesModificadas = false;
            boolean preguntasModificadas = false;
            
            //--------------------------Realizamos las comprobaciones ---------------------------//
            String duracion[] = request.getParameter("duracion").split(":");
            String fechaStr[] = request.getParameter("fecha").split("-");
            Calendar fecha = Calendar.getInstance();
            fecha.set(Integer.parseInt(fechaStr[0]), Integer.parseInt(fechaStr[1])-1, Integer.parseInt(fechaStr[2]));
            
            //examen
            if (examenEN.getFecha().get(Calendar.DAY_OF_MONTH) != fecha.get(Calendar.DAY_OF_MONTH) ||
            examenEN.getFecha().get(Calendar.MONTH) != fecha.get(Calendar.MONTH) || 
            examenEN.getFecha().get(Calendar.YEAR) != fecha.get(Calendar.YEAR) || 
            examenEN.getModalidades() != Integer.parseInt(request.getParameter("modalidades")) || 
            examenEN.getDuracion() != Integer.parseInt(duracion[0])*60 + Integer.parseInt(duracion[1]))
                examenModificado = true;
            
            //Preparación
            if (!preparacionPrincipal.getTraduccionNombre().equals(request.getParameter("nombre")) || 
            !preparacionPrincipal.getTraduccionFecha().equals(request.getParameter("fechaTexto")) || 
            !preparacionPrincipal.getTraduccionDuracion().equals(request.getParameter("duracionTexto")))
                preparacionModificada = true;
            
            
            //Instrucciones
            
            //Total de instrucciones del formulario recorridas
            int i = 1;
            //Indica el número de instrucciones real (las que no estan vacías)
            int j = 0;
            
            ArrayList<TraduccionInstruccionEN> traduccionInstrucciones = traduccionInstruccionCEN.getTraduccionesInstruccionByExamenEIdioma(examenEN, configuracionAsignaturaIdiomaEN.getIdioma());
            ArrayList<TraduccionInstruccionEN> traduccionInstruccionesAux = traduccionInstrucciones;
            
            int cant_instrucciones = traduccionInstruccionesAux.size();
            
            while(request.getParameter("inst" + i) != null && instruccionesModificadas == false)
            {
                if (request.getParameter("inst" + i) != "")
                {
                    if (!traduccionInstruccionCEN.eliminaTraduccionEnListaByEnunciado(traduccionInstruccionesAux, request.getParameter("inst" + i)))
                        instruccionesModificadas = true;
                    
                    j++;
                }
            
                i++;
            }
            
            //Anque todas coincidan puede que se haya borrado alguna
            if (cant_instrucciones != j)
                instruccionesModificadas = true;
            
            i = 0;
            
            //Preguntas
            if (preguntas.size() == examenEN.getPreguntas().size())
            {
                while(i < preguntas.size() && instruccionesModificadas == false)
                {
                    if (!examenPreguntaCEN.borrarDeLista(examenEN.getPreguntas(), preguntas.get(i).getPregunta()))
                        preguntasModificadas = true;
                    
                    i++;
                }
            }
            else
                preguntasModificadas = true;
            

            //--------------------------Realizamos la modificaciones ---------------------------//
            if (preguntasModificadas)
            {
                examenPreguntaCEN.eliminarPreguntasDeExamen(examenEN);
                
                //Necesitamos actualizar la lista de preguntas con las modificaciones realizadas
                //En la eliminacion de pregunta del examen
                examenPreguntaCEN.sustituirEnLista(preguntas, examenEN.getPreguntas());
                
                for (ExamenPreguntaEN pregunta : preguntas)
                {
                    examenPreguntaCEN.New(pregunta);

                    //Modificamos el ultimo uso, el proximo uso y las utilizaciones
                    pregunta.getPregunta().setProximoUso(false);
                    pregunta.getPregunta().setUtilizada(pregunta.getPregunta().getUtilizada()+1);
                    if (pregunta.getPregunta().getUltimoUso() == null || pregunta.getPregunta().getUltimoUso().compareTo(examenEN.getFecha()) < 0)
                        pregunta.getPregunta().setUltimoUso(examenEN.getFecha());

                    preguntaCEN.Modify(pregunta.getPregunta());
                }
                
                examenEN.setPreguntas(preguntas);
                
                //Guardamos la nueva discriminación y dificultad
                examenEN.setDiscriminacion(Double.parseDouble(request.getParameter("discriminacion")));
                examenEN.setDificultad(Double.parseDouble(request.getParameter("dificultad")));
            }
            if (examenModificado)
            {           
                examenEN.setFecha(fecha);
                examenEN.setModalidades(Integer.parseInt(request.getParameter("modalidades")));
                examenEN.setDuracion(Integer.parseInt(duracion[0])*60 + Integer.parseInt(duracion[1]));
                
            }
            //Para no guardar dos veces el examen
            if (preguntasModificadas || examenModificado)
            {
                examenCEN.Modify(examenEN);
            }
            if (preparacionModificada)
            {
                preparacionPrincipal.setTraduccionNombre(request.getParameter("nombre"));
                preparacionPrincipal.setTraduccionFecha(request.getParameter("fechaTexto"));
                preparacionPrincipal.setTraduccionDuracion(request.getParameter("duracionTexto"));
                preparacionPrincipal.setRevisada(true);
                preparacionExamenIdiomaCEN.Modify(preparacionPrincipal);
            }
            if (instruccionesModificadas)
            {
                //Esto elimina las traducciones también (Lo necesitamos para que se retraduzcan)
                instruccionCEN.eliminarInstruccionesExamen(examenEN);
                ArrayList<InstruccionEN> nuevas = new ArrayList<InstruccionEN>();
                               
                i = 1;
                while(request.getParameter("inst" + i) != null)
                {
                    if (request.getParameter("inst" + i) != "")
                    {
                        InstruccionEN instruccionEN = new InstruccionEN();
                        instruccionEN.setExamen(examenEN);
                        instruccionCEN.New(instruccionEN);
                        
                        TraduccionInstruccionEN traduccionInstruccionEN = new TraduccionInstruccionEN();
                        traduccionInstruccionEN.setInstruccion(instruccionEN);
                        traduccionInstruccionEN.setIdioma(configuracionAsignaturaIdiomaEN.getIdioma());
                        traduccionInstruccionEN.setEnunciado(request.getParameter("inst" + i));
                        traduccionInstruccionCEN.New(traduccionInstruccionEN);
                        
                        nuevas.add(instruccionEN);
                    }
                    i++;
                }
                examenEN.setInstrucciones(nuevas);
            }
            //Marcar las preparaciones como no revisadas
            if (preparacionModificada || instruccionesModificadas)
            {
                ArrayList<PreparacionExamenIdiomaEN> preparaciones = preparacionExamenIdiomaCEN.getPreparacionesExamenByExamen(examenEN);
                
                for (PreparacionExamenIdiomaEN preparacion : preparaciones)
                {
                    //Si no es el idioma principal lo marcamos como no revisado
                    if (preparacion.getIdioma().getCodigo() != configuracionAsignaturaIdiomaEN.getIdioma().getCodigo())
                    {
                        preparacion.setRevisada(false);
                        preparacionExamenIdiomaCEN.Modify(preparacion);
                    }
                }
            }
            
            response.sendRedirect("detalleExamen.jsp?examen=" + examenEN.getCodigo());
        }
        else
        {
            session.setAttribute("preguntasExamen", examenEN.getPreguntas());

            contenidoPre += "<form name=\"editar\" action=\"detalleExamen.jsp?examen=" + examenEN.getCodigo() + "\" method=\"POST\" accept-charset=\"utf-8\">";
            contenidoPre += "<div style=\"float: right; margin-right: 2px\" class=\"boton_pregunta\" onClick=\"location.href='verExamenes.jsp'\" />Volver</div>";
            if (!examenEN.isAnalizado())
                contenidoPre += "<div style=\"float: right; margin-right: 10px\" class=\"botonEditar\"><div id=\"boton_examen\" onClick=\"editarExamen()\" />Editar Examen</div></div>";
            
            contenidoPre += "<br />";
            contenidoPre += "<div id=\"caja\">";
            contenidoPre += "<h3 class=\"margen\">Datos del examen:</h3>";
            contenidoPre += "<div class=\"borde\">";
            contenidoPre += "<div class=\"margen\">Nombre del examen: <input type=\"text\" id=\"nombre\" name=\"nombre\" size=\"50\" value=\"" + preparacionPrincipal.getTraduccionNombre() + "\"disabled/></div>";
            contenidoPre += "<div class=\"margen\">Fecha: <input type=\"date\" id=\"fecha\" name=\"fecha\" style=\"width: 11em\" value=\"" + examenEN.getFecha().get(Calendar.YEAR) + "-"
                    + Utiles.rellenaConCeros(String.valueOf((examenEN.getFecha().get(Calendar.MONTH) + 1)), 2) + "-" + Utiles.rellenaConCeros(String.valueOf(examenEN.getFecha().get(Calendar.DAY_OF_MONTH)), 2) + "\" disabled/>";
            contenidoPre += " en texto: <input type=\"text\" id=\"fechaTexto\" name=\"fechaTexto\" size=\"40\" disabled value=\"" + preparacionPrincipal.getTraduccionFecha() + "\"/></div>";
            contenidoPre += "<div class=\"margen\">Cantidad de modalidades: <input type=\"number\" id=\"modalidades\" name=\"modalidades\" min=\"1\" max=\"20\" value=\"" + examenEN.getModalidades() + "\"disabled /></div>";
            contenidoPre += "<div class=\"margen\">Duración: <input type=\"time\" id=\"duracion\" name=\"duracion\" value=\"" + Utiles.rellenaConCeros(String.valueOf((int)Math.floor(examenEN.getDuracion()/60)), 2)
                    + ":" + Utiles.rellenaConCeros(String.valueOf(examenEN.getDuracion()%60), 2) + "\" disabled> en texto: <input type=\"text\" id=\"duracionTexto\" name=\"duracionTexto\" size=\"40\" value=\"" + preparacionPrincipal.getTraduccionDuracion() + "\" disabled /></div>";
            contenidoPre += "<div class=\"margen\">Índice de dificultad: <input type=\"text\" id=\"dificultad\" name=\"dificultad\" size=\"6\" value=\" " + examenEN.getDificultad() + "\" disabled />";
            contenidoPre += " Índice de discriminación <input type=\"text\" id=\"discriminacion\" name=\"discriminacion\" size=\"6\" value=\" " + examenEN.getDiscriminacion() + "\" disabled /></div>";
            contenidoPre += "</div>";
            contenidoPre += "<br />";
            contenidoPre += "<div id=\"keywords_externo\">";
            contenidoPre += "<h3 class=\"margen\">Instrucciones:</h3>";
            contenidoPre += "<div class=\"borde\"><br />";
            contenidoPre += "<div id=\"instrucciones\">";

            if (examenEN.getInstrucciones().size() == 0)
            {    
                contenidoPre += "<div class=\"margen\">Instruccion 1:&nbsp;&nbsp;";
                contenidoPre += "<textarea class=\"instruccion\" style=\"vertical-align: top\" name=\"inst1\" cols=\"70\" rows=\"3\" disabled></textarea>";
                contenidoPre += "</div>";
            }
            else
            {
                for (InstruccionEN instruccionEN: examenEN.getInstrucciones())
                {
                    instrucciones++;

                    TraduccionInstruccionEN traduccionInstruccionEN = traduccionInstruccionCEN.getTraduccionInstruccionByInstruccionEIdioma(instruccionEN, configuracionAsignaturaIdiomaEN.getIdioma());

                    contenidoPre += "<div class=\"margen\">Instruccion " + instrucciones + ":&nbsp;&nbsp;";
                    contenidoPre += "<textarea class=\"instruccion\" style=\"vertical-align: top\" name=\"inst" + instrucciones + "\" cols=\"70\" rows=\"3\" disabled>" + Utiles.textoHTML(traduccionInstruccionEN.getEnunciado()) + "</textarea>";
                    contenidoPre += "</div>";
                }
            }

            contenidoPre += "</div><br/>";
            contenidoPre += "<input class=\"margen\" type=\"button\" id=\"nuevaInstruccion\" onClick=\"anyadirInstruccion()\" value=\"Añadir Instrucción\" disabled>";
            contenidoPre += "<br /></div>";
            contenidoPre += "<br />";
            contenidoPre += "</div>";
            contenidoPre += "</div>";
            contenidoPre += "<br /><br />";
            contenidoPre += "</form>";
            contenidoPost += "<div id=\"error\"></div><br />";
 
            contenidoPost += "<div style=\"float: right; margin-right: 2px\" class=\"boton_pregunta\" onClick=\"location.href='verExamenes.jsp'\" />Volver</div>";
            
            if (!examenEN.isAnalizado())
                contenidoPost += "<div style=\"float: right; margin-right: 10px\" class=\"botonEditar\"><div id=\"boton_examen\" onClick=\"editarExamen()\" />Editar Examen</div></div>";
            contenidoPost += "<br />";
            
        }
    }
    else
        response.sendRedirect("verExamenes.jsp");
    
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><%= titulo %></title>
        <link rel="stylesheet" href="styles/main-stylesheet.css" />
        <link rel="stylesheet" href="styles/menu.css" /> 
        <script type="text/javascript" src="script/javascript.js"></script>
    </head>
    <body onLoad="actualizarContadorInstrucciones(<%=instrucciones%>)">
        <%@include file="includes/menu.jsp"%>
        <div id="tooltip"></div>
        <div id="contenido" onMouseOver="hideTooltip()">
            <%=contenidoPre%>
            <h3 id="seleccionar" class="margen_doble"></h3>
            <div id="ext_filtro" style="display: none">
            <jsp:include page="includes/filtroPreguntas.jsp">
                <jsp:param name="modo" value="seleccionarEditar" />
            </jsp:include>  
            </div>
            <form name="formPreguntas">
                <div id="listaSeleccionarConfirmacion"></div>
                <h3 id="seleccionadas" class="margen_doble">Preguntas:</h3>
                <form name=formPreguntas>
                    <div id="listaSeleccionadasConfirmacion">
                        <jsp:include page="includes/listadoPreguntas.jsp">
                            <jsp:param name="modo" value="seleccionadasEditar" />
                            <jsp:param name="pagina" value="1" />
                            <jsp:param name="fechaMax" value=""/>
                            <jsp:param name="editar" value="False"/>
                        </jsp:include>
                    </div>
                </form>
            </form>
            <%=contenidoPost%>
        </div>
    </body>
</html>
