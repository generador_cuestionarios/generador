<%-- 
    Copyright (C) 2014 Raúl Sempere Trujillo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Document   : reclasificar
    Created on : 18-jul-2014, 14:45:00
    Author     : Raúl Sempere Trujillo
--%>

<%-- 
    Este documento muestra la barra del estado de reclasificación. Inicia el
    hilo de reclasificacion de las preguntas e inicia el temporizador 
    para actualizar la barra con javascript
--%>

<%@page import="CEN.ClasificacionTemaCEN"%>
<%@page import="EN.AsignaturaEN"%>
<%@page import="clases_generador.HiloReclasificar"%>
<%@page import="EN.ClasificacionTemaEN"%>
<%@page import="CEN.PreguntaCEN"%>

<% String titulo = "Reclasificar preguntas - Generador Inteligente de Cuestinarios"; %>

<%
    AsignaturaEN asignaturaEN = (AsignaturaEN)session.getAttribute("asignatura");
    ClasificacionTemaCEN clasificacionTemaCEN;
    clasificacionTemaCEN = new ClasificacionTemaCEN();
    session.setAttribute("reclasificar", clasificacionTemaCEN);
    
    HiloReclasificar hiloReclasificar = new HiloReclasificar(asignaturaEN, clasificacionTemaCEN);
        
    //Este hilo se enctarga de procesar toda la clasificación
    //Asi el sistema queda libre para mostrar la barra de progreso
     hiloReclasificar.start();   
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><%= titulo %></title>
        <link rel="stylesheet" href="styles/main-stylesheet.css" />
        <link rel="stylesheet" href="styles/menu.css" />
        <script type="text/javascript" src="script/javascript.js"></script>
    </head>
    <body onLoad="barraProgresoReclasificar();">
        <%@include file="includes/menu.jsp"%>
        <div id="contenido">
            <div id="caja">
                <h2 class="margen">Reclasificando preguntas</h2>
                <div id="barra"></div>
            </div>
        </div>
    </body>
</html>
