<%-- 
    Copyright (C) 2014 Ra�l Sempere Trujillo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Document   : guardarExamen
    Created on : 11-jul-2014, 15:56:00
    Author     : Ra�l Sempere Trujillo
--%>

<%-- 
    Este documento se emplea para guardar el nuevo examen que el usuario ha
    creado.
--%>

<%@page import="EN.TraduccionInstruccionEN"%>
<%@page import="EN.InstruccionEN"%>
<%@page import="EN.ExamenPreguntaEN"%>
<%@page import="CEN.ExamenPreguntaCEN"%>
<%@page import="CEN.TraduccionInstruccionCEN"%>
<%@page import="CEN.InstruccionCEN"%>
<%@page import="clases_generador.Traduccion"%>
<%@page import="CEN.PreguntaCEN"%>
<%@page import="CEN.PreparacionExamenIdiomaCEN"%>
<%@page import="CEN.ExamenCEN"%>
<%@page import="EN.PreguntaEN"%>
<%@page import="java.util.ArrayList"%>
<%@page import="EN.PreparacionExamenIdiomaEN"%>
<%@page import="EN.ExamenEN"%>
<%@page import="EN.AsignaturaEN"%>
<%@page import="clases_generador.XMLManager"%>

<%
    request.setCharacterEncoding("UTF-8");
    
    if (session.getAttribute("usuario") == null)
    {
       response.sendRedirect("login.jsp");
    }
    else if (session.getAttribute("asignatura") == null)
    {
       response.sendRedirect("asignaturas.jsp");
    }
    else
    {
        ExamenCEN examenCEN = new ExamenCEN();
        PreparacionExamenIdiomaCEN preparacionCEN = new PreparacionExamenIdiomaCEN();
        PreguntaCEN preguntaCEN = new PreguntaCEN();
        InstruccionCEN instruccionCEN = new InstruccionCEN();
        TraduccionInstruccionCEN traduccionInstruccionCEN = new TraduccionInstruccionCEN();
        ExamenPreguntaCEN examenPreguntaCEN = new ExamenPreguntaCEN();
        
        ExamenEN examenEN = (ExamenEN)session.getAttribute("examen");
        PreparacionExamenIdiomaEN preparacionEN = (PreparacionExamenIdiomaEN)session.getAttribute("preparacion");
        ArrayList<PreguntaEN> preguntas = (ArrayList<PreguntaEN>)session.getAttribute("listaSeleccionadas");
        ExamenPreguntaEN examenPreguntaEN;
        InstruccionEN instruccionEN;
        TraduccionInstruccionEN traduccionInstruccionEN;
        
        //Guardamos el examen
        examenCEN.New(examenEN);
        
        //Guardamos la preparaci�n en el idioma principal
        preparacionCEN.New(preparacionEN); 
        
        //Guardamos las preguntas del examen
        
        double discriminacion = 0;
        double dificultad = 0;
        
        for (PreguntaEN preguntaEN: preguntas)
        {
            examenPreguntaEN = new ExamenPreguntaEN();
            
            examenPreguntaEN.setExamen(examenEN);
            examenPreguntaEN.setPregunta(preguntaEN);
            
            examenPreguntaCEN.New(examenPreguntaEN);
            
            //Modificamos el ultimo uso, el proximo uso y las veces que se ha utilizado la
            preguntaEN.setProximoUso(false);
            preguntaEN.setUtilizada(preguntaEN.getUtilizada()+1);
            if (preguntaEN.getUltimoUso() == null || preguntaEN.getUltimoUso().compareTo(examenEN.getFecha()) < 0)
                preguntaEN.setUltimoUso(examenEN.getFecha());
            
            preguntaCEN.Modify(preguntaEN);
            
            discriminacion += preguntaEN.getIndiceDiscriminacion();
            dificultad += preguntaEN.getIndiceDificultad();
        }
        
        examenEN.setDificultad(dificultad/preguntas.size());
        examenEN.setDiscriminacion(discriminacion/preguntas.size());
        examenCEN.Modify(examenEN); 
        
        //Guardamos las instrucciones
        int i = 1;
        
        while(request.getParameter("inst" + i) != null)
        {
            if (request.getParameter("inst" + i) != "")
            {
                //Primero la instrucci�n
                instruccionEN = new InstruccionEN();

                instruccionEN.setExamen(examenEN);
                instruccionCEN.New(instruccionEN);
 
                //Luego la traduccion de la misma al idioma principal
                traduccionInstruccionEN = new TraduccionInstruccionEN();
                
                traduccionInstruccionEN.setInstruccion(instruccionEN);
                traduccionInstruccionEN.setIdioma(preparacionEN.getIdioma());
                traduccionInstruccionEN.setEnunciado(request.getParameter("inst" + i));
                
                traduccionInstruccionCEN.New(traduccionInstruccionEN);
            }
            
            i++;
        }
        //Limpiamos todas las variables de sesi�n
        session.removeAttribute("examen");
        session.removeAttribute("preparacion");
        session.removeAttribute("listaSeleccionadas");
        
        response.sendRedirect("verTraduccionesExamen.jsp?examen=" + examenEN.getCodigo());
    }
%>
