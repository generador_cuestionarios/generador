/* 
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : OpcionEN
 * Created on : 15-abr-2014, 00:46:00
 * Author     : Raúl Sempere Trujillo
 */

package EN;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Raúl Sempere Trujillo
 * 
 * Clase EN de la entidad AccesoUsuarioAsignatura
 */
public class OpcionEN implements Serializable
{
    /**
     * Variable privada: pregunta de la opción
     */
    private PreguntaEN pregunta;
    /**
     * Variable privada: lista de traducciones de la opción
     */
    private ArrayList<TraduccionOpcionEN> traducciones;
    /**
     * Variable privada: código de la respuesta
     */
    private int codigo;
    /**
     * Variable privada: indica si es la respuesta correcta
     */
    private boolean correcta;
    
    /**
     * Constructor por defecto
     */
    public OpcionEN()
    {
        pregunta = null;
        traducciones = null;
        codigo = 0;
        correcta = false;
    }
    
    /**
     * Constructor parametrizado. inicializa la clase con los valores  
     * pasados por parametro
     * 
     * @param pregunta PreguntaEN a la que pertenece la opción
     * @param traducciones ArrayList con la lista de traducciones de la opción
     * @param codigo int con el código de la opción
     * @param correcta boolean que indica si la opción es o no correcta
     */
    public OpcionEN(PreguntaEN pregunta, ArrayList<TraduccionOpcionEN> traducciones, int codigo, boolean correcta)
    {
        this.pregunta = pregunta;
        this.traducciones = traducciones;
        this.codigo = codigo;
        this.correcta = correcta;
    }

    /**
     * @return the pregunta
     */
    public PreguntaEN getPregunta()
    {
        return pregunta;
    }

    /**
     * @param pregunta the pregunta to set
     */
    public void setPregunta(PreguntaEN pregunta)
    {
        this.pregunta = pregunta;
    }

    /**
     * @return the traducciones
     */
    public ArrayList<TraduccionOpcionEN> getTraducciones()
    {
        return traducciones;
    }

    /**
     * @param traducciones the traducciones to set
     */
    public void setTraducciones(ArrayList<TraduccionOpcionEN> traducciones)
    {
        this.traducciones = traducciones;
    }

    /**
     * @return the codigo
     */
    public int getCodigo()
    {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(int codigo)
    {
        this.codigo = codigo;
    }

    /**
     * @return the correcta
     */
    public boolean isCorrecta()
    {
        return correcta;
    }

    /**
     * @param correcta the correcta to set
     */
    public void setCorrecta(boolean correcta)
    {
        this.correcta = correcta;
    }
}
