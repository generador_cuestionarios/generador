/* 
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : ClasificacionTemaEN
 * Created on : 15-abr-2014, 00:46:00
 * Author     : Raúl Sempere Trujillo
 */

package EN;

import java.io.Serializable;

/**
 *
 * @author Raúl Sempere Trujillo
 * 
 * Clase EN de la entidad AccesoUsuarioAsignatura
 */
public class ClasificacionTemaEN implements Serializable
{
    /**
     * Variable privada: tema en el que ha sido clasificada
     */
    private TemaEN tema;
    /**
     * Variable privada: pregunta clasificada
     */
    private PreguntaEN pregunta;
    /**
     * Variable privada: indica si la clasificacion ha sido revisada
     */
    private boolean revisada;
    
    /**
     * Constructor por defecto
     */
    public ClasificacionTemaEN()
    {
        tema = null;
        pregunta = null;
        revisada = false;
    }
    
    /**
     * Constructor parametrizado. inicializa la clase con los valores  
     * pasados por parametro
     * 
     * @param tema TemaEN de la clasificación
     * @param pregunta PreguntaEN de la clasificación
     * @param revisada bool que indica si la clasificación ha sido revisada
     */
    public ClasificacionTemaEN(TemaEN tema, PreguntaEN pregunta, boolean revisada)
    {
        this.tema = tema;
        this.pregunta = pregunta;
        this.revisada = revisada;
    }

    /**
     * @return the tema
     */
    public TemaEN getTema()
    {
        return tema;
    }

    /**
     * @param tema the tema to set
     */
    public void setTema(TemaEN tema)
    {
        this.tema = tema;
    }

    /**
     * @return the pregunta
     */
    public PreguntaEN getPregunta()
    {
        return pregunta;
    }

    /**
     * @param pregunta the pregunta to set
     */
    public void setPregunta(PreguntaEN pregunta)
    {
        this.pregunta = pregunta;
    }

    /**
     * @return the revisada
     */
    public boolean isRevisada()
    {
        return revisada;
    }

    /**
     * @param revisada the revisada to set
     */
    public void setRevisada(boolean revisada)
    {
        this.revisada = revisada;
    }
    
}
