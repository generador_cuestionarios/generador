/* 
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : PreguntaEN
 * Created on : 15-abr-2014, 00:46:00
 * Author     : Raúl Sempere Trujillo
 */

package EN;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;

/**
 *
 * @author Raúl Sempere Trujillo
 * 
 * Clase EN de la entidad Pregunta
 * Para poder funcionar correctamente como nosotros queremos tomcat necesita
 * que esta clase sea serializable
 */
public class PreguntaEN implements Serializable
{
    /**
     * Variable privada: asignatura a la que pertenece la pregunta
     */
    private AsignaturaEN asignatura;
    /**
     * Variable privada: clasificación de la pregunta en un tema
     */
    private ClasificacionTemaEN clasificacion;
    /**
     * Variable privada: ArrayList con la lista de opciones de la pregunta
     */
    private ArrayList<OpcionEN> opciones;
    /**
     * Variable privada: ArrayList con las traducciones de la pregunta
     */
    private ArrayList<TraduccionPreguntaEN> traducciones;
    /**
     * Variable privada: ArrayList con los examenes en los que se utiliza la pregunta
     */
    private ArrayList<ExamenPreguntaEN> examenes;
    /**
     * Variable privada: codigo del la pregunta
     */
    private int codigo;
    /**
     * Variable privada: cantidad de veces que se ha contestado esta pregunta
     */
    private int contestada;
    /**
     * Variable privada: numero de aciertos de esta pregunta
     */
    private int aciertos;
    /**
     * Variable privada: numero de veces que la pregunta se ha dejado en blanco
     */
    private int noContestada;
    /**
     * Variable privada: numero de veces que aparece en un examen
     */
    private int utilizada;
    /**
     * Variable privada: índice de dificultad de la pregunta
     */
    private double indiceDificultad;
    /**
     * Variable privada: índice de discriminación de la pregunta
     */
    private double indiceDiscriminacion;
    /**
     * Variable privada: indica si la pregunta se puede utilizar en los exámenes
     */
    private boolean habilitada;
    /**
     * Variable privada: indica si se debe forzar esta pregunta en el próximo examen
     */
    private boolean proximoUso;
   /**
     * Variable privada: Fecha del ultimo uso en un examen de la pregunta. se puede calcular a partir de los examenes
     * pero mantenerla en la pregunta agiliza los listados y las búsquedas
     */
    private Calendar ultimoUso;
    
    /**
     * Constructor por defecto.
     */
    public PreguntaEN()
    {
        asignatura = null;
        clasificacion = null;
        opciones = null;
        examenes = null;
        codigo = 0;
        contestada = 0;
        aciertos = 0;
        noContestada = 0;
        utilizada = 0;
        indiceDificultad = 0.0;
        indiceDiscriminacion = 0.0;
        traducciones = null;
        habilitada = true;
        proximoUso = false;
        ultimoUso = null;
    }
    
    /**
     * Constructor parametrizado. inicializa la clase con los valores de una pregunta
     * 
     * @param asignatura AsignaturaEN a la que pertenece la pregunta
     * @param clasificacion ClasificacionTemaEN. Clasificacion en un tema de la pregunta
     * @param opciones ArrayList. Opciones para responder de la pregunta
     * @param traducciones ArrayList. Traducciones disponibles para la pregunta
     * @param examenes ArrayList. Examenes en los que aparece la pregunta
     * @param codigo int con el codigo de la pregunta
     * @param contestada int. Cantidad de veces que ha sido contestada
     * @param aciertos int. Cantidad de veces que ha sido respondida correctamente
     * @param noContestada int. Cantidad de veces que se ha dejado en blanco
     * @param utilizada int. veces que ha aparecido en un examen
     * @param indiceDificultad double. Indice de dificultad
     * @param indiceDiscriminacion double. Indice de discriminación
     * @param habilitada boolean. Indica si la pregunta se puede utilziar en los exámenes
     * @param proximoUso boolean. Indica si la pregunta se forzará en el siguiente examen
     * @param ultimoUso Calendar. Fecha de la ultima aparición en un examen
     */
    public PreguntaEN(AsignaturaEN asignatura, ClasificacionTemaEN clasificacion, ArrayList<OpcionEN> opciones, ArrayList<TraduccionPreguntaEN> traducciones,
            ArrayList<ExamenPreguntaEN> examenes, int codigo, int contestada, int aciertos, int noContestada, int utilizada, double indiceDificultad, double indiceDiscriminacion, 
            boolean habilitada, boolean proximoUso, Calendar ultimoUso)
    {
        this.asignatura = asignatura;
        this.clasificacion = clasificacion;
        this.opciones = opciones;
        this.examenes = examenes;
        this.codigo = codigo;
        this.contestada = contestada;
        this.aciertos = aciertos;
        this.noContestada = noContestada;
        this.indiceDificultad = indiceDificultad;
        this.indiceDiscriminacion = indiceDiscriminacion;
        this.traducciones = traducciones;
        this.habilitada = habilitada;
        this.proximoUso = proximoUso;
        this.ultimoUso = ultimoUso;
    }

    /**
     * @return the asignatura
     */
    public AsignaturaEN getAsignatura()
    {
        return asignatura;
    }

    /**
     * @param asignatura the asignatura to set
     */
    public void setAsignatura(AsignaturaEN asignatura)
    {
        this.asignatura = asignatura;
    }

    /**
     * @return the codigo
     */
    public int getCodigo()
    {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(int codigo)
    {
        this.codigo = codigo;
    }

    /**
     * @return the contestada
     */
    public int getContestada()
    {
        return contestada;
    }

    /**
     * @param contestada the contestada to set
     */
    public void setContestada(int contestada)
    {
        this.contestada = contestada;
    }

    /**
     * @return the aciertos
     */
    public int getAciertos()
    {
        return aciertos;
    }

    /**
     * @param aciertos the aciertos to set
     */
    public void setAciertos(int aciertos)
    {
        this.aciertos = aciertos;
    }

    /**
     * @return the indiceDificultad
     */
    public double getIndiceDificultad()
    {
        return indiceDificultad;
    }

    /**
     * @param indiceDificultad the indiceDificultad to set
     */
    public void setIndiceDificultad(double indiceDificultad)
    {
        this.indiceDificultad = indiceDificultad;
    }

    /**
     * @return the indiceDiscriminacion
     */
    public double getIndiceDiscriminacion()
    {
        return indiceDiscriminacion;
    }

    /**
     * @param indiceDiscriminacion the indiceDiscriminacion to set
     */
    public void setIndiceDiscriminacion(double indiceDiscriminacion)
    {
        this.indiceDiscriminacion = indiceDiscriminacion;
    }

    /**
     * @return the clasificacion
     */
    public ClasificacionTemaEN getClasificacion()
    {
        return clasificacion;
    }

    /**
     * @param clasificacion the clasificacion to set
     */
    public void setClasificacion(ClasificacionTemaEN clasificacion)
    {
        this.clasificacion = clasificacion;
    }

    /**
     * @return the opciones
     */
    public ArrayList<OpcionEN> getOpciones()
    {
        return opciones;
    }

    /**
     * @param opciones the opciones to set
     */
    public void setOpciones(ArrayList<OpcionEN> opciones)
    {
        this.opciones = opciones;
    }

    /**
     * @return the traducciones
     */
    public ArrayList<TraduccionPreguntaEN> getTraducciones() {
        return traducciones;
    }

    /**
     * @param traducciones the traducciones to set
     */
    public void setTraducciones(ArrayList<TraduccionPreguntaEN> traducciones) {
        this.traducciones = traducciones;
    }

    /**
     * @return the examenes
     */
    public ArrayList<ExamenPreguntaEN> getExamenes() {
        return examenes;
    }

    /**
     * @param examenes the examenes to set
     */
    public void setExamenes(ArrayList<ExamenPreguntaEN> examenes) {
        this.examenes = examenes;
    }

    /**
     * @return the habilitada
     */
    public boolean isHabilitada() {
        return habilitada;
    }

    /**
     * @param habilitada the habilitada to set
     */
    public void setHabilitada(boolean habilitada) {
        this.habilitada = habilitada;
    }

    /**
     * @return the proximoUso
     */
    public boolean isProximoUso() {
        return proximoUso;
    }

    /**
     * @param proximoUso the proximoUso to set
     */
    public void setProximoUso(boolean proximoUso) {
        this.proximoUso = proximoUso;
    }

    /**
     * @return the ultimoUso
     */
    public Calendar getUltimoUso() {
        return ultimoUso;
    }

    /**
     * @param ultimoUso the ultimoUso to set
     */
    public void setUltimoUso(Calendar ultimoUso) {
        this.ultimoUso = ultimoUso;
    }

    /**
     * @return the noContestada
     */
    public int getNoContestada() {
        return noContestada;
    }

    /**
     * @param noContestada the noContestada to set
     */
    public void setNoContestada(int noContestada) {
        this.noContestada = noContestada;
    }

    /**
     * @return the utilizada
     */
    public int getUtilizada() {
        return utilizada;
    }

    /**
     * @param utilizada the utilizada to set
     */
    public void setUtilizada(int utilizada) {
        this.utilizada = utilizada;
    }
    
    /**
     * @return el número de fallos calculandolo
     */
    public int getFallos() {
        return contestada-aciertos;
    }
}