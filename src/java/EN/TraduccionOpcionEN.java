/* 
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : TraduccionOpcionEN
 * Created on : 15-abr-2014, 00:46:00
 * Author     : Raúl Sempere Trujillo
 */

package EN;

import java.io.Serializable;

/**
 *
 * @author Raúl Sempere Trujillo
 * 
 * Clase EN de la entidad AccesoUsuarioAsignatura
 */
public class TraduccionOpcionEN implements Serializable
{
    /**
     * Variable privada: opción de la tracucción
     */
    private OpcionEN opcion;
    /**
     * Variable privada: idioma de la traduccion
     */
    private IdiomaEN idioma;
    /**
     * Variable privada: enunciado de la opción
     */
    private String enunciado;
    
    /**
     * Constructor por defecto
     */
    public TraduccionOpcionEN()
    { 
        opcion = null;
        idioma = null;
        enunciado = "";
    }
    
    /**
     * Constructor parametrizado. inicializa la clase con los valores  
     * pasados por parametro
     * 
     * @param opcion OpcionEN que se esta traduciendo
     * @param idioma IdiomaEN de la traducción
     * @param enunciado String con el enunciado de la opción traducido
     */
    public TraduccionOpcionEN(OpcionEN opcion, IdiomaEN idioma, String enunciado)
    {
        this.opcion = opcion;
        this.idioma = idioma;
        this.enunciado = enunciado;
    }

    /**
     * @return the opcion
     */
    public OpcionEN getOpcion()
    {
        return opcion;
    }

    /**
     * @param opcion the opcion to set
     */
    public void setOpcion(OpcionEN opcion)
    {
        this.opcion = opcion;
    }

    /**
     * @return the idioma
     */
    public IdiomaEN getIdioma()
    {
        return idioma;
    }

    /**
     * @param idioma the idioma to set
     */
    public void setIdioma(IdiomaEN idioma)
    {
        this.idioma = idioma;
    }

    /**
     * @return the enunciado
     */
    public String getEnunciado()
    {
        return enunciado;
    }

    /**
     * @param enunciado the enunciado to set
     */
    public void setEnunciado(String enunciado)
    {
        this.enunciado = enunciado;
    }
    
}
