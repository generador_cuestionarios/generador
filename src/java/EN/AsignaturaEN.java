/* 
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : AsignaturaEN
 * Created on : 15-abr-2014, 00:46:00
 * Author     : Raúl Sempere Trujillo
 */

package EN;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author rst14
 */
public class AsignaturaEN implements Serializable
{
    /**
     * Variable privada: codigo de la asignatura
     */
    private int codigo;
    /**
     * Variable privada: user (nombre de usuario)
     */
    private int numeroOpciones;
    /**
     * Variable privada: ArrayList con la lista de accesos de usuarios 
     * de la asignatura
     */
    private ArrayList<AccesoUsuarioAsignaturaEN> accesosUsuarioAsignatura;
    /**
     * Variable privada: ArrayList con la lista examanes de la asignatura
     */
    private ArrayList<ExamenEN> examenes;
    /**
     * Variable privada: ArrayList con la lista de temas de la asignatura
     */
    private ArrayList<TemaEN> temas;
    /**
     * Variable privada: ArrayList con la lista de preguntas de la asignatura
     */
    private ArrayList<PreguntaEN> preguntas;
    /**
     * Variable privada: ArrayList con la lista idiomas configurados para la 
     * asignatura
     */
    private ArrayList<ConfiguracionAsignaturaIdiomaEN> configuracionesIdioma;  

    /**
     * Constructor por defecto.
     */
    public AsignaturaEN()
    {
        codigo = 0;
        numeroOpciones = 0;
        accesosUsuarioAsignatura = null;
        examenes = null;
        temas = null;
        preguntas = null;
        configuracionesIdioma = null;
    }
    
    /**
     * Constructor parametrizado. inicializa la clase con los valores de uns 
     * asignatura pasados por parametro
     * @param codigo int con el codigo del usuario
     * @param numeroOpciones String. nombre del usuario
     * que tiene el usuario
     * @param accesosUsuarioAsignatura ArrayList con la lista de accesos de 
     * usuarios a esta asignatura
     * @param examenes ArrayList con los examenes de la asignatura
     * @param temas ArrayList con la lista de temas de la asignatura
     * @param preguntas ArrayList con la lista de preguntas de la asignatura
     * @param configuracionesIdioma ArrayList con la lista idiomas configurados 
     * para la asignatura
     */
    public AsignaturaEN(int codigo, int numeroOpciones, ArrayList<AccesoUsuarioAsignaturaEN> accesosUsuarioAsignatura, ArrayList<ExamenEN> examenes, ArrayList<TemaEN> temas, ArrayList<PreguntaEN> preguntas, ArrayList<ConfiguracionAsignaturaIdiomaEN> configuracionesIdioma)
    {
        this.codigo = codigo;
        this.numeroOpciones = numeroOpciones;
        this.accesosUsuarioAsignatura = accesosUsuarioAsignatura;
        this.examenes = examenes;
        this.temas = temas;
        this.preguntas = preguntas;
        this.configuracionesIdioma = configuracionesIdioma;
    }

    /**
     * @return the codigo
     */
    public int getCodigo()
    {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(int codigo)
    {
        this.codigo = codigo;
    }

    /**
     * @return the numeroOpciones
     */
    public int getNumeroOpciones()
    {
        return numeroOpciones;
    }

    /**
     * @param numeroOpciones the numeroOpciones to set
     */
    public void setNumeroOpciones(int numeroOpciones)
    {
        this.numeroOpciones = numeroOpciones;
    }

    /**
     * @return the accesosUsuarioAsignatura
     */
    public ArrayList<AccesoUsuarioAsignaturaEN> getAccesosUsuarioAsignatura()
    {
        return accesosUsuarioAsignatura;
    }

    /**
     * @param accesosUsuarioAsignatura the accesosUsuarioAsignatura to set
     */
    public void setAccesosUsuarioAsignatura(ArrayList<AccesoUsuarioAsignaturaEN> accesosUsuarioAsignatura)
    {
        this.accesosUsuarioAsignatura = accesosUsuarioAsignatura;
    }

    /**
     * @return the temas
     */
    public ArrayList<TemaEN> getTemas()
    {
        return temas;
    }

    /**
     * @param temas the temas to set
     */
    public void setTemas(ArrayList<TemaEN> temas)
    {
        this.temas = temas;
    }

    /**
     * @return the preguntas
     */
    public ArrayList<PreguntaEN> getPreguntas()
    {
        return preguntas;
    }

    /**
     * @param preguntas the preguntas to set
     */
    public void setPreguntas(ArrayList<PreguntaEN> preguntas)
    {
        this.preguntas = preguntas;
    }

    /**
     * @return the configuracionesIdioma
     */
    public ArrayList<ConfiguracionAsignaturaIdiomaEN> getConfiguracionesIdioma()
    {
        return configuracionesIdioma;
    }

    /**
     * @param configuracionesIdioma the configuracionesIdioma to set
     */
    public void setConfiguracionesIdioma(ArrayList<ConfiguracionAsignaturaIdiomaEN> configuracionesIdioma)
    {
        this.configuracionesIdioma = configuracionesIdioma;
    }

    /**
     * @return the examenes
     */
    public ArrayList<ExamenEN> getExamenes() {
        return examenes;
    }

    /**
     * @param examenes the examenes to set
     */
    public void setExamenes(ArrayList<ExamenEN> examenes) {
        this.examenes = examenes;
    }
}
