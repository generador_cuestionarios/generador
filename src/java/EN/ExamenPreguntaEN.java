/* 
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : ExamenPreguntaEN
 * Created on : 15-abr-2014, 00:46:00
 * Author     : Raúl Sempere Trujillo
 */

package EN;

import java.io.Serializable;

/**
 *
 * @author Raúl Sempere Trujillo
 * 
 * Clase EN de la entidad ExamenPregunta
 * Clase EN de la entidad ExamenPregunta
 */
public class ExamenPreguntaEN implements Serializable
{
  /**
     * Variable privada: Examen
     */
    private ExamenEN examen;
    /**
     * Variable privada: Pregunta
     */
    private PreguntaEN pregunta;

    /**
     * Constructor por defecto
     */
    public ExamenPreguntaEN()
    {
        examen = null;
        pregunta = null;
    }
    
    /**
     * Constructor parametrizado. inicializa la clase con los valores de un tema
     * @param examen
     * @param pregunta
     */
    public ExamenPreguntaEN(ExamenEN examen, PreguntaEN pregunta)
    {
        this.examen = examen;
        this.pregunta = pregunta;
    }

    /**
     * @return the examen
     */
    public ExamenEN getExamen() {
        return examen;
    }

    /**
     * @param examen the examen to set
     */
    public void setExamen(ExamenEN examen) {
        this.examen = examen;
    }

    /**
     * @return the pregunta
     */
    public PreguntaEN getPregunta() {
        return pregunta;
    }

    /**
     * @param pregunta the pregunta to set
     */
    public void setPregunta(PreguntaEN pregunta) {
        this.pregunta = pregunta;
    }
}