/* 
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : InstruccionEN
 * Created on : 15-abr-2014, 00:46:00
 * Author     : Raúl Sempere Trujillo
 */

package EN;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Raúl Sempere Trujillo
 * 
 * Clase EN de la entidad Instrucción
 */
public class InstruccionEN implements Serializable
{
    /**
     * Variable privada: examen al que pertenece la instrucción
     */
    private ExamenEN examen;
    /**
     * Variable privada: Lista de traducciones disponibles para la instrucción
     */
    private ArrayList<TraduccionInstruccionEN> traduccionesInstruccion;
    /**
     * Variable privada: codigo de la instrucción
     */
    private int codigo;
    
    /**
     * Constructor por defecto.
     */
    public InstruccionEN()
    {
        examen = null;
        codigo = 0;
    }
    
    /**
     * Constructor parametrizado. inicializa la clase con los valores de una instruccion
     * 
     * @param examen ExamenEN al que pertenece la instrucción
     * @param traduccionesInstruccion ArrayList con la lista de traducciones de la instrucción
     * @param codigo int con el codigo de la instrucción
     */
    public InstruccionEN(ExamenEN examen, ArrayList<TraduccionInstruccionEN> traduccionesInstruccion, int codigo)
    {
        this.examen = examen;
        this.codigo = codigo;
    }

    /**
     * @return the examen
     */
    public ExamenEN getExamen() {
        return examen;
    }

    /**
     * @param examen the examen to set
     */
    public void setExamen(ExamenEN examen) {
        this.examen = examen;
    }

    /**
     * @return the traduccionesInstruccion
     */
    public ArrayList<TraduccionInstruccionEN> getTraduccionesInstruccion() {
        return traduccionesInstruccion;
    }

    /**
     * @param traduccionesInstruccion the traduccionesInstruccion to set
     */
    public void setTraduccionesInstruccion(ArrayList<TraduccionInstruccionEN> traduccionesInstruccion) {
        this.traduccionesInstruccion = traduccionesInstruccion;
    }

    /**
     * @return the codigo
     */
    public int getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }
}