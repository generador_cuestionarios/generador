/* 
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : IdiomaEN
 * Created on : 15-abr-2014, 00:46:00
 * Author     : Raúl Sempere Trujillo
 */

package EN;

import java.io.Serializable;

/**
 *
 * @author Raúl Sempere Trujillo
 * 
 * Clase EN de la entidad idioma
 */
public class IdiomaEN implements Serializable
{
    /**
     * Variable privada: codigo del idioma
     */
    private int codigo;
    /**
     * Variable privada: nombre del idioma
     */
    private String nombre;
    /**
     * Variable privada: traduccion de la palabra "instrucciones" en el idioma
     */
    private String traduccionInstrucciones;
    /**
     * Variable privada: traduccion de la palabra "preguntas" en el idioma
     */
    private String traduccionPreguntas;
    /**
     * Variable privada: traduccion de la palabra "modalidad" en el idioma
     */
    private String traduccionModalidad;
    /**
     * Variable privada: traduccion de la palabra "duración" en el idioma
     */
    private String traduccionDuracion;
    /**
     * Variable privada: traduccion de la palabra "respuestas" en el idioma
     */
    private String traduccionRespuestas;
    /**
     * Variable privada: indica el paquete babel a cargar al generar LaTeX
     */
    private String paqueteBabel;
    
    /**
     * Constructor por defecto.
     */
    public IdiomaEN()
    {
        codigo = 0;
        nombre = "";
        traduccionInstrucciones = "";
        traduccionPreguntas = "";
        traduccionModalidad = "";
        traduccionDuracion = "";
        traduccionRespuestas = "";
        paqueteBabel = "";
    }
    
    /**
     * Constructor parametrizado. inicializa la clase con los valores de un idioma
     * 
     * @param codigo Int con el código del idioma
     * @param nombre String con el nombre del idioma
     * @param traduccionInstrucciones String. traduccion de la palabra "instrucciones" en el idioma
     * @param traduccionPreguntas String. traduccion de la palabra "preguntas" en el idioma
     * @param traduccionModalidad String. traduccion de la palabra "modalidad" en el idioma
     * @param traduccionDuracion String. traduccion de la palabra "duración" en el idioma
     * @param traduccionRespuestas String. traduccion de la palabra "respuestas" en el idioma
     * @param paqueteBabel String. indica el paquete babel a cargar al generar LaTeX
     */
    public IdiomaEN(int codigo, String nombre, String traduccionInstrucciones, String traduccionPreguntas, String traduccionModalidad, 
            String traduccionDuracion, String traduccionRespuestas, String paqueteBabel)
    {
        this.codigo = codigo;
        this.nombre = nombre;
        this.traduccionInstrucciones = traduccionInstrucciones;
        this.traduccionPreguntas = traduccionPreguntas;
        this.traduccionModalidad = traduccionModalidad;
        this.traduccionDuracion = traduccionDuracion;
        this.traduccionRespuestas = traduccionRespuestas;
    }

    /**
     * @return the codigo
     */
    public int getCodigo()
    {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(int codigo)
    {
        this.codigo = codigo;
    }

    /**
     * @return the nombre
     */
    public String getNombre()
    {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    /**
     * @return the traduccionInstrucciones
     */
    public String getTraduccionInstrucciones()
    {
        return traduccionInstrucciones;
    }

    /**
     * @param traduccionInstrucciones the traduccionInstrucciones to set
     */
    public void setTraduccionInstrucciones(String traduccionInstrucciones)
    {
        this.traduccionInstrucciones = traduccionInstrucciones;
    }

    /**
     * @return the traduccionPreguntas
     */
    public String getTraduccionPreguntas()
    {
        return traduccionPreguntas;
    }

    /**
     * @param traduccionPreguntas the traduccionPreguntas to set
     */
    public void setTraduccionPreguntas(String traduccionPreguntas)
    {
        this.traduccionPreguntas = traduccionPreguntas;
    }

    /**
     * @return the traduccionModalidad
     */
    public String getTraduccionModalidad()
    {
        return traduccionModalidad;
    }

    /**
     * @param traduccionModalidad the traduccionModalidad to set
     */
    public void setTraduccionModalidad(String traduccionModalidad)
    {
        this.traduccionModalidad = traduccionModalidad;
    }

    /**
     * @return the traduccionDuracion
     */
    public String getTraduccionDuracion() {
        return traduccionDuracion;
    }

    /**
     * @param traduccionDuracion the traduccionDuracion to set
     */
    public void setTraduccionDuracion(String traduccionDuracion) {
        this.traduccionDuracion = traduccionDuracion;
    }

    /**
     * @return the traduccionRespuestas
     */
    public String getTraduccionRespuestas() {
        return traduccionRespuestas;
    }

    /**
     * @param traduccionRespuestas the traduccionRespuestas to set
     */
    public void setTraduccionRespuestas(String traduccionRespuestas) {
        this.traduccionRespuestas = traduccionRespuestas;
    }

    /**
     * @return the paqueteBabel
     */
    public String getPaqueteBabel() {
        return paqueteBabel;
    }

    /**
     * @param paqueteBabel the paqueteBabel to set
     */
    public void setPaqueteBabel(String paqueteBabel) {
        this.paqueteBabel = paqueteBabel;
    }

}
