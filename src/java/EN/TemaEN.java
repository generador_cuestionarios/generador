/* 
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : TemaEN
 * Created on : 15-abr-2014, 00:46:00
 * Author     : Raúl Sempere Trujillo
 */

package EN;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Raúl Sempere Trujillo
 * 
 * Clase EN de la entidad Tema
 */
public class TemaEN implements Serializable
{
    /**
     * Variable privada: asignatura a la que pertenece el tema
     */
    private AsignaturaEN asignatura;
    /**
     * Variable privada: codigo del tema
     */
    private int codigo;
    /**
     * Variable privada: nombre del tema (en el idioma primcipal)
     */
    private String nombre;
    /**
     * Variable privada: ArrayList con la lista de keywords del tema
     */
    private ArrayList<KeywordEN> keywords;
    /**
     * Variable privada: ArrayList con la lista clasificaciones de preguntas al tema
     */
    private ArrayList<ClasificacionTemaEN> clasificaciones;    
    
    /**
     * Constructor por defecto.
     */
    public TemaEN()
    {
        asignatura = null;
        codigo = 0;
        nombre = "";
        keywords = null;
        clasificaciones = null;
    }
    
    /**
     * Constructor parametrizado. inicializa la clase con los valores de un tema
     * 
     * @param asignatura AsignaturaEN a la que pertenece el tema
     * @param codigo int con el codigo del usuario
     * @param nombre String. nombre del tema
     * @param keywords ArrayList con la lista de keywords del tema
     * @param clasificaciones ArrayList con la lista de clasificaciones de preguntas
     */
    public TemaEN(AsignaturaEN asignatura, int codigo, String nombre, ArrayList<KeywordEN> keywords, ArrayList<ClasificacionTemaEN> clasificaciones)
    {
        this.asignatura = asignatura;
        this.codigo = codigo;
        this.nombre = nombre;
        this.keywords = keywords;
        this.clasificaciones = clasificaciones;
    }

    /**
     * @return the asignatura
     */
    public AsignaturaEN getAsignatura()
    {
        return asignatura;
    }

    /**
     * @param asignatura the asignatura to set
     */
    public void setAsignatura(AsignaturaEN asignatura)
    {
        this.asignatura = asignatura;
    }
    
    /**
     * @return the codigo
     */
    public int getCodigo()
    {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(int codigo)
    {
        this.codigo = codigo;
    }

    /**
     * @return the nombre
     */
    public String getNombre()
    {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    /**
     * @return the keywords
     */
    public ArrayList<KeywordEN> getKeywords()
    {
        return keywords;
    }

    /**
     * @param keywords the keywords to set
     */
    public void setKeywords(ArrayList<KeywordEN> keywords)
    {
        this.keywords = keywords;
    }

    /**
     * @return the clasificaciones
     */
    public ArrayList<ClasificacionTemaEN> getClasificaciones()
    {
        return clasificaciones;
    }

    /**
     * @param clasificaciones the clasificaciones to set
     */
    public void setClasificaciones(ArrayList<ClasificacionTemaEN> clasificaciones)
    {
        this.clasificaciones = clasificaciones;
    }
}