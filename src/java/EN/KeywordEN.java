/* 
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : KeywordEN
 * Created on : 15-abr-2014, 00:46:00
 * Author     : Raúl Sempere Trujillo
 */

package EN;

import java.io.Serializable;

/**
 *
 * @author Raúl Sempere Trujillo
 * 
 * Clase EN de la entidad Keyword
 */
public class KeywordEN implements Serializable
{
  /**
     * Variable privada: codigo del keyword
     */
    private int codigo;
    /**
     * Variable privada: palabra de la keyword
     */
    private String palabra;
    /**
     * Variable privada: Int con el peso de la keyword para el tema
     */
    private int peso;
    /**
     * Variable privada: TemaEN con el tema al que la keyword pertenece
     */
    private TemaEN tema;
    
    /**
     * Constructor por defecto.
     */
    public KeywordEN()
    {
        codigo = 0;
        palabra = "";
        peso = 0;
        tema = null;
    }
    
    /**
     * Constructor parametrizado. inicializa la clase con los valores  
     * pasados por parametro
     * 
     * @param codigo int, codigo de la keyword
     * @param palabra String, palabra de la keyword
     * @param peso int con el peso de la keyword
     * @param tema TemaEN con el tema al que la keyword pertenece
     */
    public KeywordEN(int codigo, String palabra, int peso, TemaEN tema)
    {
        this.codigo = codigo;
        this.palabra = palabra;
        this.peso = peso;
        this.tema = tema;
    }

    /**
     * @return the codigo
     */
    public int getCodigo()
    {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(int codigo)
    {
        this.codigo = codigo;
    }

    /**
     * @return the palabra
     */
    public String getPalabra()
    {
        return palabra;
    }

    /**
     * @param palabra the palabra to set
     */
    public void setPalabra(String palabra)
    {
        this.palabra = palabra;
    }

    /**
     * @return the peso
     */
    public int getPeso()
    {
        return peso;
    }

    /**
     * @param peso the peso to set
     */
    public void setPeso(int peso)
    {
        this.peso = peso;
    }

    /**
     * @return the tema
     */
    public TemaEN getTema()
    {
        return tema;
    }

    /**
     * @param tema the tema to set
     */
    public void setTema(TemaEN tema)
    {
        this.tema = tema;
    }
}