/* 
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : ConfiguracionAsignaturaIdiomaEN
 * Created on : 15-abr-2014, 00:46:00
 * Author     : Raúl Sempere Trujillo
 */

package EN;

import java.io.Serializable;

/**
 *
 * @author Raúl Sempere Trujillo
 * 
 * Clase EN de la entidad AccesoUsuarioAsignatura
 */
public class ConfiguracionAsignaturaIdiomaEN implements Serializable
{
    /**
     * Variable privada: asignatura que se configura en el idioma
     */
    private AsignaturaEN asignatura;
    /**
     * Variable privada: idioma de la configuración
     */
    private IdiomaEN idioma;
    /**
     * Variable privada: nombre de la asignatura en el idioma
     */
    private String nombreAsignatura;
    /**
     * Variable privada: indica si es el idioma principal de la asignatura
     */
    private boolean idiomaPrincipal;
    /**
     * Variable privada: String con el titulo del traductor utilizado para 
     * traducir del idioma principal a este idioma (no se utiliza si el idioma
     * que se esta configurando es el principal)
     */
    private String traductorApertium;

    /**
     * Constructor por defecto
     */
    public ConfiguracionAsignaturaIdiomaEN()
    {
        this.asignatura = null;
        this.idioma = null;
        this.nombreAsignatura = "";
        this.idiomaPrincipal = false;
        this.traductorApertium = "";
    }
    
    /**
     * Constructor parametrizado. inicializa la clase con los valores  
     * pasados por parametro
     * 
     * @param asignatura AsignaturaEN que se esta configurando
     * @param idioma IdiomaEN de la configuración
     * @param nombreAsignatura String con el nombre de la asignatura en el idioma
     * configurado
     * @param idiomaPrincipal bool que indica si el idioma es el principal de la
     * asignatura
     * @param traductorApertium String con el nombre del par de traducción a utilizar
     * para traducir del idioma base al idioma configurado
     */
    public ConfiguracionAsignaturaIdiomaEN(AsignaturaEN asignatura, IdiomaEN idioma, String nombreAsignatura, boolean idiomaPrincipal, String traductorApertium)
    {
        this.asignatura = asignatura;
        this.idioma = idioma;
        this.nombreAsignatura = nombreAsignatura;
        this.idiomaPrincipal = idiomaPrincipal;
        this.traductorApertium = traductorApertium;
    }

    /**
     * @return the asignatura
     */
    public AsignaturaEN getAsignatura()
    {
        return asignatura;
    }

    /**
     * @param asignatura the asignatura to set
     */
    public void setAsignatura(AsignaturaEN asignatura)
    {
        this.asignatura = asignatura;
    }

    /**
     * @return the idioma
     */
    public IdiomaEN getIdioma()
    {
        return idioma;
    }

    /**
     * @param idioma the idioma to set
     */
    public void setIdioma(IdiomaEN idioma)
    {
        this.idioma = idioma;
    }

    /**
     * @return the nombreAsignatura
     */
    public String getNombreAsignatura()
    {
        return nombreAsignatura;
    }

    /**
     * @param nombreAsignatura the nombreAsignatura to set
     */
    public void setNombreAsignatura(String nombreAsignatura)
    {
        this.nombreAsignatura = nombreAsignatura;
    }

    /**
     * @return the idiomaPrincipal
     */
    public boolean isIdiomaPrincipal()
    {
        return idiomaPrincipal;
    }

    /**
     * @param idiomaPrincipal the idiomaPrincipal to set
     */
    public void setIdiomaPrincipal(boolean idiomaPrincipal)
    {
        this.idiomaPrincipal = idiomaPrincipal;
    }

    /**
     * @return the traductorApertium
     */
    public String getTraductorApertium()
    {
        return traductorApertium;
    }

    /**
     * @param traductorApertium the traductorApertium to set
     */
    public void setTraductorApertium(String traductorApertium)
    {
        this.traductorApertium = traductorApertium;
    }
}
