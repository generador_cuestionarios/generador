/* 
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : UsuarioEN
 * Created on : 15-abr-2014, 00:46:00
 * Author     : Raúl Sempere Trujillo
 */

package EN;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Raúl Sempere Trujillo
 * 
 * Clase EN de la entidad Usuario
 */
public class UsuarioEN implements Serializable
{
    /**
     * Variable privada: codigo del usuario
     */
    private int codigo;
    /**
     * Variable privada: user (nombre de usuario)
     */
    private String user;
    /**
     * Variable privada: contraseña del usuario
     */
    private String password;
    /**
     * Variable privada: estado de administrador del usuario
     */
    private boolean administrador;
    /**
     * Variable privada: ArrayList con la lista de accesos del usuario
     */
    private ArrayList<AccesoUsuarioAsignaturaEN> accesosUsuarioAsignatura;
    
    /**
     * Constructor por defecto.
     */
    public UsuarioEN()
    {
        codigo = 0;
        user = "";
        password = "";
        administrador = false;
        accesosUsuarioAsignatura = null;
    }
    
    /**
     * Constructor parametrizado. inicializa la clase con los valores de un 
     * usuario pasados por parametros.
     * 
     * @param codigo int con el codigo del usuario
     * @param user String. nombre del usuario
     * @param password String. Password del usuario
     * @param administrador Boolean. Indica si el usuario es administrador
     * @param accesosUsuarioAsignatura ArrayList con los permisos a cada asignatura 
     * que tiene el usuario
     */
    public UsuarioEN(int codigo, String user, String password, boolean administrador, ArrayList<AccesoUsuarioAsignaturaEN> accesosUsuarioAsignatura)
    {
        this.codigo = codigo;
        this.user = user;
        this.password = password;
        this.administrador = administrador;
        this.accesosUsuarioAsignatura = accesosUsuarioAsignatura;
    }

    /**
     * @return the codigo
     */
    public int getCodigo()
    {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(int codigo)
    {
        this.codigo = codigo;
    }

    /**
     * @return the user
     */
    public String getUser()
    {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(String user)
    {
        this.user = user;
    }

    /**
     * @return the password
     */
    public String getPassword()
    {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password)
    {
        this.password = password;
    }

    /**
     * @return the administrador
     */
    public boolean isAdministrador()
    {
        return administrador;
    }

    /**
     * @param administrador the administrador to set
     */
    public void setAdministrador(boolean administrador)
    {
        this.administrador = administrador;
    }

    /**
     * @return the accesosUsuarioAsignatura
     */
    public ArrayList<AccesoUsuarioAsignaturaEN> getAccesosUsuarioAsignatura()
    {
        return accesosUsuarioAsignatura;
    }

    /**
     * @param accesosUsuarioAsignatura the accesosUsuarioAsignatura to set
     */
    public void setAccesosUsuarioAsignatura(ArrayList<AccesoUsuarioAsignaturaEN> accesosUsuarioAsignatura)
    {
        this.accesosUsuarioAsignatura = accesosUsuarioAsignatura;
    }
}