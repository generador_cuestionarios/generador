/* 
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : ExamenEN
 * Created on : 15-abr-2014, 00:46:00
 * Author     : Raúl Sempere Trujillo
 */

package EN;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;

/**
 *
 * @author Raúl Sempere Trujillo
 * 
 * Clase EN de la entidad Examen
 */
public class ExamenEN implements Serializable
{
    /**
     * Variable privada: asignatura a la que pertenece el examen
     */
    private AsignaturaEN asignatura;
    /**
     * Variable privada: preparaciónes del examen en idiomas
     */
    private ArrayList<PreparacionExamenIdiomaEN> preparacionIdiomas;
    /**
     * Variable privada: ArrayList con la lista de preguntas del examen
     */
    private ArrayList<ExamenPreguntaEN> preguntas;
    /**
     * Variable privada: ArrayList con las instrucciones del examen
     */
    private ArrayList<InstruccionEN> instrucciones;
    /**
     * Variable privada: codigo del examen
     */
    private int codigo;
    /**
     * Variable privada: fecha del examen
     */
    private Calendar fecha;
    /**
     * Variable privada: cantidad de modalidades que tendrá el examen
     */
    private int modalidades;
    /**
     * Variable privada: semilla de ordenación de las preguntas
     */
    private long semilla;
    /**
     * Variable privada: indica si el examen ha sido o no analizado
     */
    private boolean analizado;
    /**
     * Variable privada: índice de dificultad del examen
     */
    private double dificultad;
    /**
     * Variable privada: índice de discriminación del examen
     */
    private double discriminacion;
    /**
     * Variable privada: duración del examen
     */
    private int duracion;
    /**
     * Variable privada: preguntas que han sido contestadas en el examen
     */
    private int contestadas;
        /**
     * Variable privada: preguntas que no han sido contestadas
     */
    private int noContestadas;
        /**
     * Variable privada: de las preguntas contestadas cuantos aciertos
     */
    private int aciertos;

    
    /**
     * Constructor por defecto.
     */
    public ExamenEN()
    {
        asignatura = null;
        preparacionIdiomas = null;
        preguntas = null;
        instrucciones = null;
        codigo = 0;
        fecha = null;
        modalidades = 0;
        semilla = 0;
        dificultad = 0.0;
        analizado = false;
        discriminacion = 0.0;
        duracion = 0;
        contestadas = 0;
        noContestadas = 0;
        aciertos = 0;
    }
    
    /**
     * Constructor parametrizado. inicializa la clase con los valores  
     * pasados por parametro
     * 
     * @param asignatura AsignaturaEN del examen
     * @param preparacionIdiomas ArrayList con las preparaciónes del examen en idiomas
     * @param preguntas ArrayList con las ExamenPregunta del examen
     * @param instrucciones ArrayList con la lista de instrucciones del examen
     * @param codigo int con el código del examen
     * @param fecha Calendar con la fecha del examen
     * @param modalidades int con la cantidad de modalidades dele xamen
     * @param semilla long con la semilla de generación de las modalidades
     * @param dificultad double con la dificultad del examen
     * @param analizado boolean que indica si el examen ha sido analizado
     * @param discriminacion double con la discriminación del examne
     * @param duracion int con la duración en minutos del examen
     * @param contestadas int con la cantidad de preguntas contestadas en el examen
     * @param noContestadas int con la cantidad de preguntas no contestadas
     * @param aciertos int con el número de aciertos
     */
    public ExamenEN(AsignaturaEN asignatura, ArrayList<PreparacionExamenIdiomaEN> preparacionIdiomas, 
            ArrayList<ExamenPreguntaEN> preguntas, ArrayList<InstruccionEN> instrucciones, int codigo, Calendar fecha, 
            int modalidades, long semilla, boolean analizado, double dificultad, double discriminacion, int duracion,
            int contestadas, int noContestadas, int aciertos)
    {
        this.asignatura = asignatura;
        this.preparacionIdiomas = preparacionIdiomas;
        this.preguntas = preguntas;
        this.instrucciones = instrucciones;
        this.codigo = codigo;
        this.fecha = fecha;
        this.modalidades = modalidades;
        this.semilla = semilla;
        this.analizado = analizado;
        this.dificultad = dificultad;
        this.discriminacion = discriminacion;
        this.duracion = duracion;
        this.contestadas = contestadas;
        this.noContestadas = noContestadas;
    }

    /**
     * @return the asignatura
     */
    public AsignaturaEN getAsignatura() {
        return asignatura;
    }

    /**
     * @param asignatura the asignatura to set
     */
    public void setAsignatura(AsignaturaEN asignatura) {
        this.asignatura = asignatura;
    }

    /**
     * @return the preparacionIdiomas
     */
    public ArrayList<PreparacionExamenIdiomaEN> getPreparacionIdiomas() {
        return preparacionIdiomas;
    }

    /**
     * @param preparacionIdiomas the preparacionIdiomas to set
     */
    public void setPreparacionIdiomas(ArrayList<PreparacionExamenIdiomaEN> preparacionIdiomas) {
        this.preparacionIdiomas = preparacionIdiomas;
    }

    /**
     * @return the instrucciones
     */
    public ArrayList<InstruccionEN> getInstrucciones() {
        return instrucciones;
    }

    /**
     * @param instrucciones the instrucciones to set
     */
    public void setInstrucciones(ArrayList<InstruccionEN> instrucciones) {
        this.instrucciones = instrucciones;
    }

    /**
     * @return the codigo
     */
    public int getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    /**
     * @return the fecha
     */
    public Calendar getFecha() {
        return fecha;
    }

    /**
     * @param fecha the fecha to set
     */
    public void setFecha(Calendar fecha) {
        this.fecha = fecha;
    }

    /**
     * @return the modalidades
     */
    public int getModalidades() {
        return modalidades;
    }

    /**
     * @param modalidades the modalidades to set
     */
    public void setModalidades(int modalidades) {
        this.modalidades = modalidades;
    }

    /**
     * @return the semilla
     */
    public long getSemilla() {
        return semilla;
    }

    /**
     * @param semilla the semilla to set
     */
    public void setSemilla(long semilla) {
        this.semilla = semilla;
    }

    /**
     * @return the analizado
     */
    public boolean isAnalizado() {
        return analizado;
    }

    /**
     * @param analizado the analizado to set
     */
    public void setAnalizado(boolean analizado) {
        this.analizado = analizado;
    }

    /**
     * @return the dificultad
     */
    public double getDificultad() {
        return dificultad;
    }

    /**
     * @param dificultad the dificultad to set
     */
    public void setDificultad(double dificultad) {
        this.dificultad = dificultad;
    }

    /**
     * @return the discriminacion
     */
    public double getDiscriminacion() {
        return discriminacion;
    }

    /**
     * @param discriminacion the discriminacion to set
     */
    public void setDiscriminacion(double discriminacion) {
        this.discriminacion = discriminacion;
    }

    /**
     * @return the duracion
     */
    public int getDuracion() {
        return duracion;
    }

    /**
     * @param duracion the duracion to set
     */
    public void setDuracion(int duracion) {
        this.duracion = duracion;
    }

    /**
     * @return the preguntas
     */
    public ArrayList<ExamenPreguntaEN> getPreguntas() {
        return preguntas;
    }

    /**
     * @param preguntas the preguntas to set
     */
    public void setPreguntas(ArrayList<ExamenPreguntaEN> preguntas) {
        this.preguntas = preguntas;
    }

    /**
     * @return the contestadas
     */
    public int getContestadas() {
        return contestadas;
    }

    /**
     * @param contestadas the contestadas to set
     */
    public void setContestadas(int contestadas) {
        this.contestadas = contestadas;
    }

    /**
     * @return the noContestadas
     */
    public int getNoContestadas() {
        return noContestadas;
    }

    /**
     * @param noContestadas the noContestadas to set
     */
    public void setNoContestadas(int noContestadas) {
        this.noContestadas = noContestadas;
    }

    /**
     * @return the aciertos
     */
    public int getAciertos() {
        return aciertos;
    }

    /**
     * @param aciertos the aciertos to set
     */
    public void setAciertos(int aciertos) {
        this.aciertos = aciertos;
    }
}

  