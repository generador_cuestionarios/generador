/* 
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : TraduccionInstruccionEN
 * Created on : 15-abr-2014, 00:46:00
 * Author     : Raúl Sempere Trujillo
 */

package EN;

import java.io.Serializable;

/**
 *
 * @author Raúl Sempere Trujillo
 * 
 * Clase EN de la entidad TraduccionInstruccion
 */
public class TraduccionInstruccionEN implements Serializable
{
    /**
     * Variable privada: intrucción que se está traduciendo
     */
    private InstruccionEN instruccion;
    /**
     * Variable privada: idioma al que se traduce la instrucción
     */
    private IdiomaEN idioma;
    /**
     * Variable privada: texto con la instrucción traducida
     */
    private String enunciado;
    
    /**
     * Constructor por defecto.
     */
    public TraduccionInstruccionEN()
    {
        instruccion = null;
        idioma = null;
        enunciado = "";
    }
    
    /**
     * Constructor parametrizado. inicializa la clase con los valores  
     * pasados por parametro
     * 
     * @param instruccion IntruccionEN que se está traduciendo
     * @param idioma IdiomaEN al que se traduce la instrucción
     * @param enunciado String instrucción traducida
     */
    public TraduccionInstruccionEN(InstruccionEN instruccion, IdiomaEN idioma, String enunciado)
    {
        this.instruccion = instruccion;
        this.idioma = idioma;
        this.enunciado = enunciado;
    }

    /**
     * @return the instruccion
     */
    public InstruccionEN getInstruccion() {
        return instruccion;
    }

    /**
     * @param instruccion the instruccion to set
     */
    public void setInstruccion(InstruccionEN instruccion) {
        this.instruccion = instruccion;
    }

    /**
     * @return the idioma
     */
    public IdiomaEN getIdioma() {
        return idioma;
    }

    /**
     * @param idioma the idioma to set
     */
    public void setIdioma(IdiomaEN idioma) {
        this.idioma = idioma;
    }

    /**
     * @return the enunciado
     */
    public String getEnunciado() {
        return enunciado;
    }

    /**
     * @param enunciado the enunciado to set
     */
    public void setEnunciado(String enunciado) {
        this.enunciado = enunciado;
    }
}