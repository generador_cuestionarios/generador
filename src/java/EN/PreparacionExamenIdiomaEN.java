/* 
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : PreparacionExamenIdiomaEN
 * Created on : 15-abr-2014, 00:46:00
 * Author     : Raúl Sempere Trujillo
 */

package EN;

import java.io.Serializable;

/**
 *
 * @author Raúl Sempere Trujillo
 * 
 * Clase EN de la entidad PreparacionExamenIdioma
 */
public class PreparacionExamenIdiomaEN implements Serializable
{
    /**
     * Variable privada: Examen al que pertenece la preparación
     */
    private ExamenEN examen;
    /**
     * Variable privada: idioma en el que se prepara el examen
     */
    private IdiomaEN idioma;
    /**
     * Variable privada: traduccion de la fecha del examen (texto)
     */
    private String traduccionFecha;
    /**
     * Variable privada: traducción del nombre del examen (texto)
     */
    private String traduccionNombre;
    /**
     * Variable privada: traducción de la duración del examen (texto)
     */
    private String traduccionDuracion;
    /**
     * Variable privada: marca si ha sido revisada despues de una modificación
     */
    private Boolean revisada;
    
    /**
     * Constructor por defecto.
     */
    public PreparacionExamenIdiomaEN()
    {
        examen = null;
        idioma = null;
        traduccionFecha = "";
        traduccionNombre = "";
        traduccionDuracion = "";

    }
    
    /**
     * Constructor parametrizado. inicializa la clase con los valores  
     * pasados por parametro
     * 
     * @param examen Examen que está preparado (traducido)
     * @param idioma IdiomaEN de la preparacion
     * @param traduccionFecha String con la fecha escrita en texto en el idioma
     * @param traduccionNombre String con el nombre del examen en el idioma
     * @param traduccionDuracion String con la duracion escrita en texto en el idioma
     */
    public PreparacionExamenIdiomaEN(ExamenEN examen, IdiomaEN idioma, String traduccionFecha, String traduccionNombre, String traduccionDuracion)
    {
        this.examen = examen;
        this.idioma = idioma;
        this.traduccionFecha = traduccionFecha;
        this.traduccionNombre = traduccionNombre;
        this.traduccionDuracion = traduccionDuracion;
    }

    /**
     * @return the examen
     */
    public ExamenEN getExamen() {
        return examen;
    }

    /**
     * @param examen the examen to set
     */
    public void setExamen(ExamenEN examen) {
        this.examen = examen;
    }

    /**
     * @return the idioma
     */
    public IdiomaEN getIdioma() {
        return idioma;
    }

    /**
     * @param idioma the idioma to set
     */
    public void setIdioma(IdiomaEN idioma) {
        this.idioma = idioma;
    }

    /**
     * @return the traduccionFecha
     */
    public String getTraduccionFecha() {
        return traduccionFecha;
    }

    /**
     * @param traduccionFecha the traduccionFecha to set
     */
    public void setTraduccionFecha(String traduccionFecha) {
        this.traduccionFecha = traduccionFecha;
    }

    /**
     * @return the traduccionNombre
     */
    public String getTraduccionNombre() {
        return traduccionNombre;
    }

    /**
     * @param traduccionNombre the traduccionNombre to set
     */
    public void setTraduccionNombre(String traduccionNombre) {
        this.traduccionNombre = traduccionNombre;
    }

    /**
     * @return the traduccionDuracion
     */
    public String getTraduccionDuracion() {
        return traduccionDuracion;
    }

    /**
     * @param traduccionDuracion the traduccionDuracion to set
     */
    public void setTraduccionDuracion(String traduccionDuracion) {
        this.traduccionDuracion = traduccionDuracion;
    }

    /**
     * @return the revisada
     */
    public Boolean isRevisada() {
        return revisada;
    }

    /**
     * @param revisada the revisada to set
     */
    public void setRevisada(Boolean revisada) {
        this.revisada = revisada;
    }
}