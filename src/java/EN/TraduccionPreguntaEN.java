/* 
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : TraduccionPreguntaEN
 * Created on : 15-abr-2014, 00:46:00
 * Author     : Raúl Sempere Trujillo
 */

package EN;

import java.io.Serializable;

/**
 *
 * @author Raúl Sempere Trujillo
 * 
 * Clase EN de la entidad AccesoUsuarioAsignatura
 */
public class TraduccionPreguntaEN implements Serializable
{
    /**
     * Variable privada: pregunta de la tracucción
     */
    private PreguntaEN pregunta;
    /**
     * Variable privada: idioma de la traduccion
     */
    private IdiomaEN idioma;
    /**
     * Variable privada: enunciado de la opción
     */
    private String enunciado;
    /**
     * Variable privada: indica si la traduccion ha sido revisada
     */
    private boolean revisada;
    
    /**
     * Constructor por defecto
     */
    public TraduccionPreguntaEN()
    {
        pregunta = null;
        idioma = null;
        enunciado = "";
        revisada = false;
    }
    
    /**
     * Constructor parametrizado. inicializa la clase con los valores  
     * pasados por parametro
     * 
     * @param pregunta PreguntaEN que se esta traduciendo
     * @param idioma IdiomaEN de la traduccioón
     * @param enunciado String con el enunciado de la pregunta en el idioma de
     * la traducción
     * @param revisada boolean que indica si la traducción ha sido revisada 
     */
    public TraduccionPreguntaEN(PreguntaEN pregunta, IdiomaEN idioma, String enunciado, boolean revisada)
    {
        this.pregunta = pregunta;
        this.idioma = idioma;
        this.enunciado = enunciado;
        this.revisada = revisada;
    }

    /**
     * @return the pregunta
     */
    public PreguntaEN getPregunta()
    {
        return pregunta;
    }

    /**
     * @param pregunta the pregunta to set
     */
    public void setPregunta(PreguntaEN pregunta)
    {
        this.pregunta = pregunta;
    }

    /**
     * @return the idioma
     */
    public IdiomaEN getIdioma()
    {
        return idioma;
    }

    /**
     * @param idioma the idioma to set
     */
    public void setIdioma(IdiomaEN idioma)
    {
        this.idioma = idioma;
    }

    /**
     * @return the enunciado
     */
    public String getEnunciado()
    {
        return enunciado;
    }

    /**
     * @param enunciado the enunciado to set
     */
    public void setEnunciado(String enunciado)
    {
        this.enunciado = enunciado;
    }

    /**
     * @return the revisada
     */
    public boolean isRevisada()
    {
        return revisada;
    }

    /**
     * @param revisada the revisada to set
     */
    public void setRevisada(boolean revisada)
    {
        this.revisada = revisada;
    }
    
}
