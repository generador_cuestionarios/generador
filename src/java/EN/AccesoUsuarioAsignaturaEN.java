/* 
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : AccesoUsuarioAsignaturaCEN
 * Created on : 15-abr-2014, 00:46:00
 * Author     : Raúl Sempere Trujillo
 */

package EN;

import java.io.Serializable;

/**
 *
 * @author Raúl Sempere Trujillo
 * 
 * Clase EN de la entidad AccesoUsuarioAsignatura
 */
public class AccesoUsuarioAsignaturaEN implements Serializable
{
    /**
     * Variable privada: usuario del acceso
     */
    private UsuarioEN usuario;
    /**
     * Variable privada: asignatura a la que se tiene acceso
     */
    private AsignaturaEN asignatura;
    /**
     * Variable privada: indica si administra la asignatura
     */
    private boolean administrador;
    
   /**
    * Constructor por defecto
    */
    public AccesoUsuarioAsignaturaEN()
    {
        usuario = null;
        asignatura = null;
        administrador = false;
    }
    
    /**
     * Constructor parametrizado. inicializa la clase con los valores  
     * pasados por parametro
     * 
     * @param usuario usuario del acceso
     * @param asignatura asignatura del acceso
     * @param administrador indica si el usuario administra la asignatura
     */
    public AccesoUsuarioAsignaturaEN(UsuarioEN usuario, AsignaturaEN asignatura, boolean administrador)
    {
        this.usuario = usuario;
        this.asignatura = asignatura;
        this.administrador = administrador;
    }

    /**
     * @return the usuario
     */
    public UsuarioEN getUsuario()
    {
        return usuario;
    }

    /**
     * @param usuario the usuario to set
     */
    public void setUsuario(UsuarioEN usuario)
    {
        this.usuario = usuario;
    }

    /**
     * @return the asignatura
     */
    public AsignaturaEN getAsignatura()
    {
        return asignatura;
    }

    /**
     * @param asignatura the asignatura to set
     */
    public void setAsignatura(AsignaturaEN asignatura)
    {
        this.asignatura = asignatura;
    }

    /**
     * @return the administrador
     */
    public boolean isAdministrador() {
        return administrador;
    }

    /**
     * @param administrador the administrador to set
     */
    public void setAdministrador(boolean administrador) {
        this.administrador = administrador;
    }

}
