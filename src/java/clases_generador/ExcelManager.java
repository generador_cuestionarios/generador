/* 
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : ExcellManager
 * Created on : 15-abr-2014, 00:46:00
 * Author     : Raúl Sempere Trujillo
 */

package clases_generador;

import CEN.ExamenCEN;
import CEN.PreguntaCEN;
import CEN.ExamenPreguntaCEN;
import EN.ExamenEN;
import EN.ExamenPreguntaEN;
import EN.PreguntaEN;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

/**
 * Clase que se gestiona el analisis de los documentos en Excel con
 * información estadísticos de los exámenes 
 * 
 * @author Raúl Sempere Trujillo
 */
public class ExcelManager
{
    /**
     * Variable privada: indica si ha habido algun problema durante la lectura
     * del fichero Excel
     */
    private boolean correcto;
    /**
     * Variable privada: almacena el mensaje de error que será mostrado al cliente
     * al finalizar el análisis
     */
    private String error;
    /**
     * Variable final privada: Almacena el examen que se analizará
     */
    private final ExamenEN examenEN;
    /**
     * Variable final privada: Almacena la ruta del fichero Excel a analizar.
     */
    private final String rutaExcel;
    
    /**
     * Constructor de la clase. Establece el examen y la ruta del fichero excel
     * 
     * @param examenEN ExamenEN que se analizará
     * @param ruta String con la ruta del excel a examinar
     */
    public ExcelManager(ExamenEN examenEN, String ruta)
    {
        correcto = true;
        error = "";
        this.examenEN = examenEN;
        rutaExcel = ruta;
    }
    
    /**
     * Examina el fichero Excel y si no ocurre ningún error, almacena toda la
     * información en el objeto ExamenEN de la clase lista para ser guardada en 
     * la base de datos
     * 
     * @throws CADException Error al acceder a la base de datos
     * @throws FileNotFoundException No se encontró el fichero Excel
     * @throws IOException Error al acceder o al leer el fichero Excel
     */
    public void examinarExcel() throws CADException, FileNotFoundException, IOException
    {
        ExamenCEN examenCEN = new ExamenCEN();
        ExamenPreguntaCEN examenPreguntaCEN = new ExamenPreguntaCEN();
        examenCEN.rellenarPreguntas(examenEN);
        PreguntaEN preguntaEN;
        FileInputStream archivo = null;
        int randPregunta, indice;

        try
        {
            archivo = new FileInputStream(rutaExcel);
            POIFSFileSystem poiFile = new POIFSFileSystem(archivo); 
            
            //Las estadísticas aparecen a partir de la fila 5
            int numFila = 5;

            HSSFWorkbook libro = new HSSFWorkbook(poiFile);
            HSSFSheet hoja = libro.getSheet("Estadísticas");
            
            //Las estadísticas aparecen a partir de la fila 5
            HSSFRow fila;
            
            //Celdas
            //1   -> Modalidad
            //2   -> Pregunta
            //4   -> A)
            //6   -> B)
            //8   -> C)
            //10  -> D)
            //12  -> E)
            //14  -> F)
            //16  -> No contestada
            //18  -> Fallos
            //26 -> Dificultad
            //27 -> Discriminación
            
            for (int j = 1; j <= examenEN.getModalidades(); j++)
            {
                //Hacemos una copia de las preguntas (para ir sustrayendo)
                ArrayList<ExamenPreguntaEN> preguntas = (ArrayList<ExamenPreguntaEN>)examenEN.getPreguntas().clone();
                
                //creamos un rando con la semilla de la modalidad
                Random random = new Random();
                random.setSeed(examenEN.getSemilla() + j);
                try
                { 
                    for (int i = 1; i <= examenEN.getPreguntas().size(); i++)
                    {
                        fila = hoja.getRow(numFila);
                        
                        //Recogemos la información
                        double dificultad = 0;
                        double discriminacion = 0;
                        int contestadas = 0;
                        int no_contestadas = 0;
                        int aciertos;
                        
                        //Modalidad
                        int modalidad = Integer.parseInt(fila.getCell(1).toString());
                        
                        //Pregunta
                        int pregunta = (int)Double.parseDouble(fila.getCell(2).toString());
                        
                        //Contestaciones
                        //A)
                        if (fila.getCell(4) != null && !fila.getCell(4).toString().equals(""))
                            contestadas += (int)Double.parseDouble(fila.getCell(4).toString());
                        //B)
                        if (fila.getCell(6) != null && !fila.getCell(6).toString().equals(""))
                            contestadas += (int)Double.parseDouble(fila.getCell(6).toString());
                        //C)
                        if (fila.getCell(8) != null && !fila.getCell(8).toString().equals(""))
                            contestadas += (int)Double.parseDouble(fila.getCell(8).toString());
                        //D)
                        if (fila.getCell(10) != null && !fila.getCell(10).toString().equals(""))
                            contestadas += (int)Double.parseDouble(fila.getCell(10).toString());
                        //E)
                        if (fila.getCell(12) != null && !fila.getCell(12).toString().equals(""))
                            contestadas += (int)Double.parseDouble(fila.getCell(12).toString());
                        //F)
                        if (fila.getCell(14) != null && !fila.getCell(14).toString().equals(""))
                            contestadas += (int)Double.parseDouble(fila.getCell(14).toString());

                        //No contestada
                        if (fila.getCell(16) != null && !fila.getCell(16).toString().equals(""))
                            no_contestadas = (int)Double.parseDouble(fila.getCell(16).toString());
                        
                        //Aciertos
                        aciertos = contestadas;
                        if (fila.getCell(18) != null && !fila.getCell(18).toString().equals(""))
                            aciertos -= (int)Double.parseDouble(fila.getCell(18).toString());
                        
                        //Dificultad
                        if (!fila.getCell(26).toString().equals("-"))
                            dificultad = Double.parseDouble(fila.getCell(26).toString());     
                        
                        //Discriminación
                        if (!fila.getCell(27).toString().equals("-"))
                            discriminacion = Double.parseDouble(fila.getCell(27).toString());
                        
                        //Si el formato esta bien (no ha saltado una excepción), validamos la información
                        if (modalidad != j)
                        {
                            setCorrecto(false);
                            setError("Ocurrió un error al leer la fila " + numFila + ":<br /><br />No se encontró una pregunta de la modalidad esperada. "
                                    + "¿Está intentando analizar el examen con el fichero excel correcto?");
                            break;
                        }
                        if (pregunta != i)
                        {
                            setCorrecto(false);
                            setError("Ocurrió un error al leer la fila " + numFila + ":<br /><br />No se encontró el número de pregunta esperado. "
                                    + "¿Está intentando analizar el examen con el fichero excel correcto?");
                            break;
                        }
                        
                        //Llegados a este punto el formato esta bien y la información es correcta.
                        //Pasamos a identificar la pregunta y a modificar la información
                        
                        //Generamos el numero aleatorio con la semilla
                        randPregunta = random.nextInt(preguntas.size());
                        
                        //Generamos X randoms para simular los randoms realizados al generar las opciones de las preguntas
                        for (int k = 0; k < examenEN.getAsignatura().getNumeroOpciones(); k++)
                        {
                            random.nextInt(preguntas.size());
                        }
                        
                        //Buscamos la pregunta en el examen
                        indice = examenPreguntaCEN.indiceEnLista(examenEN.getPreguntas(), preguntas.get(randPregunta).getPregunta());
                        
                        //Eliminamos la pregunta de la lista auxiliar
                        preguntas.remove(randPregunta);
                        
                        //actualizamos la información de la pregunta
                        preguntaEN = examenEN.getPreguntas().get(indice).getPregunta();
                        
                        int examinados_acu = preguntaEN.getContestada()+preguntaEN.getNoContestada();
                        int examinados_act = contestadas + no_contestadas;
                        
                        //Actualizamos el indice de dificultad de forma proporcional con el numero de examinados
                        double nuevaDificultad = ((preguntaEN.getIndiceDificultad()*examinados_acu) + (dificultad*examinados_act))/(examinados_acu+examinados_act);
                        preguntaEN.setIndiceDificultad(nuevaDificultad);
                        
                        //Actualizamos el indice de discriminacion de forma proporcional con el numero de examinados
                        double nuevaDiscriminacion = ((preguntaEN.getIndiceDiscriminacion()*examinados_acu) + (discriminacion*examinados_act))/(examinados_acu+examinados_act);
                        preguntaEN.setIndiceDiscriminacion(nuevaDiscriminacion);
                        
                        //Guardamos los nuevos valores de contestadas aciertos y no contestadas en la pregunta
                        preguntaEN.setContestada(preguntaEN.getContestada() + contestadas);
                        preguntaEN.setAciertos(preguntaEN.getAciertos() + aciertos);
                        preguntaEN.setNoContestada(preguntaEN.getNoContestada() + no_contestadas);
                        
                        //Guardamos tambien los datos en el examen
                        examenEN.setContestadas(examenEN.getContestadas() + contestadas);
                        examenEN.setAciertos(examenEN.getAciertos() + aciertos);
                        examenEN.setNoContestadas(examenEN.getNoContestadas() + no_contestadas);
                        
                        numFila++;
                    }
                }
                catch(Exception ex)
                {
                    setCorrecto(false);
                    setError("Ocurrió el siguiente error al leer la fila " + numFila + ":<br /><br />Puede que exista un problema con el formato o que este excel no "
                            + "corresponda al examen que se intenta analizar.");
                    
                    archivo.close();
                    break;
                }
            }
            archivo.close();
        }
        catch (Exception ex)
        {
            if (archivo != null)
                archivo.close();
            
            setCorrecto(false);
            setError("Ocurrió el siguiente error al exameniar el Excel:<br /><br />" + ex.getMessage());
        }
    }
    
    /**
     * Aplica a las preguntas del examen toda la información rocogida durante
     * el metodo ExaminarExcel()
     * 
     * @throws CADException Error de acceso a la base de datos
     */
    public void aplicarExcel() throws CADException
    {
        if (isCorrecto())
        {
            PreguntaCEN preguntaCEN = new PreguntaCEN();
            ExamenCEN examenCEN = new ExamenCEN();
            
            examenEN.setDificultad(0);
            examenEN.setDiscriminacion(0);
            
            for (ExamenPreguntaEN pregunta : examenEN.getPreguntas())
            {
                preguntaCEN.Modify(pregunta.getPregunta());
                
                examenEN.setDificultad(examenEN.getDificultad() + pregunta.getPregunta().getIndiceDificultad());
                examenEN.setDiscriminacion(examenEN.getDiscriminacion() + pregunta.getPregunta().getIndiceDiscriminacion());
            }
            
            //Recalculamos los nuevos indices tras el análisis
            examenEN.setDificultad(examenEN.getDificultad()/examenEN.getPreguntas().size());
            examenEN.setDiscriminacion(examenEN.getDiscriminacion()/examenEN.getPreguntas().size());
            
            examenEN.setAnalizado(true);
            examenCEN.Modify(examenEN);
        }

    }

    /**
     * @return the correcto
     */
    public boolean isCorrecto() {
        return correcto;
    }

    /**
     * @param correcto the correcto to set
     */
    public void setCorrecto(boolean correcto) {
        this.correcto = correcto;
    }

    /**
     * @return the error
     */
    public String getError() {
        return error;
    }

    /**
     * @param error the error to set
     */
    public void setError(String error) {
        this.error = error;
    }

    /**
     * @return the examenEN
     */
    public ExamenEN getExamenEN() {
        return examenEN;
    }

    /**
     * @return the rutaExcel
     */
    public String getRutaExcel() {
        return rutaExcel;
    }

}
