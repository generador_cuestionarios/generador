/* 
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : AccesoBD
 * Created on : 15-abr-2014, 00:46:00
 * Author     : Raúl Sempere Trujillo
 */

package clases_generador;

import java.sql.*;

/**
 * Clase que gestiona el acceso a la base de datos de la aplicación
 * 
 * @author Raúl Sempere Trujillo
 */
public class AccesoBD
{
    /**
     * Variable privada: conexión a la base de datos.
     */
    private Connection conexion ;
    /**
     * Variable estática privada: almacena la cadena de conexión para todas
     * las instancias.
     */
    private static String cadenaConexion;
    /**
     * Variable estática privada: indica si la cadena de conexión ha sido
     * establecida para todas las instancias.
     */
    private static boolean establecida = false;
    
    /**
     * Establece la cadena de conexión para todas las instancias de la clase
     * 
     * @param _cadenaConexion String con la cadena de conexión
     */
    public static void setCadenaConexion(String _cadenaConexion)
    {
        cadenaConexion = _cadenaConexion;
        establecida = true;
    }
    
    /**
     * Realiza la conexión con la base de datos
     * 
     * @return boolean true si se pudo conectar, false en caso contrario
     * @throws CADException Si ocurre un error en la conexión se envia un
     * CADException a la capa CAD.
     */
    public boolean Conectar() throws CADException
    {
        boolean conectado;
        
        try
        {
            String password = "";
            String datosConexion[] = cadenaConexion.split("@");
            
            //Esto se hace porque puede estar sin contraseña y daría error al acceder a datosConexion[2]
            if (datosConexion.length == 3)
                password = datosConexion[2];
                    
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            conexion = DriverManager.getConnection(datosConexion[0], datosConexion[1], password);
            conectado = true;
        }
        catch (SQLException ex)
        {
            conectado = false;
            throw new CADException("No se pudo conectar con la base de datos.");
        }
        
        return conectado;
    }
    
    /**
     * Fucnion que devuelve la conexión para que pueda utilizarse
     * 
     * @return Conection con la conexión establecida
     */
    public Connection getConexion()
    {
        return conexion;
    }
    
    /**
     * Metodo que cierra la conexión con la base de datos
     * 
     * @return boolean que indica si se ha desconectado de la BD correctamente
     */
    public boolean Desconectar()
    {
        boolean desconectado;
        
        try
        {
            conexion.close();
            desconectado = true;
        }
        catch (SQLException ex)
        {
            desconectado = false;
        }
        
        return desconectado;
    }

    /**
     * @return the establecida
     */
    public static boolean isEstablecida() {
        return establecida;
    }
}
