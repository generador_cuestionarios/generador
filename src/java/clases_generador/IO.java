/* 
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : IO
 * Created on : 15-abr-2014, 00:46:00
 * Author     : Raúl Sempere Trujillo
 */

package clases_generador;

import java.io.*;

/**
 * Esta clase se utiliza para leer y guardar ficheros de texto de una forma fácil
 * y rápida.
 * 
 * @author Raul Sempere Trujillo
 */
public class IO 
{
    /**
     * Lee un fichero y devuelve un string con la lectura
     * 
     * @param nombre Sring. archivo a leer
     * @return String con la lectura
     */
    static public String leerFichero(String nombre)
    {
        String linea;
        String texto = "";
        
        File archivo = new File(nombre);
        try
        {
            FileReader fr = new FileReader (archivo);
            BufferedReader br = new BufferedReader(fr);
            
            if ((linea = br.readLine()) != null)
            {
                texto += linea;
            
                while((linea = br.readLine()) != null)
                {
                    texto += ('\n' + linea);
                }
            }
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }
        
        return texto;
    }
    
    /**
     * Indica si un fichero existe
     * 
     * @param file Archivo a comprobar
     * @return true si existe, false si no existe
     */
    static public boolean existe(File file)
    {
        return file.exists();        
    }
    
    /**
     * Indica si un fichero existe
     * 
     * @param nombre ruta del fichero a comprobar
     * @return true si existe, false si no existe
     */
    static public boolean existe(String nombre)
    {
        File file = new File(nombre);
        return file.exists();        
    }
    
    /**
     * 
     * Crea o sustituye un fichero con el contenido del string pasado
     * por parametro
     * 
     * @param nombre ruta del fichero a guardar
     * @param contenido contenido del fichero
     */
    static public void escribirFichero(String nombre, String contenido)
    {
        try
        {
            File file = new File(nombre);

            //Si no existe se crea
            if (!file.exists()) {
                file.createNewFile();
            }

            FileWriter fw = new FileWriter(file);
            BufferedWriter bw = new BufferedWriter(fw);

            bw.write(contenido);
            bw.close();


        } catch (IOException ex)
        {
            ex.printStackTrace();
        }
    }
}
