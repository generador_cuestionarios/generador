/* 
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : XMLManager
 * Created on : 15-abr-2014, 00:46:00
 * Author     : Raúl Sempere Trujillo
 */

package clases_generador;

import CEN.ConfiguracionAsignaturaIdiomaCEN;
import CEN.OpcionCEN;
import CEN.PreguntaCEN;
import CEN.TraduccionOpcionCEN;
import CEN.TraduccionPreguntaCEN;
import EN.AsignaturaEN;
import EN.ConfiguracionAsignaturaIdiomaEN;
import EN.IdiomaEN;
import EN.OpcionEN;
import EN.PreguntaEN;
import EN.TraduccionOpcionEN;
import EN.TraduccionPreguntaEN;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Clase que se gestiona el análisis y la carga de pregntas desde ficheos XML
 * 
 * @author Raúl Sempere Trujillo
 */
public class XMLManager
{
    /**
     * Variable privada: almacena el número de preguntas totales a cargar
     */
    private int numPreguntas;
    /**
     * Variable privada: Indica si ha ocurrido o no algun error durante el 
     * proceso
     */
    private boolean correcto;
    /**
     * Variable privada: almacena el mensaje de error que será mostrado al cliente
     * al finalizar el análisis
     */
    private String error;
    /**
     * Variable privada: almacena el número actual de preguntas cargadas para
     * poder mostrar al cliente el estado actual de la carga
     */
    private int preguntasCargadas;
    /**
     * Variable privada: AsignaturaEN donde se cargarán las preguntas
     */
    private final AsignaturaEN asignaturaEN;
    /**
     * Variable privada: Ruta del fichero XML donde se encuentran las preguntas
     */
    private final String rutaXML;
     /**
     * Variable privada: ArrayList donde se almacenaran las preguntas leidas
     * desde el fichero XML
     */
    private ArrayList<PreguntaEN> listaPreguntas; 
    
    /**
     * Constructor de la clase. Establece la asignatura y la ruta del fichero XML
     * 
     * @param asignatura AsignaturaEN que se analizará
     * @param ruta String con la ruta del xml a examinar
     */
    public XMLManager(AsignaturaEN asignatura, String ruta)
    {
        numPreguntas = 0;
        correcto = true;
        error = "";
        asignaturaEN = asignatura;
        rutaXML = ruta;
        listaPreguntas = null;
        preguntasCargadas = 0;
    }
    
    /**
     * Examina el fichero XML en busca de preguntas y las almacena en la clase.
     * 
     * @throws CADException Error al acceder a la base de datos
     * @throws FileNotFoundException No se encontró el fichero XML
     * @throws IOException Error al acceder o al leer el fichero XML
     */
    public void examinarXML() throws CADException, FileNotFoundException, IOException
    {
        listaPreguntas = new ArrayList<PreguntaEN>();
        DocumentBuilderFactory dbFactory ;
        DocumentBuilder dBuilder;
        Document doc;
        InputStream fXmlFile = null;
        
        PreguntaEN preguntaEN;
        TraduccionPreguntaEN traduccionPreguntaEN;
        ConfiguracionAsignaturaIdiomaCEN configuracionAsignaturaIdiomaCEN = new ConfiguracionAsignaturaIdiomaCEN();
        IdiomaEN idiomaEN = configuracionAsignaturaIdiomaCEN.getConfiguracionIdiomaPrincipal(asignaturaEN).getIdioma();
        OpcionEN opcionEN;
        TraduccionOpcionEN traduccionOpcionEN;
        ArrayList<TraduccionOpcionEN> traducciones;

        try
        {
            fXmlFile = new FileInputStream(rutaXML);
            
            dbFactory = DocumentBuilderFactory.newInstance();
            dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(fXmlFile);
            doc.normalizeDocument();
     
            NodeList preguntas = doc.getElementsByTagName("preg");

            //Por cada pregunta del XMLManager
            for (int i = 0; i < preguntas.getLength(); i++)
            {  
                Node preguntaNodo = preguntas.item(i);
                
                if (preguntaNodo.getNodeType() == Node.ELEMENT_NODE)
                {
                    setNumPreguntas(getNumPreguntas() + 1);
                    
                    Element pregunta = (Element)preguntaNodo;

                    NodeList enunciados = pregunta.getElementsByTagName("enun");
                    NodeList opciones = pregunta.getElementsByTagName("op"); 
                    int correctas = 0;
                    
                    for (int j = 0; j < opciones.getLength(); j++)
                    {
                        if (opciones.item(j).getAttributes().getLength() > 0 && opciones.item(j).getAttributes().item(0).getTextContent().equals("1"))
                        correctas++;
                    }
                    
                    //Comprobamos la sintaxis de cada pregunta
                    //Si la sintaxis en una pregunta falla, salimos del bucle
                    if (enunciados.getLength() != 1)
                    {
                        setCorrecto(false);
                        setError("Error en el enunciado de la pregunta " + getNumPreguntas());
                        break;
                    }
                    else if (correctas != 1)
                    {
                        setCorrecto(false);
                        setError("Error en el número de opciones correctas de la pregunta " + getNumPreguntas());
                        break;
                    }
                    else if (opciones.getLength() != asignaturaEN.getNumeroOpciones())
                    {
                        setCorrecto(false);
                        setError("Error en el número de opciones de la pregunta " + getNumPreguntas() + ", deben ser: " + asignaturaEN.getNumeroOpciones());
                        break;
                    }
                    //Si la sintaxis es correcta las guardamos en una estructura
                    else
                    {
                        ArrayList<OpcionEN> opcionesEN = new ArrayList<OpcionEN>();

                        //Creamos la pregunta
                        preguntaEN = new PreguntaEN();
                        preguntaEN.setAsignatura(asignaturaEN);
                        preguntaEN.setAciertos(0);
                        preguntaEN.setContestada(0);
                        preguntaEN.setNoContestada(0);
                        preguntaEN.setUtilizada(0);
                        preguntaEN.setIndiceDificultad(0.5);
                        preguntaEN.setIndiceDiscriminacion(0);     
                        preguntaEN.setHabilitada(true);
                        preguntaEN.setProximoUso(false);
                        
                        //Le añadimos el enunciado en el idioma de la asignatura
                        traduccionPreguntaEN = new TraduccionPreguntaEN();
                        traduccionPreguntaEN.setEnunciado(pregunta.getElementsByTagName("enun").item(0).getTextContent());
                        traduccionPreguntaEN.setIdioma(idiomaEN);
                        traduccionPreguntaEN.setPregunta(preguntaEN);
                        traduccionPreguntaEN.setRevisada(true);
                        ArrayList<TraduccionPreguntaEN> traduccionesPregunta = new ArrayList<TraduccionPreguntaEN>();
                        traduccionesPregunta.add(traduccionPreguntaEN);
                        preguntaEN.setTraducciones(traduccionesPregunta);
                        
                        for (int j = 0; j < opciones.getLength(); j++)
                        {
                            //Ahora para cada una de las opciones la creamos
                            opcionEN = new OpcionEN();
                            if (opciones.item(j).getAttributes().getLength() > 0 && opciones.item(j).getAttributes().item(0).getTextContent().equals("1"))
                                opcionEN.setCorrecta(true);
                            else
                                opcionEN.setCorrecta(false);
                            opcionEN.setPregunta(preguntaEN);

                            //y el enunciado de la opcion el idioma de la asignatura
                            traduccionOpcionEN = new TraduccionOpcionEN();
                            traduccionOpcionEN.setEnunciado(opciones.item(j).getTextContent());
                            traduccionOpcionEN.setIdioma(idiomaEN);
                            traduccionOpcionEN.setOpcion(opcionEN);
                            traducciones = new ArrayList<TraduccionOpcionEN>();
                            traducciones.add(traduccionOpcionEN);
                            opcionEN.setTraducciones(traducciones);
                            
                            opcionesEN.add(opcionEN);
                        }
                        
                        preguntaEN.setOpciones(opcionesEN);
 
                        //Guardamos la pregunta en la lista
                        listaPreguntas.add(preguntaEN);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            if (fXmlFile != null)
                fXmlFile.close();
            
            setCorrecto(false);
            setError("Ocurrió el siguiente error al exameniar el XML: " + ex.getMessage());
        }
    }
    
    /**
     * Carga las preguntas almacenadas en la clase previamente con la funcion
     * ExaminarPreguntas() en la base de datos.
     * 
     * @param clasificar boolean que indica si las preguntas se clasificarán
     * @param proximoUso boolean que indica si las preguntas se marcaran
     * para usarse en el próximo examen
     * @throws CADException Error en el acceso a la base de datos
     * @throws Exception Error con el traductor Apertium
     */
    public void cargarPreguntas(boolean clasificar, boolean proximoUso) throws CADException, Exception
    {
        //Si previamente se ha leido un xml correcto
        boolean traducir = true;
        
        if (correcto)
        {
            TraduccionPreguntaCEN traduccionPreguntaCEN = new TraduccionPreguntaCEN();
            OpcionCEN opcionCEN = new OpcionCEN();
            TraduccionOpcionCEN traduccionOpcionCEN = new TraduccionOpcionCEN();
            PreguntaCEN preguntaCEN = new PreguntaCEN();
            Traduccion traductor = new Traduccion(ConfigManager.getApertiumPackages());
            ConfiguracionAsignaturaIdiomaCEN configuracionAsignaturaIdiomaCEN = new ConfiguracionAsignaturaIdiomaCEN();
            ArrayList<ConfiguracionAsignaturaIdiomaEN> configuraciones;
            configuraciones = configuracionAsignaturaIdiomaCEN.getConfiguracionesAsignaturaIdiomaByAsignatura(asignaturaEN);
            
            for (PreguntaEN pregunta: listaPreguntas)
            {
                setPreguntasCargadas(getPreguntasCargadas() + 1);
                
                if (proximoUso)
                    pregunta.setProximoUso(true);
                
                preguntaCEN.New(pregunta); 
                traduccionPreguntaCEN.New(pregunta.getTraducciones().get(0));
                
                //Traducioms la pregunta en todos los demás idiomas de la asignatura
                if (traducir)
                {   
                    for (ConfiguracionAsignaturaIdiomaEN configuracion: configuraciones)
                    {
                        if (configuracion.getIdioma().getCodigo() != pregunta.getTraducciones().get(0).getIdioma().getCodigo())
                        {
                            traductor.seleccionarTraduccion(configuracion.getTraductorApertium());    
                            TraduccionPreguntaEN traduccionPregunta = new TraduccionPreguntaEN();
                            traduccionPregunta.setIdioma(configuracion.getIdioma());
                            traduccionPregunta.setPregunta(pregunta);
                            traduccionPregunta.setRevisada(false);
                            traduccionPregunta.setEnunciado(traductor.traducir(pregunta.getTraducciones().get(0).getEnunciado()));
                            
                            traduccionPreguntaCEN.New(traduccionPregunta);
                        }
                    }
                }
                    
                
                for (OpcionEN opcion: pregunta.getOpciones())
                {
                    opcionCEN.New(opcion);
                    traduccionOpcionCEN.New(opcion.getTraducciones().get(0));
                    
                    //Traducioms la opción en todos los idiomas de la asignatura
                    if (traducir)
                    {
                        for (ConfiguracionAsignaturaIdiomaEN configuracion: configuraciones)
                        {
                            if (configuracion.getIdioma().getCodigo() != opcion.getTraducciones().get(0).getIdioma().getCodigo())
                            {
                                traductor.seleccionarTraduccion(configuracion.getTraductorApertium());    
                                TraduccionOpcionEN traduccionOpcion = new TraduccionOpcionEN();
                                traduccionOpcion.setIdioma(configuracion.getIdioma());
                                traduccionOpcion.setOpcion(opcion);
                                traduccionOpcion.setEnunciado(traductor.traducir(opcion.getTraducciones().get(0).getEnunciado()));

                                traduccionOpcionCEN.New(traduccionOpcion);
                            }
                        }
                    }
                }
                
                if (clasificar)
                {
                    preguntaCEN.clasificar(pregunta);
                }
            }
        }
    }

    /**
     * @return the numPreguntas
     */
    public int getNumPreguntas()
    {
        return numPreguntas;
    }

    /**
     * @param numPreguntas the numPreguntas to set
     */
    public void setNumPreguntas(int numPreguntas)
    {
        this.numPreguntas = numPreguntas;
    }

    /**
     * @return the correcto
     */
    public boolean isCorrecto()
    {
        return correcto;
    }

    /**
     * @param correcto the correcto to set
     */
    public void setCorrecto(boolean correcto)
    {
        this.correcto = correcto;
    }

    /**
     * @return the error
     */
    public String getError()
    {
        return error;
    }

    /**
     * @param error the error to set
     */
    public void setError(String error)
    {
        this.error = error;
    }

    /**
     * @return the preguntasCargadas
     */
    public int getPreguntasCargadas()
    {
        return preguntasCargadas;
    }

    /**
     * @param preguntasCargadas the preguntasCargadas to set
     */
    public void setPreguntasCargadas(int preguntasCargadas)
    {
        this.preguntasCargadas = preguntasCargadas;
    }
    
    public int getPorcentajeCargadas()
    {
        return (preguntasCargadas*100)/numPreguntas;
    }
}
