/* 
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : ConfigManager
 * Created on : 15-abr-2014, 00:46:00
 * Author     : Raúl Sempere Trujillo
 */

package clases_generador;

/**
 * Clase estática que maneja el fichero de configuración obteniendo la 
 * información del mismo bajo petición.
 * 
 * @author Raúl Sempere Trujillo
 */
public class ConfigManager
{
    /**
     * Variable final estática privada: Almacena la ruta del fichero de configuración.
     */
    private static final String fichero = "./generador_cuestionarios.cfg";
    /**
     * Variable estática privada: Almacena la carpeta dentro de webapps donde
     * se encuentra instalada aplicación
     */
    private static String app = "";
    /**
     * Variable estática privada: Almacena la cadena de conexión
     */
    private static String cadenaConexion = "";
    /**
     * Variable estática privada: Almacena la ruta donde se encuentran los
     * paquetes de traducción de Apertium
     */
    private static String apertiumPackages = "";
    
    /**
     * Devuelve el nombre de la carpeta dentro de webapps donde está instalada
     * la aplicación almacenada en la clase, si no ha sido establecida, la 
     * recoge del fichero de configuración.
     * 
     * @return String con el nombre de la carpeta dentro de webapps
     */
    public static String getWebApp()
    { 
        if (app.equals(""))
        {
            String webapp = "";

            String cfg = IO.leerFichero(fichero);
            String lineas[] = cfg.split("\n");

            for (String linea : lineas)
            {
                if (linea.startsWith("WebAppFolder"))
                {
                    webapp = linea.substring("WebAppFolder".length()+1);
                }
            }
            
            app = webapp;
        }
        
        return app;
    }
    
    /**
     * Devuelve la cadena de conexión almacenada en la clase, si no ha sido
     * establecida, la recoge del fichero de configuración
     * 
     * @return String con la cadena de conexión
     */
    public static String getCadenaConexion()
    {      
        if (cadenaConexion.equals(""))
        {
            String url = "";
            String bd = "";
            String user = "";
            String password = "";

            String cfg = IO.leerFichero(fichero);
            String lineas[] = cfg.split("\n");

            for (String linea : lineas)
            {
                if (linea.startsWith("Url"))
                {
                    url = "jdbc:" + linea.substring("Url".length()+1);
                }
                else if (linea.startsWith("Database"))
                {
                    bd = linea.substring("Database".length()+1);
                }
                else if (linea.startsWith("User"))
                {
                    user = linea.substring("User".length()+1);
                }
                else if (linea.startsWith("Password"))
                {
                    try
                    {
                        password = linea.substring("Password".length()+1);
                    }
                    //Sin contraseña
                    catch (IndexOutOfBoundsException ex)
                    {
                        password = "";
                    }
                }
            }

            //Añadimos la barra al final de la ruta si el usuario no la ha incluido
            if (!url.endsWith("/"))
                url += "/";
            
            cadenaConexion = url + bd + "@" + user+ "@" + password;
        }
        
        return cadenaConexion;
    }
    
    /**
     * Devuelve la ruta de los paquetes de traduccion Apertium almacenada en
     * la clase, si no ha sido establecida, la recoge del fichero
     * de configuración
     * 
     * @return String con la ruta de los paquetes Apertium
     */
    public static String getApertiumPackages()
    {
        if (apertiumPackages.equals(""))
        {
            String ruta = "";

            String cfg = IO.leerFichero(fichero);
            String lineas[] = cfg.split("\n");

            for (String linea : lineas)
            {
                if (linea.startsWith("ApertiumPackages"))
                {
                    ruta = linea.substring("ApertiumPackages".length()+1);
                }
            }
            //Añadimos la barra al final de la ruta si el usuario no la ha incluido
            if (!ruta.endsWith("/"))
                ruta += "/";
            
            apertiumPackages = ruta;
        }
        
        return apertiumPackages;
    }
}
