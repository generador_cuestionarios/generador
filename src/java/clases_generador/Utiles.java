/* 
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : Utiles
 * Created on : 15-abr-2014, 00:46:00
 * Author     : Raúl Sempere Trujillo
 */

package clases_generador;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Esta clase se utiliza para implementar utilidades varias necesarias para la
 * aplicación.
 * 
 * @author Raul Sempere Trujillo
 */
public class Utiles
{
    /**
     * Transforma cualquier cadena que se quiera guardar en la base de datos, 
     * modificando cualquier carácter ilegal e introduciendo caracteres de escape.
     * 
     * @param texto String con el texto a modificar
     * @return String con el texto válido
     */
    public static String StringBD(String texto)
    {
        return texto.replace("\\", "\\\\").replace("\'", "\\\'").replace("\"", "\\\"");
    }
    
    /**
     * Modifica cualquier etiqueta HTML para que pueda aparecer en una página web
     * como si fuera texto (sin que sea interpretado como etiquetas)
     * 
     * @param texto Texto a modificar
     * @return Tecto válido para escribir en HTML
     */
    public static String textoHTML(String texto)
    {
        return texto.replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;");
    }
    
    /**
     * rellena cualquier número con ceros por la izquierda y lo 
     * devuelve en forma de cadena.
     * 
     * @param numero String con el número a añadir ceros
     * @param tam int con el tamaño que debe tener el número
     * @return String con el número con ceros por la izquierda
     */
    public static String rellenaConCeros(String numero, int tam)
    {
        while (numero.length() < tam)
        {
            numero = "0" + numero;
        }
        
        return numero;
    }
    
    /**
     * rellena cualquier número con ceros por la izquierda y lo 
     * devuelve en forma de cadena.
     * 
     * @param numero int con el número a añadir ceros
     * @param tam int con el tamaño que debe tener el número
     * @return String con el número con ceros por la izquierda
     */
    public static String rellenaConCeros(int numero, int tam)
    {
        String numeroStr = String.valueOf(numero);
        
        while (numeroStr.length() < tam)
        {
            numeroStr = "0" + numeroStr;
        }
        
        return numeroStr;
    }
    
    /**
     * Reondea el número pasado por parametro
     * 
     * @param numero double con el número a redondear
     * @param decimales int con el número de decimales
     * @return double con el número redondeado
     */
    public static double redondeo (double numero, int decimales)
    {
        return Math.round(numero*Math.pow(10,decimales))/Math.pow(10,decimales);
    }
    
    //Ruta del fichero NOmbre dentro dle zip
    /**
     * Comprime la lista de ficheros pasada por parametro y crea un ZIP
     * 
     * @param rutas ArrayList con la lista de las rutas de ficheros a comprimir
     * @param nombres ArrayList con la lista de los nombres que tendrán los ficheros
     * comprimidos
     * @param rutaZip String con la ruta de salida del ZIP 
     */
    public static void crearZip(ArrayList<String> rutas, ArrayList<String> nombres, String rutaZip)
    {
        //Sacado de www.manual-java.com/manualdejava/compresion-y-decompresion-de-archivos-zip-con-java/
        try
        {
            int buffer_size = 512;
            // Reference to the file we will be adding to the zipfile
            BufferedInputStream origin;
            // Reference to our zip file
            FileOutputStream dest = new FileOutputStream(rutaZip);
            // Wrap our destination zipfile with a ZipOutputStream
            ZipOutputStream out = new ZipOutputStream( 

            new BufferedOutputStream(dest));
            // Create a byte[] buffer that we will read data 

            // from the source
            // files into and then transfer it to the zip file
            byte[] data = new byte[ buffer_size ];
            // Iterate over all of the files in our list
            for (int i = 0; i < rutas.size(); i++)
            {
                System.out.println("Comprimiendo: " + nombres.get(i));
                FileInputStream fi = new FileInputStream(rutas.get(i));
                origin = new BufferedInputStream(fi, buffer_size);
                // Setup the entry in the zip file
                ZipEntry entry = new ZipEntry(nombres.get(i));
                out.putNextEntry( entry );
                // Read data from the source file and write it out to the zip file
                int count;
                while((count = origin.read(data, 0, buffer_size)) != -1 )
                {
                    out.write(data, 0, count);
                }
                // Close the source file
                origin.close();
             }
        // Close the zip file
        out.close();
        }
        catch( Exception e )
        {
            e.printStackTrace();
        }
    }
}
