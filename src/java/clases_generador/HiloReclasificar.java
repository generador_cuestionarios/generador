/* 
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : HiloReclasificar
 * Created on : 15-jun-2014, 00:46:00
 * Author     : Raúl Sempere Trujillo
 */

package clases_generador;

import CEN.ClasificacionTemaCEN;
import EN.AsignaturaEN;

/**
 * Clase que se encarga de lanzar la reclasificación en segundo plano.
 * 
 * @author Raúl Sempere Trujillo
 */
public class HiloReclasificar extends Thread
{
    /**
     * Variable final privada: Asignatura donde se reclasificarán las preguntas
     */
    private final AsignaturaEN asignaturaEN;
    /**
     * Variable final privada: instancia ClasificacionTemaCEN que se utilizará
     * para realizar la reclasificación
     */
    private final ClasificacionTemaCEN clasificacionTemaCEN;
    
    /**
     * Constructor por defecto. Establece los objetos necesarios para realizar
     * la reclasificación
     * 
     * @param asignaturaEN AsignaturaEN donde se reclasificarán las preguntas
     * @param clasificacionTemaCEN ClasificacionTemaCENque se utilizará
     * para realizar la reclasificación
     */
    public HiloReclasificar(AsignaturaEN asignaturaEN, ClasificacionTemaCEN clasificacionTemaCEN)
    {
        this.asignaturaEN = asignaturaEN;
        this.clasificacionTemaCEN = clasificacionTemaCEN;
    }
    
    /**
     * Inicia la reclasificación en segundo plano.
     */
    @Override
    public void run ()
    {
        try
        {
            clasificacionTemaCEN.reclasificarPreguntas(asignaturaEN);
        }
        catch (CADException ex)
        {
            ex.printStackTrace();
        }
    }
}