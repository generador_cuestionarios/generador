/* 
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : HiloXML
 * Created on : 15-abr-2014, 00:46:00
 * Author     : Raúl Sempere Trujillo
 */

package clases_generador;

/**
 * Clase que se encarga de lanzar la carga de preguntas desde el fichero
 * XML en segundo plano.
 * 
 * @author Raúl Sempere Trujillo
 */
public class HiloXML extends Thread
{
    /**
     * Variable final privada: Instancia de XMLManager que cargará las preguntas.
     */
    private final XMLManager xml;
    /**
     * Variable final privada: Indica si las preguntas se clasificarán durante
     * la carga.
     */
    private final boolean clasificar;
    /**
     * Variable final privada: Indica si las preguntas cargadas se marcaran
     * para utilizarse en el próximo examen.
     */
    private final boolean proximoUso;
    
    /**
     * Constructor por defecto. Establece los objetos necesarios para realizar
     * la carga de las preguntas
     * 
     * @param xml XMLManager  que cargará las preguntas.
     * @param clasificar boolean que indica si las preguntas se clasificarán 
     * durante la carga.
     * @param proximoUso boolean que indica si las preguntas cargadas se 
     * marcaran para utilizarse en el próximo examen.
     */
    public HiloXML(XMLManager xml, boolean clasificar, boolean proximoUso)
    {
        this.xml = xml;
        this.clasificar = clasificar;
        this.proximoUso = proximoUso;
    }
    
    /**
     * Inicia la carga de preguntas en segundo plano.
     */
    @Override
    public void run ()
    {
        try
        {
            xml.cargarPreguntas(clasificar, proximoUso);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }
}