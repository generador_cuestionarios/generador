/* 
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : ExamenLaTeX
 * Created on : 15-abr-2014, 00:46:00
 * Author     : Raúl Sempere Trujillo
 */

package clases_generador;

import CEN.ConfiguracionAsignaturaIdiomaCEN;
import CEN.ExamenCEN;
import CEN.PreguntaCEN;
import CEN.PreparacionExamenIdiomaCEN;
import CEN.TraduccionInstruccionCEN;
import CEN.TraduccionOpcionCEN;
import CEN.TraduccionPreguntaCEN;
import EN.ConfiguracionAsignaturaIdiomaEN;
import EN.ExamenEN;
import EN.ExamenPreguntaEN;
import EN.IdiomaEN;
import EN.InstruccionEN;
import EN.OpcionEN;
import EN.PreparacionExamenIdiomaEN;
import EN.TraduccionInstruccionEN;
import EN.TraduccionOpcionEN;
import EN.TraduccionPreguntaEN;
import java.util.ArrayList;
import java.util.Random;

/**
 * Clase estática que se encarga de la generación de los exámenes LaTeX
 * 
 * @author Raúl Sempere Trujillo
 */
public class ExamenLaTeX
{
    /**
     * Recibe un examen, un idioma y una modalidad y genera el LaTeX.
     * 
     * @param examenEN ExamenEN del que se generará el LaTeX
     * @param idiomaEN IdiomaEN idioma del examen a generar
     * @param modalidad int con el nñumero de modalidad a generar
     * @return String con el código LaTeX listo para compilar en PDF
     * @throws CADException Error al acceder a la base de datos.
     */
    public static String generar(ExamenEN examenEN, IdiomaEN idiomaEN, int modalidad) throws CADException
    {
        PreparacionExamenIdiomaCEN preparacionExamenIdiomaCEN = new PreparacionExamenIdiomaCEN();
        ConfiguracionAsignaturaIdiomaCEN configuracionAsignaturaIdiomaCEN = new ConfiguracionAsignaturaIdiomaCEN();
        ExamenCEN examenCEN = new ExamenCEN();
        TraduccionInstruccionCEN traduccionInstruccionCEN = new TraduccionInstruccionCEN();
        TraduccionPreguntaCEN traduccionPreguntaCEN = new TraduccionPreguntaCEN();
        PreguntaCEN preguntaCEN = new PreguntaCEN();
        TraduccionOpcionCEN traduccionOpcionCEN = new TraduccionOpcionCEN();
        
        PreparacionExamenIdiomaEN preparacionExamenIdiomaEN;
        ConfiguracionAsignaturaIdiomaEN configuracionAsignaturaIdiomaEN;
        TraduccionInstruccionEN traduccionInstruccionEN;
        TraduccionPreguntaEN traduccionPreguntaEN;
        TraduccionOpcionEN traduccionOpcionEN;
        

        preparacionExamenIdiomaEN = preparacionExamenIdiomaCEN.getPreparacionExamenByExamenEIdioma(examenEN, idiomaEN);
        configuracionAsignaturaIdiomaEN = configuracionAsignaturaIdiomaCEN.getConfiguracionAsignaturaIdiomaByAsignaturaEIdioma(examenEN.getAsignatura(), idiomaEN);
        examenCEN.rellenarInstrucciones(examenEN);
        examenCEN.rellenarPreguntas(examenEN);

        String LaTeX = "";
        
        LaTeX += "\\documentclass[a4paper,12pt]{examdesign}\n";
        if (!idiomaEN.getPaqueteBabel().equals(""))
            
            LaTeX += "\\usepackage[" + idiomaEN.getPaqueteBabel() + "]{babel}\n";
        
        LaTeX += "\\usepackage[utf8]{inputenc}\n";
        LaTeX += "\\ShortKey\n";
        LaTeX += "\\NumberOfVersions{1}\n";
        LaTeX += "\\NoRearrange\n";
        LaTeX += "\\ContinuousNumbering\n";
        LaTeX += "\\SectionPrefix{} \n";
        LaTeX += "\\DefineAnswerWrapper{}{}\n";
        LaTeX += "\\SectionFont{\\bf}\n";
        LaTeX += "\\usepackage[total={14cm,25.8cm}]{geometry}\n";
        LaTeX += "\\usepackage{palatino}\n";
        LaTeX += "\\usepackage{color}\n";
        LaTeX += "\\begin{document}\n";
        LaTeX += "\\begin{examtop}\n";
        LaTeX += "\\begin{center}\n";
        LaTeX += "\\large{" + configuracionAsignaturaIdiomaEN.getNombreAsignatura() + "} \\\\\n";
        LaTeX += preparacionExamenIdiomaEN.getTraduccionFecha() + "\\\\\n";
        LaTeX += preparacionExamenIdiomaEN.getTraduccionNombre() + " (" + idiomaEN.getTraduccionDuracion() + ": " + preparacionExamenIdiomaEN.getTraduccionDuracion() + ") \\\\[1.5ex]\n";
        LaTeX += "\\fbox{" + idiomaEN.getTraduccionModalidad() + " {\\bf " + modalidad + "}} \\\\\n";
        LaTeX += "\\end{center}\n";
        
        if (examenEN.getInstrucciones().size() > 0)
        {
            LaTeX += "\\paragraph{" + idiomaEN.getTraduccionInstrucciones() + ":}\n";

            //Instrucciones del examen
            LaTeX += "\\begin{itemize}\n";

            for (InstruccionEN instruccion: examenEN.getInstrucciones())
            {
                traduccionInstruccionEN = traduccionInstruccionCEN.getTraduccionInstruccionByInstruccionEIdioma(instruccion, idiomaEN);

                LaTeX += "\\item " + traduccionInstruccionEN.getEnunciado() + "\n";
            }

            LaTeX += "\\end{itemize}\n";
        }

        LaTeX += "\\end{examtop}\n";    
        LaTeX += "\\begin{keytop}\n";
        LaTeX += "\\begin{center}\n";
        LaTeX += "\\large{" + configuracionAsignaturaIdiomaEN.getNombreAsignatura() + "} \\\\\n";
        LaTeX += preparacionExamenIdiomaEN.getTraduccionFecha() + "\\\\[1.5ex]\n";
        LaTeX += "{\\Large " + idiomaEN.getTraduccionRespuestas() + ". " + idiomaEN.getTraduccionModalidad() + ": \\fbox{\\bf " + modalidad + "}}\n";
        LaTeX += "\\end{center}\n";
        LaTeX += "\\end{keytop}\n";
        
        //Preguntas del examen
        LaTeX += "\\begin{multiplechoice}[title={" + idiomaEN.getTraduccionPreguntas() + ":}]\n";
        
        Random random = new Random();
        random.setSeed(examenEN.getSemilla() + modalidad);
        
        //Clonamos la lista de preguntas para poder ir sacandolas sin afectar al objeto examen
        ArrayList<ExamenPreguntaEN> preguntas = (ArrayList<ExamenPreguntaEN>)examenEN.getPreguntas().clone();
        int randPregunta;
        int randOpcion;
        
        while (preguntas.size() > 0)
        {
            //Seleccionamos una pregunta aleatoria (con la semilla) de las que nos faltan
            randPregunta = random.nextInt(preguntas.size());
            
            preguntaCEN.rellenarOpciones(preguntas.get(randPregunta).getPregunta());
            preguntas.get(randPregunta).getPregunta().getOpciones();
            
            traduccionPreguntaEN = traduccionPreguntaCEN.getTraduccionPreguntaByPreguntaEIdioma(preguntas.get(randPregunta).getPregunta(), idiomaEN);
            
            LaTeX += "\\begin{question}\n";
            LaTeX += traduccionPreguntaEN.getEnunciado() + "\n";
            
            //Opciones de la pregunta
            ArrayList<OpcionEN> opciones = preguntas.get(randPregunta).getPregunta().getOpciones();
            
            while (opciones.size() > 0)
            {
                randOpcion = random.nextInt(opciones.size());
                
                traduccionOpcionEN = traduccionOpcionCEN.getTraduccionOpcionByOpcionEIdioma(opciones.get(randOpcion), idiomaEN);
                
                if (opciones.get(randOpcion).isCorrecta())
                    LaTeX += "\\choice[!]{" + traduccionOpcionEN.getEnunciado() + "}\n";
                else
                    LaTeX += "\\choice{" + traduccionOpcionEN.getEnunciado() + "}\n";
                
                opciones.remove(randOpcion);
            }
            
            LaTeX += "\\end{question}\n";
            
            //Eliminamos la pregunta de la lista
            preguntas.remove(randPregunta);      
        }        
        
        LaTeX += "\\end{multiplechoice}\n";
        LaTeX += "\\end{document}\n";
        
        return LaTeX;
    }
}
