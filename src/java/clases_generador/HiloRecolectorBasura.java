/* 
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : HiloRecolectorBasura
 * Created on : 12-ago-2014, 00:46:00
 * Author     : Raúl Sempere Trujillo
 */

package clases_generador;

import java.io.File;
import java.util.Calendar;


/**
 * Clase que se encarga de eliminar los ficheros temporales antiguos para no
 * que no se llene el disco duro
 * 
 * @author Raúl Sempere Trujillo
 */
public class HiloRecolectorBasura extends Thread
{   
    /**
     * Inicia el recolector de ficheros antiguos en segundo plano.
     */
    @Override
    public void run ()
    {
        String CATALINA_HOME = System.getenv("CATALINA_HOME");
        
        if (CATALINA_HOME.endsWith("/") || CATALINA_HOME.endsWith("\\"))
            CATALINA_HOME = CATALINA_HOME.substring(0, CATALINA_HOME.length()-1);
        
        String rutaTemp = CATALINA_HOME + "/webapps/" + ConfigManager.getWebApp() + "/temp";
        
        File temp = new File(rutaTemp);
        
        //Los ficheros se comprueban siempre cada 5 minutos hasta que la
        //aplicación se cierra
        while (true)
        {
            if (temp.exists())
            {
                File[] ficheros = temp.listFiles();
                for (File fichero : ficheros)
                {
                    long tiempoMillis = (Calendar.getInstance()).getTimeInMillis() - fichero.lastModified();

                    //Si fue creado o modificado hace más de 1/2 hora (1800000 ms)
                    if (tiempoMillis > 1800000)
                    {
                        System.out.println("Eliminando temporal: " + fichero.getName());
                        fichero.delete();
                    }
                }
            }

            try
            {
                //cada 5 minutos se eliminan los ficheros de más de una hora
                //de antigüedad
                Thread.sleep(300000);
            }
            catch (InterruptedException ex)
            {
                ex.printStackTrace();
            }
        }
    }
}