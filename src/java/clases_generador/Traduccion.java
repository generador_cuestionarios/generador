/* 
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : Traduccion
 * Created on : 15-abr-2014, 00:46:00
 * Author     : Raúl Sempere Trujillo
 */

package clases_generador;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Arrays;
import java.util.HashMap;
import org.apertium.Translator;

/**
 * Clase que gestiona la traducción con Apertium
 * 
 * @author Raul Sempere Trujillo
 */
public class Traduccion
{
    /**
     * Variable privada: relaciona titulos con modos (traducciones)
     */
    private final HashMap<String, String> titleToBase;
    /**
     * Variable privada: relaciona titutlos con bases (paquetes)
     */
    private final HashMap<String, String> titleToMode;
    /**
     * Variable privada: lista de todas las traducciones cargadas
     */
    private final Object[] listaTitulos;
    /**
     * Variable privada: sirve para indicar a cualquier hilo si estamos en 
     * si la clase se encuentra en el proceso de traduccion
     */
    private boolean traduciendo = false;
    
    /**
     * Variable privada: Se utiliza para filtrar la busqueda de paquetes de traducciones
     */
    private static final FilenameFilter filter = new FilenameFilter()
    {
        @Override public boolean accept(File dir, String name)
        {
            return name.matches("apertium-[a-z][a-z][a-z]?-[a-z][a-z][a-z]?.jar");
        }
    };

    /**
     * Constructor de la clase
     * 
     * @param rutaPaquetes String con la ruta de donde se cargaran los paquetes 
     * de idiomas
     * @throws Exception si se intenta cargar un paquete inválido
     */
    public Traduccion(String rutaPaquetes) throws Exception
    {
        titleToBase = new HashMap<String, String>();
        titleToMode = new HashMap<String, String>();
        Translator.setDisplayAmbiguity(false);
        Translator.setDisplayMarks(false);
        Translator.setParallelProcessingEnabled(false);
        Translator.setCacheEnabled(true);

        File carpeta = new File(rutaPaquetes);      
        File paquetes[] = carpeta.listFiles(filter);
        for (File p : paquetes)
        {
            String base = p.getPath();
            Translator.setBase(base);
            for (String mode : Translator.getAvailableModes())
            {
                String title = Translator.getTitle(mode);
                titleToBase.put(title, base);
                titleToMode.put(title, mode);
            }
        }

        listaTitulos = titleToBase.keySet().toArray();
        Arrays.sort(listaTitulos);
    }
    
    /**
     * Funcion que devuelve la lista de titulos de las traducciones disponibles
     * @return Object[] con la lista de titulos cargados
     */ 
    public Object[] getTraducciones()
    {
        return listaTitulos;
    }
    
    /**
     * Carga una traducción para que pueda ser utilizada con el metodo traducir
     *
     * @param traduccion String con el título de la traducción a realizar
     * @throws Exception si se intenta cargar un paquete o un modo inválido    
     */
    public void seleccionarTraduccion(String traduccion) throws Exception
    {
        try
        {
            Translator.setBase(titleToBase.get(traduccion));
            Translator.setMode(titleToMode.get(traduccion));
        }
        
        catch (Exception ex)
        {
            //Imprimir en el log
            throw ex;
        }
    }
    
    /**
     * Traduce un texto utilizando un traductor cargado previamente
     *
     * @param texto String con el título de la traducción a realizar
     * @return String con la traducción realizada
     * @throws Exception si se intenta traducir sin seleccionar un modo correcto previamente   
     */
    public String traducir(String texto) throws Exception
    {
        String traduccion = "";
        
        try
        {
            //Traducimos de forma secuencial (si no crashea)
            while (traduciendo)
                Thread.sleep(5);
            traduciendo = true;
            traduccion = Translator.translate(texto);
            traduciendo = false;
        }

        catch (Exception ex)
        {
            //Error en el modo seleccionado
            throw ex;
        }
        return traduccion;
    }
}
