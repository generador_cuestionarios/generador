/* 
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : Clasificador
 * Created on : 15-abr-2014, 00:46:00
 * Author     : Raúl Sempere Trujillo
 */

package clasificador;

import EN.TemaEN;
import java.util.ArrayList;

/**
 * Clase abstracta que representa el clasificador de las preguntas
 * 
 * @author Raúl Sempere Trujillo
 */
public abstract class Clasificador
{   
    /**
     * Devuelve el tema de la asignatura (preguntaEN.getAsignatura().getTemas())
     * que mejor se ajusta a la pregunta, según algún criterio de clasificación.
     * 
     * @param enunciado String con el enunciado y la opcion correcta de la pregunta a clasificar
     * @param temas ArrayList listado de temas posibles en los que clasificar
     * @return TemaEN selecionado por el clasificador
     */
    public abstract TemaEN clasificar(String enunciado, ArrayList<TemaEN> temas);
}
