/* 
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : ClasificadorKeywords
 * Created on : 15-abr-2014, 00:46:00
 * Author     : Raúl Sempere Trujillo
 */

package clasificador;

import EN.TemaEN;
import java.util.ArrayList;

/**
 * Implementación del clasificador basado en keywords
 * 
 * @author Raúl Sempere Trujillo
 */
public class ClasificadorKeywords extends Clasificador
{
    /**
     * Implementa la función abstracta clasificar para el clasificador 
     * basado en keywords
     * 
     * @param enunciado String con el enunciado y la opcion correcta de la pregunta a clasificar
     * @param temas ArrayList listado de temas posibles en los que clasificar
     * @return TemaEN selecionado por el clasificador
     */
    @Override
    public TemaEN clasificar(String enunciado, ArrayList<TemaEN> temas)
    {
        TemaEN tema = null;
        int max = 0, actual;
        
        //para cada tema cargamos la lista de keywords (si esta vacía) y evaluamos
        for (TemaEN t : temas)
        {
            actual = 0;
            
            //Sumamos las ponderaciones de las keywords que se encuentran
            for (int i = 0; i < t.getKeywords().size(); i++)
            {
                if (enunciado.contains(t.getKeywords().get(i).getPalabra()))
                    actual += t.getKeywords().get(i).getPeso();
            }        
            
            if (max < actual)
            {
                max = actual;
                tema = t;
            }
        }
        
        //Devolvemos el tema que tiene la suma de las keywords encontrados mayor
        return tema;
    }
    
}
