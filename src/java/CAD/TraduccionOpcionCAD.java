/* 
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : TraduccionOpcionCAD
 * Created on : 15-abr-2014, 00:46:00
 * Author     : Raúl Sempere Trujillo
 */

package CAD;

import clases_generador.CADException;
import CEN.IdiomaCEN;
import CEN.OpcionCEN;
import EN.IdiomaEN;
import EN.OpcionEN;
import EN.PreguntaEN;
import EN.TraduccionOpcionEN;
import clases_generador.AccesoBD;
import clases_generador.ConfigManager;
import clases_generador.Utiles;
import java.sql.*;
import java.util.ArrayList;

/**
 * Clase que gestiona el acceso a la base de datos de la
 * entidad TraduccionOpcion
 * 
 * @author Raúl Sempere Trujillo
 */
public class TraduccionOpcionCAD
{
    
    /**
     * Variable privada: Acceso a la base de datos
     */
    private final AccesoBD BD;
    
    /**
     * Constructor por defecto. Establece la cadena de conexión si esta no está
     * establecida y realiza una conexión con la base de datos.
     */
    public TraduccionOpcionCAD()
    {
        if (!AccesoBD.isEstablecida())
            AccesoBD.setCadenaConexion(ConfigManager.getCadenaConexion());
        
        BD = new AccesoBD();
    }
    
    /**
     * Inserta el objeto traduccionOpcionEN en la base de datos
     * 
     * @param traduccionOpcionEN TraduccionOpcionEN a crear en la base datos
     * @return true en caso de éxito, false si no se pudo modificar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean New(TraduccionOpcionEN traduccionOpcionEN) throws CADException
    {
        boolean insertado = true;
   
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            st.execute("INSERT INTO opcion_idioma (oid_asignatura_pregunta_opcion, oid_pregunta_opcion, oid_opcion, oid_idioma, oid_enunciado) VALUES ("
                    + traduccionOpcionEN.getOpcion().getPregunta().getAsignatura().getCodigo() + ", " + traduccionOpcionEN.getOpcion().getPregunta().getCodigo() + ", "
                    + traduccionOpcionEN.getOpcion().getCodigo() + ", " + traduccionOpcionEN.getIdioma().getCodigo() + ", '"
                    + Utiles.StringBD(traduccionOpcionEN.getEnunciado()) + "')"); 
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            
            //Error al insertar
            if (ex.getErrorCode() == 1062)
                insertado = false;
            //Error en el acceso
            else
                throw new CADException(ex.getMessage());
        }
        
        return insertado;
    }
    
    /**
     * Modifica el objeto traduccionOpcionEN pasado por parámetro 
     * en la base de datos con los nuevos datos que contiene
     * 
     * @param traduccionOpcionEN TraduccionOpcionEN a modificar en la base de datos
     * @return true en caso de éxito, false si no se pudo modificar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean Modify(TraduccionOpcionEN traduccionOpcionEN) throws CADException
    {
        boolean modificado = true;
   
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            st.execute("UPDATE opcion_idioma SET "
                    + "oid_enunciado = '" + Utiles.StringBD(traduccionOpcionEN.getEnunciado()) + "' "
                    + "WHERE oid_asignatura_pregunta_opcion = " + traduccionOpcionEN.getOpcion().getPregunta().getAsignatura().getCodigo() + " AND oid_pregunta_opcion = " + traduccionOpcionEN.getOpcion().getPregunta().getCodigo()
                    + " AND oid_opcion = " + traduccionOpcionEN.getOpcion().getCodigo() + " AND oid_idioma = " + traduccionOpcionEN.getIdioma().getCodigo());
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            
            //Error al modificar
            if (ex.getErrorCode() == 1062)
                modificado = false;
            //Error en el acceso
            else
                throw new CADException(ex.getMessage());
        }
        
        return modificado;
    }
    
    /**
     * Elimina el objeto traduccionOpcionEN pasado por parámetro de
     * la base de datos
     * 
     * @param traduccionOpcionEN TraduccionOpcionEN a eliminar de la base de datos
     * @return true en caso de éxito, false si no se pudo eliminar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean Delete(TraduccionOpcionEN traduccionOpcionEN) throws CADException
    {
        boolean eliminado = true;
   
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            st.execute("DELETE FROM opcion_idioma "
                    + "WHERE oid_asignatura_pregunta_opcion = " + traduccionOpcionEN.getOpcion().getPregunta().getAsignatura().getCodigo() + " AND oid_pregunta_opcion = " + traduccionOpcionEN.getOpcion().getPregunta().getCodigo()
                    + " AND oid_opcion = " + traduccionOpcionEN.getOpcion().getCodigo() + " AND oid_idioma = " + traduccionOpcionEN.getIdioma().getCodigo());
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            
            //Error al eliminar
            if (ex.getErrorCode() == 1062)
                eliminado = false;
            //Error en el acceso
            else
                throw new CADException(ex.getMessage());
        }
        
        return eliminado;
    }
    
    /**
     * Obtiene todas las traducciones disponibles de una opción
     * 
     * @param opcionEN OpcionEN de la que se van a obtener las traducciones
     * @return ArrayList con la lista de traducciones de la opción
     * @throws CADException Error en el acceso a la base de datos
     */
    public ArrayList<TraduccionOpcionEN> getTraduccionesOpcionByOpcion(OpcionEN opcionEN) throws CADException
    {
        ArrayList<TraduccionOpcionEN> traducciones = new ArrayList<TraduccionOpcionEN>();
        TraduccionOpcionEN traduccionOpcionEN;
        IdiomaCEN idiomaCEN = new IdiomaCEN();
        
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT oid_asignatura_pregunta_opcion, oid_pregunta_opcion, oid_opcion, oid_idioma, oid_enunciado FROM opcion_idioma WHERE oid_asignatura_pregunta_opcion=" + opcionEN.getPregunta().getAsignatura().getCodigo() + " AND oid_pregunta_opcion=" + opcionEN.getPregunta().getCodigo() + " AND oid_opcion=" + opcionEN.getCodigo());
            
            while (rs.next())
            {
                traduccionOpcionEN = new TraduccionOpcionEN();
                
                traduccionOpcionEN.setOpcion(opcionEN);
                traduccionOpcionEN.setIdioma(idiomaCEN.getIdiomaByCodigo(rs.getInt("oid_idioma")));
                traduccionOpcionEN.setEnunciado(rs.getString("oid_enunciado"));
                
                traducciones.add(traduccionOpcionEN);
            }
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            throw new CADException(ex.getMessage());
        }

        return traducciones;
    }

    /**
     * Obtiene la traducción de una opción en un idioma concreto
     * 
     * @param opcionEN OpcionEN de la que obtendremos la traducción
     * @param idiomaEN IdiomaEN idioma de la traducción obtenida
     * @return TraduccionOpcionEN con la traducción
     * @throws CADException Error en el acceso a la base de datos
     */
    public TraduccionOpcionEN getTraduccionOpcionByOpcionEIdioma(OpcionEN opcionEN, IdiomaEN idiomaEN) throws CADException
    {
        TraduccionOpcionEN traduccionOpcionEN = null;
        
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT oid_asignatura_pregunta_opcion, oid_pregunta_opcion, oid_opcion, oid_idioma, oid_enunciado FROM opcion_idioma WHERE oid_asignatura_pregunta_opcion=" + opcionEN.getPregunta().getAsignatura().getCodigo() + " AND oid_pregunta_opcion=" + opcionEN.getPregunta().getCodigo() + " AND oid_opcion=" + opcionEN.getCodigo() + " AND oid_idioma=" + idiomaEN.getCodigo());
            
            if (rs.next())
            {
                traduccionOpcionEN = new TraduccionOpcionEN();
                
                traduccionOpcionEN.setOpcion(opcionEN);
                traduccionOpcionEN.setIdioma(idiomaEN);
                traduccionOpcionEN.setEnunciado(rs.getString("oid_enunciado"));
            }
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            throw new CADException(ex.getMessage());
        }

        return traduccionOpcionEN;
    }

    /**
     * Obtiene la traducción en un idioma de todas lsa opciones de una pregunta
     * 
     * @param preguntaEN PreguntaEN de la que obtendremos las opciones
     * @param idiomaEN IdiomaEN de la traduccion de las opciones
     * @return ArrayList con la traducción de las opciones
     * @throws CADException Error en el acceso a la base de datos
     */
    public ArrayList<TraduccionOpcionEN> getTraduccionesOpcionByPreguntaEIdioma(PreguntaEN preguntaEN, IdiomaEN idiomaEN) throws CADException
    {
        ArrayList<TraduccionOpcionEN> traducciones = new ArrayList<TraduccionOpcionEN>();
        TraduccionOpcionEN traduccionOpcionEN;
        OpcionCEN opcionCEN = new OpcionCEN();
        
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT oid_asignatura_pregunta_opcion, oid_pregunta_opcion, oid_opcion, oid_idioma, oid_enunciado FROM opcion_idioma WHERE oid_asignatura_pregunta_opcion=" + preguntaEN.getAsignatura().getCodigo() + " AND oid_pregunta_opcion=" + preguntaEN.getCodigo() + " AND oid_idioma=" + idiomaEN.getCodigo());
            
            while (rs.next())
            {
                traduccionOpcionEN = new TraduccionOpcionEN();
                
                traduccionOpcionEN.setOpcion(opcionCEN.getOpcionByCodigo(preguntaEN, rs.getInt("oid_opcion")));
                traduccionOpcionEN.setIdioma(idiomaEN);
                traduccionOpcionEN.setEnunciado(rs.getString("oid_enunciado"));
                
                traducciones.add(traduccionOpcionEN);
            }
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            throw new CADException(ex.getMessage());
        }

        return traducciones;
    }
}
