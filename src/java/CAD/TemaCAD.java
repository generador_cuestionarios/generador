/* 
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : TEmaCAD
 * Created on : 15-abr-2014, 00:46:00
 * Author     : Raúl Sempere Trujillo
 */

package CAD;

import clases_generador.CADException;
import EN.AsignaturaEN;
import EN.TemaEN;
import clases_generador.AccesoBD;
import clases_generador.ConfigManager;
import clases_generador.Utiles;
import java.sql.*;
import java.util.ArrayList;

/**
 * Clase que gestiona el acceso a la base de datos de la
 * entidad Tema
 * 
 * @author Raúl Sempere Trujillo
 */
public class TemaCAD
{
    
    /**
     * Variable privada: Acceso a la base de datos
     */
    private final AccesoBD BD;
    /**
     * Variable privada estática: Cerrojo que sirve para que solo una instancia
     * de la clase pueda obtener un nuevo codigo simultáneamente
     */
    private static Boolean cerrojo = null;
    
    /**
     * Función que recibe el código de una asígnatura y devuelve
     * el código del siguiente tema dentro de la asígnatura.
     * 
     * @param asignatura int con el código de la asignatura
     * @return int con el código del nuevo tema
     * @throws CADException Error en el acceso a la base de datos.
     */
    private int next(int asignatura) throws CADException
    {
        int nuevo_codigo;
        
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT tem_codigo FROM tema WHERE tem_asignatura=" + asignatura + " ORDER BY tem_codigo DESC");
            
            if (rs.next())
                nuevo_codigo = rs.getInt("tem_codigo") + 1;
            else
                nuevo_codigo = 1;
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            cerrojo = false;
            throw new CADException(ex.getMessage());
        }

        return nuevo_codigo;
    }
    
    /**
     * Constructor por defecto. Establece la cadena de conexión si esta no está
     * establecida y realiza una conexión con la base de datos.
     */
    public TemaCAD()
    {
        if (cerrojo == null)
            cerrojo = false;
        
        if (!AccesoBD.isEstablecida())
            AccesoBD.setCadenaConexion(ConfigManager.getCadenaConexion());
        
        BD = new AccesoBD();
    }

    /**
     * Inserta el objeto temaEN en la base de datos
     * 
     * @param temaEN TemaEN a crear en la base datos
     * @return true en caso de éxito, false si no se pudo modificar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean New(TemaEN temaEN) throws CADException
    {
        boolean insertado = false;
   
        while (cerrojo)
        {
            try
            {
                Thread.sleep(5);
            }
            catch (Exception ex)
            {             
            }
        }
        
        cerrojo = true;
        
        int nuevo_codigo = next(temaEN.getAsignatura().getCodigo());
        
        cerrojo = false;
        
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            st.execute("INSERT INTO tema (tem_asignatura, tem_codigo, tem_nombre) VALUES ("
                    + temaEN.getAsignatura().getCodigo() + ", " + nuevo_codigo + ", '" + Utiles.StringBD(temaEN.getNombre())+ "')"); 
            
            BD.Desconectar();
            temaEN.setCodigo(nuevo_codigo);
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            
            //Error al insertar
            if (ex.getErrorCode() == 1062)
                insertado = false;
            //Error en el acceso
            else
                throw new CADException(ex.getMessage());
        }
        
        return insertado;
    }
    
    /**
     * Modifica el objeto temaEN pasado por parámetro 
     * en la base de datos con los nuevos datos que contiene
     * 
     * @param temaEN TemaEN a modificar en la base de datos
     * @return true en caso de éxito, false si no se pudo modificar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean Modify(TemaEN temaEN) throws CADException
    {
        boolean modificado = false;
        String query = "";
        
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            
            query = "UPDATE tema SET tem_nombre = '" + Utiles.StringBD(temaEN.getNombre())
                    + "' WHERE tem_asignatura = " + temaEN.getAsignatura().getCodigo() + " AND tem_codigo = " + temaEN.getCodigo();
                    
            st.execute(query); 
            
            BD.Desconectar();

        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            
            //Error al modificar
            if (ex.getErrorCode() == 1062)
                modificado = false;
            //Error en el acceso
            else
                throw new CADException(ex.getMessage());
        }
        
        return modificado;
    }
    
    /**
     * Elimina el objeto temaEN pasado por parámetro de
     * la base de datos
     * 
     * @param temaEN TemaEN a eliminar de la base de datos
     * @return true en caso de éxito, false si no se pudo eliminar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean Delete(TemaEN temaEN) throws CADException
    {
        boolean eliminado = false;
        String query = "";
        
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            
            query = "DELETE FROM tema WHERE tem_asignatura = " + temaEN.getAsignatura().getCodigo() + " AND tem_codigo = " + temaEN.getCodigo();
                    
            st.execute(query); 
            
            BD.Desconectar();

        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            
            //Error al eliminar
            if (ex.getErrorCode() == 1062)
                eliminado = false;
            //Error en el acceso
            else
                throw new CADException(ex.getMessage());
        }
        
        return eliminado;
    }
    
    /**
     * Recupera el tema de la asignatura y el código pasado por parámentro
     * 
     * @param asignaturaEN AsignaturaEN del tema a recuperar
     * @param codigo int con el código del tema a recuperar
     * @return TemaEN recuperado de la base de datos
     * @throws CADException Error en el acceso a la base de datos
     */
    public TemaEN getTemaByCodigo(AsignaturaEN asignaturaEN, int codigo) throws CADException
    {
        TemaEN temaEN = null;
        
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT tem_asignatura, tem_codigo, tem_nombre FROM tema WHERE tem_asignatura=" + asignaturaEN.getCodigo() + " AND tem_codigo=" + codigo);
            
            if (rs.next())
            {
                temaEN = new TemaEN();
                
                temaEN.setAsignatura(asignaturaEN);
                temaEN.setCodigo(rs.getInt("tem_codigo"));
                temaEN.setNombre(rs.getString("tem_nombre"));
            }
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            throw new CADException(ex.getMessage());
        }

        return temaEN;
    }
    
    /**
     * Recupera el la lista de temas de la asignatura pasada por parámentro
     * 
     * @param asignaturaEN AsignaturaEN de la cual se obtendrá la lista de temas
     * @return ArrayList con la lista de temas recuperados de la base de datos
     * @throws CADException Error en el acceso a la base de datos
     */
    public ArrayList<TemaEN> getTemasByAsignatura(AsignaturaEN asignaturaEN) throws CADException
    {
        ArrayList<TemaEN> temas = new ArrayList<TemaEN>();
        TemaEN temaEN;
        
        
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT tem_asignatura, tem_codigo, tem_nombre FROM tema WHERE tem_asignatura=" + asignaturaEN.getCodigo());
            
            while (rs.next())
            {
                temaEN = new TemaEN();
                
                temaEN.setAsignatura(asignaturaEN);
                temaEN.setCodigo(rs.getInt("tem_codigo"));
                temaEN.setNombre(rs.getString("tem_nombre"));
                
                temas.add(temaEN);
            }
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            throw new CADException(ex.getMessage());
        }

        return temas;
    }
}
