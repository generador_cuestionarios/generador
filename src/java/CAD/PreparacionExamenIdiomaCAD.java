/* 
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : PreparacionExamenIdiomaCAD
 * Created on : 15-abr-2014, 00:46:00
 * Author     : Raúl Sempere Trujillo
 */

package CAD;

import clases_generador.CADException;
import CEN.IdiomaCEN;
import EN.ExamenEN;
import EN.IdiomaEN;
import EN.PreparacionExamenIdiomaEN;
import clases_generador.AccesoBD;
import clases_generador.ConfigManager;
import clases_generador.Utiles;
import java.sql.*;
import java.util.ArrayList;

/**
 * Clase que gestiona el acceso a la base de datos de la
 * entidad PreparacionExamenIdioma
 * 
 * @author Raúl Sempere Trujillo
 */
public class PreparacionExamenIdiomaCAD
{
    
    /**
     * Variable privada: Acceso a la base de datos
     */
    private final AccesoBD BD;
    
    /**
     * Constructor por defecto. Establece la cadena de conexión si esta no está
     * establecida y realiza una conexión con la base de datos.
     */
    public PreparacionExamenIdiomaCAD()
    {
        if (!AccesoBD.isEstablecida())
            AccesoBD.setCadenaConexion(ConfigManager.getCadenaConexion());
        
        BD = new AccesoBD();
    }
    
    /**
     * Inserta el objeto preparacionExamenIdiomaEN en la base de datos
     * 
     * @param preparacionExamenIdiomaEN PreparacionExamenIdiomaEN a crear en la base datos
     * @return true en caso de éxito, false si no se pudo modificar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean New(PreparacionExamenIdiomaEN preparacionExamenIdiomaEN) throws CADException
    {
        boolean insertado = true;
        
        String query = "";
   
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            query = "INSERT INTO idioma_examen (ide_asignatura_examen, ide_examen, ide_idioma, ide_traduccion_fecha, ide_traduccion_nombre, ide_traduccion_duracion, ide_revisada) VALUES ("
                    + preparacionExamenIdiomaEN.getExamen().getAsignatura().getCodigo() + ", " + preparacionExamenIdiomaEN.getExamen().getCodigo() + ", "
                    + preparacionExamenIdiomaEN.getIdioma().getCodigo() + ", '" + Utiles.StringBD(preparacionExamenIdiomaEN.getTraduccionFecha()) + "', '" + Utiles.StringBD(preparacionExamenIdiomaEN.getTraduccionNombre()) + "', '"
                    + Utiles.StringBD(preparacionExamenIdiomaEN.getTraduccionDuracion()) + "', " + preparacionExamenIdiomaEN.isRevisada() + ")";
            
            st.execute(query); 
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            
            //Error al insertar
            if (ex.getErrorCode() == 1062)
                insertado = false;
            //Error en el acceso
            else
                throw new CADException(query);
        }
        
        return insertado;
    }

    /**
     * Modifica el objeto preparacionExamenIdiomaEN pasado por parámetro 
     * en la base de datos con los nuevos datos que contiene
     * 
     * @param preparacionExamenIdiomaEN PreparacionExamenIdiomaEN a modificar en la base de datos
     * @return true en caso de éxito, false si no se pudo modificar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean Modify(PreparacionExamenIdiomaEN preparacionExamenIdiomaEN) throws CADException
    {
        boolean modificado = true;
        String query;

        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
                    
            query = "UPDATE idioma_examen SET "
                    + "ide_traduccion_fecha = '" + preparacionExamenIdiomaEN.getTraduccionFecha() + "', "
                    + "ide_traduccion_nombre = '" + preparacionExamenIdiomaEN.getTraduccionNombre() + "', "
                    + "ide_traduccion_duracion = '" + preparacionExamenIdiomaEN.getTraduccionDuracion() + "', "
                    + "ide_revisada = " + preparacionExamenIdiomaEN.isRevisada() + " "
                    + "WHERE ide_asignatura_examen = " + preparacionExamenIdiomaEN.getExamen().getAsignatura().getCodigo() + " AND ide_examen = " + preparacionExamenIdiomaEN.getExamen().getCodigo() + " AND ide_idioma = " + preparacionExamenIdiomaEN.getIdioma().getCodigo();
            
            st.execute(query); 
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            
            //Error al modificar
            if (ex.getErrorCode() == 1062)
                modificado = false;
            //Error en el acceso
            else
                throw new CADException(ex.getMessage());
        }
        
        return modificado;
    } 
    
    /**
     * Elimina el objeto preparacionExamenIdiomaEN pasado por parámetro de
     * la base de datos
     * 
     * @param preparacionExamenIdiomaEN PreparacionExamenIdiomaEN a eliminar de la base de datos
     * @return true en caso de éxito, false si no se pudo eliminar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean Delete(PreparacionExamenIdiomaEN preparacionExamenIdiomaEN) throws CADException
    {
        boolean eliminado = true;
        String query;

        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
                    
            query = "DELETE FROM idioma_examen "
                    + "WHERE ide_asignatura_examen = " + preparacionExamenIdiomaEN.getExamen().getAsignatura().getCodigo() + " AND ide_examen = " + preparacionExamenIdiomaEN.getExamen().getCodigo() + " AND ide_idioma = " + preparacionExamenIdiomaEN.getIdioma().getCodigo();
            
            st.execute(query); 
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            
            //Error al eliminar
            if (ex.getErrorCode() == 1062)
                eliminado = false;
            //Error en el acceso
            else
                throw new CADException(ex.getMessage());
        }
        
        return eliminado;
    }
    
    /**
     * Obtiene las preparaciones en todos los idiomas de un examen
     * pasado por parámentro
     * 
     * @param examenEN ExamenEN del que se desea obtener las preparaciones
     * @return ArrayList con las traducciones del examen
     * @throws CADException Error en el acceso a la base de datos
     */
    public ArrayList<PreparacionExamenIdiomaEN> getPreparacionesExamenByExamen(ExamenEN examenEN) throws CADException
    {
        ArrayList<PreparacionExamenIdiomaEN> preparaciones = new ArrayList<PreparacionExamenIdiomaEN>();
        PreparacionExamenIdiomaEN preparacionExamenIdiomaEN;
        IdiomaCEN idiomaCEN = new IdiomaCEN();
        
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT ide_asignatura_examen, ide_examen, ide_idioma, ide_traduccion_fecha, ide_traduccion_nombre, ide_traduccion_duracion, ide_revisada FROM idioma_examen WHERE ide_asignatura_examen=" + examenEN.getAsignatura().getCodigo() + " AND ide_examen=" + examenEN.getCodigo());
            
            while (rs.next())
            {
                preparacionExamenIdiomaEN = new PreparacionExamenIdiomaEN();
                
                preparacionExamenIdiomaEN.setExamen(examenEN);
                preparacionExamenIdiomaEN.setIdioma(idiomaCEN.getIdiomaByCodigo(rs.getInt("ide_idioma")));
                preparacionExamenIdiomaEN.setTraduccionFecha(rs.getString("ide_traduccion_fecha"));
                preparacionExamenIdiomaEN.setTraduccionNombre(rs.getString("ide_traduccion_nombre"));
                preparacionExamenIdiomaEN.setTraduccionDuracion(rs.getString("ide_traduccion_duracion"));
                preparacionExamenIdiomaEN.setRevisada(rs.getBoolean("ide_revisada"));
                
                preparaciones.add(preparacionExamenIdiomaEN);
            }
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            throw new CADException(ex.getMessage());
        }

        return preparaciones;
    }
    
    /**
     * Obtiene las preparaciones (solo de los idiomas actualmente configurados
     * en la asignatura) de un examen pasado por parámentro
     * 
     * @param examenEN ExamenEN del que se desea obtener las preparaciones
     * @return ArrayList con las traducciones del examen
     * @throws CADException Error en el acceso a la base de datos
     */
    public ArrayList<PreparacionExamenIdiomaEN> getPreparacionesDisponiblesByExamen(ExamenEN examenEN) throws CADException
    {
        ArrayList<PreparacionExamenIdiomaEN> preparaciones = new ArrayList<PreparacionExamenIdiomaEN>();
        PreparacionExamenIdiomaEN preparacionExamenIdiomaEN;
        IdiomaCEN idiomaCEN = new IdiomaCEN();
        
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT ide_asignatura_examen, ide_examen, ide_idioma, ide_traduccion_fecha, ide_traduccion_nombre, ide_traduccion_duracion, ide_revisada "
                    + "FROM idioma_examen "
                    + "WHERE ide_asignatura_examen = " + examenEN.getAsignatura().getCodigo() + " AND ide_examen = " + examenEN.getCodigo() + 
                    " AND ide_idioma IN (SELECT aid_idioma FROM asignatura_idioma WHERE aid_asignatura = " + examenEN.getAsignatura().getCodigo() + ")");
            
            while (rs.next())
            {
                preparacionExamenIdiomaEN = new PreparacionExamenIdiomaEN();
                
                preparacionExamenIdiomaEN.setExamen(examenEN);
                preparacionExamenIdiomaEN.setIdioma(idiomaCEN.getIdiomaByCodigo(rs.getInt("ide_idioma")));
                preparacionExamenIdiomaEN.setTraduccionFecha(rs.getString("ide_traduccion_fecha"));
                preparacionExamenIdiomaEN.setTraduccionNombre(rs.getString("ide_traduccion_nombre"));
                preparacionExamenIdiomaEN.setTraduccionDuracion(rs.getString("ide_traduccion_duracion"));
                preparacionExamenIdiomaEN.setRevisada(rs.getBoolean("ide_revisada"));
                
                preparaciones.add(preparacionExamenIdiomaEN);
            }
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            throw new CADException(ex.getMessage());
        }

        return preparaciones;
    }

    /**
     * Obtiene la preparación de un examen en un idioma pasado pro parámentro
     * 
     * @param examenEN ExamenEN del que se desea obtener la preparacion
     * @param idiomaEN IdiomaEN idioma de la preparacion
     * @return PreparacionExamenIdiomaEN con la preparación del examen
     * @throws CADException Error en el acceso a la base de datos
     */
    public PreparacionExamenIdiomaEN getPreparacionExamenByExamenEIdioma(ExamenEN examenEN, IdiomaEN idiomaEN) throws CADException
    {
        PreparacionExamenIdiomaEN preparacionExamenIdiomaEN = null;
        
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT ide_asignatura_examen, ide_examen, ide_idioma, ide_traduccion_fecha, ide_traduccion_nombre, ide_traduccion_duracion, ide_revisada FROM idioma_examen WHERE ide_asignatura_examen=" + examenEN.getAsignatura().getCodigo() + " AND ide_examen=" + examenEN.getCodigo() + " AND ide_idioma=" + idiomaEN.getCodigo());
            
            if (rs.next())
            {
                preparacionExamenIdiomaEN = new PreparacionExamenIdiomaEN();
                
                preparacionExamenIdiomaEN.setExamen(examenEN);
                preparacionExamenIdiomaEN.setIdioma(idiomaEN);
                preparacionExamenIdiomaEN.setTraduccionFecha(rs.getString("ide_traduccion_fecha"));
                preparacionExamenIdiomaEN.setTraduccionNombre(rs.getString("ide_traduccion_nombre"));
                preparacionExamenIdiomaEN.setTraduccionDuracion(rs.getString("ide_traduccion_duracion"));
                preparacionExamenIdiomaEN.setRevisada(rs.getBoolean("ide_revisada"));
            }
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            throw new CADException(ex.getMessage());
        }

        return preparacionExamenIdiomaEN;
    }    
    
}
