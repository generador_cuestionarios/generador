/* 
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : ExamenPreguntaCAD
 * Created on : 15-abr-2014, 00:46:00
 * Author     : Raúl Sempere Trujillo
 */

package CAD;

import clases_generador.CADException;
import CEN.ExamenCEN;
import CEN.PreguntaCEN;
import EN.ExamenEN;
import EN.ExamenPreguntaEN;
import EN.IdiomaEN;
import EN.PreguntaEN;
import clases_generador.AccesoBD;
import clases_generador.ConfigManager;
import java.sql.*;
import java.util.ArrayList;


/**
 * Clase que gestiona el acceso a la base de datos de la
 * entidad ExamenPregunta
 * 
 * @author Raúl Sempere Trujillo
 */
public class ExamenPreguntaCAD
{
    
    /**
     * Variable privada: Acceso a la base de datos
     */
    private final AccesoBD BD;
    
    /**
     * Constructor por defecto. Establece la cadena de conexión si esta no está
     * establecida y realiza una conexión con la base de datos.
     */
    public ExamenPreguntaCAD()
    {
        if (!AccesoBD.isEstablecida())
            AccesoBD.setCadenaConexion(ConfigManager.getCadenaConexion());
        
        BD = new AccesoBD();
    }
    
    /**
     * Inserta el objeto examenPreguntaEN en la base de datos
     * 
     * @param examenPreguntaEN ExamenPreguntaEN a crear en la base datos
     * @return true en caso de éxito, false si no se pudo modificar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean New(ExamenPreguntaEN examenPreguntaEN) throws CADException
    {
        boolean insertado = true;
        String query = "";
        
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            query = "INSERT INTO examen_pregunta (exp_asignatura_examen, exp_examen, exp_asignatura_pregunta, exp_pregunta) VALUES ("
                    + examenPreguntaEN.getExamen().getAsignatura().getCodigo() + ", " + examenPreguntaEN.getExamen().getCodigo() + ", "
                    + examenPreguntaEN.getPregunta().getAsignatura().getCodigo() + ", " + examenPreguntaEN.getPregunta().getCodigo() + ")"; 
            
            st.execute(query); 
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            
            //Error al insertar
            if (ex.getErrorCode() == 1062)
                insertado = false;
            //Error en el acceso
            else
                throw new CADException(ex.getMessage());
        }
        
        return insertado;
    }
    
    /**
     * Elimina el objeto examenEN pasado por parámetro de
     * la base de datos
     * 
     * @param examenEN ExamenEN a eliminar de la base de datos
     * @return true en caso de éxito, false si no se pudo eliminar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean eliminarPreguntasDeExamen(ExamenEN examenEN) throws CADException
    {
        boolean eliminadas = true;
        String query;
        
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            query = "DELETE FROM examen_pregunta WHERE exp_asignatura_examen = " + examenEN.getAsignatura().getCodigo() + " AND exp_examen = " + examenEN.getCodigo();
            
            st.execute(query); 
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            
            //Error al insertar
            if (ex.getErrorCode() == 1062)
                eliminadas = false;
            //Error en el acceso
            else
                throw new CADException(ex.getMessage());
        }
        
        return eliminadas;
    }
    
    /**
     * Devuelve las preguntas de un examen que estan sin revisar en el idioma
     * pasado por parámetro
     * 
     * @param examenEN ExamenEN de las preguntas
     * @param idiomaEN IdiomaEN en el que no estan revisadas
     * @return ArrayList con el listado de ExamenPreguntas
     * @throws CADException Error en el acceso a la base de datos
     */
    public ArrayList<ExamenPreguntaEN> getExamenesPreguntaSinRevisarByExamenEIdioma(ExamenEN examenEN, IdiomaEN idiomaEN) throws CADException
    {
        ArrayList<ExamenPreguntaEN> examenPreguntas = new ArrayList<ExamenPreguntaEN>();
        ExamenPreguntaEN examenPreguntaEN;
        PreguntaCEN preguntaCEN = new PreguntaCEN();
        
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            
            String query = "SELECT exp_asignatura_examen, exp_examen, exp_asignatura_pregunta, exp_pregunta "
                    + "FROM examen_pregunta LEFT OUTER JOIN pregunta_idioma ON exp_asignatura_examen = pid_asignatura_pregunta AND exp_pregunta = pid_pregunta AND "
                    + "pid_idioma = " + idiomaEN.getCodigo() + " WHERE (pid_revisada is null OR pid_revisada = false) AND "
                    + "exp_asignatura_examen = " + examenEN.getAsignatura().getCodigo() + " AND exp_examen = " + examenEN.getCodigo();
            
            ResultSet rs = st.executeQuery(query);
            
            while (rs.next())
            {
                examenPreguntaEN = new ExamenPreguntaEN();
                
                examenPreguntaEN.setExamen(examenEN);
                examenPreguntaEN.setPregunta(preguntaCEN.getPreguntaByCodigo(examenEN.getAsignatura(), rs.getInt("exp_pregunta")));
                
                examenPreguntas.add(examenPreguntaEN);
            }
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            throw new CADException(ex.getMessage());
        }

        return examenPreguntas;
    }
    
    /**
     * Devuelve las preguntas de un examen pasado por parámentro
     * 
     * @param examenEN ExamenEN del que se quieren recuperar las preguntas
     * @return ArrayList con el listado de ExamenPreguntas
     * @throws CADException Error en el acceso a la base de datos
     */
    public ArrayList<ExamenPreguntaEN> getExamenPreguntasByExamen(ExamenEN examenEN) throws CADException
    {
        ArrayList<ExamenPreguntaEN> examenPreguntas = new ArrayList<ExamenPreguntaEN>();
        ExamenPreguntaEN examenPreguntaEN;
        PreguntaCEN preguntaCEN = new PreguntaCEN();
        
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT exp_asignatura_examen, exp_examen, exp_asignatura_pregunta, exp_pregunta FROM examen_pregunta WHERE exp_asignatura_examen=" + examenEN.getAsignatura().getCodigo() + " AND exp_examen=" + examenEN.getCodigo());
            
            while (rs.next())
            {
                examenPreguntaEN = new ExamenPreguntaEN();
                
                examenPreguntaEN.setExamen(examenEN);
                examenPreguntaEN.setPregunta(preguntaCEN.getPreguntaByCodigo(examenEN.getAsignatura(), rs.getInt("exp_pregunta")));
                
                examenPreguntas.add(examenPreguntaEN);
            }
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            throw new CADException(ex.getMessage());
        }

        return examenPreguntas;
    }

    /**
     * Devuelve todos los examenes donde aparece la pregunta pasada por
     * parámentro
     * 
     * @param preguntaEN PreguntaEN de la que queremos los ExamenesPregunta
     * @return ArrayList con el listado de ExamenPreguntas
     * @throws CADException Error en el acceso a la base de datos
     */
    public ArrayList<ExamenPreguntaEN> getExamenesPreguntaByPregunta(PreguntaEN preguntaEN) throws CADException
    {
        ArrayList<ExamenPreguntaEN> examenPreguntas = new ArrayList<ExamenPreguntaEN>();
        ExamenPreguntaEN examenPreguntaEN;
        ExamenCEN examenCEN = new ExamenCEN();
        
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT exp_asignatura_examen, exp_examen, exp_asignatura_pregunta, exp_pregunta FROM examen_pregunta WHERE exp_asignatura_pregunta=" + preguntaEN.getAsignatura().getCodigo() + " AND exp_pregunta=" + preguntaEN.getCodigo());
            
            while (rs.next())
            {
                examenPreguntaEN = new ExamenPreguntaEN();
                
                examenPreguntaEN.setExamen(examenCEN.getExamenByCodigo(preguntaEN.getAsignatura(), rs.getInt("exp_examen")));
                examenPreguntaEN.setPregunta(preguntaEN);
                
                examenPreguntas.add(examenPreguntaEN);
            }
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            throw new CADException(ex.getMessage());
        }

        return examenPreguntas;
    }
}
