/* 
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : UsuarioCAD
 * Created on : 15-abr-2014, 00:46:00
 * Author     : Raúl Sempere Trujillo
 */

package CAD;

import clases_generador.CADException;
import EN.AsignaturaEN;
import EN.UsuarioEN;
import clases_generador.AccesoBD;
import clases_generador.ConfigManager;
import clases_generador.Utiles;
import java.sql.*;
import java.util.ArrayList;

/**
 * Clase que gestiona el acceso a la base de datos de la
 * entidad Usuario
 * 
 * @author Raúl Sempere Trujillo
 */
public class UsuarioCAD
{
    
    /**
     * Variable privada: Acceso a la base de datos
     */
    private final AccesoBD BD;
    
    /**
     * Constructor por defecto. Establece la cadena de conexión si esta no está
     * establecida y realiza una conexión con la base de datos.
     */
    public UsuarioCAD()
    {
        if (!AccesoBD.isEstablecida())
            AccesoBD.setCadenaConexion(ConfigManager.getCadenaConexion());
        
        BD = new AccesoBD();
    }
    
    /**
     * Inserta el objeto usuarioEN en la base de datos
     * 
     * @param usuarioEN UsuarioEN a crear en la base datos
     * @return true en caso de éxito, false si no se pudo modificar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean New(UsuarioEN usuarioEN) throws CADException
    {
        boolean insertado = true;
   
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            st.execute("INSERT INTO usuario (usu_codigo, usu_user, usu_password, usu_administrador) VALUES ("
                    + usuarioEN.getCodigo() + ", '" + Utiles.StringBD(usuarioEN.getUser()) + "', '"
                    + Utiles.StringBD(usuarioEN.getPassword()) + "', " + usuarioEN.isAdministrador()+ ")", Statement.RETURN_GENERATED_KEYS); 
            
            ResultSet rs = st.getGeneratedKeys();
            
            if (rs.next()) 
            {   
                usuarioEN.setCodigo(rs.getInt(1));
                insertado = true;
            }

            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            
            //Error al insertar
            if (ex.getErrorCode() == 1062)
                insertado = false;
            //Error en el acceso
            else
                throw new CADException(ex.getMessage());
        }
        
        return insertado;
    }
    
    /**
     * Modifica el objeto usuarioEN pasado por parámetro 
     * en la base de datos con los nuevos datos que contiene
     * 
     * @param usuarioEN UsuarioEN a modificar en la base de datos
     * @return true en caso de éxito, false si no se pudo modificar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean Modify(UsuarioEN usuarioEN) throws CADException
    {
        boolean modificado = true;
   
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            st.execute("UPDATE usuario SET "
                    + "usu_user = '" + usuarioEN.getUser() + "', "
                    + "usu_password = '" + usuarioEN.getPassword() + "', "
                    + "usu_administrador = " + usuarioEN.isAdministrador()
                    + " WHERE usu_codigo = " + usuarioEN.getCodigo());

            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            
            //Error al modificar
            if (ex.getErrorCode() == 1062)
                modificado = false;
            //Error en el acceso
            else
                throw new CADException(ex.getMessage());
        }
        
        return modificado;
    }
    
    /**
     * Elimina el objeto usuarioEN pasado por parámetro de
     * la base de datos
     * 
     * @param usuarioEN UsuarioEN a eliminar de la base de datos
     * @return true en caso de éxito, false si no se pudo eliminar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean Delete (UsuarioEN usuarioEN) throws CADException
    {
        boolean eliminado = true;
   
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            st.execute("DELETE FROM usuario WHERE usu_codigo = " + usuarioEN.getCodigo());

            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            
            //Error al eliminar
            if (ex.getErrorCode() == 1062)
                eliminado = false;
            //Error en el acceso
            else
                throw new CADException(ex.getMessage());
        }
        
        return eliminado;
    }
    
    /**
     * Recupera un usuario de la base de datos a través de su código
     * 
     * @param codigo int con el código de usuario a obtener
     * @return UsuarioEN datos del usuario recuperados de la base de datos
     * @throws CADException Error al acceder a la base de datos        
     */
    public UsuarioEN getUsuarioByCodigo(int codigo) throws CADException
    {
        UsuarioEN usuarioEN = null;
   
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT usu_codigo, usu_user, usu_password, usu_administrador FROM usuario WHERE usu_codigo=" + codigo);
            
            if (rs.next())
            {
                usuarioEN = new UsuarioEN();
                
                usuarioEN.setCodigo(rs.getInt("usu_codigo"));
                usuarioEN.setUser(rs.getString("usu_user"));
                usuarioEN.setPassword(rs.getString("usu_password"));
                usuarioEN.setAdministrador(rs.getBoolean("usu_administrador"));
            }
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            throw new CADException(ex.getMessage());
        }

        return usuarioEN;
    }
    
    /**
     * Obtiene un usuario por su nombre
     * 
     * @param nombre String con el nombre del usuario a recuperar
     * @return UsuarioEN recuperado de la base de datos
     * @throws CADException Error al acceder a la base de datos    
     */
    public UsuarioEN getUsuarioByNombre(String nombre) throws CADException
    {
        UsuarioEN usuarioEN = null;
   
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT usu_codigo, usu_user, usu_password, usu_administrador FROM usuario WHERE usu_user='" + nombre + "'");
            
            if (rs.next())
            {
                usuarioEN = new UsuarioEN();
                
                usuarioEN.setCodigo(rs.getInt("usu_codigo"));
                usuarioEN.setUser(rs.getString("usu_user"));
                usuarioEN.setPassword(rs.getString("usu_password"));
                usuarioEN.setAdministrador(rs.getBoolean("usu_administrador"));
            }
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            throw new CADException(ex.getMessage());
        }

        return usuarioEN;
    }
    
    /**
     * Obtiene la lista de usuarios que no tienen ningún tipo de acceso a la
     * asignatura pasada por parámetro
     * 
     * @param asignaturaEN AsignaturaEN donde se mirará que los usuarios no
     * tengan acceso
     * @return ArrayList con la lista de usuarios
     * @throws CADException Error al acceder a la base de datos   
     */
    public ArrayList<UsuarioEN> getUsuariosSinAccesoAsignatura(AsignaturaEN asignaturaEN) throws CADException
    {
        UsuarioEN usuarioEN;
        ArrayList<UsuarioEN> usuarios = new ArrayList<UsuarioEN>();
                
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT usu_codigo, usu_user, usu_password, usu_administrador FROM usuario "
                    + "WHERE usu_codigo NOT IN (SELECT usa_usuario FROM usuario_asignatura WHERE usa_asignatura = " + asignaturaEN.getCodigo() + ") ORDER BY usu_user DESC");
            
            while (rs.next())
            {
                usuarioEN = new UsuarioEN();
                
                usuarioEN.setCodigo(rs.getInt("usu_codigo"));
                usuarioEN.setUser(rs.getString("usu_user"));
                usuarioEN.setPassword(rs.getString("usu_password"));
                usuarioEN.setAdministrador(rs.getBoolean("usu_administrador"));
                
                usuarios.add(usuarioEN);
            }
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            throw new CADException(ex.getMessage());
        }

        return usuarios;
    }
    
    /**
     * Recupera la lista completa de usuarios del sistema
     * 
     * @return ArrayList con la lista completa de usuarios
     * @throws CADException Error al acceder a la base de datos   
     */
    public ArrayList<UsuarioEN> getUsuarios() throws CADException
    {
        UsuarioEN usuarioEN;
        ArrayList<UsuarioEN> usuarios = new ArrayList<UsuarioEN>();
                
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT usu_codigo, usu_user, usu_password, usu_administrador FROM usuario ORDER BY usu_user");
            
            while (rs.next())
            {
                usuarioEN = new UsuarioEN();
                
                usuarioEN.setCodigo(rs.getInt("usu_codigo"));
                usuarioEN.setUser(rs.getString("usu_user"));
                usuarioEN.setPassword(rs.getString("usu_password"));
                usuarioEN.setAdministrador(rs.getBoolean("usu_administrador"));
                
                usuarios.add(usuarioEN);
            }
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            throw new CADException(ex.getMessage());
        }

        return usuarios;
    }
    
    /**
     * Obtiene un usuario de la base de datos a través de su nombre y contraseña
     * @param user String con el login del usuario a recuperar
     * @param password String con el password del usuario a recuperar
     * @return UsuarioEN datos del usuario recuperados de la base de datos
     * @throws CADException Error al acceder a la base de datos        
     */
    public UsuarioEN getUsuarioByLogin(String user, String password) throws CADException
    {
        UsuarioEN usuarioEN = null;
   
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT usu_codigo, usu_user, usu_password, usu_administrador FROM usuario WHERE usu_user='" + user + "' AND usu_password='" + password + "' limit 1");
            if (rs.next())
            {
                usuarioEN = new UsuarioEN();
                
                usuarioEN.setCodigo(rs.getInt("usu_codigo"));
                usuarioEN.setUser(rs.getString("usu_user"));
                usuarioEN.setPassword(rs.getString("usu_password"));
                usuarioEN.setAdministrador(rs.getBoolean("usu_administrador"));
            }
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            throw new CADException(ex.getMessage());
        }

        return usuarioEN;
    }
}
