/* 
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : InstruccionCAD
 * Created on : 15-abr-2014, 00:46:00
 * Author     : Raúl Sempere Trujillo
 */

package CAD;

import clases_generador.CADException;
import EN.ExamenEN;
import EN.InstruccionEN;
import clases_generador.AccesoBD;
import clases_generador.ConfigManager;
import java.sql.*;
import java.util.ArrayList;

/**
 * Clase que gestiona el acceso a la base de datos de la
 * entidad Instruccion
 * 
 * @author Raúl Sempere Trujillo
 */
public class InstruccionCAD
{
    
    /**
     * Variable privada: Acceso a la base de datos
     */
    private final AccesoBD BD;
    /**
     * Variable privada estática: Cerrojo que sirve para que solo una instancia
     * de la clase pueda obtener un nuevo codigo simultáneamente
     */
    private static Boolean cerrojo = null;
    
    /**
     * Constructor por defecto. Lee los datos de la conexión de un fichero e 
     * inicia una conexión con la base de datos
     */
    public InstruccionCAD()
    {
        if (cerrojo == null)
            cerrojo = false;
        
        if (!AccesoBD.isEstablecida())
            AccesoBD.setCadenaConexion(ConfigManager.getCadenaConexion());
        
        BD = new AccesoBD();
    }
    
    /**
     * Función que recibe el código de una asígnatura y un examen y devuelve
     * código de la siguiente instrucción dentro del examen.
     * 
     * @param asignatura int con el código de la asignatura
     * @param examen int con el código del examen
     * @return int con el código de la nueva instrucción
     * @throws CADException Error en el acceso a la base de datos.
     */
    private int next(int asignatura, int examen) throws CADException
    {
        int nuevo_codigo;
        
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT ins_codigo FROM instruccion WHERE ins_asignatura_examen=" + asignatura + " AND ins_examen=" + examen + " ORDER BY ins_codigo DESC");
            
            if (rs.next())
                nuevo_codigo = rs.getInt("ins_codigo") + 1;
            else
                nuevo_codigo = 1;
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            cerrojo = false;
            throw new CADException(ex.getMessage());
        }

        return nuevo_codigo;
    }
    
    /**
     * Inserta el objeto instruccionEN en la base de datos
     * 
     * @param instruccionEN InstruccionEN a crear en la base datos
     * @return true en caso de éxito, false si no se pudo modificar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean New(InstruccionEN instruccionEN) throws CADException
    {
        boolean insertado = true;
        String query = "";
   
        while (cerrojo)
        {
            try
            {
                Thread.sleep(5);
            }
            catch (Exception ex)
            {             
            }
        }
        
        cerrojo = true;
        
        int nuevo_codigo = next(instruccionEN.getExamen().getAsignatura().getCodigo(), instruccionEN.getExamen().getCodigo());
        
        cerrojo = false;
   
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            query = "INSERT INTO instruccion (ins_asignatura_examen, ins_examen, ins_codigo) VALUES ("
                    + instruccionEN.getExamen().getAsignatura().getCodigo() + ", " + instruccionEN.getExamen().getCodigo() + ", "
                    + nuevo_codigo + ")";
            
            st.execute(query); 
            
            BD.Desconectar();
            instruccionEN.setCodigo(nuevo_codigo);
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            
            //Error al insertar
            if (ex.getErrorCode() == 1062)
                insertado = false;
            //Error en el acceso
            else
                throw new CADException(ex.getMessage());
        }
        
        return insertado;
    }
    
    /**
     * Elimina todas las intrucciones del examen pasado por parámetro
     * 
     * @param examenEN ExamenEN del que se borrarán las instrucciones
     * @return true en caso de éxito, false si no se pudo eliminar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean eliminarInstruccionesExamen(ExamenEN examenEN) throws CADException
    {
        boolean eliminados = true;
        String query;

        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            query = "DELETE FROM instruccion WHERE ins_asignatura_examen = " + examenEN.getAsignatura().getCodigo() + " AND ins_examen = " + examenEN.getCodigo();
            
            st.execute(query); 
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            
            //Error al eliminar
            if (ex.getErrorCode() == 1062)
                eliminados = false;
            //Error en el acceso
            else
                throw new CADException(ex.getMessage());
        }
        
        return eliminados;
    }

    /**
     * Obtiene la lista de instrucciones de un examen pasado por parámentro
     * 
     * @param examenEN ExamenEN del que se van a obtener las instrucciones
     * @return ArrayList con la lista de instrucciones.
     * @throws CADException Error en el acceso a la base de datos
     */
    public ArrayList<InstruccionEN> getInstruccionesByExamen(ExamenEN examenEN) throws CADException
    {
        ArrayList<InstruccionEN> instrucciones = new ArrayList<InstruccionEN>();
        InstruccionEN instruccionEN;
        
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT ins_asignatura_examen, ins_examen, ins_codigo FROM instruccion WHERE ins_asignatura_examen=" + examenEN.getAsignatura().getCodigo() + " AND ins_examen=" + examenEN.getCodigo());
            
            while (rs.next())
            {
                instruccionEN = new InstruccionEN();
                
                instruccionEN.setCodigo(rs.getInt("ins_codigo"));
                instruccionEN.setExamen(examenEN);
                
                instrucciones.add(instruccionEN);
            }
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            throw new CADException(ex.getMessage());
        }

        return instrucciones;
    }
}
