/* 
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : ExamenCAD
 * Created on : 15-abr-2014, 00:46:00
 * Author     : Raúl Sempere Trujillo
 */

package CAD;

import clases_generador.CADException;
import EN.AsignaturaEN;
import EN.ExamenEN;
import EN.PreguntaEN;
import clases_generador.AccesoBD;
import clases_generador.ConfigManager;
import java.sql.*;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Clase que gestiona el acceso a la base de datos de la
 * entidad Examen
 * 
 * @author Raúl Sempere Trujillo
 */
public class ExamenCAD
{
    
    /**
     * Variable privada: Acceso a la base de datos
     */
    private final AccesoBD BD;
    /**
     * Variable privada estática: Cerrojo que sirve para que solo una instancia
     * de la clase pueda obtener un nuevo codigo simultáneamente
     */
    private static Boolean cerrojo = null;
    
    /**
     * Función que recibe el código de una asígnatura y devuelve el código
     * del siguiente examen dentro de dicha asignatura.
     * 
     * @param asignatura int con el código de la asignatura
     * @return int con el código del nuevo examen
     * @throws CADException Error en el acceso a la base de datos.
     */
    private int next(int asignatura) throws CADException
    {
        int nuevo_codigo;
        
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT exa_codigo FROM examen WHERE exa_asignatura=" + asignatura + " ORDER BY exa_codigo DESC");
            
            if (rs.next())
                nuevo_codigo = rs.getInt("exa_codigo") + 1;
            else
                nuevo_codigo = 1;
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            cerrojo = false;
            throw new CADException(ex.getMessage());
        }

        return nuevo_codigo;
    }
    
    /**
     * Constructor por defecto. Establece la cadena de conexión si esta no está
     * establecida y realiza una conexión con la base de datos.
     */
    public ExamenCAD()
    {
        if (cerrojo == null)
            cerrojo = false;
        
        if (!AccesoBD.isEstablecida())
            AccesoBD.setCadenaConexion(ConfigManager.getCadenaConexion());
        
        BD = new AccesoBD();
    }
    
    /**
     * Inserta el objeto examenEN en la base de datos
     * 
     * @param examenEN ExamenEN a crear en la base datos
     * @return true en caso de éxito, false si no se pudo modificar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean New(ExamenEN examenEN) throws CADException
    {
        boolean insertado = true;
        String query = "";
   
        while (cerrojo)
        {
            try
            {
                Thread.sleep(5);
            }
            catch (Exception ex)
            {             
            }
        }
        
        cerrojo = true;
        
        int nuevo_codigo = next(examenEN.getAsignatura().getCodigo());
        
        cerrojo = false;
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();

            query = "INSERT INTO examen (exa_asignatura, exa_codigo, exa_fecha, exa_modalidades, exa_semilla, "
                    + "exa_analizado, exa_dificultad, exa_discriminacion, exa_duracion, exa_contestadas, exa_no_contestadas, exa_aciertos) VALUES ("
                    + examenEN.getAsignatura().getCodigo() + ", " + nuevo_codigo + ", '"
                    + examenEN.getFecha().get(Calendar.YEAR) + "-" + (examenEN.getFecha().get(Calendar.MONTH) + 1) + "-" + examenEN.getFecha().get(Calendar.DAY_OF_MONTH) + "', "
                    + examenEN.getModalidades() + ", " + examenEN.getSemilla() + ", " + examenEN.isAnalizado() + ", " + examenEN.getDificultad() + ", "
                    + examenEN.getDiscriminacion() + ", " + examenEN.getDuracion() + ", " + examenEN.getContestadas() + ", " + examenEN.getNoContestadas()+ ", " + examenEN.getAciertos() + ")";
            
            st.execute(query); 
            
            BD.Desconectar();
            examenEN.setCodigo(nuevo_codigo);
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            
            //Error al insertar
            if (ex.getErrorCode() == 1062)
                insertado = false;
            //Error en el acceso
            else
                throw new CADException(ex.getMessage());
        }
        
        return insertado;
    }

    /**
     * Modifica el objeto examenEN pasado por parámetro 
     * en la base de datos con los nuevos datos que contiene
     * 
     * @param examenEN ExamenEN a modificar en la base de datos
     * @return true en caso de éxito, false si no se pudo modificar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean Modify(ExamenEN examenEN) throws CADException
    {
        boolean modificado = true;
        String query;

        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();

            query = "UPDATE examen SET "
                    + "exa_fecha = '" + examenEN.getFecha().get(Calendar.YEAR) + "-" + (examenEN.getFecha().get(Calendar.MONTH) + 1) + "-" + examenEN.getFecha().get(Calendar.DAY_OF_MONTH) + "', "
                    + "exa_modalidades = " + examenEN.getModalidades() + ", "
                    + "exa_semilla = " + examenEN.getSemilla() + ", "
                    + "exa_analizado = " + examenEN.isAnalizado()  + ", "
                    + "exa_dificultad = " + examenEN.getDificultad() + ", "
                    + "exa_discriminacion = " + examenEN.getDiscriminacion() + ", "
                    + "exa_duracion = " + examenEN.getDuracion() + ", "
                    + "exa_contestadas = " + examenEN.getContestadas()+ ", "
                    + "exa_no_contestadas = " + examenEN.getNoContestadas()+ ", "
                    + "exa_aciertos = " + examenEN.getAciertos()+ " "
                    + "WHERE exa_asignatura = " + examenEN.getAsignatura().getCodigo() + " AND exa_codigo = " + examenEN.getCodigo();
            
            st.execute(query); 
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            
            //Error al modificar
            if (ex.getErrorCode() == 1062)
                modificado = false;
            //Error en el acceso
            else
                throw new CADException(ex.getMessage());
        }
        
        return modificado;
    }
          
    /**
     * Elimina el objeto examenEN pasado por parámetro de
     * la base de datos
     * 
     * @param examenEN ExamenEN a eliminar de la base de datos
     * @return true en caso de éxito, false si no se pudo eliminar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean Delete(ExamenEN examenEN) throws CADException
    {
        boolean eliminado = true;
        String query;

        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();

            query = "DELETE FROM examen WHERE exa_asignatura = " + examenEN.getAsignatura().getCodigo() + " AND exa_codigo = " + examenEN.getCodigo();
            
            st.execute(query); 
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            
            //Error al eliminar
            if (ex.getErrorCode() == 1062)
                eliminado = false;
            //Error en el acceso
            else
                throw new CADException(ex.getMessage());
        }
        
        return eliminado;
    }
    
    /**
     * Obtiene un examen de la base de datos y lo devuelve
     * 
     * @param asignaturaEN AsignaturaEN a la que pertenece el examen
     * @param codigo int. Código del examen a recuperar
     * @return ExamenEN con el examen recuperado
     * @throws CADException Error en el acceso a la base de datos
     */
    public ExamenEN getExamenByCodigo(AsignaturaEN asignaturaEN, int codigo) throws CADException
    {
        ExamenEN examenEN = null;
        
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT exa_asignatura, exa_codigo, exa_fecha, exa_modalidades, exa_semilla, exa_analizado,"
                    + " ROUND(exa_dificultad, 2), ROUND(exa_discriminacion, 2), exa_duracion, exa_contestadas, exa_no_contestadas,"
                    + " exa_aciertos FROM examen WHERE exa_asignatura=" + asignaturaEN.getCodigo() + " AND exa_codigo=" + codigo);
            
            if (rs.next())
            {
                examenEN = new ExamenEN();
                
                examenEN.setAsignatura(asignaturaEN);
                examenEN.setCodigo(rs.getInt("exa_codigo"));
                Calendar cal = Calendar.getInstance();
                cal.setTime(rs.getDate("exa_fecha"));
                examenEN.setFecha(cal);
                examenEN.setModalidades(rs.getInt("exa_modalidades"));
                examenEN.setSemilla(rs.getLong("exa_semilla"));
                examenEN.setAnalizado(rs.getBoolean("exa_analizado"));
                examenEN.setDificultad(rs.getDouble("ROUND(exa_dificultad, 2)"));
                examenEN.setDiscriminacion(rs.getDouble("ROUND(exa_discriminacion, 2)"));
                examenEN.setDuracion(rs.getInt("exa_duracion"));
                examenEN.setContestadas(rs.getInt("exa_contestadas"));
                examenEN.setNoContestadas(rs.getInt("exa_no_contestadas"));
                examenEN.setAciertos(rs.getInt("exa_aciertos"));
            }
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            throw new CADException(ex.getMessage());
        }

        return examenEN;
    }
    
    /**
     * Obtiene la fecha de último uso de la pregunta pasada por parámetro
     * 
     * @param preguntaEN PreguntaEN de la que se quiere conocer el último uso
     * @return Calendar con la fecha de último uso
     * @throws CADException Error en el acceso a la base de datos
     */
    public Calendar getUltimoUsoPreguntaByPregunta(PreguntaEN preguntaEN) throws CADException
    {
            
        Calendar ultimoUso = null;
        
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT exa_fecha FROM examen, examen_pregunta WHERE exa_asignatura = exp_asignatura_examen AND exa_asignatura =" + preguntaEN.getAsignatura().getCodigo() + " AND exp_pregunta =" + preguntaEN.getCodigo() + " AND exp_asignatura_examen = exa_asignatura AND exa_codigo = exp_examen AND exp_asignatura_pregunta = exa_asignatura ORDER BY exa_fecha DESC LIMIT 1");
            
            if (rs.next())
            {
                ultimoUso = Calendar.getInstance();
                ultimoUso.setTime(rs.getDate("exa_fecha"));
            }
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            throw new CADException(ex.getMessage());
        }

        return ultimoUso;
    }
    
    /**
     * Devuelve toda la lista de exámenes de la asignatura pasada por parámetro
     * 
     * @param asignaturaEN AsignaturaEN de la que se devolverán los exámenes
     * @return ArrayList con la lista de exámenes de la asignatura
     * @throws CADException Error en el acceso a la base de datos
     */
    public ArrayList<ExamenEN> getExamenesByAsignatura(AsignaturaEN asignaturaEN) throws CADException
    {
        ArrayList<ExamenEN> examenes = new ArrayList<ExamenEN>();
        ExamenEN examenEN;
        
        
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT exa_asignatura, exa_codigo, exa_fecha, exa_modalidades, exa_semilla, exa_analizado, "
                    + "ROUND(exa_dificultad, 2), ROUND(exa_discriminacion, 2), exa_duracion, exa_contestadas, exa_no_contestadas, exa_aciertos "
                    + "FROM examen WHERE exa_asignatura=" + asignaturaEN.getCodigo() + " ORDER BY exa_fecha DESC");
            
            while (rs.next())
            {
                examenEN = new ExamenEN();
                
                examenEN.setAsignatura(asignaturaEN);
                examenEN.setCodigo(rs.getInt("exa_codigo"));
                Calendar cal = Calendar.getInstance();
                cal.setTime(rs.getDate("exa_fecha"));
                examenEN.setFecha(cal);
                examenEN.setModalidades(rs.getInt("exa_modalidades"));
                examenEN.setSemilla(rs.getLong("exa_semilla"));
                examenEN.setAnalizado(rs.getBoolean("exa_analizado"));
                examenEN.setDificultad(rs.getDouble("ROUND(exa_dificultad, 2)"));
                examenEN.setDiscriminacion(rs.getDouble("ROUND(exa_discriminacion, 2)"));
                examenEN.setDuracion(rs.getInt("exa_duracion"));
                examenEN.setContestadas(rs.getInt("exa_contestadas"));
                examenEN.setNoContestadas(rs.getInt("exa_no_contestadas"));
                examenEN.setAciertos(rs.getInt("exa_aciertos"));
                
                examenes.add(examenEN);
            }
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            throw new CADException(ex.getMessage());
        }

        return examenes;
    }
    
    /**
     * Devuelve los examenes de la asignatura que cumplan con el filtro pasado
     * por parámetro
     * 
     * @param asignaturaEN AsignaturaEN de la que se devolverán los exámenes
     * @param filtro String del WHERE en formato SQL para filtrar los exámenes
     * @return ArrayList con la lista de exámenes de la asignatura
     * @throws CADException Error en el acceso a la base de datos
     */
    public ArrayList<ExamenEN> getExamenesByAsignaturaYFiltro(AsignaturaEN asignaturaEN, String filtro) throws CADException
    {
        ArrayList<ExamenEN> examenes = new ArrayList<ExamenEN>();
        ExamenEN examenEN;
        
        
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            
            String query =  "SELECT exa_asignatura, exa_codigo, exa_fecha, exa_modalidades, exa_semilla, exa_analizado, "
                    + "ROUND(exa_dificultad, 2), ROUND(exa_discriminacion, 2), exa_duracion, exa_contestadas, exa_no_contestadas, exa_aciertos, ide_traduccion_nombre "
                    + "FROM examen, idioma_examen "
                    + "WHERE exa_asignatura = ide_asignatura_examen AND ide_asignatura_examen = " + asignaturaEN.getCodigo() + " AND exa_codigo = ide_examen AND ide_idioma = (SELECT aid_idioma FROM asignatura_idioma WHERE aid_asignatura = " + asignaturaEN.getCodigo() + " AND aid_idioma_principal = true)";

            ResultSet rs = st.executeQuery(query + filtro + "ORDER BY exa_codigo DESC");
            
            while (rs.next())
            {
                examenEN = new ExamenEN();
                
                examenEN.setAsignatura(asignaturaEN);
                examenEN.setCodigo(rs.getInt("exa_codigo"));
                Calendar cal = Calendar.getInstance();
                cal.setTime(rs.getDate("exa_fecha"));
                examenEN.setFecha(cal);
                examenEN.setModalidades(rs.getInt("exa_modalidades"));
                examenEN.setSemilla(rs.getLong("exa_semilla"));
                examenEN.setAnalizado(rs.getBoolean("exa_analizado"));
                examenEN.setDificultad(rs.getDouble("ROUND(exa_dificultad, 2)"));
                examenEN.setDiscriminacion(rs.getDouble("ROUND(exa_discriminacion, 2)"));
                examenEN.setDuracion(rs.getInt("exa_duracion"));
                examenEN.setContestadas(rs.getInt("exa_contestadas"));
                examenEN.setNoContestadas(rs.getInt("exa_no_contestadas"));
                examenEN.setAciertos(rs.getInt("exa_aciertos"));
                
                examenes.add(examenEN);
            }
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            throw new CADException(ex.getMessage());
        }

        return examenes;
    }
}
