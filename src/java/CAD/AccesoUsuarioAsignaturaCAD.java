/* 
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : AccesoUsuarioAsignaturaCAD
 * Created on : 15-abr-2014, 00:46:00
 * Author     : Raúl Sempere Trujillo
 */

package CAD;

import clases_generador.CADException;
import CEN.AsignaturaCEN;
import CEN.UsuarioCEN;
import EN.AccesoUsuarioAsignaturaEN;
import EN.AsignaturaEN;
import EN.UsuarioEN;
import clases_generador.AccesoBD;
import clases_generador.ConfigManager;
import java.sql.*;
import java.util.ArrayList;

/**
 * Clase que gestiona el acceso a la base de datos de la
 * entidad AccesoUsuarioAsignatura
 * 
 * @author Raúl Sempere Trujillo
 */
public class AccesoUsuarioAsignaturaCAD
{
    
    /**
     * Variable privada: Acceso a la base de datos
     */
    private final AccesoBD BD;
    
    /**
     * Constructor por defecto. Establece la cadena de conexión si esta no está
     * establecida y realiza una conexión con la base de datos.
     */
    public AccesoUsuarioAsignaturaCAD()
    {
        if (!AccesoBD.isEstablecida())
            AccesoBD.setCadenaConexion(ConfigManager.getCadenaConexion());
        
        BD = new AccesoBD();
    }
    
    /**
     * Inserta el objeto accesoUsuarioAsignaturaEN en la base de datos
     * 
     * @param accesoUsuarioAsignaturaEN AccesoUsuarioAsignaturaEN a crear en la base datos
     * @return true en caso de éxito, false si no se pudo modificar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean New (AccesoUsuarioAsignaturaEN accesoUsuarioAsignaturaEN) throws CADException
    {
        boolean insertado = false;
   
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            st.execute("INSERT INTO usuario_asignatura (usa_asignatura, usa_usuario, usa_administrador) VALUES ("
                    + accesoUsuarioAsignaturaEN.getAsignatura().getCodigo() + ", " + accesoUsuarioAsignaturaEN.getUsuario().getCodigo() + ", " + accesoUsuarioAsignaturaEN.isAdministrador() + ")"); 
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            
            //Error al insertar
            if (ex.getErrorCode() == 1062)
                insertado = false;
            //Error en el acceso
            else
                throw new CADException(ex.getMessage());
        }
        
        return insertado;
    }
    
    /**
     * Modifica el objeto accesoUsuarioAsignaturaEN pasado por parámetro 
     * en la base de datos con los nuevos datos que contiene
     * 
     * @param accesoUsuarioAsignaturaEN AccesoUsuarioAsignaturaEN a modificar en la base de datos
     * @return true en caso de éxito, false si no se pudo modificar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean Modify (AccesoUsuarioAsignaturaEN accesoUsuarioAsignaturaEN) throws CADException
    {
        boolean modificado = false;
   
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            
            String query = "UPDATE usuario_asignatura SET "
                    + "usa_administrador = " + accesoUsuarioAsignaturaEN.isAdministrador()
                    + " WHERE usa_asignatura = " + accesoUsuarioAsignaturaEN.getAsignatura().getCodigo() + " AND usa_usuario = " + accesoUsuarioAsignaturaEN.getUsuario().getCodigo();
            
            st.execute(query);
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            
            //Error al modificar
            if (ex.getErrorCode() == 1062)
                modificado = false;
            //Error en el acceso
            else
                throw new CADException(ex.getMessage());
        }
        
        return modificado;
    }
    
    /**
     * Elimina el objeto accesoUsuarioAsignaturaEN pasado por parámetro de
     * la base de datos
     * 
     * @param accesoUsuarioAsignaturaEN AccesoUsuarioAsignaturaEN a eliminar 
     * de la base de datos
     * @return true en caso de éxito, false si no se pudo eliminar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean Delete (AccesoUsuarioAsignaturaEN accesoUsuarioAsignaturaEN) throws CADException
    {
        boolean eliminado = false;
   
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            
            String query = "DELETE FROM usuario_asignatura "
                    + "WHERE usa_asignatura = " + accesoUsuarioAsignaturaEN.getAsignatura().getCodigo() + " AND usa_usuario = " + accesoUsuarioAsignaturaEN.getUsuario().getCodigo();
            
            st.execute(query);
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            
            //Error al eliminar
            if (ex.getErrorCode() == 1062)
                eliminado = false;
            //Error en el acceso
            else
                throw new CADException(ex.getMessage());
        }
        
        return eliminado;
    }
    
    /**
     * Obtiene un ArrayList con los accesos de un usuario pasado por parámetro
     * 
     * @param usuarioEN del que se quiere obtener la lista de accesos
     * @return ArrayList con la lista de accesos.
     * @throws CADException Error al acceder a la base de datos
     */
    public ArrayList<AccesoUsuarioAsignaturaEN> getAccesosByUsuario(UsuarioEN usuarioEN) throws CADException
    {
        ArrayList<AccesoUsuarioAsignaturaEN> accesos = new ArrayList<AccesoUsuarioAsignaturaEN>();
        AccesoUsuarioAsignaturaEN accesoUsuarioAsignaturaEN;
        AsignaturaCEN asignaturaCEN = new AsignaturaCEN();
        
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT usa_asignatura, usa_usuario, usa_administrador FROM usuario_asignatura WHERE usa_usuario='" + usuarioEN.getCodigo() + "'");
            
            while (rs.next())
            {
                accesoUsuarioAsignaturaEN = new AccesoUsuarioAsignaturaEN();
                
                accesoUsuarioAsignaturaEN.setAsignatura(asignaturaCEN.getAsignaturaByCodigo(rs.getInt("usa_asignatura")));
                accesoUsuarioAsignaturaEN.setUsuario(usuarioEN);
                accesoUsuarioAsignaturaEN.setAdministrador(rs.getBoolean("usa_administrador"));
                
                accesos.add(accesoUsuarioAsignaturaEN);
            }
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            throw new CADException(ex.getMessage());
        }

        return accesos;
    }
    
    /**
     * Obtiene la lista de accesos de todos los usuarios de una asignatura
     * pasada por parámetro
     * 
     * @param asignaturaEN de la que se quieren obtener los accesos
     * @return ArrayList con la lista de accesos.
     * @throws CADException Error al acceder a la base de datos
     */
    public ArrayList<AccesoUsuarioAsignaturaEN> getAccesosByAsignatura(AsignaturaEN asignaturaEN) throws CADException
    {
        ArrayList<AccesoUsuarioAsignaturaEN> accesos = new ArrayList<AccesoUsuarioAsignaturaEN>();
        AccesoUsuarioAsignaturaEN accesoUsuarioAsignaturaEN;
        UsuarioCEN usuarioCEN = new UsuarioCEN();
        
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT usa_asignatura, usa_usuario, usa_administrador FROM usuario_asignatura WHERE usa_asignatura=" + asignaturaEN.getCodigo() + " ORDER BY usa_usuario DESC");
            
            while (rs.next())
            {
                accesoUsuarioAsignaturaEN = new AccesoUsuarioAsignaturaEN();
                
                accesoUsuarioAsignaturaEN.setAsignatura(asignaturaEN);
                accesoUsuarioAsignaturaEN.setUsuario(usuarioCEN.getUsuarioByCodigo(rs.getInt("usa_usuario")));
                accesoUsuarioAsignaturaEN.setAdministrador(rs.getBoolean("usa_administrador"));
                
                accesos.add(accesoUsuarioAsignaturaEN);
            }
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            throw new CADException(ex.getMessage());
        }

        return accesos;
    }
    
    /**
     * Devuelve los accesos del usuario en los que él es el único administrador de la asignatura
     * 
     * @param usuarioEN del que se quieren obtener los accesos
     * @return ArrayList con la lista de accesos.
     * @throws CADException Error al acceder a la base de datos
     */
    public ArrayList<AccesoUsuarioAsignaturaEN> getAccesosUsuarioUnicoAdmin(UsuarioEN usuarioEN) throws CADException
    {
        ArrayList<AccesoUsuarioAsignaturaEN> accesos = new ArrayList<AccesoUsuarioAsignaturaEN>();
        AccesoUsuarioAsignaturaEN accesoUsuarioAsignaturaEN;
        AsignaturaCEN asignaturaCEN = new AsignaturaCEN();
        
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT usa_asignatura, usa_usuario, usa_administrador FROM usuario_asignatura p WHERE p.usa_usuario = " + usuarioEN.getCodigo() + " AND p.usa_administrador = true AND \n"
                + "(SELECT COUNT(*) FROM usuario_asignatura q WHERE q.usa_usuario <> p.usa_usuario AND q.usa_administrador = true AND\n"
                + "p.usa_asignatura = q.usa_asignatura) = 0;");
            
            while (rs.next())
            {
                accesoUsuarioAsignaturaEN = new AccesoUsuarioAsignaturaEN();
                
                accesoUsuarioAsignaturaEN.setAsignatura(asignaturaCEN.getAsignaturaByCodigo(rs.getInt("usa_asignatura")));
                accesoUsuarioAsignaturaEN.setUsuario(usuarioEN);
                accesoUsuarioAsignaturaEN.setAdministrador(rs.getBoolean("usa_administrador"));
                
                accesos.add(accesoUsuarioAsignaturaEN);
            }
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            throw new CADException(ex.getMessage());
        }

        return accesos;
    }
    
    /**
     * Obtiene el acceso de un usuario concreto en una asignatura concreta
     * 
     * @param asignaturaEN de la que se obtendrá el acceso
     * @param usuarioEN del que se obtendrá el acceso
     * @return AccesoUsuarioAsignaturaEN con el acceso o null si no existe 
     * ningún acceso para esa asignatura y usuario
     * @throws CADException Error al acceder a la base de datos
     */
    public AccesoUsuarioAsignaturaEN getAccesoByAsignaturaYUsuario(AsignaturaEN asignaturaEN, UsuarioEN usuarioEN) throws CADException
    {
        AccesoUsuarioAsignaturaEN accesoUsuarioAsignaturaEN = null;
        
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT usa_asignatura, usa_usuario, usa_administrador FROM usuario_asignatura WHERE usa_asignatura=" + asignaturaEN.getCodigo() + " AND usa_usuario=" + usuarioEN.getCodigo());
            
            if (rs.next())
            {
                accesoUsuarioAsignaturaEN = new AccesoUsuarioAsignaturaEN();
                
                accesoUsuarioAsignaturaEN.setAsignatura(asignaturaEN);
                accesoUsuarioAsignaturaEN.setUsuario(usuarioEN);
                accesoUsuarioAsignaturaEN.setAdministrador(rs.getBoolean("usa_administrador"));
            }
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            throw new CADException(ex.getMessage());
        }

        return accesoUsuarioAsignaturaEN;
    }
}
