/* 
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : AsignaturaCAD
 * Created on : 15-abr-2014, 00:46:00
 * Author     : Raúl Sempere Trujillo
 */

package CAD;

import clases_generador.CADException;
import EN.AsignaturaEN;
import clases_generador.AccesoBD;
import clases_generador.ConfigManager;
import java.sql.*;

/**
 * Clase que gestiona el acceso a la base de datos de la entidad Asignatura
 * 
 * @author Raúl Sempere Trujillo
 */
public class AsignaturaCAD
{
    
    /**
     * Variable privada: Acceso a la base de datos
     */
    private final AccesoBD BD;
    
    /**
     * Constructor por defecto. Establece la cadena de conexión si esta no está
     * establecida y realiza una conexión con la base de datos.
     */
    public AsignaturaCAD()
    {
        if (!AccesoBD.isEstablecida())
            AccesoBD.setCadenaConexion(ConfigManager.getCadenaConexion());
        
        BD = new AccesoBD();
    }
    
    /**
     * Inserta el objeto asignaturaEN en la base de datos
     * 
     * @param asignaturaEN AsignaturaEN a crear en la base datos
     * @return true en caso de éxito, false si no se pudo modificar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean New (AsignaturaEN asignaturaEN) throws CADException
    {
        boolean insertado = false;
   
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            st.execute("INSERT INTO asignatura (asi_numeroOpciones) VALUES ("
                    + asignaturaEN.getNumeroOpciones() + ")", Statement.RETURN_GENERATED_KEYS);
            
            ResultSet rs = st.getGeneratedKeys();
            
            if (rs.next()) 
            {   
                asignaturaEN.setCodigo(rs.getInt(1));
                insertado = true;
            }
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            
            //Error al insertar
            if (ex.getErrorCode() == 1062)
                insertado = false;
            //Error en el acceso
            else
                throw new CADException(ex.getMessage());
        }
        
        return insertado;
    }
    
    /**
     * Modifica el objeto asignaturaEN pasado por parámetro 
     * en la base de datos con los nuevos datos que contiene
     * 
     * @param asignaturaEN AsignaturaEN a modificar en la base de datos
     * @return true en caso de éxito, false si no se pudo modificar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean Modify (AsignaturaEN asignaturaEN) throws CADException
    {
        boolean modificado = false;
   
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            st.execute("UPDATE asignatura SET "
                    + "asi_numeroOpciones = " + asignaturaEN.getNumeroOpciones()
                    + " WHERE asi_codigo = " + asignaturaEN.getCodigo());
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            
            //Error al modificar
            if (ex.getErrorCode() == 1062)
                modificado = false;
            //Error en el acceso
            else
                throw new CADException(ex.getMessage());
        }
        
        return modificado;
    }
    
    /**
     * Elimina el objeto asignaturaEN pasado por parámetro de
     * la base de datos
     * 
     * @param asignaturaEN AsignaturaEN a eliminar de la base de datos
     * @return true en caso de éxito, false si no se pudo eliminar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean Delete (AsignaturaEN asignaturaEN) throws CADException
    {
        boolean eliminado = false;
   
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            st.execute("DELETE FROM asignatura WHERE asi_codigo = " + asignaturaEN.getCodigo());
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            
            //Error al eliminar
            if (ex.getErrorCode() == 1062)
                eliminado = false;
            //Error en el acceso
            else
                throw new CADException(ex.getMessage());
        }
        
        return eliminado;
    }
    
    /**
     * Obtiene de la base de datos la asignatura con el código pasado por
     * parámentro
     * 
     * @param codigo de la asignatura que se quiere recuperar
     * @return AsignaturaEN recuperada de la base de datos o null si no se
     * encontró
     * @throws CADException Error en el acceso a la base de datos
     */
    public AsignaturaEN getAsignaturaByCodigo(int codigo) throws CADException
    {
        AsignaturaEN asignaturaEN = null;
   
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT asi_codigo, asi_numeroOpciones FROM asignatura WHERE asi_codigo=" + codigo);
            
            if (rs.next())
            {
                asignaturaEN = new AsignaturaEN();
                
                asignaturaEN.setCodigo(rs.getInt("asi_codigo"));
                asignaturaEN.setNumeroOpciones(rs.getInt("asi_numeroOpciones"));
            }
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            throw new CADException(ex.getMessage());
        }

        return asignaturaEN;
    }
}
