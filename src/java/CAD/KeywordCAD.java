/* 
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : KeywordCAD
 * Created on : 15-abr-2014, 00:46:00
 * Author     : Raúl Sempere Trujillo
 */

package CAD;

import clases_generador.CADException;
import EN.KeywordEN;
import EN.TemaEN;
import clases_generador.AccesoBD;
import clases_generador.ConfigManager;
import clases_generador.Utiles;
import java.sql.*;
import java.util.ArrayList;

/**
 * Clase que gestiona el acceso a la base de datos de la
 * entidad Keyword
 * 
 * @author Raúl Sempere Trujillo
 */
public class KeywordCAD
{
    
    /**
     * Variable privada: Acceso a la base de datos
     */
    private final AccesoBD BD;
    /**
     * Variable privada estática: Cerrojo que sirve para que solo una instancia
     * de la clase pueda obtener un nuevo codigo simultáneamente
     */
    private static Boolean cerrojo = null;
    
    /**
     * Constructor por defecto. Establece la cadena de conexión si esta no está
     * establecida y realiza una conexión con la base de datos.
     */
    public KeywordCAD()
    {
        if (cerrojo == null)
            cerrojo = false;
        
        if (!AccesoBD.isEstablecida())
            AccesoBD.setCadenaConexion(ConfigManager.getCadenaConexion());
        
        BD = new AccesoBD();
    }

    /**
     * Función que recibe el código de una asígnatura y un tema y devuelve
     * código de la siguiente keyword dentro del tema.
     * 
     * @param asignatura int con el código de la asignatura
     * @param tema int con el código del tema
     * @return int con el código de la nueva keyword
     * @throws CADException Error en el acceso a la base de datos.
     */
    private int next(int asignatura, int tema) throws CADException
    {
        int nuevo_codigo;
        
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT key_codigo FROM keyword WHERE key_asignatura_tema=" + asignatura + " AND key_tema=" + tema + " ORDER BY key_codigo DESC");
            
            if (rs.next())
                nuevo_codigo = rs.getInt("key_codigo") + 1;
            else
                nuevo_codigo = 1;
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            cerrojo = false;
            throw new CADException(ex.getMessage());
        }

        return nuevo_codigo;
    }
    
    /**
     * Inserta el objeto keywordEN en la base de datos
     * 
     * @param keywordEN KeywordEN a crear en la base datos
     * @return true en caso de éxito, false si no se pudo modificar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean New(KeywordEN keywordEN) throws CADException
    {
        boolean insertado = true;
        String query;
   
        while (cerrojo)
        {
            try
            {
                Thread.sleep(5);
            }
            catch (Exception ex)
            {             
            }
        }
        
        cerrojo = true;
        
        int nuevo_codigo = next(keywordEN.getTema().getAsignatura().getCodigo(), keywordEN.getTema().getCodigo());
        
        cerrojo = false;
   
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            query = "INSERT INTO keyword (key_asignatura_tema, key_tema, key_codigo, key_palabra, key_peso) VALUES ("
                    + keywordEN.getTema().getAsignatura().getCodigo() + ", " + keywordEN.getTema().getCodigo() + ", "
                    + nuevo_codigo + ", '" + Utiles.StringBD(keywordEN.getPalabra()) + "', " + keywordEN.getPeso()+ ")"; 
            
            st.execute(query); 
            
            BD.Desconectar();
            keywordEN.setCodigo(nuevo_codigo);
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            
            //Error al insertar
            if (ex.getErrorCode() == 1062)
                insertado = false;
            //Error en el acceso
            else
                throw new CADException(ex.getMessage());
        }
        
        return insertado;
    }
    
    /**
     * Elimina el objeto keywordEN pasado por parámetro de
     * la base de datos
     * 
     * @param keywordEN KeywordEN a eliminar de la base de datos
     * @return true en caso de éxito, false si no se pudo eliminar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean Delete(KeywordEN keywordEN) throws CADException
    {
        boolean eliminado = true;
        String query;
   
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            query = "DELETE FROM keyword WHERE key_asignatura_tema = " + keywordEN.getTema().getAsignatura().getCodigo() + " AND key_tema = " + keywordEN.getTema().getCodigo() + " AND key_codigo = " + keywordEN.getCodigo();

            st.execute(query); 
            
            BD.Desconectar();

        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            
            //Error al eliminar
            if (ex.getErrorCode() == 1062)
                eliminado = false;
            //Error en el acceso
            else
                throw new CADException(ex.getMessage());
        }
        
        return eliminado;
    }
    
    /**
     * Devuelve la lista de keywords de un tema
     * 
     * @param temaEN TemaEN del que se obtendrán los keywords
     * @return ArrayList con la lista de keywords
     * @throws CADException Error en el acceso a la base de datos 
     */
    public ArrayList<KeywordEN> getKeywordsByTema(TemaEN temaEN) throws CADException
    {
        ArrayList<KeywordEN> keywords = new ArrayList<KeywordEN>();
        KeywordEN keywordEN;
        
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT key_asignatura_tema, key_tema, key_codigo, key_palabra, key_peso FROM keyword WHERE key_asignatura_tema=" + temaEN.getAsignatura().getCodigo() + " AND key_tema=" + temaEN.getCodigo());
            
            while (rs.next())
            {
                keywordEN = new KeywordEN();
                
                keywordEN.setCodigo(rs.getInt("key_codigo"));
                keywordEN.setPalabra(rs.getString("key_palabra"));
                keywordEN.setPeso(rs.getInt("key_peso"));
                keywordEN.setTema(temaEN);
                
                keywords.add(keywordEN);
            }
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            throw new CADException(ex.getMessage());
        }

        return keywords;
    }

    /**
     * Obtiene un keyword concreto de un tema por su código dentro del tema
     * 
     * @param temaEN TemaEN del que se obtendrá el keyword
     * @param codigo int con el código del keyword a recuperar
     * @return KeywordEN keyword obtenido
     * @throws CADException Error en el acceso a la base de datos 
     */
     public KeywordEN getKeywordByCodigo(TemaEN temaEN, int codigo) throws CADException
    {
        KeywordEN keywordEN = null;
        
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT key_asignatura_tema, key_tema, key_codigo, key_palabra, key_peso FROM keyword WHERE key_asignatura_tema=" + temaEN.getAsignatura().getCodigo() + " AND key_tema=" + temaEN.getCodigo() + " AND key_codigo=" + codigo);
            
            if (rs.next())
            {
                keywordEN = new KeywordEN();
                
                keywordEN.setCodigo(rs.getInt("key_codigo"));
                keywordEN.setPalabra(rs.getString("key_palabra"));
                keywordEN.setPeso(rs.getInt("key_peso"));
                keywordEN.setTema(temaEN);
            }
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            throw new CADException(ex.getMessage());
        }

        return keywordEN;
    }
}
