/* 
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : ConfiguracionAsignaturaIdiomaCAD
 * Created on : 15-abr-2014, 00:46:00
 * Author     : Raúl Sempere Trujillo
 */

package CAD;

import clases_generador.CADException;
import CEN.AsignaturaCEN;
import CEN.IdiomaCEN;
import EN.AsignaturaEN;
import EN.ConfiguracionAsignaturaIdiomaEN;
import EN.IdiomaEN;
import clases_generador.AccesoBD;
import clases_generador.ConfigManager;
import clases_generador.Utiles;
import java.sql.*;
import java.util.ArrayList;

/**
 * Clase que gestiona el acceso a la base de datos de la
 * entidad ConfiguracionAsignaturaIdioma
 * 
 * @author Raúl Sempere Trujillo
 */
public class ConfiguracionAsignaturaIdiomaCAD
{
    
    /**
     * Variable privada: Acceso a la base de datos
     */
    private final AccesoBD BD;
    
    /**
     * Constructor por defecto. Establece la cadena de conexión si esta no está
     * establecida y realiza una conexión con la base de datos.
     */
    public ConfiguracionAsignaturaIdiomaCAD()
    {
        if (!AccesoBD.isEstablecida())
            AccesoBD.setCadenaConexion(ConfigManager.getCadenaConexion());
        
        BD = new AccesoBD();
    }
    
    /**
     * Inserta el objeto configuracionAsignaturaIdiomaEN en la base de datos
     * 
     * @param configuracionAsignaturaIdiomaEN ConfiguracionAsignaturaIdiomaEN a crear en la base datos
     * @return true en caso de éxito, false si no se pudo modificar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean New(ConfiguracionAsignaturaIdiomaEN configuracionAsignaturaIdiomaEN) throws CADException
    {
        boolean insertado = false;
   
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            st.execute("INSERT INTO asignatura_idioma (aid_asignatura, aid_idioma, aid_idioma_principal, aid_traduccion_asignatura, aid_traductor_apertium) VALUES ("
                    + configuracionAsignaturaIdiomaEN.getAsignatura().getCodigo() + ", " + configuracionAsignaturaIdiomaEN.getIdioma().getCodigo() + ", "
                    + configuracionAsignaturaIdiomaEN.isIdiomaPrincipal() + ", '" + Utiles.StringBD(configuracionAsignaturaIdiomaEN.getNombreAsignatura()) + "', '" + configuracionAsignaturaIdiomaEN.getTraductorApertium().replace("→", "%to%") + "')"); 
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            
            //Error al insertar
            if (ex.getErrorCode() == 1062)
                insertado = false;
            //Error en el acceso
            else
                throw new CADException(ex.getMessage());
        }
        
        return insertado;
    }
    
    /**
     * Modifica el objeto configuracionAsignaturaIdiomaEN pasado por parámetro 
     * en la base de datos con los nuevos datos que contiene
     * 
     * @param configuracionAsignaturaIdiomaEN ConfiguracionAsignaturaIdiomaEN a modificar en la base de datos
     * @return true en caso de éxito, false si no se pudo modificar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean Modify(ConfiguracionAsignaturaIdiomaEN configuracionAsignaturaIdiomaEN) throws CADException
    {
        boolean modificado = false;
   
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            st.execute("UPDATE asignatura_idioma SET "
                    + "aid_idioma_principal = " + configuracionAsignaturaIdiomaEN.isIdiomaPrincipal() + ", "
                    + "aid_traduccion_asignatura = '" + Utiles.StringBD(configuracionAsignaturaIdiomaEN.getNombreAsignatura()) + "', "
                    + "aid_traductor_apertium = '" + configuracionAsignaturaIdiomaEN.getTraductorApertium().replace("→", "%to%") + "' "
                    + "WHERE aid_asignatura = " + configuracionAsignaturaIdiomaEN.getAsignatura().getCodigo() + " AND "
                    + "aid_idioma = " + configuracionAsignaturaIdiomaEN.getIdioma().getCodigo());
 
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            
            //Error al modificar
            if (ex.getErrorCode() == 1062)
                modificado = false;
            //Error en el acceso
            else
                throw new CADException(ex.getMessage());
        }
        
        return modificado;
    }
    
    /**
     * Elimina el objeto configuracionAsignaturaIdiomaEN pasado por parámetro de
     * la base de datos
     * 
     * @param configuracionAsignaturaIdiomaEN ConfiguracionAsignaturaIdiomaEN a eliminar de la base de datos
     * @return true en caso de éxito, false si no se pudo eliminar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean Delete(ConfiguracionAsignaturaIdiomaEN configuracionAsignaturaIdiomaEN) throws CADException
    {
        boolean eliminado = false;
   
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            st.execute("DELETE FROM asignatura_idioma "
                    + "WHERE aid_asignatura = " + configuracionAsignaturaIdiomaEN.getAsignatura().getCodigo() + " AND "
                    + "aid_idioma = " + configuracionAsignaturaIdiomaEN.getIdioma().getCodigo());
 
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            
            //Error al eliminar
            if (ex.getErrorCode() == 1062)
                eliminado = false;
            //Error en el acceso
            else
                throw new CADException(ex.getMessage());
        }
        
        return eliminado;
    }
    
    /**
     * Obtiene la lista de configuraciones de una asignatura pasada por
     * parámetro
     * 
     * @param asignaturaEN AsignaturaEN de la que se quieren obtener las
     * configuraciones
     * @return ArrayList con la lista de configuraciones
     * @throws CADException Error en el acceso a la base de datos
     */
    public ArrayList<ConfiguracionAsignaturaIdiomaEN> getConfiguracionesAsignaturaIdiomaByAsignatura(AsignaturaEN asignaturaEN) throws CADException
    {
        ArrayList<ConfiguracionAsignaturaIdiomaEN> configuraciones = new ArrayList<ConfiguracionAsignaturaIdiomaEN>();
        ConfiguracionAsignaturaIdiomaEN configuracionAsignaturaIdiomaEN;
        IdiomaCEN idiomaCEN = new IdiomaCEN();
        
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT aid_asignatura, aid_idioma, aid_idioma_principal, aid_traduccion_asignatura, aid_traductor_apertium FROM asignatura_idioma WHERE aid_asignatura=" + asignaturaEN.getCodigo());
            
            while (rs.next())
            {
                configuracionAsignaturaIdiomaEN = new ConfiguracionAsignaturaIdiomaEN();
                
                configuracionAsignaturaIdiomaEN.setAsignatura(asignaturaEN);
                configuracionAsignaturaIdiomaEN.setIdioma(idiomaCEN.getIdiomaByCodigo(rs.getInt("aid_idioma")));
                configuracionAsignaturaIdiomaEN.setIdiomaPrincipal(rs.getBoolean("aid_idioma_principal"));
                configuracionAsignaturaIdiomaEN.setNombreAsignatura(rs.getString("aid_traduccion_asignatura"));
                configuracionAsignaturaIdiomaEN.setTraductorApertium(rs.getString("aid_traductor_apertium").replace("%to%", "→"));
                
                configuraciones.add(configuracionAsignaturaIdiomaEN);
            }
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            throw new CADException(ex.getMessage());
        }

        return configuraciones;
    }
    
    /**
     * Obtiene la configuracion de una asignatura en un idioma pasado por
     * parámetro
     * 
     * @param asignaturaEN AsignaturaEN de la que se quiere obtener la
     * configuracion
     * @param idiomaEN IdiomaEN de la configuración a obtener
     * @return ConfiguracionAsignaturaIdiomaEN con laconfiguracion en el idioma.
     * @throws CADException Error en el acceso a la base de datos
     */
    public ConfiguracionAsignaturaIdiomaEN getConfiguracionesAsignaturaIdiomaByAsignaturaEIdioma(AsignaturaEN asignaturaEN, IdiomaEN idiomaEN) throws CADException
    {
        ConfiguracionAsignaturaIdiomaEN configuracionAsignaturaIdiomaEN = null;
        
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT aid_asignatura, aid_idioma, aid_idioma_principal, aid_traduccion_asignatura, aid_traductor_apertium "
                    + "FROM asignatura_idioma WHERE aid_asignatura=" + asignaturaEN.getCodigo() + " AND aid_idioma=" + idiomaEN.getCodigo());
            
            if (rs.next())
            {
                configuracionAsignaturaIdiomaEN = new ConfiguracionAsignaturaIdiomaEN();
                
                configuracionAsignaturaIdiomaEN.setAsignatura(asignaturaEN);
                configuracionAsignaturaIdiomaEN.setIdioma(idiomaEN);
                configuracionAsignaturaIdiomaEN.setIdiomaPrincipal(rs.getBoolean("aid_idioma_principal"));
                configuracionAsignaturaIdiomaEN.setNombreAsignatura(rs.getString("aid_traduccion_asignatura"));
                configuracionAsignaturaIdiomaEN.setTraductorApertium(rs.getString("aid_traductor_apertium").replace("%to%", "→"));
            }
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            throw new CADException(ex.getMessage());
        }

        return configuracionAsignaturaIdiomaEN;
    }
    
    /**
     * Obtiene la configuracion del idioma principal de una asignatura
     * pasada por parámetro
     * 
     * @param asignaturaEN AsignaturaEN de la que se quiere obtener la
     * configuracion
     * @return ConfiguracionAsignaturaIdiomaEN con la configuracion en el idioma.
     * @throws CADException Error en el acceso a la base de datos
     */
    public ConfiguracionAsignaturaIdiomaEN getConfiguracionIdiomaPrincipal(AsignaturaEN asignaturaEN) throws CADException
    {
        ConfiguracionAsignaturaIdiomaEN configuracionAsignaturaIdiomaEN = null;
        IdiomaCEN idiomaCEN = new IdiomaCEN();
        
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT aid_asignatura, aid_idioma, aid_idioma_principal, aid_traduccion_asignatura, aid_traductor_apertium FROM asignatura_idioma WHERE aid_asignatura=" + asignaturaEN.getCodigo() + " AND aid_idioma_principal=true");
            
            if (rs.next())
            {
                configuracionAsignaturaIdiomaEN = new ConfiguracionAsignaturaIdiomaEN();
                
                configuracionAsignaturaIdiomaEN.setAsignatura(asignaturaEN);
                configuracionAsignaturaIdiomaEN.setIdioma(idiomaCEN.getIdiomaByCodigo(rs.getInt("aid_idioma")));
                configuracionAsignaturaIdiomaEN.setIdiomaPrincipal(rs.getBoolean("aid_idioma_principal"));
                configuracionAsignaturaIdiomaEN.setNombreAsignatura(rs.getString("aid_traduccion_asignatura"));
                configuracionAsignaturaIdiomaEN.setTraductorApertium(rs.getString("aid_traductor_apertium").replace("%to%", "→"));
            }
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            throw new CADException(ex.getMessage());
        }

        return configuracionAsignaturaIdiomaEN;
    }  
    
    /**
     * Obtiene la lista de configuraciones de todas las asignaturas cuyo idioma
     * pasado por parámetro es el principal.
     * 
     * @param idiomaEN IdiomaEN del cual se obtendran las configuraciones
     * @return ArrayList con la lista de configuraciones obtenidas
     * @throws CADException Error en el acceso a la base de datos
     */
    public ArrayList<ConfiguracionAsignaturaIdiomaEN> getConfiguracionesPrincipalesByIdioma(IdiomaEN idiomaEN) throws CADException
    {
        ConfiguracionAsignaturaIdiomaEN configuracionAsignaturaIdiomaEN;
        AsignaturaCEN asignaturaCEN = new AsignaturaCEN();
        ArrayList<ConfiguracionAsignaturaIdiomaEN> configuraciones = new ArrayList<ConfiguracionAsignaturaIdiomaEN>();
        
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT aid_asignatura, aid_idioma, aid_idioma_principal, aid_traduccion_asignatura, aid_traductor_apertium FROM asignatura_idioma WHERE aid_idioma=" + idiomaEN.getCodigo() + " AND aid_idioma_principal=true");
            
            while (rs.next())
            {
                configuracionAsignaturaIdiomaEN = new ConfiguracionAsignaturaIdiomaEN();
                
                configuracionAsignaturaIdiomaEN.setAsignatura(asignaturaCEN.getAsignaturaByCodigo(rs.getInt("aid_asignatura")));
                configuracionAsignaturaIdiomaEN.setIdioma(idiomaEN);
                configuracionAsignaturaIdiomaEN.setIdiomaPrincipal(rs.getBoolean("aid_idioma_principal"));
                configuracionAsignaturaIdiomaEN.setNombreAsignatura(rs.getString("aid_traduccion_asignatura"));
                configuracionAsignaturaIdiomaEN.setTraductorApertium(rs.getString("aid_traductor_apertium").replace("%to%", "→"));
                
                configuraciones.add(configuracionAsignaturaIdiomaEN);
            }
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            throw new CADException(ex.getMessage());
        }

        return configuraciones;
    }
    
    /**
     * Obtiene la lista de configuraciones principales de todas las asignaturas
     * 
     * @return ArrayList con la lista de configuraciones obtenidas
     * @throws CADException Error en el acceso a la base de datos
     */
    public ArrayList<ConfiguracionAsignaturaIdiomaEN> getConfiguracionIdiomaPrincipalAsignaturas() throws CADException
    {
        ConfiguracionAsignaturaIdiomaEN configuracionAsignaturaIdiomaEN;
        ArrayList<ConfiguracionAsignaturaIdiomaEN> configuracionAsignaturas = new ArrayList<ConfiguracionAsignaturaIdiomaEN>();
        IdiomaCEN idiomaCEN = new IdiomaCEN();
        AsignaturaCEN asignaturaCEN = new AsignaturaCEN();
        
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT aid_asignatura, aid_idioma, aid_idioma_principal, aid_traduccion_asignatura, aid_traductor_apertium FROM asignatura_idioma WHERE aid_idioma_principal=true ORDER BY aid_traduccion_asignatura DESC");
            
            while (rs.next())
            {
                configuracionAsignaturaIdiomaEN = new ConfiguracionAsignaturaIdiomaEN();
                
                configuracionAsignaturaIdiomaEN.setAsignatura(asignaturaCEN.getAsignaturaByCodigo(rs.getInt("aid_asignatura")));
                configuracionAsignaturaIdiomaEN.setIdioma(idiomaCEN.getIdiomaByCodigo(rs.getInt("aid_idioma")));
                configuracionAsignaturaIdiomaEN.setIdiomaPrincipal(rs.getBoolean("aid_idioma_principal"));
                configuracionAsignaturaIdiomaEN.setNombreAsignatura(rs.getString("aid_traduccion_asignatura"));
                configuracionAsignaturaIdiomaEN.setTraductorApertium(rs.getString("aid_traductor_apertium").replace("%to%", "→"));
                
                configuracionAsignaturas.add(configuracionAsignaturaIdiomaEN);
            }
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            throw new CADException(ex.getMessage());
        }

        return configuracionAsignaturas;
    }  

}
