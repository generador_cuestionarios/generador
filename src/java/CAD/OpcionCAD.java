/* 
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : OpcionCAD
 * Created on : 15-abr-2014, 00:46:00
 * Author     : Raúl Sempere Trujillo
 */

package CAD;

import clases_generador.CADException;
import EN.OpcionEN;
import EN.PreguntaEN;
import clases_generador.AccesoBD;
import clases_generador.ConfigManager;
import java.sql.*;
import java.util.ArrayList;

/**
 * Clase que gestiona el acceso a la base de datos de la
 * entidad Opcion
 * 
 * @author Raúl Sempere Trujillo
 */
public class OpcionCAD
{
    
    /**
     * Variable privada: Acceso a la base de datos
     */
    private final AccesoBD BD;
    /**
     * Variable privada estática: Cerrojo que sirve para que solo una instancia
     * de la clase pueda obtener un nuevo codigo simultáneamente
     */
    private static Boolean cerrojo = null;
    
    /**
     * Constructor por defecto. Establece la cadena de conexión si esta no está
     * establecida y realiza una conexión con la base de datos.
     */
    public OpcionCAD()
    {
        if (cerrojo == null)
            cerrojo = false;
        
        if (!AccesoBD.isEstablecida())
            AccesoBD.setCadenaConexion(ConfigManager.getCadenaConexion());
        
        BD = new AccesoBD();
    }
    
    /**
     * Función que recibe el código de una asígnatura y una pregunta y devuelve
     * el código de la siguiente opción dentro de la pregunta.
     * 
     * @param asignatura int con el código de la asignatura
     * @param pregunta int con el código de la pregunta
     * @return int con el código de la nueva opción
     * @throws CADException Error en el acceso a la base de datos.
     */
    private int next(int asignatura, int pregunta) throws CADException
    {
        int nuevo_codigo;
        
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT opc_codigo FROM opcion WHERE opc_asignatura_pregunta=" + asignatura + " AND opc_pregunta=" + pregunta + " ORDER BY opc_codigo DESC");
            
            if (rs.next())
                nuevo_codigo = rs.getInt("opc_codigo") + 1;
            else
                nuevo_codigo = 1;
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            cerrojo = false;
            throw new CADException(ex.getMessage());
        }

        return nuevo_codigo;
    }
    
    /**
     * Inserta el objeto opcionEN en la base de datos
     * 
     * @param opcionEN OpcionEN a crear en la base datos
     * @return true en caso de éxito, false si no se pudo modificar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean New(OpcionEN opcionEN) throws CADException
    {
        boolean insertado = true;
        String query = "";
   
        while (cerrojo)
        {
            try
            {
                Thread.sleep(5);
            }
            catch (Exception ex)
            {             
            }
        }
        
        cerrojo = true;
        
        int nuevo_codigo = next(opcionEN.getPregunta().getAsignatura().getCodigo(), opcionEN.getPregunta().getCodigo());
        
        cerrojo = false;
  
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            query = "INSERT INTO opcion (opc_asignatura_pregunta, opc_pregunta, opc_codigo, opc_correcta) VALUES ("
                    + opcionEN.getPregunta().getAsignatura().getCodigo() + ", " + opcionEN.getPregunta().getCodigo() + ", "
                    + nuevo_codigo + ", " + opcionEN.isCorrecta() + ")";
            
            st.execute(query); 
            
            BD.Desconectar();
            opcionEN.setCodigo(nuevo_codigo);
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            
            //Error al insertar
            if (ex.getErrorCode() == 1062)
                insertado = false;
            //Error en el acceso
            else
                throw new CADException(ex.getMessage());
        }
        
        return insertado;
    }

    /**
     * Modifica el objeto opcionEN pasado por parámetro 
     * en la base de datos con los nuevos datos que contiene
     * 
     * @param opcionEN OpcionEN a modificar en la base de datos
     * @return true en caso de éxito, false si no se pudo modificar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean Modify(OpcionEN opcionEN) throws CADException
    {
        boolean modificado = true;
        String query = "";

        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            query = "UPDATE opcion SET "
                    + "opc_correcta = " + opcionEN.isCorrecta() + " "
                    + "WHERE opc_asignatura_pregunta = " + opcionEN.getPregunta().getAsignatura().getCodigo() + " AND opc_pregunta = " + opcionEN.getPregunta().getCodigo() + " AND opc_codigo = " + opcionEN.getCodigo();
 
            st.execute(query); 
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            
            //Error al modificar
            if (ex.getErrorCode() == 1062)
                modificado = false;
            //Error en el acceso
            else
                throw new CADException(ex.getMessage());
        }
        
        return modificado;
    }    
    
    /**
     * Elimina el objeto opcionEN pasado por parámetro de
     * la base de datos
     * 
     * @param opcionEN OpcionEN a eliminar de la base de datos
     * @return true en caso de éxito, false si no se pudo eliminar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean Delete(OpcionEN opcionEN) throws CADException
    {
        boolean eliminado = true;
        String query = "";

        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            query = "DELETE FROM opcion "
                    + "WHERE opc_asignatura_pregunta = " + opcionEN.getPregunta().getAsignatura().getCodigo() + " AND opc_pregunta = " + opcionEN.getPregunta().getCodigo() + " AND opc_codigo = " + opcionEN.getCodigo();
 
            st.execute(query); 
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            
            //Error al eliminar
            if (ex.getErrorCode() == 1062)
                eliminado = false;
            //Error en el acceso
            else
                throw new CADException(ex.getMessage());
        }
        
        return eliminado;
    }   
    
    /**
     * Devuelve las opciones de una  pregunta pasada por parámetro
     * 
     * @param preguntaEN PreguntaEN de la que se quieren recuperar las opciones
     * @return ArrayList con la lista de opciones
     * @throws CADException Error en el acceso a la base de datos
     */
    public ArrayList<OpcionEN> getOpcionesByPregunta(PreguntaEN preguntaEN) throws CADException
    {
        ArrayList<OpcionEN> opciones = new ArrayList<OpcionEN>();
        OpcionEN opcionEN;
        
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT opc_asignatura_pregunta, opc_pregunta, opc_codigo, opc_correcta FROM opcion WHERE opc_asignatura_pregunta=" + preguntaEN.getAsignatura().getCodigo() + " AND opc_pregunta=" + preguntaEN.getCodigo());
            
            while (rs.next())
            {
                opcionEN = new OpcionEN();
                
                opcionEN.setPregunta(preguntaEN);
                opcionEN.setCodigo(rs.getInt("opc_codigo"));
                opcionEN.setCorrecta(rs.getBoolean("opc_correcta"));
                
                opciones.add(opcionEN);
            }
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            throw new CADException(ex.getMessage());
        }

        return opciones;
    }

    /**
     * Obtiene la ocpión de una pregunta pasada como parámetro por código
     * 
     * @param preguntaEN PreguntaEN de la que queremos obtener la opción
     * @param codigo int con el código de la opción a recuperar
     * @return OpcionEN con la opción
     * @throws CADException Error en el acceso a la base de datos 
     */
    public OpcionEN getOpcionByCodigo(PreguntaEN preguntaEN, int codigo) throws CADException
    {
        OpcionEN opcionEN = null;
        
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT opc_asignatura_pregunta, opc_pregunta, opc_codigo, opc_correcta FROM opcion WHERE opc_asignatura_pregunta=" + preguntaEN.getAsignatura().getCodigo() + " AND opc_pregunta=" + preguntaEN.getCodigo() + " AND opc_codigo=" + codigo);
            
            if (rs.next())
            {
                opcionEN = new OpcionEN();
                
                opcionEN.setPregunta(preguntaEN);
                opcionEN.setCodigo(rs.getInt("opc_codigo"));
                opcionEN.setCorrecta(rs.getBoolean("opc_correcta"));
            }
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            throw new CADException(ex.getMessage());
        }

        return opcionEN;
    }
}
