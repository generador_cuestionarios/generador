/* 
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : ClasificacionTemaCAD
 * Created on : 15-abr-2014, 00:46:00
 * Author     : Raúl Sempere Trujillo
 */

package CAD;

import clases_generador.CADException;
import CEN.PreguntaCEN;
import CEN.TemaCEN;
import EN.AsignaturaEN;
import EN.ClasificacionTemaEN;
import EN.PreguntaEN;
import EN.TemaEN;
import clases_generador.AccesoBD;
import clases_generador.ConfigManager;
import java.sql.*;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Clase que gestiona el acceso a la base de datos de la
 * entidad ClasificacionTema
 * 
 * @author Raúl Sempere Trujillo
 */
public class ClasificacionTemaCAD
{
    
    /**
     * Variable privada: Acceso a la base de datos
     */
    private final AccesoBD BD;
    
    /**
     * Constructor por defecto. Establece la cadena de conexión si esta no está
     * establecida y realiza una conexión con la base de datos.
     */
    public ClasificacionTemaCAD()
    {
        if (!AccesoBD.isEstablecida())
            AccesoBD.setCadenaConexion(ConfigManager.getCadenaConexion());
        
        BD = new AccesoBD();
    }
    
    /**
     * Utiliza la clase CAD para insertar el objeto clasificacionTemaEN 
     * en la base de datos
     * 
     * @param clasificacionTemaEN ClasificacionTemaEN a crear en la base datos
     * @return true en caso de éxito, false si no se pudo modificar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean New (ClasificacionTemaEN clasificacionTemaEN) throws CADException
    {
        boolean insertado = false;
   
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            st.execute("INSERT INTO clasificacion_tema (cla_asignatura_pregunta, cla_pregunta, cla_asignatura_tema, cla_tema, cla_revisada) VALUES ("
                    + clasificacionTemaEN.getPregunta().getAsignatura().getCodigo() + ", " + clasificacionTemaEN.getPregunta().getCodigo() + ", "
                    + clasificacionTemaEN.getTema().getAsignatura().getCodigo() + ", " + clasificacionTemaEN.getTema().getCodigo() + ", " + clasificacionTemaEN.isRevisada() + ")"); 
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            
            //Error al insertar
            if (ex.getErrorCode() == 1062)
                insertado = false;
            //Error en el acceso
            else
                throw new CADException(ex.getMessage());
        }
        
        return insertado;
    }
    
    /**
     * Utiliza la clase CAD para modificar el objeto clasificacionTemaEN 
     * pasado por parámetro en la base de datos con los nuevos datos que contiene
     * 
     * @param clasificacionTemaEN ClasificacionTemaEN a modificar en la base de datos
     * @return true en caso de éxito, false si no se pudo modificar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean Modify (ClasificacionTemaEN clasificacionTemaEN) throws CADException
    {
        boolean modificado = false;
        String query;
   
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            query = "UPDATE clasificacion_tema SET "
                    + "cla_revisada =" + clasificacionTemaEN.isRevisada()
                    + " WHERE cla_asignatura_pregunta=" + clasificacionTemaEN.getPregunta().getAsignatura().getCodigo() + " AND cla_pregunta=" + clasificacionTemaEN.getPregunta().getCodigo()
                    + " AND cla_asignatura_tema=" + clasificacionTemaEN.getTema().getAsignatura().getCodigo() + " AND cla_tema=" + clasificacionTemaEN.getTema().getCodigo();
            st.execute(query);
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            
            //Error al modificar
            if (ex.getErrorCode() == 1062)
                modificado = false;
            //Error en el acceso
            else
                throw new CADException(ex.getMessage());
        }
        
        return modificado;
    }
    
    /**
     * Utiliza la clase CAD para eliminar el objeto clasificacionTemaEN 
     * pasado por parámetro de la base de datos
     * 
     * @param clasificacionTemaEN ClasificacionTemaEN a eliminar
     * de la base de datos
     * @return true en caso de éxito, false si no se pudo eliminar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean Delete (ClasificacionTemaEN clasificacionTemaEN) throws CADException
    {
        boolean eliminado = false;
        String query;
   
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            query = "DELETE FROM clasificacion_tema "
                    + "WHERE cla_asignatura_pregunta=" + clasificacionTemaEN.getPregunta().getAsignatura().getCodigo() + " AND cla_pregunta=" + clasificacionTemaEN.getPregunta().getCodigo()
                    + " AND cla_asignatura_tema=" + clasificacionTemaEN.getTema().getAsignatura().getCodigo() + " AND cla_tema=" + clasificacionTemaEN.getTema().getCodigo();
            st.execute(query);
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            
            //Error al eliminar
            if (ex.getErrorCode() == 1062)
                eliminado = false;
            //Error en el acceso
            else
                throw new CADException(ex.getMessage());
        }
        
        return eliminado;
    }
    
    /**
     * Obtiene el número de preguntas clasificadas, revisados y sin revisar
     * en la asignatura pasada por parámetro
     * 
     * @param asignaturaEN AsignaturaEN de la que se quiere consultar el número de preguntas
     * clasificadas
     * @return int con el número de preguntas clasificadas
     * @throws CADException Error en el acceso a la base de datos
     */
    public int getCantClasificacionesByAsignatura(AsignaturaEN asignaturaEN) throws CADException
    {
        int cantidad = -1;
        
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT COUNT(*) as cantidad FROM clasificacion_tema WHERE cla_asignatura_pregunta=" + asignaturaEN.getCodigo() + " AND cla_asignatura_tema=" + asignaturaEN.getCodigo());
            
            if(rs.next())
                cantidad = rs.getInt("cantidad");
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            throw new CADException(ex.getMessage());
        }

        return cantidad;
    }

    /**
     * Devuelve un ArrayList con la lista de clasificaciónes para un tema
     * pasado por parámetro.
     * 
     * @param temaEN TemaEN del que queremos recuperar las clasificaciones.
     * @return ArrayList con la lista de clasificaciónes.
     * @throws CADException Error en el acceso a la base de datos
     */
    public ArrayList<ClasificacionTemaEN> getClasificacionesByTema(TemaEN temaEN) throws CADException
    {
        ArrayList<ClasificacionTemaEN> clasificaciones = new ArrayList<ClasificacionTemaEN>();
        ClasificacionTemaEN clasificacionTemaEN;
        PreguntaCEN preguntaCEN = new PreguntaCEN();
        
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT cla_asignatura_pregunta, cla_pregunta, cla_asignatura_tema, cla_tema, cla_revisada FROM clasificacion_tema WHERE cla_asignatura_tema=" + temaEN.getAsignatura().getCodigo() + " AND cla_tema=" + temaEN.getCodigo());
            
            while (rs.next())
            {
                clasificacionTemaEN = new ClasificacionTemaEN();
                
                clasificacionTemaEN.setTema(temaEN);
                clasificacionTemaEN.setPregunta(preguntaCEN.getPreguntaByCodigo(temaEN.getAsignatura(), rs.getInt("cla_pregunta")));
                clasificacionTemaEN.setRevisada(rs.getBoolean("cla_revisada"));
                
                clasificaciones.add(clasificacionTemaEN);
            }
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            throw new CADException(ex.getMessage());
        }

        return clasificaciones;
    }
    
    /**
     * Devuelve un ArrayList con la lista de clasificaciónes (solo las 
     * revisadas)para un tema pasado por parámetro.
     * 
     * @param temaEN TemaEN del que queremos recuperar las clasificaciones.
     * @return ArrayList con la lista de clasificaciónes.
     * @throws CADException Error en el acceso a la base de datos
     */
    public ArrayList<ClasificacionTemaEN> getClasificacionesRevisadasByTema(TemaEN temaEN) throws CADException
    {
        ArrayList<ClasificacionTemaEN> clasificaciones = new ArrayList<ClasificacionTemaEN>();
        ClasificacionTemaEN clasificacionTemaEN;
        PreguntaCEN preguntaCEN = new PreguntaCEN();
        
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT cla_asignatura_pregunta, cla_pregunta, cla_asignatura_tema, cla_tema, cla_revisada FROM clasificacion_tema WHERE cla_asignatura_tema=" + temaEN.getAsignatura().getCodigo() + " AND cla_tema=" + temaEN.getCodigo() + " AND cla_revisada=true");
            
            while (rs.next())
            {
                clasificacionTemaEN = new ClasificacionTemaEN();
                
                clasificacionTemaEN.setTema(temaEN);
                clasificacionTemaEN.setPregunta(preguntaCEN.getPreguntaByCodigo(temaEN.getAsignatura(), rs.getInt("cla_pregunta")));
                clasificacionTemaEN.setRevisada(rs.getBoolean("cla_revisada"));
                
                clasificaciones.add(clasificacionTemaEN);
            }
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            throw new CADException(ex.getMessage());
        }

        return clasificaciones;
    }  

    /**
     * Obtiene el número de preguntas clasificadas revisados en el tema
     * pasado por parámetro
     * 
     * @param temaEN TemaEN del que se quiere consultar el número de preguntas
     * clasificadas
     * @return int con el número de preguntas clasificadas
     * @throws CADException Error en el acceso a la base de datos
     */
    public int getCantidadClasificacionesRevisadasByTema(TemaEN temaEN) throws CADException
    {
        int cantidad = 0;
        
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT COUNT(*) FROM clasificacion_tema WHERE cla_asignatura_tema=" + temaEN.getAsignatura().getCodigo() + " AND cla_tema=" + temaEN.getCodigo() + " AND cla_revisada=true");
            
            if (rs.next())
            {
                cantidad = rs.getInt("COUNT(*)");
            }
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            throw new CADException(ex.getMessage());
        }

        return cantidad;
    }  

    /**
     * Obtiene la lista de preguntas clasificadas revisados en el tema
     * pasado por parámetro y que además esten habilitadas
     * 
     * @param temaEN TemaEN del que se quiere obtener el listado de preguntas
     * @return ArrayList la lista de clasificaciones
     * @throws CADException Error en el acceso a la base de datos
     */
    public ArrayList<ClasificacionTemaEN> getClasificacionesDisponiblesByTema(TemaEN temaEN) throws CADException
    {
        ArrayList<ClasificacionTemaEN> clasificaciones = new ArrayList<ClasificacionTemaEN>();
        ClasificacionTemaEN clasificacionTemaEN;
        PreguntaCEN preguntaCEN = new PreguntaCEN();
        
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT cla_asignatura_pregunta, cla_pregunta, cla_asignatura_tema, cla_tema, cla_revisada FROM clasificacion_tema, pregunta WHERE cla_pregunta = pre_codigo AND cla_asignatura_tema = pre_asignatura AND cla_asignatura_tema=" + temaEN.getAsignatura().getCodigo() + " AND cla_tema=" + temaEN.getCodigo() + " AND cla_revisada=true AND pre_habilitada = true");
            
            while (rs.next())
            {
                clasificacionTemaEN = new ClasificacionTemaEN();
                
                clasificacionTemaEN.setTema(temaEN);
                clasificacionTemaEN.setPregunta(preguntaCEN.getPreguntaByCodigo(temaEN.getAsignatura(), rs.getInt("cla_pregunta")));
                clasificacionTemaEN.setRevisada(rs.getBoolean("cla_revisada"));
                
                clasificaciones.add(clasificacionTemaEN);
            }
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            throw new CADException(ex.getMessage());
        }

        return clasificaciones;
    }
    
    /**
     * Obtiene la lista de preguntas clasificadas revisados en el tema
     * pasado por parámetro, que además esten habilitadas y que además no
     * se hayan usado a partir de la fecha "ultimoUso"
     * 
     * @param temaEN TemaEN del que se quiere obtener el listado de clasificaciones
     * @param ultimoUso Calendar fecha limite de último uso de las preguntas
     * @return ArrayList la lista de preguntas
     * @throws CADException Error en el acceso a la base de datos
     */
    public ArrayList<ClasificacionTemaEN> getClasificacionesDisponiblesByTemaYUltimoUso(TemaEN temaEN, Calendar ultimoUso) throws CADException
    {
        ArrayList<ClasificacionTemaEN> clasificaciones = new ArrayList<ClasificacionTemaEN>();
        ClasificacionTemaEN clasificacionTemaEN;
        PreguntaCEN preguntaCEN = new PreguntaCEN();
        
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            
            String query = "SELECT cla_asignatura_pregunta, cla_pregunta, cla_asignatura_tema, cla_tema, cla_revisada FROM clasificacion_tema, pregunta "
                    + "WHERE cla_pregunta = pre_codigo AND cla_asignatura_tema = pre_asignatura AND cla_asignatura_tema=" + temaEN.getAsignatura().getCodigo() + " AND cla_tema=" + temaEN.getCodigo() + " AND "
                    + "(pre_ultimo_uso <= '" + ultimoUso.get(Calendar.YEAR) + "-" + (ultimoUso.get(Calendar.MONTH) - 1) + "-" + ultimoUso.get(Calendar.DAY_OF_MONTH) + "'"
                    + " OR pre_ultimo_uso is NULL) AND cla_revisada=true AND pre_habilitada = true";
            
            ResultSet rs = st.executeQuery(query);
            
            while (rs.next())
            {
                clasificacionTemaEN = new ClasificacionTemaEN();
                
                clasificacionTemaEN.setTema(temaEN);
                clasificacionTemaEN.setPregunta(preguntaCEN.getPreguntaByCodigo(temaEN.getAsignatura(), rs.getInt("cla_pregunta")));
                clasificacionTemaEN.setRevisada(rs.getBoolean("cla_revisada"));
                
                clasificaciones.add(clasificacionTemaEN);
            }
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            throw new CADException(ex.getMessage());
        }

        return clasificaciones;
    }    

    /**
     * Obtiene la clasificación de una pregunta pasada por parámetro
     * 
     * @param preguntaEN PreguntaEN de la que se quiere obtener la clasificación
     * @return ArrayList la lista de preguntas
     * @throws CADException Error en el acceso a la base de datos
     */
    public ClasificacionTemaEN getClasificacionByPregunta(PreguntaEN preguntaEN) throws CADException
    {
        ClasificacionTemaEN clasificacionTemaEN = null;
        TemaCEN temaCEN = new TemaCEN();
        
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT cla_asignatura_pregunta, cla_pregunta, cla_asignatura_tema, cla_tema, cla_revisada FROM clasificacion_tema WHERE cla_asignatura_pregunta=" + preguntaEN.getAsignatura().getCodigo() + " AND cla_pregunta=" + preguntaEN.getCodigo());
            
            if (rs.next())
            {
                clasificacionTemaEN = new ClasificacionTemaEN();
                
                clasificacionTemaEN.setTema(temaCEN.getTemaByCodigo(preguntaEN.getAsignatura(), rs.getInt("cla_tema")));
                clasificacionTemaEN.setPregunta(preguntaEN);
                clasificacionTemaEN.setRevisada(rs.getBoolean("cla_revisada"));
            }
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            throw new CADException(ex.getMessage());
        }

        return clasificacionTemaEN;
    }    
}
