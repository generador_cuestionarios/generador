/* 
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : IdiomaCAD
 * Created on : 15-abr-2014, 00:46:00
 * Author     : Raúl Sempere Trujillo
 */

package CAD;

import clases_generador.CADException;
import EN.AsignaturaEN;
import EN.IdiomaEN;
import clases_generador.AccesoBD;
import clases_generador.ConfigManager;
import clases_generador.Utiles;
import java.sql.*;
import java.util.ArrayList;

/**
 * Clase que gestiona el acceso a la base de datos de la
 * entidad Idioma
 * 
 * @author Raúl Sempere Trujillo
 */
public class IdiomaCAD
{
    
    /**
     * Variable privada: Acceso a la base de datos
     */
    private final AccesoBD BD;
    
    /**
     * Constructor por defecto. Establece la cadena de conexión si esta no está
     * establecida y realiza una conexión con la base de datos.
     */
    public IdiomaCAD()
    {
        if (!AccesoBD.isEstablecida())
            AccesoBD.setCadenaConexion(ConfigManager.getCadenaConexion());
        
        BD = new AccesoBD();
    }
    
    /**
     * Inserta el objeto idiomaEN en la base de datos
     * 
     * @param idiomaEN IdiomaEN a crear en la base datos
     * @return true en caso de éxito, false si no se pudo modificar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean New (IdiomaEN idiomaEN) throws CADException
    {
        boolean insertado = false;
   
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            st.execute("INSERT INTO idioma (idi_codigo, idi_nombre, idi_traduccion_instruccion, idi_traduccion_pregunta, idi_traduccion_modalidad, "
                    + "idi_traduccion_duracion, idi_traduccion_respuestas, idi_paquete_babel) VALUES ("
                    + idiomaEN.getCodigo() + ", '" + Utiles.StringBD(idiomaEN.getNombre()) + "', '"
                    + Utiles.StringBD(idiomaEN.getTraduccionInstrucciones()) + "', '" + Utiles.StringBD(idiomaEN.getTraduccionPreguntas()) + "', '"
                    + Utiles.StringBD(idiomaEN.getTraduccionModalidad()) + "', '" + Utiles.StringBD(idiomaEN.getTraduccionDuracion()) + "', '"
                    + Utiles.StringBD(idiomaEN.getTraduccionRespuestas()) + "', '" + Utiles.StringBD(idiomaEN.getPaqueteBabel()) + "')", Statement.RETURN_GENERATED_KEYS); 
            
            ResultSet rs = st.getGeneratedKeys();
            
            if (rs.next()) 
            {   
                idiomaEN.setCodigo(rs.getInt(1));
                insertado = true;
            }

            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            
            //Error al insertar
            if (ex.getErrorCode() == 1062)
                insertado = false;
            //Error en el acceso
            else
                throw new CADException(ex.getMessage());
        }
        
        return insertado;
    }
    
    /**
     * Modifica el objeto idiomaEN pasado por parámetro 
     * en la base de datos con los nuevos datos que contiene
     * 
     * @param idiomaEN IdiomaEN a modificar en la base de datos
     * @return true en caso de éxito, false si no se pudo modificar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean Modify (IdiomaEN idiomaEN) throws CADException
    {
        boolean modificado = false;
   
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            st.execute("UPDATE idioma SET "
                    + "idi_nombre = '" + Utiles.StringBD(idiomaEN.getNombre()) + "', "
                    + "idi_traduccion_instruccion = '" + Utiles.StringBD(idiomaEN.getTraduccionInstrucciones()) + "', "
                    + "idi_traduccion_pregunta = '" + Utiles.StringBD(idiomaEN.getTraduccionPreguntas()) + "', "
                    + "idi_traduccion_modalidad = '" + Utiles.StringBD(idiomaEN.getTraduccionModalidad()) + "', "
                    + "idi_traduccion_duracion = '" + Utiles.StringBD(idiomaEN.getTraduccionDuracion()) + "', "
                    + "idi_traduccion_respuestas = '" + Utiles.StringBD(idiomaEN.getTraduccionRespuestas()) + "', "
                    + "idi_paquete_babel = '" + Utiles.StringBD(idiomaEN.getPaqueteBabel()) + "' "
                    + "WHERE idi_codigo = " + idiomaEN.getCodigo());
                   
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            
            //Error al modificar
            if (ex.getErrorCode() == 1062)
                modificado = false;
            //Error en el acceso
            else
                throw new CADException(ex.getMessage());
        }
        
        return modificado;
    }
    
    /**
     * Elimina el objeto idiomaEN pasado por parámetro de
     * la base de datos
     * 
     * @param idiomaEN IdiomaEN a eliminar de la base de datos
     * @return true en caso de éxito, false si no se pudo eliminar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean Delete (IdiomaEN idiomaEN) throws CADException
    {
        boolean eliminado = false;
   
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            st.execute("DELETE FROM idioma WHERE idi_codigo = " + idiomaEN.getCodigo());
                   
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            
            //Error al eliminar
            if (ex.getErrorCode() == 1062)
                eliminado = false;
            //Error en el acceso
            else
                throw new CADException(ex.getMessage());
        }
        
        return eliminado;
    }
    
    /**
     * Recupera un idioma de la base de datos a través de su código
     * 
     * @param codigo int con el código del idioma a obtener
     * @return IdiomaEN datos del idioma recuperados de la base de datos
     * @throws CADException Error al acceder a la base de datos        
     */
    public IdiomaEN getIdiomaByCodigo(int codigo) throws CADException
    {
        IdiomaEN idiomaEN = null;
   
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT idi_codigo, idi_nombre, idi_traduccion_instruccion, idi_traduccion_pregunta, "
                    + "idi_traduccion_modalidad, idi_traduccion_duracion, idi_traduccion_respuestas, idi_paquete_babel FROM idioma WHERE idi_codigo=" + codigo);
            
            if (rs.next())
            {
                idiomaEN = new IdiomaEN();
                
                idiomaEN.setCodigo(rs.getInt("idi_codigo"));
                idiomaEN.setNombre(rs.getString("idi_nombre"));
                idiomaEN.setTraduccionInstrucciones(rs.getString("idi_traduccion_instruccion"));
                idiomaEN.setTraduccionPreguntas(rs.getString("idi_traduccion_pregunta"));
                idiomaEN.setTraduccionModalidad(rs.getString("idi_traduccion_modalidad"));
                idiomaEN.setTraduccionDuracion(rs.getString("idi_traduccion_duracion"));
                idiomaEN.setTraduccionRespuestas(rs.getString("idi_traduccion_respuestas"));
                idiomaEN.setPaqueteBabel(rs.getString("idi_paquete_babel"));
            }
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            throw new CADException(ex.getMessage());
        }

        return idiomaEN;
    }
    
    /**
     * Recupera un idioma de la base de datos a través de su nombre
     * 
     * @param nombre { con el nombre delidioma a obtener
     * @return IdiomaEN datos del idioma recuperados de la base de datos
     * @throws CADException Error al acceder a la base de datos        
     */
    public IdiomaEN getIdiomaByNombre(String nombre) throws CADException
    {
        IdiomaEN idiomaEN = null;
   
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT idi_codigo, idi_nombre, idi_traduccion_instruccion, idi_traduccion_pregunta, "
                    + "idi_traduccion_modalidad, idi_traduccion_duracion, idi_traduccion_respuestas, idi_paquete_babel FROM idioma WHERE idi_nombre='" + nombre + "'");
            
            if (rs.next())
            {
                idiomaEN = new IdiomaEN();
                
                idiomaEN.setCodigo(rs.getInt("idi_codigo"));
                idiomaEN.setNombre(rs.getString("idi_nombre"));
                idiomaEN.setTraduccionInstrucciones(rs.getString("idi_traduccion_instruccion"));
                idiomaEN.setTraduccionPreguntas(rs.getString("idi_traduccion_pregunta"));
                idiomaEN.setTraduccionModalidad(rs.getString("idi_traduccion_modalidad"));
                idiomaEN.setTraduccionDuracion(rs.getString("idi_traduccion_duracion"));
                idiomaEN.setTraduccionRespuestas(rs.getString("idi_traduccion_respuestas"));
                idiomaEN.setPaqueteBabel(rs.getString("idi_paquete_babel"));
            }
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            throw new CADException(ex.getMessage());
        }

        return idiomaEN;
    }

    /**
     * Recupera la lista de idiomas que no estan configurados en
     * una asignatura
     * 
     * @param asignaturaEN AsignaturaEN donde los idiomas no estan configurados
     * @return ArrayList con la lista de idiomas que no estan configurados
     * @throws CADException Error al acceder a la base de datos        
     */    
    public ArrayList<IdiomaEN> getIdiomasSinConfigurarAsignatura(AsignaturaEN asignaturaEN) throws CADException
    {
        ArrayList<IdiomaEN> idiomas = new ArrayList<IdiomaEN>();
        IdiomaEN idiomaEN;
   
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT idi_codigo, idi_nombre, idi_traduccion_instruccion, idi_traduccion_pregunta,"
                    + " idi_traduccion_modalidad, idi_traduccion_duracion, idi_traduccion_respuestas, idi_paquete_babel "
                    + "FROM idioma WHERE idi_codigo NOT IN (SELECT aid_idioma FROM asignatura_idioma WHERE aid_asignatura = " + asignaturaEN.getCodigo() + ") ORDER BY idi_nombre DESC");
            
            while (rs.next())
            {
                idiomaEN = new IdiomaEN();
                
                idiomaEN.setCodigo(rs.getInt("idi_codigo"));
                idiomaEN.setNombre(rs.getString("idi_nombre"));
                idiomaEN.setTraduccionInstrucciones(rs.getString("idi_traduccion_instruccion"));
                idiomaEN.setTraduccionPreguntas(rs.getString("idi_traduccion_pregunta"));
                idiomaEN.setTraduccionModalidad(rs.getString("idi_traduccion_modalidad"));
                idiomaEN.setTraduccionDuracion(rs.getString("idi_traduccion_duracion"));
                idiomaEN.setTraduccionRespuestas(rs.getString("idi_traduccion_respuestas"));
                idiomaEN.setPaqueteBabel(rs.getString("idi_paquete_babel"));
                
                idiomas.add(idiomaEN);
            }
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            throw new CADException(ex.getMessage());
        }

        return idiomas;
    }
    
    /**
     * Recupera la lista de idiomas completa del sistema
     * 
     * @return ArrayList con la lista de idiomas dados de alta en el sistema
     * @throws CADException Error al acceder a la base de datos        
     */  
    public ArrayList<IdiomaEN> getIdiomas() throws CADException
    {
        ArrayList<IdiomaEN> idiomas = new ArrayList<IdiomaEN>();
        IdiomaEN idiomaEN;
   
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT idi_codigo, idi_nombre, idi_traduccion_instruccion, idi_traduccion_pregunta, "
                    + "idi_traduccion_modalidad, idi_traduccion_duracion, idi_traduccion_respuestas, idi_paquete_babel FROM idioma ORDER BY idi_nombre");
            
            while (rs.next())
            {
                idiomaEN = new IdiomaEN();
                
                idiomaEN.setCodigo(rs.getInt("idi_codigo"));
                idiomaEN.setNombre(rs.getString("idi_nombre"));
                idiomaEN.setTraduccionInstrucciones(rs.getString("idi_traduccion_instruccion"));
                idiomaEN.setTraduccionPreguntas(rs.getString("idi_traduccion_pregunta"));
                idiomaEN.setTraduccionModalidad(rs.getString("idi_traduccion_modalidad"));
                idiomaEN.setTraduccionDuracion(rs.getString("idi_traduccion_duracion"));
                idiomaEN.setTraduccionRespuestas(rs.getString("idi_traduccion_respuestas"));
                idiomaEN.setPaqueteBabel(rs.getString("idi_paquete_babel"));
                
                idiomas.add(idiomaEN);
            }
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            throw new CADException(ex.getMessage());
        }

        return idiomas;
    }
}
