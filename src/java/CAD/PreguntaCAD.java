/* 
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : PreguntaCAD
 * Created on : 15-abr-2014, 00:46:00
 * Author     : Raúl Sempere Trujillo
 */

package CAD;

import clases_generador.CADException;
import EN.AsignaturaEN;
import EN.PreguntaEN;
import clases_generador.AccesoBD;
import clases_generador.ConfigManager;
import java.sql.*;
import java.util.ArrayList;
import java.util.Calendar;

/**
 *Clase que gestiona el acceso a la base de datos de la
 * entidad Pregunta
 * 
 * @author Raúl Sempere Trujillo
 */
public class PreguntaCAD
{
    
    /**
     * Variable privada: Acceso a la base de datos
     */
    private final AccesoBD BD;
    /**
     * Variable privada estática: Cerrojo que sirve para que solo una instancia
     * de la clase pueda obtener un nuevo codigo simultáneamente
     */
    private static Boolean cerrojo = null;
    
    /**
     * Función que recibe el código de una asígnatura y devuelve
     * código de la siguiente pregunta dentro de la asignatura.
     * 
     * @param asignatura int con el código de la asignatura
     * @return int con el código de la nueva pregunta
     * @throws CADException Error en el acceso a la base de datos.
     */
    private int next(int asignatura) throws CADException
    {
        int nuevo_codigo;
        
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT pre_codigo FROM pregunta WHERE pre_asignatura=" + asignatura + " ORDER BY pre_codigo DESC");
            
            if (rs.next())
                nuevo_codigo = rs.getInt("pre_codigo") + 1;
            else
                nuevo_codigo = 1;
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            cerrojo = false;
            throw new CADException(ex.getMessage());
        }

        return nuevo_codigo;
    }
    
    /**
     * Constructor por defecto. Establece la cadena de conexión si esta no está
     * establecida y realiza una conexión con la base de datos.
     */
    public PreguntaCAD()
    {
        if (cerrojo == null)
            cerrojo = false;
        
        if (!AccesoBD.isEstablecida())
            AccesoBD.setCadenaConexion(ConfigManager.getCadenaConexion());
        
        BD = new AccesoBD();
        
    }
    
    /**
     * Inserta el objeto preguntaEN en la base de datos
     * 
     * @param preguntaEN PreguntaEN a crear en la base datos
     * @return true en caso de éxito, false si no se pudo modificar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean New(PreguntaEN preguntaEN) throws CADException
    {
        boolean insertado = true;
        String query = "";
   
        while (cerrojo)
        {
            try
            {
                Thread.sleep(5);
            }
            catch (Exception ex)
            {             
            }
        }
        
        cerrojo = true;
        
        int nuevo_codigo = next(preguntaEN.getAsignatura().getCodigo());
        
        cerrojo = false;
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            
            if (preguntaEN.getUltimoUso() != null)
            {
                query = "INSERT INTO pregunta (pre_asignatura, pre_codigo, pre_contestada, pre_aciertos, pre_no_contestada, pre_utilizada, pre_discriminacion, pre_dificultad, pre_habilitada, pre_proximo_uso, pre_ultimo_uso) VALUES ("
                    + preguntaEN.getAsignatura().getCodigo() + ", " + nuevo_codigo + ", "
                    + preguntaEN.getContestada() + ", " + preguntaEN.getAciertos() + ", " + preguntaEN.getNoContestada() + ", "
                    + preguntaEN.getUtilizada() + ", " + preguntaEN.getIndiceDiscriminacion()+ ", " + preguntaEN.getIndiceDificultad() + ", "
                    + preguntaEN.isHabilitada() + ", " + preguntaEN.isProximoUso() + ", '"
                    + preguntaEN.getUltimoUso().get(Calendar.YEAR) + "-" + (preguntaEN.getUltimoUso().get(Calendar.MONTH) + 1) + "-" + preguntaEN.getUltimoUso().get(Calendar.DAY_OF_MONTH) + "')";
            }
            else
            {
                query = "INSERT INTO pregunta (pre_asignatura, pre_codigo, pre_contestada, pre_aciertos, pre_no_contestada, pre_utilizada, pre_discriminacion, pre_dificultad, pre_habilitada, pre_proximo_uso) VALUES ("
                    + preguntaEN.getAsignatura().getCodigo() + ", " + nuevo_codigo + ", "
                    + preguntaEN.getContestada() + ", " + preguntaEN.getAciertos() + ", " + preguntaEN.getNoContestada() + ", "
                    + preguntaEN.getUtilizada()+ ", " + preguntaEN.getIndiceDiscriminacion()+ ", " + preguntaEN.getIndiceDificultad() + ", "
                    + preguntaEN.isHabilitada() + ", " + preguntaEN.isProximoUso() + ")";
            }
            
            st.execute(query); 
            
            BD.Desconectar();
            preguntaEN.setCodigo(nuevo_codigo);
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            
            //Error al insertar
            if (ex.getErrorCode() == 1062)
                insertado = false;
            //Error en el acceso
            else
                throw new CADException(ex.getMessage());
        }
        
        return insertado;
    }
    
    /**
     * Modifica el objeto preguntaEN pasado por parámetro 
     * en la base de datos con los nuevos datos que contiene
     * 
     * @param preguntaEN PreguntaEN a modificar en la base de datos
     * @return true en caso de éxito, false si no se pudo modificar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean Modify(PreguntaEN preguntaEN) throws CADException
    {
        boolean modificado = true;
        String query = "";
        
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            
            if (preguntaEN.getUltimoUso() != null)
            {
                query = "UPDATE pregunta SET "
                    + "pre_contestada = " + preguntaEN.getContestada() + ", "
                    + "pre_aciertos = " + preguntaEN.getAciertos() + ", "
                    + "pre_no_contestada = " + preguntaEN.getNoContestada() + ", "
                    + "pre_utilizada = " + preguntaEN.getUtilizada() + ", "
                    + "pre_discriminacion = " + preguntaEN.getIndiceDiscriminacion() + ", "
                    + "pre_dificultad = " + preguntaEN.getIndiceDificultad() + ", "
                    + "pre_habilitada = " + preguntaEN.isHabilitada() + ", "
                    + "pre_proximo_uso = " + preguntaEN.isProximoUso() + ", "
                    + "pre_ultimo_uso = '" + preguntaEN.getUltimoUso().get(Calendar.YEAR) + "-" + (preguntaEN.getUltimoUso().get(Calendar.MONTH) + 1) + "-" + preguntaEN.getUltimoUso().get(Calendar.DAY_OF_MONTH) + "' "
                    + "WHERE pre_asignatura = " + preguntaEN.getAsignatura().getCodigo() + " AND pre_codigo = " + preguntaEN.getCodigo();
            }
            else
            {
                query = "UPDATE pregunta SET "
                    + "pre_contestada = " + preguntaEN.getContestada() + ", "
                    + "pre_aciertos = " + preguntaEN.getAciertos() + ", "
                    + "pre_no_contestada = " + preguntaEN.getNoContestada() + ", "
                    + "pre_utilizada = " + preguntaEN.getUtilizada() + ", "
                    + "pre_discriminacion = " + preguntaEN.getIndiceDiscriminacion() + ", "
                    + "pre_dificultad = " + preguntaEN.getIndiceDificultad() + ", "
                    + "pre_habilitada = " + preguntaEN.isHabilitada() + ", "
                    + "pre_proximo_uso = " + preguntaEN.isProximoUso() + ", "
                    + "pre_ultimo_uso = null "
                    + "WHERE pre_asignatura = " + preguntaEN.getAsignatura().getCodigo() + " AND pre_codigo = " + preguntaEN.getCodigo();
            }
            
            st.execute(query); 
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            
            //Error al modificar
            if (ex.getErrorCode() == 1062)
                modificado = false;
            //Error en el acceso
            else
                throw new CADException(ex.getMessage());
        }
        
        return modificado;
    }

    /**
     * Elimina el objeto preguntaEN pasado por parámetro de
     * la base de datos
     * 
     * @param preguntaEN PreguntaEN a eliminar de la base de datos
     * @return true en caso de éxito, false si no se pudo eliminar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean Delete(PreguntaEN preguntaEN) throws CADException
    {
        boolean eliminado = true;
        String query = "";
        
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            
            query = "DELETE FROM pregunta "
               + "WHERE pre_asignatura = " + preguntaEN.getAsignatura().getCodigo() + " AND pre_codigo = " + preguntaEN.getCodigo();
            
            st.execute(query); 
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            
            //Error al eliminar
            if (ex.getErrorCode() == 1062)
                eliminado = false;
            //Error en el acceso
            else
                throw new CADException(ex.getMessage());
        }
        
        return eliminado;
    }
    
    /**
     * Obtiene una pregunta por la asignatura y su código
     * 
     * @param asignaturaEN AsignaturaEN de la pregunta a recuperar
     * @param codigo int con el código de la pregunta a recuperar
     * @return PreguntaEn recuperada
     * @throws CADException Error en el acceso a la base de datos
     */
    public PreguntaEN getPreguntaByCodigo(AsignaturaEN asignaturaEN, int codigo) throws CADException
    {
        PreguntaEN preguntaEN = null;
        
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT pre_asignatura, pre_codigo, pre_contestada, pre_aciertos, pre_no_contestada, pre_utilizada, ROUND(pre_discriminacion, 2), ROUND(pre_dificultad, 2), pre_habilitada, pre_proximo_uso, pre_ultimo_uso FROM pregunta WHERE pre_asignatura=" + asignaturaEN.getCodigo() + " AND pre_codigo=" + codigo);
            
            if (rs.next())
            {
                preguntaEN = new PreguntaEN();
                
                preguntaEN.setAsignatura(asignaturaEN);
                preguntaEN.setCodigo(rs.getInt("pre_codigo"));
                preguntaEN.setContestada(rs.getInt("pre_contestada"));
                preguntaEN.setAciertos(rs.getInt("pre_aciertos"));
                preguntaEN.setNoContestada(rs.getInt("pre_no_contestada"));
                preguntaEN.setUtilizada(rs.getInt("pre_utilizada"));
                preguntaEN.setIndiceDificultad(rs.getDouble("ROUND(pre_dificultad, 2)"));
                preguntaEN.setIndiceDiscriminacion(rs.getDouble("ROUND(pre_discriminacion, 2)"));
                preguntaEN.setHabilitada(rs.getBoolean("pre_habilitada"));
                preguntaEN.setProximoUso(rs.getBoolean("pre_proximo_uso"));
                if (rs.getDate("pre_ultimo_uso") != null)
                {
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(rs.getDate("pre_ultimo_uso"));
                    preguntaEN.setUltimoUso(cal);
                }
            }
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            throw new CADException(ex.getMessage());
        }

        return preguntaEN;
    }
    
    /**
     * Obtiene todas las preguntas de la asignatura pasada por parámetro
     * 
     * @param asignaturaEN AsignaturaEN de la que se recuperarán las preguntas
     * @return ArrayList con la lista de preguntas de la asignatura
     * @throws CADException Error en el acceso a la base de datos
     */
    public ArrayList<PreguntaEN> getPreguntasByAsignatura(AsignaturaEN asignaturaEN) throws CADException
    {
        ArrayList<PreguntaEN> preguntas = new ArrayList<PreguntaEN>();
        PreguntaEN preguntaEN;
        
        
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT pre_asignatura, pre_codigo, pre_contestada, pre_aciertos, pre_no_contestada, pre_utilizada, ROUND(pre_discriminacion, 2), ROUND(pre_dificultad, 2), pre_habilitada, pre_proximo_uso, pre_ultimo_uso FROM pregunta WHERE pre_asignatura=" + asignaturaEN.getCodigo() + " ORDER BY pre_codigo DESC");
            
            while (rs.next())
            {
                preguntaEN = new PreguntaEN();
                
                preguntaEN.setAsignatura(asignaturaEN);
                preguntaEN.setCodigo(rs.getInt("pre_codigo"));
                preguntaEN.setContestada(rs.getInt("pre_contestada"));
                preguntaEN.setAciertos(rs.getInt("pre_aciertos"));
                preguntaEN.setNoContestada(rs.getInt("pre_no_contestada"));
                preguntaEN.setUtilizada(rs.getInt("pre_utilizada"));
                preguntaEN.setIndiceDificultad(rs.getDouble("ROUND(pre_dificultad, 2)"));
                preguntaEN.setIndiceDiscriminacion(rs.getDouble("ROUND(pre_discriminacion, 2)"));
                preguntaEN.setHabilitada(rs.getBoolean("pre_habilitada"));
                preguntaEN.setProximoUso(rs.getBoolean("pre_proximo_uso"));
                if (rs.getDate("pre_ultimo_uso") != null)
                {
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(rs.getDate("pre_ultimo_uso"));
                    preguntaEN.setUltimoUso(cal);
                }
                
                preguntas.add(preguntaEN);
            }
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            throw new CADException(ex.getMessage());
        }

        return preguntas;
    }
    
    /**
     * Obtiene solo las preguntas sin clasificar de la asignatura
     * pasada por parámetro
     * 
     * @param asignaturaEN AsignaturaEN de la que se recuperarán las preguntas
     * @return ArrayList con la lista de preguntas de la asignatura
     * @throws CADException Error en el acceso a la base de datos
     */
    public ArrayList<PreguntaEN> getPreguntasSinClasificarByAsignatura(AsignaturaEN asignaturaEN) throws CADException
    {
        ArrayList<PreguntaEN> preguntas = new ArrayList<PreguntaEN>();
        PreguntaEN preguntaEN;
        
        
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT pre_asignatura, pre_codigo, pre_contestada, pre_aciertos, pre_no_contestada, pre_utilizada, ROUND(pre_discriminacion, 2), ROUND(pre_dificultad, 2), pre_habilitada, pre_proximo_uso, pre_ultimo_uso "
                    + "FROM pregunta LEFT OUTER JOIN clasificacion_tema ON pre_codigo = cla_pregunta WHERE pre_asignatura = " + asignaturaEN.getCodigo() + " AND cla_tema is null");
            
            while (rs.next())
            {
                preguntaEN = new PreguntaEN();
                
                preguntaEN.setAsignatura(asignaturaEN);
                preguntaEN.setCodigo(rs.getInt("pre_codigo"));
                preguntaEN.setContestada(rs.getInt("pre_contestada"));
                preguntaEN.setAciertos(rs.getInt("pre_aciertos"));
                preguntaEN.setNoContestada(rs.getInt("pre_no_contestada"));
                preguntaEN.setUtilizada(rs.getInt("pre_utilizada"));
                preguntaEN.setIndiceDificultad(rs.getDouble("ROUND(pre_dificultad, 2)"));
                preguntaEN.setIndiceDiscriminacion(rs.getDouble("ROUND(pre_discriminacion, 2)"));
                preguntaEN.setHabilitada(rs.getBoolean("pre_habilitada"));
                preguntaEN.setProximoUso(rs.getBoolean("pre_proximo_uso"));
                if (rs.getDate("pre_ultimo_uso") != null)
                {
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(rs.getDate("pre_ultimo_uso"));
                    preguntaEN.setUltimoUso(cal);
                }
                
                preguntas.add(preguntaEN);
            }
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            throw new CADException(ex.getMessage());
        }

        return preguntas;
    }     
            
    /**
     * Obtiene solo las preguntas habilitadas de la asignatura 
     * pasada por parámetro
     * 
     * @param asignaturaEN AsignaturaEN de la que se recuperarán las preguntas
     * @return ArrayList con la lista de preguntas de la asignatura
     * @throws CADException Error en el acceso a la base de datos
     */
    public ArrayList<PreguntaEN> getPreguntasHabilitadasByAsignatura(AsignaturaEN asignaturaEN) throws CADException
    {
        ArrayList<PreguntaEN> preguntas = new ArrayList<PreguntaEN>();
        PreguntaEN preguntaEN;
        
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            String query = "SELECT pre_asignatura, pre_codigo, pre_contestada, pre_aciertos, pre_no_contestada, pre_utilizada, ROUND(pre_discriminacion, 2), ROUND(pre_dificultad, 2), pre_habilitada, pre_proximo_uso, pre_ultimo_uso "
                    + "FROM pregunta WHERE pre_asignatura=" + asignaturaEN.getCodigo()
                    + " AND pre_habilitada = true ORDER BY pre_codigo DESC";
            
            ResultSet rs = st.executeQuery(query);
            while (rs.next())
            {
                preguntaEN = new PreguntaEN();
                
                preguntaEN.setAsignatura(asignaturaEN);
                preguntaEN.setCodigo(rs.getInt("pre_codigo"));
                preguntaEN.setContestada(rs.getInt("pre_contestada"));
                preguntaEN.setAciertos(rs.getInt("pre_aciertos"));
                preguntaEN.setNoContestada(rs.getInt("pre_no_contestada"));
                preguntaEN.setUtilizada(rs.getInt("pre_utilizada"));
                preguntaEN.setIndiceDificultad(rs.getDouble("ROUND(pre_dificultad, 2)"));
                preguntaEN.setIndiceDiscriminacion(rs.getDouble("ROUND(pre_discriminacion, 2)"));
                preguntaEN.setHabilitada(rs.getBoolean("pre_habilitada"));
                preguntaEN.setProximoUso(rs.getBoolean("pre_proximo_uso"));
                if (rs.getDate("pre_ultimo_uso") != null)
                {
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(rs.getDate("pre_ultimo_uso"));
                    preguntaEN.setUltimoUso(cal);
                }
                
                preguntas.add(preguntaEN);
            }
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            throw new CADException(ex.getMessage());
        }

        return preguntas;
    }

    /**
     * Obtiene las preguntas habilitadas filtradas por fecha de último uso
     * de la asignatura pasada como parámentro
     * 
     * @param asignaturaEN AsignaturaEN de la que se recuperarán las preguntas
     * @param ultimoUso fecha de último uso máximo con el que se filtraran
     * las preguntas
     * @return ArrayList con la lista de preguntas de la asignatura
     * @throws CADException Error en el acceso a la base de datos
     */
    public ArrayList<PreguntaEN> getPreguntasHabilitadasByAsignaturaYUltimoUso(AsignaturaEN asignaturaEN, Calendar ultimoUso) throws CADException
    {
        ArrayList<PreguntaEN> preguntas = new ArrayList<PreguntaEN>();
        PreguntaEN preguntaEN;
        
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            String query = "SELECT pre_asignatura, pre_codigo, pre_contestada, pre_aciertos, pre_no_contestada, pre_utilizada, ROUND(pre_discriminacion, 2), ROUND(pre_dificultad, 2), pre_habilitada, pre_proximo_uso, pre_ultimo_uso "
                    + "FROM pregunta WHERE pre_asignatura=" + asignaturaEN.getCodigo()  + " AND "
                    + "pre_ultimo_uso <= '" + ultimoUso.get(Calendar.YEAR) + "-" + (ultimoUso.get(Calendar.MONTH) - 1) + "-" + ultimoUso.get(Calendar.DAY_OF_MONTH) + "' "
                    + "AND pre_habilitada = true ORDER BY pre_codigo DESC";
            
            ResultSet rs = st.executeQuery(query);
            while (rs.next())
            {
                preguntaEN = new PreguntaEN();
                
                preguntaEN.setAsignatura(asignaturaEN);
                preguntaEN.setCodigo(rs.getInt("pre_codigo"));
                preguntaEN.setContestada(rs.getInt("pre_contestada"));
                preguntaEN.setAciertos(rs.getInt("pre_aciertos"));
                preguntaEN.setNoContestada(rs.getInt("pre_no_contestada"));
                preguntaEN.setUtilizada(rs.getInt("pre_utilizada"));
                preguntaEN.setIndiceDificultad(rs.getDouble("ROUND(pre_dificultad, 2)"));
                preguntaEN.setIndiceDiscriminacion(rs.getDouble("ROUND(pre_discriminacion, 2)"));
                preguntaEN.setHabilitada(rs.getBoolean("pre_habilitada"));
                preguntaEN.setProximoUso(rs.getBoolean("pre_proximo_uso"));
                if (rs.getDate("pre_ultimo_uso") != null)
                {
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(rs.getDate("pre_ultimo_uso"));
                    preguntaEN.setUltimoUso(cal);
                }
                
                preguntas.add(preguntaEN);
            }
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            throw new CADException(ex.getMessage());
        }

        return preguntas;
    }
     
    /**
     * Obtiene las preguntas de una asignatura pasada por parametro que cumplan 
     * el fltro pasado por parámentro
     * 
     * @param asignaturaEN AsignaturaEN de la que se recuperarán las preguntas
     * @param filtro String del WHERE en formato SQL para filtrar las preguntas
     * @return ArrayList con la lista de preguntas de la asignatura
     * @throws CADException Error en el acceso a la base de datos
     */
    public ArrayList<PreguntaEN> getPreguntasByAsignaturaYFiltro(AsignaturaEN asignaturaEN, String filtro) throws CADException
    {
        ArrayList<PreguntaEN> preguntas = new ArrayList<PreguntaEN>();
        PreguntaEN preguntaEN;
        
        
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            
            String query = "SELECT pre_asignatura, pre_codigo, pre_contestada, pre_aciertos, pre_no_contestada, pre_utilizada, ROUND(pre_discriminacion, 2), ROUND(pre_dificultad, 2), pre_habilitada, pre_proximo_uso, pre_ultimo_uso, pid_enunciado, cla_tema, cla_revisada "
                    + "FROM pregunta_idioma, pregunta p LEFT OUTER JOIN clasificacion_tema ON cla_asignatura_pregunta = pre_asignatura AND cla_pregunta = pre_codigo "
                    + "WHERE pre_asignatura=" + asignaturaEN.getCodigo() + " AND pre_asignatura = pid_asignatura_pregunta AND pre_codigo = pid_pregunta AND pid_idioma = (SELECT aid_idioma FROM asignatura_idioma WHERE aid_asignatura =" + asignaturaEN.getCodigo() + " AND aid_idioma_principal = true)";
           
            ResultSet rs = st.executeQuery(query + filtro + " ORDER BY p.pre_codigo DESC ");
            
            while (rs.next())
            {
                preguntaEN = new PreguntaEN();
                
                preguntaEN.setAsignatura(asignaturaEN);
                preguntaEN.setCodigo(rs.getInt("pre_codigo"));
                preguntaEN.setContestada(rs.getInt("pre_contestada"));
                preguntaEN.setAciertos(rs.getInt("pre_aciertos"));
                preguntaEN.setNoContestada(rs.getInt("pre_no_contestada"));
                preguntaEN.setUtilizada(rs.getInt("pre_utilizada"));
                preguntaEN.setIndiceDificultad(rs.getDouble("ROUND(pre_dificultad, 2)"));
                preguntaEN.setIndiceDiscriminacion(rs.getDouble("ROUND(pre_discriminacion, 2)"));
                preguntaEN.setHabilitada(rs.getBoolean("pre_habilitada"));
                preguntaEN.setProximoUso(rs.getBoolean("pre_proximo_uso"));
                if (rs.getDate("pre_ultimo_uso") != null)
                {
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(rs.getDate("pre_ultimo_uso"));
                    preguntaEN.setUltimoUso(cal);
                }
                
                preguntas.add(preguntaEN);
            }
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            throw new CADException(ex.getMessage());
        }

        return preguntas;
    }
        
    /**
     * Obtiene las preguntas marcadas con "utilizar en el próximo examen
     * de una asignatura pasada por parametro
     * 
     * @param asignaturaEN AsignaturaEN de la que se recuperarán las preguntas
     * @return ArrayList con la lista de preguntas de la asignatura
     * @throws CADException Error en el acceso a la base de datos
     */
    public ArrayList<PreguntaEN> getPreguntasProximoExamenByAsignatura(AsignaturaEN asignaturaEN) throws CADException
    {
        ArrayList<PreguntaEN> preguntas = new ArrayList<PreguntaEN>();
        PreguntaEN preguntaEN;
        
        
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT pre_asignatura, pre_codigo, pre_contestada, pre_aciertos, pre_no_contestada, pre_utilizada, ROUND(pre_discriminacion, 2), ROUND(pre_dificultad, 2), pre_habilitada, pre_proximo_uso, pre_ultimo_uso FROM pregunta WHERE pre_asignatura=" + asignaturaEN.getCodigo() + " AND pre_proximo_uso = true");
            
            while (rs.next())
            {
                preguntaEN = new PreguntaEN();
                
                preguntaEN.setAsignatura(asignaturaEN);
                preguntaEN.setCodigo(rs.getInt("pre_codigo"));
                preguntaEN.setContestada(rs.getInt("pre_contestada"));
                preguntaEN.setAciertos(rs.getInt("pre_aciertos"));
                preguntaEN.setNoContestada(rs.getInt("pre_no_contestada"));
                preguntaEN.setUtilizada(rs.getInt("pre_utilizada"));
                preguntaEN.setIndiceDificultad(rs.getDouble("ROUND(pre_dificultad, 2)"));
                preguntaEN.setIndiceDiscriminacion(rs.getDouble("ROUND(pre_discriminacion, 2)"));
                preguntaEN.setHabilitada(rs.getBoolean("pre_habilitada"));
                preguntaEN.setProximoUso(rs.getBoolean("pre_proximo_uso"));
                if (rs.getDate("pre_ultimo_uso") != null)
                {
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(rs.getDate("pre_ultimo_uso"));
                    preguntaEN.setUltimoUso(cal);
                }
                
                preguntas.add(preguntaEN);
            }
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            throw new CADException(ex.getMessage());
        }

        return preguntas;
    }
}
