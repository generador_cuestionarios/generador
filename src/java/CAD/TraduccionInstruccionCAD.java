/* 
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : TraduccionInstruccionCAD
 * Created on : 15-abr-2014, 00:46:00
 * Author     : Raúl Sempere Trujillo
 */

package CAD;

import clases_generador.CADException;
import CEN.IdiomaCEN;
import EN.ExamenEN;
import EN.IdiomaEN;
import EN.InstruccionEN;
import EN.TraduccionInstruccionEN;
import clases_generador.AccesoBD;
import clases_generador.ConfigManager;
import clases_generador.Utiles;
import java.sql.*;
import java.util.ArrayList;

/**
 * Clase que gestiona el acceso a la base de datos de la
 * entidad TraduccionInstruccion
 * 
 * @author Raúl Sempere Trujillo
 */
public class TraduccionInstruccionCAD
{
    
    /**
     * Variable privada: Acceso a la base de datos
     */
    private final AccesoBD BD;
    
    /**
     * Constructor por defecto. Establece la cadena de conexión si esta no está
     * establecida y realiza una conexión con la base de datos.
     */
    public TraduccionInstruccionCAD()
    {
        if (!AccesoBD.isEstablecida())
            AccesoBD.setCadenaConexion(ConfigManager.getCadenaConexion());
        
        BD = new AccesoBD();
    }
    
    /**
     * Inserta el objeto traduccionInstruccionEN en la base de datos
     * 
     * @param traduccionInstruccionEN TraduccionInstruccionEN a crear en la base datos
     * @return true en caso de éxito, false si no se pudo modificar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean New(TraduccionInstruccionEN traduccionInstruccionEN) throws CADException
    {
        boolean insertado = true;
        
        String query = "";
   
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            query = "INSERT INTO instruccion_idioma (ini_asignatura_examen_instruccion, ini_examen_instruccion, ini_instruccion, ini_idioma, ini_enunciado) VALUES ("
                    + traduccionInstruccionEN.getInstruccion().getExamen().getAsignatura().getCodigo() + ", " + traduccionInstruccionEN.getInstruccion().getExamen().getCodigo() + ", "
                    + traduccionInstruccionEN.getInstruccion().getCodigo() + ", " + traduccionInstruccionEN.getIdioma().getCodigo() + ", '"
                    + Utiles.StringBD(traduccionInstruccionEN.getEnunciado()) + "')";
            
            st.execute(query); 
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            
            //Error al insertar
            if (ex.getErrorCode() == 1062)
                insertado = false;
            //Error en el acceso
            else
                throw new CADException(query);
        }
        
        return insertado;
    }
    
    /**
     * Modifica el objeto traduccionInstruccionEN pasado por parámetro 
     * en la base de datos con los nuevos datos que contiene
     * 
     * @param traduccionInstruccionEN TraduccionInstruccionEN a modificar en la base de datos
     * @return true en caso de éxito, false si no se pudo modificar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean Modify(TraduccionInstruccionEN traduccionInstruccionEN) throws CADException
    {
        boolean modificado = true;
        
        String query = "";
   
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();

            query = "UPDATE instruccion_idioma SET "
                    + "ini_enunciado = '" + Utiles.StringBD(traduccionInstruccionEN.getEnunciado()) + "' "
                    + "WHERE ini_asignatura_examen_instruccion = " + traduccionInstruccionEN.getInstruccion().getExamen().getAsignatura().getCodigo() + " AND ini_examen_instruccion = " + traduccionInstruccionEN.getInstruccion().getExamen().getCodigo() + " AND ini_instruccion = " + traduccionInstruccionEN.getInstruccion().getCodigo() + " AND ini_idioma = " + traduccionInstruccionEN.getIdioma().getCodigo();
            
            st.execute(query); 
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            
            //Error al modificar
            if (ex.getErrorCode() == 1062)
                modificado = false;
            //Error en el acceso
            else
                throw new CADException(query);
        }
        
        return modificado;
    }

    /**
     * Elimina el objeto traduccionInstruccionEN pasado por parámetro de
     * la base de datos
     * 
     * @param traduccionInstruccionEN TraduccionInstruccionEN a eliminar de la base de datos
     * @return true en caso de éxito, false si no se pudo eliminar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean Delete(TraduccionInstruccionEN traduccionInstruccionEN) throws CADException
    {
        boolean modificado = true;
        
        String query = "";
   
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();

            query = "DELETE FROM instruccion_idioma "
                    + "WHERE ini_asignatura_examen_instruccion = " + traduccionInstruccionEN.getInstruccion().getExamen().getAsignatura().getCodigo() + " AND ini_examen_instruccion = " + traduccionInstruccionEN.getInstruccion().getExamen().getCodigo() + " AND ini_instruccion = " + traduccionInstruccionEN.getInstruccion().getCodigo() + " AND ini_idioma = " + traduccionInstruccionEN.getIdioma().getCodigo();
            
            st.execute(query); 
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            
            //Error al modificar
            if (ex.getErrorCode() == 1062)
                modificado = false;
            //Error en el acceso
            else
                throw new CADException(query);
        }
        
        return modificado;
    }
    
    /**
     * Obiene todas las traducciones de una instrucción
     * 
     * @param instruccionEN InstruccionEN de la que se obtendrán las traducciones
     * @return ArrayList con la lista de traducciones
     * @throws CADException Error en el acceso a la base de datos
     */
    public ArrayList<TraduccionInstruccionEN> getTraduccionesInstruccionByInstruccion(InstruccionEN instruccionEN) throws CADException
    {
        ArrayList<TraduccionInstruccionEN> traducciones = new ArrayList<TraduccionInstruccionEN>();
        TraduccionInstruccionEN traduccionInstruccionEN;
        IdiomaCEN idiomaCEN = new IdiomaCEN();
        
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT ini_asignatura_examen_instruccion, ini_examen_instruccion, ini_instruccion, ini_idioma, ini_enunciado FROM instruccion_idioma "
                    + "WHERE ini_asignatura_examen_instruccion=" + instruccionEN.getExamen().getAsignatura().getCodigo() + " AND ini_examen_instruccion="
                    + instruccionEN.getExamen().getCodigo() + " AND ini_instruccion=" + instruccionEN.getCodigo());
            
            while (rs.next())
            {
                traduccionInstruccionEN = new TraduccionInstruccionEN();
                
                traduccionInstruccionEN.setInstruccion(instruccionEN);
                traduccionInstruccionEN.setIdioma(idiomaCEN.getIdiomaByCodigo(rs.getInt("ini_idioma")));
                traduccionInstruccionEN.setEnunciado(rs.getString("ini_enunciado"));
                
                traducciones.add(traduccionInstruccionEN);
            }
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            throw new CADException(ex.getMessage());
        }

        return traducciones;
    }
    
    /**
     * Obtiene las traducciones en un idioma pasado por parámetro de todas
     * las instrucciones de un examen
     * 
     * @param examenEN ExamenEN del que se obtendrán las instrucciones
     * @param idiomaEN IdiomaEN idioma de la traducción de las instrucciones
     * @return ArrayList con la traduccion de las instrucciones
     * @throws CADException Error en el acceso a la base de datos
     */
    public ArrayList<TraduccionInstruccionEN> getTraduccionesInstruccionByExamenEIdioma(ExamenEN examenEN, IdiomaEN idiomaEN) throws CADException
    {
        ArrayList<TraduccionInstruccionEN> traducciones = new ArrayList<TraduccionInstruccionEN>();
        TraduccionInstruccionEN traduccionInstruccionEN;
        InstruccionEN instruccionEN;
        
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT ini_asignatura_examen_instruccion, ini_examen_instruccion, ini_instruccion, ini_idioma, ini_enunciado FROM instruccion_idioma "
                    + "WHERE ini_asignatura_examen_instruccion=" + examenEN.getAsignatura().getCodigo() + " AND ini_examen_instruccion="
                    + examenEN.getCodigo() + " AND ini_idioma=" + idiomaEN.getCodigo() + " ORDER BY ini_instruccion");
            
            while (rs.next())
            {
                instruccionEN = new InstruccionEN();
                instruccionEN.setCodigo(Integer.parseInt(rs.getString("ini_instruccion")));
                instruccionEN.setExamen(examenEN);
                
                traduccionInstruccionEN = new TraduccionInstruccionEN();
                
                traduccionInstruccionEN.setInstruccion(instruccionEN);
                traduccionInstruccionEN.setIdioma(idiomaEN);
                traduccionInstruccionEN.setEnunciado(rs.getString("ini_enunciado"));
                
                traducciones.add(traduccionInstruccionEN);
            }
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            throw new CADException(ex.getMessage());
        }

        return traducciones;
    }

    /**
     * Obtiene la traducción de una instrucción en un idioma concreto
     * 
     * @param instruccionEN InstruccionEN de la que se obtendrá la traducción
     * @param idiomaEN IdiomaEN de la traducción    
     * @return TraduccionInstruccionEN con la traducción de la instrucción
     * @throws CADException Error en el acceso a la base de datos
     */
    public TraduccionInstruccionEN getTraduccionInstruccionByInstruccionEIdioma(InstruccionEN instruccionEN, IdiomaEN idiomaEN) throws CADException
    {
        TraduccionInstruccionEN traduccionInstruccionEN = null;
        
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT ini_asignatura_examen_instruccion, ini_examen_instruccion, ini_instruccion, ini_idioma, ini_enunciado "
                    + "FROM instruccion_idioma WHERE ini_asignatura_examen_instruccion=" + instruccionEN.getExamen().getAsignatura().getCodigo() + " AND ini_examen_instruccion=" 
                    + instruccionEN.getExamen().getCodigo() + " AND ini_instruccion=" + instruccionEN.getCodigo() + " AND ini_idioma=" + idiomaEN.getCodigo());
            
            if (rs.next())
            {
                traduccionInstruccionEN = new TraduccionInstruccionEN();
                
                traduccionInstruccionEN.setInstruccion(instruccionEN);
                traduccionInstruccionEN.setIdioma(idiomaEN);
                traduccionInstruccionEN.setEnunciado(rs.getString("ini_enunciado"));
   
            }
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            throw new CADException(ex.getMessage());
        }

        return traduccionInstruccionEN;
    }    
    
}
