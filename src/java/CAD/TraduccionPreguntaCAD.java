/* 
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : TraduccionPreguntaCAD
 * Created on : 15-abr-2014, 00:46:00
 * Author     : Raúl Sempere Trujillo
 */

package CAD;

import clases_generador.CADException;
import CEN.IdiomaCEN;
import EN.IdiomaEN;
import EN.PreguntaEN;
import EN.TraduccionPreguntaEN;
import clases_generador.AccesoBD;
import clases_generador.ConfigManager;
import clases_generador.Utiles;
import java.sql.*;
import java.util.ArrayList;

/**
 * Clase que gestiona el acceso a la base de datos de la
 * entidad TraduccionPregunta
 * 
 * @author Raúl Sempere Trujillo
 */
public class TraduccionPreguntaCAD
{
    
    /**
     * Variable privada: Acceso a la base de datos
     */
    private final AccesoBD BD;
    
    /**
     * Constructor por defecto. Establece la cadena de conexión si esta no está
     * establecida y realiza una conexión con la base de datos.
     */
    public TraduccionPreguntaCAD()
    {
        if (!AccesoBD.isEstablecida())
            AccesoBD.setCadenaConexion(ConfigManager.getCadenaConexion());
        
        BD = new AccesoBD();
    }

    /**
     * Inserta el objeto traduccionPreguntaEN en la base de datos
     * 
     * @param traduccionPreguntaEN TraduccionPreguntaEN a crear en la base datos
     * @return true en caso de éxito, false si no se pudo modificar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean New(TraduccionPreguntaEN traduccionPreguntaEN) throws CADException
    {
        boolean insertado = true;
        
        String query = "";
   
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            query = "INSERT INTO pregunta_idioma (pid_asignatura_pregunta, pid_pregunta, pid_idioma, pid_revisada, pid_enunciado) VALUES ("
                    + traduccionPreguntaEN.getPregunta().getAsignatura().getCodigo() + ", " + traduccionPreguntaEN.getPregunta().getCodigo() + ", "
                    + traduccionPreguntaEN.getIdioma().getCodigo() + ", " + traduccionPreguntaEN.isRevisada() + ", '"
                    + Utiles.StringBD(traduccionPreguntaEN.getEnunciado()) + "')";
            
            st.execute(query); 
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            
            //Error al insertar
            if (ex.getErrorCode() == 1062)
                insertado = false;
            //Error en el acceso
            else
                throw new CADException(query);
        }
        
        return insertado;
    }
    
    /**
     * Modifica el objeto traduccionPreguntaEN pasado por parámetro 
     * en la base de datos con los nuevos datos que contiene
     * 
     * @param traduccionPreguntaEN TraduccionPreguntaEN a modificar en la base de datos
     * @return true en caso de éxito, false si no se pudo modificar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean Modify(TraduccionPreguntaEN traduccionPreguntaEN) throws CADException
    {
        boolean modificado = true;
        
        String query = "";
   
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            query = "UPDATE pregunta_idioma SET "
                    + "pid_revisada = " + traduccionPreguntaEN.isRevisada() + ", "
                    + "pid_enunciado = '" + Utiles.StringBD(traduccionPreguntaEN.getEnunciado()) + "' "
                    + "WHERE pid_asignatura_pregunta = " + traduccionPreguntaEN.getPregunta().getAsignatura().getCodigo() + " AND pid_pregunta = " + traduccionPreguntaEN.getPregunta().getCodigo() + " AND pid_idioma = " + traduccionPreguntaEN.getIdioma().getCodigo();
            
            st.execute(query); 
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            
            //Error al modificar
            if (ex.getErrorCode() == 1062)
                modificado = false;
            //Error en el acceso
            else
                throw new CADException(query);
        }
        
        return modificado;
    }
    
    /**
     * Elimina el objeto traduccionPreguntaEN pasado por parámetro de
     * la base de datos
     * 
     * @param traduccionPreguntaEN TraduccionPreguntaEN a eliminar de la base de datos
     * @return true en caso de éxito, false si no se pudo eliminar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean Delete(TraduccionPreguntaEN traduccionPreguntaEN) throws CADException
    {
        boolean eliminado = true;
        
        String query = "";
   
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            query = "DELETE FROM pregunta_idioma "
                    + "WHERE pid_asignatura_pregunta = " + traduccionPreguntaEN.getPregunta().getAsignatura().getCodigo() + " AND pid_pregunta = " + traduccionPreguntaEN.getPregunta().getCodigo() + " AND pid_idioma = " + traduccionPreguntaEN.getIdioma().getCodigo();
            
            st.execute(query); 
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            
            //Error al eliminar
            if (ex.getErrorCode() == 1062)
                eliminado = false;
            //Error en el acceso
            else
                throw new CADException(query);
        }
        
        return eliminado;
    }
    
    /**
     * Obtiene todas las traducciones de una pregunta
     * 
     * @param preguntaEN PreguntaEN de la que se quieren recuperar las
     * traducciones
     * @return ArrayList con la lista de traducciones de la pregunta
     * @throws CADException Error en el acceso a la base de datos
     */
    public ArrayList<TraduccionPreguntaEN> getTraduccionesPreguntaByPregunta(PreguntaEN preguntaEN) throws CADException
    {
        ArrayList<TraduccionPreguntaEN> traducciones = new ArrayList<TraduccionPreguntaEN>();
        TraduccionPreguntaEN traduccionPreguntaEN;
        IdiomaCEN idiomaCEN = new IdiomaCEN();
        
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT pid_asignatura_pregunta, pid_pregunta, pid_idioma, pid_revisada, pid_enunciado FROM pregunta_idioma WHERE pid_asignatura_pregunta=" + preguntaEN.getAsignatura().getCodigo() + " AND pid_pregunta=" + preguntaEN.getCodigo());
            
            while (rs.next())
            {
                traduccionPreguntaEN = new TraduccionPreguntaEN();
                
                traduccionPreguntaEN.setPregunta(preguntaEN);
                traduccionPreguntaEN.setIdioma(idiomaCEN.getIdiomaByCodigo(rs.getInt("pid_idioma")));
                traduccionPreguntaEN.setRevisada(rs.getBoolean("pid_revisada"));
                traduccionPreguntaEN.setEnunciado(rs.getString("pid_enunciado"));
                
                traducciones.add(traduccionPreguntaEN);
            }
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            throw new CADException(ex.getMessage());
        }

        return traducciones;
    }

    /**
     * Obtiene una traducción concreta de una pregunta
     * 
     * @param preguntaEN preguntaEN de la cuál obtendremos la traducción
     * @param idiomaEN IdiomaEN de la traducción
     * @return TraduccionPreguntaEN con la traducción de la pregunta
     * @throws CADException Error en el acceso a la base de datos
     */
    public TraduccionPreguntaEN getTraduccionPreguntaByPreguntaEIdioma(PreguntaEN preguntaEN, IdiomaEN idiomaEN) throws CADException
    {
        TraduccionPreguntaEN traduccionPreguntaEN = null;
        
        try
        {
            BD.Conectar();
            Statement st = BD.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT pid_asignatura_pregunta, pid_pregunta, pid_idioma, pid_revisada, pid_enunciado FROM pregunta_idioma WHERE pid_asignatura_pregunta=" + preguntaEN.getAsignatura().getCodigo() + " AND pid_pregunta=" + preguntaEN.getCodigo() + " AND pid_idioma=" + idiomaEN.getCodigo());
            
            if (rs.next())
            {
                traduccionPreguntaEN = new TraduccionPreguntaEN();
                
                traduccionPreguntaEN.setPregunta(preguntaEN);
                traduccionPreguntaEN.setIdioma(idiomaEN);
                traduccionPreguntaEN.setRevisada(rs.getBoolean("pid_revisada"));
                traduccionPreguntaEN.setEnunciado(rs.getString("pid_enunciado"));
   
            }
            
            BD.Desconectar();
        }
        catch (SQLException ex)
        {
            BD.Desconectar();
            throw new CADException(ex.getMessage());
        }

        return traduccionPreguntaEN;
    }    
    
}
