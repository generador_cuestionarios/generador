/*
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.

 * Document   : package-info
 * Created on : 04-ago-2014, 18:45:00
 * Author     : Raúl Sempere Trujillo
 */

/**
 * Contiene todas las clases CEN de la aplicación, es decir, clases
 * de alto nivel que son utilizadas directamente por el programador
 * para manejar las diferentes entidades.
 * 
 * @author Raúl Sempere Trujillo
 * @version 1.0
 */
package CEN;