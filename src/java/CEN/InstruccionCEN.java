/* 
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : InstruccionCEN
 * Created on : 15-abr-2014, 00:46:00
 * Author     : Raúl Sempere Trujillo
 */
package CEN;

import clases_generador.CADException;
import CAD.InstruccionCAD;
import EN.ExamenEN;
import EN.InstruccionEN;
import EN.TraduccionInstruccionEN;
import java.util.ArrayList;

/**
 * Capa de Entidad de Negocio de la entidad InstruccionCEN
 * 
 * @author Raúl Sempere Trujillo
 */
public class InstruccionCEN
{
    
    /**
     * Variable privada: Clase CAD de la entidad instruccion
     */
    private final InstruccionCAD instruccionCAD;
    
    /**
     * Constructor por defecto. Instancia el CAD para que gestione el acceso 
     * a la base de datos.
     */
    public InstruccionCEN()
    {
        instruccionCAD = new InstruccionCAD();
    }
    
    /**
     * Utiliza la capa CAD para insertar la instruccionEN en la base de datos
     * 
     * @param instruccionEN InstruccionEN a crear en la base datos
     * @return true en caso de éxito, false si no se pudo modificar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean New (InstruccionEN instruccionEN) throws CADException
    {
        boolean insertado = false;
        
        try
        {
            insertado = instruccionCAD.New(instruccionEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
   
        return insertado;
    }

    /**
     * Obtiene la lista de instrucciones de un examen pasado por parámentro
     * 
     * @param examenEN ExamenEN del que se van a obtener las instrucciones
     * @return ArrayList con la lista de instrucciones.
     * @throws CADException Error en el acceso a la base de datos
     */
    public ArrayList<InstruccionEN> getInstruccionesByExamen(ExamenEN examenEN) throws CADException
    {
        ArrayList<InstruccionEN> instrucciones = new ArrayList<InstruccionEN>();
        
        try
        {
            instrucciones = instruccionCAD.getInstruccionesByExamen(examenEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
        
        return instrucciones;
    }
    
    /**
     * Carga las traduciones de la InstruccionEN pasada por parámetro
     * 
     * @param instrucionEN InstruccionEN en el que se rellenaran los datos
     * @throws CADException Si se produjo un error en el CAD  
     */
    public void rellenarTraduccionesInstruccion(InstruccionEN instrucionEN) throws CADException
    {
        ArrayList<TraduccionInstruccionEN> traducciones;
        TraduccionInstruccionCEN traduccionInstruccionCEN = new TraduccionInstruccionCEN();
        
        try
        {
            traducciones = traduccionInstruccionCEN.getTraduccionesInstruccionByInstruccion(instrucionEN);
            instrucionEN.setTraduccionesInstruccion(traducciones);
        }
        catch (CADException ex)
        {
            throw ex;
        }
    }
    
    /**
     * Elimina todas las intrucciones del examen pasado por parámetro
     * 
     * @param examenEN ExamenEN del que se borrarán las instrucciones
     * @return true en caso de éxito, false si no se pudo eliminar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean eliminarInstruccionesExamen(ExamenEN examenEN) throws CADException
    {
        boolean eliminados = false;
        
        try
        {
            eliminados = instruccionCAD.eliminarInstruccionesExamen(examenEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
        
        return eliminados;
    }
    
}
