/* 
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : ClasificacionTemaCEN
 * Created on : 15-abr-2014, 00:46:00
 * Author     : Raúl Sempere Trujillo
 */

package CEN;

import clases_generador.CADException;
import CAD.ClasificacionTemaCAD;
import EN.AsignaturaEN;
import EN.ClasificacionTemaEN;
import EN.PreguntaEN;
import EN.TemaEN;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;

/**
 * Capa de Entidad de Negocio de la entidad ClasificaciónTema
 * 
 * @author Raúl Sempere Trujillo
 */
public class ClasificacionTemaCEN
{
    
    /**
     * Variable privada: Clase CAD de la entidad ClasificacionTema
     */
    private final ClasificacionTemaCAD clasificacionTemaCAD;
    
    /**
     * Indica el porcentaje de las preguntas reclasificadas durante la 
     * reclasificación
     */
    private int porcentajeReclasificadas = 0;
    
    /**
     * Constructor por defecto. Instancia el CAD para que gestione el acceso 
     * a la base de datos.
     */
    public ClasificacionTemaCEN()
    {
        clasificacionTemaCAD = new ClasificacionTemaCAD();
    }
    
     /**
     * Utiliza la capa CAD para insertar la ClasificacionTemaEN en la base de datos
     * 
     * @param clasificacionTemaEN ClasificacionTemaEN a crear en la base datos
     * @return true en caso de éxito, false si no se pudo modificar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean New (ClasificacionTemaEN clasificacionTemaEN) throws CADException
    {
        boolean insertado = false;
        
        try
        {
            insertado = clasificacionTemaCAD.New(clasificacionTemaEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
   
        return insertado;
    }

    /**
     * Utiliza la capa CAD para modificar el objeto clasificacionTemaEN pasado
     * por parámetro en la base de datos con los nuevos datos que contiene
     * 
     * @param clasificacionTemaEN ClasificacionTemaEN a modificar en la base de datos
     * @return true en caso de éxito, false si no se pudo modificar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean Modify (ClasificacionTemaEN clasificacionTemaEN) throws CADException
    {
        boolean modificado = false;
        
        try
        {
            modificado = clasificacionTemaCAD.Modify(clasificacionTemaEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
   
        return modificado;
    }
    
    /**
     * Utiliza la capa CAD para elimina el objeto clasificacionTemaEN pasado 
     * por parámetro de la base de datos
     * 
     * @param clasificacionTemaEN ClasificacionTemaEN a eliminar de la base de datos
     * @return true en caso de éxito, false si no se pudo eliminar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean Delete (ClasificacionTemaEN clasificacionTemaEN) throws CADException
    {
        boolean eliminado = false;
        
        try
        {
            eliminado = clasificacionTemaCAD.Delete(clasificacionTemaEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
   
        return eliminado;
    }
    
    /**
     * Obtiene el número de preguntas clasificadas, revisados y sin revisar
     * en la asignatura pasada por parámetro
     * 
     * @param asignaturaEN AsignaturaEN de la que se quiere consultar el número de preguntas
     * clasificadas
     * @return int con el número de preguntas clasificadas
     * @throws CADException Error en el acceso a la base de datos
     */
    public int getCantClasificacionesByAsignatura (AsignaturaEN asignaturaEN) throws CADException
    {
        int cantidad;
        
        try
        {
            cantidad = clasificacionTemaCAD.getCantClasificacionesByAsignatura(asignaturaEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
   
        return cantidad;
    }

    /**
     * Devuelve un ArrayList con la lista de clasificaciónes para un tema
     * pasado por parámetro.
     * 
     * @param temaEN TemaEN del que queremos recuperar las clasificaciones.
     * @return ArrayList con la lista de clasificaciónes.
     * @throws CADException Error en el acceso a la base de datos
     */
    public ArrayList<ClasificacionTemaEN> getClasificacionesByTema(TemaEN temaEN) throws CADException
    {
        ArrayList<ClasificacionTemaEN> clasificaciones = null;
        
        try
        {
            clasificaciones = clasificacionTemaCAD.getClasificacionesByTema(temaEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
        
        return clasificaciones;
    }
    
    /**
     * Devuelve un ArrayList con la lista de clasificaciónes (solo las 
     * revisadas)para un tema pasado por parámetro.
     * 
     * @param temaEN TemaEN del que queremos recuperar las clasificaciones.
     * @return ArrayList con la lista de clasificaciónes.
     * @throws CADException Error en el acceso a la base de datos
     */
    public ArrayList<ClasificacionTemaEN> getClasificacionesRevisadasByTema(TemaEN temaEN) throws CADException
    {
        ArrayList<ClasificacionTemaEN> clasificaciones = null;
        
        try
        {
            clasificaciones = clasificacionTemaCAD.getClasificacionesRevisadasByTema(temaEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
        
        return clasificaciones;
    }

    /**
     * Obtiene el número de preguntas clasificadas revisados en el tema
     * pasado por parámetro
     * 
     * @param temaEN TemaEN del que se quiere consultar el número de preguntas
     * clasificadas
     * @return int con el número de preguntas clasificadas
     * @throws CADException Error en el acceso a la base de datos
     */
    public int getCantidadClasificacionesRevisadasByTema(TemaEN temaEN) throws CADException
    {
        int cantClasificaciones;
        
        try
        {
            cantClasificaciones = clasificacionTemaCAD.getCantidadClasificacionesRevisadasByTema(temaEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
        
        return cantClasificaciones;
    }

    /**
     * Obtiene la lista de preguntas clasificadas revisados en el tema
     * pasado por parámetro y que además esten habilitadas
     * 
     * @param temaEN TemaEN del que se quiere obtener el listado de preguntas
     * @return ArrayList la lista de clasificaciones
     * @throws CADException Error en el acceso a la base de datos
     */
    public ArrayList<ClasificacionTemaEN> getClasificacionesDisponiblesByTema(TemaEN temaEN) throws CADException
    {
        ArrayList<ClasificacionTemaEN> clasificaciones = null;
        
        try
        {
            clasificaciones = clasificacionTemaCAD.getClasificacionesDisponiblesByTema(temaEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
        
        return clasificaciones;
    }
    
    /**
     * Obtiene la lista de preguntas clasificadas revisados en el tema
     * pasado por parámetro, que además esten habilitadas y que además no
     * se hayan usado a partir de la fecha "ultimoUso"
     * 
     * @param temaEN TemaEN del que se quiere obtener el listado de clasificaciones
     * @param ultimoUso Calendar fecha limite de último uso de las preguntas
     * @return ArrayList la lista de preguntas
     * @throws CADException Error en el acceso a la base de datos
     */
    public ArrayList<ClasificacionTemaEN> getClasificacionesDisponiblesByTemaYUltimoUso(TemaEN temaEN, Calendar ultimoUso) throws CADException
    {
        ArrayList<ClasificacionTemaEN> clasificaciones = null;
        
        try
        {
            clasificaciones = clasificacionTemaCAD.getClasificacionesDisponiblesByTemaYUltimoUso(temaEN, ultimoUso);
        }
        catch (CADException ex)
        {
            throw ex;
        }
        
        return clasificaciones;
    }

    /**
     * Obtiene la clasificación de una pregunta pasada por parámetro
     * 
     * @param preguntaEN PreguntaEN de la que se quiere obtener la clasificación
     * @return ArrayList la lista de preguntas
     * @throws CADException Error en el acceso a la base de datos
     */
    public ClasificacionTemaEN getClasificacionByPregunta(PreguntaEN preguntaEN) throws CADException
    {
        ClasificacionTemaEN clasificacion = null;
        
        try
        {
            clasificacion = clasificacionTemaCAD.getClasificacionByPregunta(preguntaEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
        
        return clasificacion;
    }
    
    /**
     * Elimina una clasificación de una lista.
     * 
     * @param lista. ArrayList con la lista de clasificacionesTema
     * @param pregunta pregunta que se busca en la lista para eliminar la clasificación.
     * 
     * @return true si se encontró la pregunta, false en caso contrario.
     */
    public boolean borrarDeLista(ArrayList<ClasificacionTemaEN> lista, PreguntaEN pregunta)
    { 
        boolean encontrada = false;
        
        for (int i = 0; i < lista.size(); i++)
        {
            if (lista.get(i).getPregunta().getCodigo() == pregunta.getCodigo())
            {
                lista.remove(i);
                
                //Si la encontramos salimos para optimizar
                encontrada = true;
                break;
            }
        }
        
        return encontrada;
    }
    
    /**
     * Devuelve una lista de preguntas para que aparezcan en el examen.
     * 
     * @param total int. Total de preguntas a generar
     * @param temaEN TemaEN. tema del que serán las preguntas
     * @param porcentaje double. Porcentaje de pregunta a extraer del tema
     * @param porcentajeCompleto double. Cuanto porcentaje (de otros temas) se tiene
     * ya completo
     * @param fechaUltimoUso Calendar. fecha de último uso máximo para las preguntas
     * @param listaSeleccionadas ArrayList. Lista de preguntas preselecciondas por el usuario
     * @param listaGeneradas ArrayList. Lista de preguntas generadas hasta el momento
     * @return ArrayList con la lista de preguntas seleccionadas del tema pasado por
     * parametro
     * @throws Exception Si se introdujeron mal los porcentajes se genera una excepción,
     * CADException si se produce un error en la base de datos.
     */
    public ArrayList<PreguntaEN> generarPreguntasTemaExamen(int total, TemaEN temaEN, double porcentaje, double porcentajeCompleto, Calendar fechaUltimoUso, ArrayList<PreguntaEN> listaSeleccionadas, ArrayList<PreguntaEN> listaGeneradas) throws Exception
    {
        ArrayList<ClasificacionTemaEN> clasificaciones;
        ArrayList<PreguntaEN> resultado = new ArrayList<PreguntaEN>();
        int elegidas = 0;
        
        //Obtenemos la lista de clasificaciones disponibles de ese tema con la fecha de ultimo uso
        if (fechaUltimoUso != null)
            clasificaciones = this.getClasificacionesDisponiblesByTemaYUltimoUso(temaEN, fechaUltimoUso);
        else
            clasificaciones = this.getClasificacionesDisponiblesByTema(temaEN);
        
        //De esas clasificaciones hay que quitar las que ya han sido seleccionadas en el examen
        for (PreguntaEN pregunta : listaSeleccionadas)
        {
            //Si la pregunta se encontraba en la lista, como esa pregunta ha ya sido elegida y es del tema
            //tenemos que generar una pregunta menos de este tema
            if (borrarDeLista(clasificaciones, pregunta))
                elegidas++;
        }
        
        //Después de filtrar, calculamos la cantidad de preguntas que tenemos que obtener
        int cantidad = (int)Math.ceil((porcentaje*total)/100)-elegidas;
        int totalRedondeado = (int)Math.ceil(((porcentajeCompleto+porcentaje)*total)/100);
        
        //Para controlar el desfase que se crea cuando el numero de preguntas sale con decimales
        //verificamos que el porcentaje total se cumple al agregar las preguntas actuales
        if (totalRedondeado < listaGeneradas.size() + cantidad)
            cantidad--;
        
        //Ahora debemos seleccionar "cantidad" numero de preguntas aleatoriamente de la lista "clasificaciones"
        Random rand = new Random();
        int pos;
        
        //Si se supera el número permitido enviamos una excepción
        if (total < (listaSeleccionadas.size() + listaGeneradas.size() + cantidad))
            throw new Exception("Número de preguntas máximo superado.");
        else
        {
            for (int i = 0; i < cantidad; i++)
            {
                //Generamos un numero aleatorio en el rango de los elementos del array y obtenemos el elemento
                pos = rand.nextInt(clasificaciones.size());
                resultado.add(clasificaciones.get(pos).getPregunta());

                //Luego lo borramos de la lista
                clasificaciones.remove(pos);
            }
        }
        //Finalmente devolvemos la lista de las elegidas
        return resultado;
    }
    
    /**
     * Genera una cantidad de preguntas de todos los temas o sin clasificar para
     * rellenar los porcentajes elegidos por el usuario para crear un examen
     * 
     * @param total int con el total de preguntas a generar
     * @param asignaturaEN AsignaturaEN de donde se cogen las preguntas
     * @param fechaUltimoUso Calendar con la fecha de ultimo uso de las preguntas
     * @param listaSeleccionadas ArrayList lista de las preguntas que ya están seleccionadas
     * @return ArrayList con la lista de preguntas generadas
     * @throws CADException Si se produce un error al acceder a los datos.
     */
    public ArrayList<PreguntaEN> generarPreguntasRellenar(int total, AsignaturaEN asignaturaEN, Calendar fechaUltimoUso, ArrayList<PreguntaEN> listaSeleccionadas) throws CADException
    {
        PreguntaCEN preguntaCEN = new PreguntaCEN();
        ArrayList<PreguntaEN> preguntas;
        ArrayList<PreguntaEN> resultado = new ArrayList<PreguntaEN>();

         //Obtenemos la lista de preguntas disponibles con la fecha de ultimo uso
        if (fechaUltimoUso != null)
            preguntas = preguntaCEN.getPreguntasHabilitadasByAsignaturaYUltimoUso(asignaturaEN, fechaUltimoUso);
        else
            preguntas = preguntaCEN.getPreguntasHabilitadasByAsignatura(asignaturaEN);
        
        //De esas preguntas hay que quitar las que ya han sido seleccionadas o generadas para el examen
        for (PreguntaEN pregunta : listaSeleccionadas)
        {
            preguntaCEN.borrarDeLista(preguntas, pregunta);
        }
        
        //Ahora tenemos en la lista  todas las preguntas disponibles filtradas por la fecha
        //de ultimo uso y que además aún no han sido seleccionadas para este examen
        
        //De esa lista cogemos aleatoriamente para rellenar el examen
        Random rand = new Random();
        int pos;
        
        try
        {
            for (int i = 0; i < total-listaSeleccionadas.size(); i++)
            {
                //Puede darse el caso de que no tengamos más elementos en la lista para rellenar el examen
                if (preguntas.size() <= 0)
                {
                    throw new Exception();
                }
                
                //Generamos un numero aleatorio en el rango de los elementos del array y obtenemos el elemento
                pos = rand.nextInt(preguntas.size());
                resultado.add(preguntas.get(pos));

                //Luego lo borramos de la lista
                preguntas.remove(pos);
            }
        }
        catch(Exception ex)
        {
            //Ponemos la lista de resultado a nulo y lo controlamos fuera
            resultado = null;
        }
        
        //Finalmente devolvemos la lista de las elegidas
        return resultado;
    }
    
    /**
     * Reclasifica als preguntas de una asignatura pasada por parámetro
     * 
     * @param asignaturaEN AsignaturaEN donde se clasificarán las preguntas.
     * @throws CADException Si se produce un error al acceder a la base de datos.
     */
    public void reclasificarPreguntas(AsignaturaEN asignaturaEN) throws CADException
    {
        int total;
        int actual = 0;
        
        //Necesitamos la lista de preguntas sin clasificar o con clasificación sin revisar
        PreguntaCEN preguntaCEN = new PreguntaCEN();
        ArrayList<PreguntaEN> preguntas = preguntaCEN.getPreguntasSinClasificarByAsignatura(asignaturaEN);
        
        total = preguntas.size();
        
        porcentajeReclasificadas = (actual*100)/total;     

        for (PreguntaEN pregunta : preguntas)
        {
            preguntaCEN.clasificar(pregunta);
            actual++;

            porcentajeReclasificadas = (actual*100)/total;
        }
    }

    /**
     * @return the porcentajeReclasificadas
     */
    public int getPorcentajeReclasificadas(){
        return porcentajeReclasificadas;
    }
}
