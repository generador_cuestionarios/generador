/* 
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : AccesoUsuarioASignaturaCEN
 * Created on : 15-abr-2014, 00:46:00
 * Author     : Raúl Sempere Trujillo
 */

package CEN;

import CAD.AccesoUsuarioAsignaturaCAD;
import clases_generador.CADException;
import EN.AccesoUsuarioAsignaturaEN;
import EN.AsignaturaEN;
import EN.UsuarioEN;
import java.util.ArrayList;

/**
 * Capa de Entidad de Negocio de la entidad AccesoUsuarioAsignatura
 * 
 * @author Raúl Sempere Trujillo
 */
public class AccesoUsuarioAsignaturaCEN
{
    
    /**
     * Variable privada: Clase CAD de la entidad Usuario
     */
    private final AccesoUsuarioAsignaturaCAD accesoUsuarioAsignaturaCAD;
    
    /**
     * Constructor por defecto. Instancia el AccesoUsuarioAsignaturaCAD para
     * que gestione el acceso a la base de datos.
     */
    public AccesoUsuarioAsignaturaCEN()
    {
        accesoUsuarioAsignaturaCAD = new AccesoUsuarioAsignaturaCAD();
    }
    
    /**
     * Utiliza la capa CAD para insertar el objeto accesoUsuarioAsignaturaEN 
     * en la base de datos
     * 
     * @param accesoUsuarioAsignaturaEN AccesoUsuarioAsignaturaEN a crear en la base datos
     * @return true en caso de éxito, false si no se pudo modificar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean New (AccesoUsuarioAsignaturaEN accesoUsuarioAsignaturaEN) throws CADException
    {
        boolean insertado = false;
        
        try
        {
            insertado = accesoUsuarioAsignaturaCAD.New(accesoUsuarioAsignaturaEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
   
        return insertado;
    }
    
    /**
     * Utiliza la capa CAD para modificar el objeto accesoUsuarioAsignaturaEN 
     * pasado por parámetro en la base de datos con los nuevos datos que contiene
     * 
     * @param accesoUsuarioAsignaturaEN AccesoUsuarioAsignaturaEN a modificar en la base de datos
     * @return true en caso de éxito, false si no se pudo modificar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean Modify (AccesoUsuarioAsignaturaEN accesoUsuarioAsignaturaEN) throws CADException
    {
        boolean modificado = false;
        
        try
        {
            modificado = accesoUsuarioAsignaturaCAD.Modify(accesoUsuarioAsignaturaEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
   
        return modificado;
    }
    
    /**
     * Utiliza la capa CAD para eliminar el objeto accesoUsuarioAsignaturaEN 
     * pasado por parámetro de la base de datos
     * 
     * @param accesoUsuarioAsignaturaEN AccesoUsuarioAsignaturaEN a eliminar
     * de la base de datos
     * @return true en caso de éxito, false si no se pudo eliminar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean Delete (AccesoUsuarioAsignaturaEN accesoUsuarioAsignaturaEN) throws CADException
    {
        boolean eliminado = false;
        
        try
        {
            eliminado = accesoUsuarioAsignaturaCAD.Delete(accesoUsuarioAsignaturaEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
   
        return eliminado;
    }
    
    /**
     * Obtiene un ArrayList con los accesos de un usuario pasado por parámetro
     * 
     * @param usuarioEN del que se quiere obtener la lista de accesos
     * @return ArrayList con la lista de accesos.
     * @throws CADException Error al acceder a la base de datos
     */
    public ArrayList<AccesoUsuarioAsignaturaEN> getAccesosByUsuario(UsuarioEN usuarioEN) throws CADException
    {
        ArrayList<AccesoUsuarioAsignaturaEN> accesos = null;
        
        try
        {
            accesos = accesoUsuarioAsignaturaCAD.getAccesosByUsuario(usuarioEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
        
        return accesos;
    }
    
    /**
     * Devuelve los accesos del usuario en los que él es el único administrador de la asignatura
     * 
     * @param usuarioEN del que se quieren obtener los accesos
     * @return ArrayList con la lista de accesos.
     * @throws CADException Error al acceder a la base de datos
     */
    public ArrayList<AccesoUsuarioAsignaturaEN> getAccesosUsuarioUnicoAdmin(UsuarioEN usuarioEN) throws CADException
    {
        ArrayList<AccesoUsuarioAsignaturaEN> accesos = null;
        
        try
        {
            accesos = accesoUsuarioAsignaturaCAD.getAccesosUsuarioUnicoAdmin(usuarioEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
        
        return accesos;
    }
            
    /**
     * Obtiene la lista de accesos de todos los usuarios de una asignatura
     * pasada por parámetro
     * 
     * @param asignaturaEN de la que se quieren obtener los accesos
     * @return ArrayList con la lista de accesos.
     * @throws CADException Error al acceder a la base de datos
     */
    public ArrayList<AccesoUsuarioAsignaturaEN> getAccesosByAsignatura(AsignaturaEN asignaturaEN) throws CADException
    {
        ArrayList<AccesoUsuarioAsignaturaEN> accesos = null;
        
        try
        {
            accesos = accesoUsuarioAsignaturaCAD.getAccesosByAsignatura(asignaturaEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
        
        return accesos;
    }
    
    /**
     * Obtiene el acceso de un usuario concreto en una asignatura concreta
     * 
     * @param asignaturaEN de la que se obtendrá el acceso
     * @param usuarioEN del que se obtendrá el acceso
     * @return AccesoUsuarioAsignaturaEN con el acceso o null si no existe 
     * ningún acceso para esa asignatura y usuario
     * @throws CADException Error al acceder a la base de datos
     */
    public AccesoUsuarioAsignaturaEN getAccesoByAsignaturaYUsuario(AsignaturaEN asignaturaEN, UsuarioEN usuarioEN) throws CADException
    {
        AccesoUsuarioAsignaturaEN accesoUsuarioAsignaturaEN = null;
        
        try
        {
            accesoUsuarioAsignaturaEN = accesoUsuarioAsignaturaCAD.getAccesoByAsignaturaYUsuario(asignaturaEN, usuarioEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
        
        return accesoUsuarioAsignaturaEN;
    }
}
