/* 
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : OpcionCEN
 * Created on : 15-abr-2014, 00:46:00
 * Author     : Raúl Sempere Trujillo
 */

package CEN;

import clases_generador.CADException;
import CAD.OpcionCAD;
import EN.OpcionEN;
import EN.PreguntaEN;
import EN.TraduccionOpcionEN;
import java.util.ArrayList;

/**
 * Capa de Entidad de Negocio de la entidad OpcionCEN
 * 
 * @author Raúl Sempere Trujillo
 */
public class OpcionCEN
{
    
    /**
     * Variable privada: Clase CAD de la entidad Opcion
     */
    private final OpcionCAD opcionCAD;
    
    /**
     * Constructor por defecto. Instancia el CAD para que gestione el acceso 
     * a la base de datos.
     */
    public OpcionCEN()
    {
        opcionCAD = new OpcionCAD();
    }
    
    /**
     * Utiliza la capa CAD para insertar la opcionEN en la base de datos
     * 
     * @param opcionEN OpcionEN a crear en la base datos
     * @return true en caso de éxito, false si no se pudo modificar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean New (OpcionEN opcionEN) throws CADException
    {
        boolean insertado = false;
        
        try
        {
            insertado = opcionCAD.New(opcionEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
   
        return insertado;
    }
    
    /**
     * Utiliza la capa CAD para modificar el objeto opcionEN pasado
     * por parámetro en la base de datos con los nuevos datos que contiene
     * 
     * @param opcionEN OpcionEN a modificar en la base de datos
     * @return true en caso de éxito, false si no se pudo modificar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean Modify (OpcionEN opcionEN) throws CADException
    {
        boolean modificado = false;
        
        try
        {
            modificado = opcionCAD.Modify(opcionEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
   
        return modificado;
    }
    
    /**
     * Utiliza la capa CAD para elimina el objeto opcionEN pasado 
     * por parámetro de la base de datos
     * 
     * @param opcionEN OpcionEN a eliminar de la base de datos
     * @return true en caso de éxito, false si no se pudo eliminar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean Delete (OpcionEN opcionEN) throws CADException
    {
        boolean eliminado = false;
        
        try
        {
            eliminado = opcionCAD.Delete(opcionEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
   
        return eliminado;
    }

    /**
     * Obtiene la ocpión de una pregunta pasada como parámetro por código
     * 
     * @param preguntaEN PreguntaEN de la que queremos obtener la opción
     * @param codigo int con el código de la opción a recuperar
     * @return OpcionEN con la opción
     * @throws CADException Error en el acceso a la base de datos 
     */
    public OpcionEN getOpcionByCodigo(PreguntaEN preguntaEN, int codigo) throws CADException
    {
        OpcionEN opcionEN = null;
        
        try
        {
            opcionEN = opcionCAD.getOpcionByCodigo(preguntaEN, codigo);
        }
        catch (CADException ex)
        {
            throw ex;
        }
        
        return opcionEN;
    }
    
    /**
     * Devuelve las opciones de una  pregunta pasada por parámetro
     * 
     * @param preguntaEN PreguntaEN de la que se quieren recuperar las opciones
     * @return ArrayList con la lista de opciones
     * @throws CADException Error en el acceso a la base de datos
     */
    public ArrayList<OpcionEN> getOpcionesByPregunta(PreguntaEN preguntaEN) throws CADException
    {
        ArrayList<OpcionEN> opciones = new ArrayList<OpcionEN>();
        
        try
        {
            opciones = opcionCAD.getOpcionesByPregunta(preguntaEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
        
        return opciones;
    }
    
    /**
     * Carga las traducciones de la OpcionEN pasada por parámetro
     * 
     * @param opcionEN OpcionEN en el que se rellenaran los datos
     * @throws CADException Si se produjo un error en el CAD  
     */
    public void rellenarTraducciones(OpcionEN opcionEN) throws CADException
    {
        TraduccionOpcionCEN traduccionOpcionCEN = new TraduccionOpcionCEN();
        ArrayList<TraduccionOpcionEN> traducciones;
        
        try
        {
            traducciones = traduccionOpcionCEN.getTraduccionesOpcionByOpcion(opcionEN);
            opcionEN.setTraducciones(traducciones);
        }
        catch (CADException ex)
        {
            throw ex;
        }
    } 
    
}
