/* 
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : TraduccionOpcionCEN
 * Created on : 15-abr-2014, 00:46:00
 * Author     : Raúl Sempere Trujillo
 */

package CEN;

import clases_generador.CADException;
import CAD.TraduccionOpcionCAD;
import EN.IdiomaEN;
import EN.OpcionEN;
import EN.PreguntaEN;
import EN.TraduccionOpcionEN;
import java.util.ArrayList;

/**
 * Capa de Entidad de Negocio de la entidad TraduccionOpcionCEN
 * 
 * @author Raúl Sempere Trujillo
 */
public class TraduccionOpcionCEN
{
    
    /**
     * Variable privada: Clase CAD de la entidad traduccionOpcion
     */
    private final TraduccionOpcionCAD traduccionOpcionCAD;
    
    /**
     * Constructor por defecto. Instancia el CAD para que gestione el acceso 
     * a la base de datos.
     */
    public TraduccionOpcionCEN()
    {
        traduccionOpcionCAD = new TraduccionOpcionCAD();
    }
    
    /**
     * Utiliza la capa CAD para insertar la traduccionOpcionEN en la base de datos
     * 
     * @param traduccionOpcionEN TraduccionOpcionEN a crear en la base datos
     * @return true en caso de éxito, false si no se pudo modificar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean New (TraduccionOpcionEN traduccionOpcionEN) throws CADException
    {
        boolean insertado = false;
        
        try
        {
            insertado = traduccionOpcionCAD.New(traduccionOpcionEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
   
        return insertado;
    }
    
    /**
     * Utiliza la capa CAD para modificar el objeto traduccionOpcionEN pasado
     * por parámetro en la base de datos con los nuevos datos que contiene
     * 
     * @param traduccionOpcionEN TraduccionOpcionEN a modificar en la base de datos
     * @return true en caso de éxito, false si no se pudo modificar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean Modify (TraduccionOpcionEN traduccionOpcionEN) throws CADException
    {
        boolean modificado = false;
        
        try
        {
            modificado = traduccionOpcionCAD.Modify(traduccionOpcionEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
   
        return modificado;
    }
    
    /**
     * Utiliza la capa CAD para elimina el objeto traduccionOpcionEN pasado 
     * por parámetro de la base de datos
     * 
     * @param traduccionOpcionEN TraduccionOpcionEN a eliminar de la base de datos
     * @return true en caso de éxito, false si no se pudo eliminar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean Delete (TraduccionOpcionEN traduccionOpcionEN) throws CADException
    {
        boolean eliminado = false;
        
        try
        {
            eliminado = traduccionOpcionCAD.Delete(traduccionOpcionEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
   
        return eliminado;
    }
    
    /**
     * Obtiene todas las traducciones disponibles de una opción
     * 
     * @param opcionEN OpcionEN de la que se van a obtener las traducciones
     * @return ArrayList con la lista de traducciones de la opción
     * @throws CADException Error en el acceso a la base de datos
     */
    public ArrayList<TraduccionOpcionEN> getTraduccionesOpcionByOpcion(OpcionEN opcionEN) throws CADException
    {
        ArrayList<TraduccionOpcionEN> traduccionesOpcion = new ArrayList<TraduccionOpcionEN>();
        
        try
        {
            traduccionesOpcion = traduccionOpcionCAD.getTraduccionesOpcionByOpcion(opcionEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
        
        return traduccionesOpcion;
    }

    /**
     * Obtiene la traducción en un idioma de todas lsa opciones de una pregunta
     * 
     * @param preguntaEN PreguntaEN de la que obtendremos las opciones
     * @param idiomaEN IdiomaEN de la traduccion de las opciones
     * @return ArrayList con la traducción de las opciones
     * @throws CADException Error en el acceso a la base de datos
     */
    public ArrayList<TraduccionOpcionEN> getTraduccionesOpcionByPreguntaEIdioma(PreguntaEN preguntaEN, IdiomaEN idiomaEN) throws CADException
    {
        ArrayList<TraduccionOpcionEN> traduccionesOpcion = new ArrayList<TraduccionOpcionEN>();
        
        try
        {
            traduccionesOpcion = traduccionOpcionCAD.getTraduccionesOpcionByPreguntaEIdioma(preguntaEN, idiomaEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
        
        return traduccionesOpcion;
    }

    /**
     * Obtiene la traducción de una opción en un idioma concreto
     * 
     * @param opcionEN OpcionEN de la que obtendremos la traducción
     * @param idiomaEN IdiomaEN idioma de la traducción obtenida
     * @return TraduccionOpcionEN con la traducción
     * @throws CADException Error en el acceso a la base de datos
     */
    public TraduccionOpcionEN getTraduccionOpcionByOpcionEIdioma(OpcionEN opcionEN, IdiomaEN idiomaEN) throws CADException
    {
        TraduccionOpcionEN traduccionOpcionEN = null;
        
        try
        {
            traduccionOpcionEN = traduccionOpcionCAD.getTraduccionOpcionByOpcionEIdioma(opcionEN, idiomaEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
        
        return traduccionOpcionEN;
    } 
    
}
