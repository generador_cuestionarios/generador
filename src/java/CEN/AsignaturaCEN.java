/* 
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : AsignaturaCEN
 * Created on : 15-abr-2014, 00:46:00
 * Author     : Raúl Sempere Trujillo
 */

package CEN;

import CAD.AsignaturaCAD;
import clases_generador.CADException;
import EN.AccesoUsuarioAsignaturaEN;
import EN.AsignaturaEN;
import EN.ConfiguracionAsignaturaIdiomaEN;
import EN.ExamenEN;
import EN.PreguntaEN;
import EN.TemaEN;
import java.util.ArrayList;

/**
 * Capa de Entidad de Negocio de la enidad Asignatura
 * 
 * @author Raúl Sempere Trujillo
 */
public class AsignaturaCEN
{
    
    /**
     * Variable privada: Clase CAD de la entidad Asignatura
     */
    private final AsignaturaCAD asignaturaCAD;
    
    /**
     * Constructor por defecto. Instancia el AsignaturaCAD para que gestione el acceso 
     * a la base de datos.
     */
    public AsignaturaCEN()
    {
        asignaturaCAD = new AsignaturaCAD();
    }
    
    /**
     * Utiliza la capa CAD para insertar la asignaturaEN en la base de datos
     * 
     * @param asignaturaEN AsignaturaEN a crear en la base datos
     * @return true en caso de éxito, false si no se pudo modificar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean New (AsignaturaEN asignaturaEN) throws CADException
    {
        boolean insertado = false;
        
        try
        {
            insertado = asignaturaCAD.New(asignaturaEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
   
        return insertado;
    }
    
    /**
     * Utiliza la capa CAD para modificar el objeto asignaturaEN pasado
     * por parámetro en la base de datos con los nuevos datos que contiene
     * 
     * @param asignaturaEN AsignaturaEN a modificar en la base de datos
     * @return true en caso de éxito, false si no se pudo modificar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean Modify (AsignaturaEN asignaturaEN) throws CADException
    {
        boolean modificado = false;
        
        try
        {
            modificado = asignaturaCAD.Modify(asignaturaEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
   
        return modificado;
    }
    
    /**
     * Utiliza la capa CAD para elimina el objeto asignaturaEN pasado 
     * por parámetro de la base de datos
     * 
     * @param asignaturaEN AsignaturaEN a eliminar de la base de datos
     * @return true en caso de éxito, false si no se pudo eliminar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean Delete (AsignaturaEN asignaturaEN) throws CADException
    {
        boolean eliminado = false;
        
        try
        {
            eliminado = asignaturaCAD.Delete(asignaturaEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
   
        return eliminado;
    }
    
    /**
     * Obtiene de la base de datos la asignatura con el código pasado por
     * parámentro
     * 
     * @param codigo de la asignatura que se quiere recuperar
     * @return AsignaturaEN recuperada de la base de datos o null si no se
     * encontró
     * @throws CADException Error en el acceso a la base de datos
     */
    public AsignaturaEN getAsignaturaByCodigo(int codigo) throws CADException
    {
        AsignaturaEN asignaturaEN = null;
        
        try
        {
            asignaturaEN = asignaturaCAD.getAsignaturaByCodigo(codigo);
        }
        catch (CADException ex)
        {
            throw ex;
        }
        
        return asignaturaEN;
    }
    
     /**
     * Carga los accesos de la AsignaturaEN pasada por parámetro
     * 
     * @param asignaturaEN asignaturaEN en el que se rellenaran los datos
     * @throws CADException Si se produjo un error en el CAD  
     */
    public void rellenarAccesos(AsignaturaEN asignaturaEN) throws CADException
    {
        ArrayList<AccesoUsuarioAsignaturaEN> accesos;
        AccesoUsuarioAsignaturaCEN accesoUsuarioAsignaturaCEN = new AccesoUsuarioAsignaturaCEN();
        
        try
        {
            accesos = accesoUsuarioAsignaturaCEN.getAccesosByAsignatura(asignaturaEN);
            asignaturaEN.setAccesosUsuarioAsignatura(accesos);
        }
        catch (CADException ex)
        {
            throw ex;
        }
    }
    
     /**
     * Carga los temas de la AsignaturaEN pasada por parámetro
     * 
     * @param asignaturaEN asignaturaEN en el que se rellenaran los datos
     * @throws CADException Si se produjo un error en el CAD  
     */
    public void rellenarTemas(AsignaturaEN asignaturaEN) throws CADException
    {
        ArrayList<TemaEN> temas;
        TemaCEN temaCEN = new TemaCEN();

        try
        {
            temas = temaCEN.getTemasByAsignatura(asignaturaEN);
            asignaturaEN.setTemas(temas);
        }
        catch (CADException ex)
        {
            throw ex;
        }
    }
    
    /**
     * Carga las preguntas de la AsignaturaEN pasada por parámetro
     * 
     * @param asignaturaEN AsignaturaEN en el que se rellenaran los datos
     * @throws CADException Si se produjo un error en el CAD  
     */
    public void rellenarPreguntas(AsignaturaEN asignaturaEN) throws CADException
    {
        ArrayList<PreguntaEN> preguntas;
        PreguntaCEN preguntaCEN = new PreguntaCEN();
        
        try
        {
            preguntas = preguntaCEN.getPreguntasByAsignatura(asignaturaEN);
            asignaturaEN.setPreguntas(preguntas);
        }
        catch (CADException ex)
        {
            throw ex;
        }
    }
    
    /**
     * Carga los examenes de la AsignaturaEN pasada por parámetro
     * 
     * @param asignaturaEN AsignaturaEN en el que se rellenaran los datos
     * @throws CADException Si se produjo un error en el CAD  
     */
    public void rellenarExamenes(AsignaturaEN asignaturaEN) throws CADException
    {
        ArrayList<ExamenEN> examenes;
        ExamenCEN examenCEN = new ExamenCEN();
        
        try
        {
            examenes = examenCEN.getExamenesByAsignatura(asignaturaEN);
            asignaturaEN.setExamenes(examenes);
        }
        catch (CADException ex)
        {
            throw ex;
        }
    }
    
    /**
     * Carga las configuraciones de la AsignaturaEN pasada por parámetro
     * 
     * @param asignaturaEN AsignaturaEN en el que se rellenaran los datos
     * @throws CADException Si se produjo un error en el CAD  
     */
    public void rellenarConfiguracionesIdioma(AsignaturaEN asignaturaEN) throws CADException
    {
        ArrayList<ConfiguracionAsignaturaIdiomaEN> configuraciones;
        ConfiguracionAsignaturaIdiomaCEN configuracionAsignaturaIdiomaCEN = new ConfiguracionAsignaturaIdiomaCEN();
        
        try
        {
            configuraciones = configuracionAsignaturaIdiomaCEN.getConfiguracionesAsignaturaIdiomaByAsignatura(asignaturaEN);
            asignaturaEN.setConfiguracionesIdioma(configuraciones);
        }
        catch (CADException ex)
        {
            throw ex;
        }
    } 
}
