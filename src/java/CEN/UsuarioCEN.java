/* 
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : UsuariocCEN
 * Created on : 15-abr-2014, 00:46:00
 * Author     : Raúl Sempere Trujillo
 */

package CEN;

import clases_generador.CADException;
import CAD.UsuarioCAD;
import EN.AccesoUsuarioAsignaturaEN;
import EN.AsignaturaEN;
import EN.UsuarioEN;
import java.util.ArrayList;

/**
 * Capa de Entidad de Negocio de la entidad Usuario
 * 
 * @author Raúl Sempere Trujillo
 */
public class UsuarioCEN
{
    
    /**
     * Variable privada: Clase CAD de la entidad Usuario
     */
    private final UsuarioCAD usuarioCAD;
    
    /**
     * Constructor por defecto. Instancia el usuarioCAD para que gestione el acceso 
     * a la base de datos.
     */
    public UsuarioCEN()
    {
        usuarioCAD = new UsuarioCAD();
    }
    
    /**
     * Utiliza la clase CAD para insertar el objeto usuarioEN en la base de datos
     * 
     * @param usuarioEN UsuarioEN a crear en la base datos
     * @return true en caso de éxito, false si no se pudo modificar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean New (UsuarioEN usuarioEN) throws CADException
    {
        boolean insertado = false;
        
        try
        {
            insertado = usuarioCAD.New(usuarioEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
   
        return insertado;
    }
    
    /**
     * Utiliza la clase CAD para modificar el objeto usuarioEN pasado por 
     * parámetro en la base de datos con los nuevos datos que contiene
     * 
     * @param usuarioEN UsuarioEN a modificar en la base de datos
     * @return true en caso de éxito, false si no se pudo modificar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean Modify (UsuarioEN usuarioEN) throws CADException
    {
        boolean modificado = false;
        
        try
        {
            modificado = usuarioCAD.Modify(usuarioEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
   
        return modificado;
    }
    
    /**
     * Utiliza la clase CAD para eliminar el objeto usuarioEN pasado por 
     * parámetro de la base de datos
     * 
     * @param usuarioEN UsuarioEN a eliminar de la base de datos
     * @return true en caso de éxito, false si no se pudo eliminar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean Delete (UsuarioEN usuarioEN) throws CADException
    {
        boolean eliminado = false;
        
        try
        {
            eliminado = usuarioCAD.Delete(usuarioEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
   
        return eliminado;
    }
    
    /**
     * Obtiene un usuario a partir de su codigo
     * 
     * @param codigo String con el codigo del usuario a recuperar
     * @return UsuarioEN datos del usuario recuperado
     * @throws CADException Si se produjo un error en el CAD  
     */
    public UsuarioEN getUsuarioByCodigo(int codigo) throws CADException
    {   
        UsuarioEN usuarioEN = null;
                
        try
        {
            usuarioEN = usuarioCAD.getUsuarioByCodigo(codigo);
        }
        catch (CADException ex)
        {
            throw ex;
        }
        
        return usuarioEN;
    }
    
    /**
     * Obtiene un usuario por su nombre
     * 
     * @param nombre String con el nombre del usuario a recuperar
     * @return UsuarioEN recuperado de la base de datos
     * @throws CADException Error al acceder a la base de datos    
     */
    public UsuarioEN getUsuarioByNombre(String nombre) throws CADException
    {   
        UsuarioEN usuarioEN = null;
                
        try
        {
            usuarioEN = usuarioCAD.getUsuarioByNombre(nombre);
        }
        catch (CADException ex)
        {
            throw ex;
        }
        
        return usuarioEN;
    }
    
    
    /**
     * Obtiene un usuario de la base de datos a través de su nombre y contraseña
     * @param user String con el login del usuario a recuperar
     * @param password String con el password del usuario a recuperar
     * @return UsuarioEN datos del usuario recuperados de la base de datos
     * @throws CADException Error al acceder a la base de datos        
     */
    public UsuarioEN getUsuarioByLogin(String user, String password) throws CADException
    {   
        UsuarioEN usuarioEN = null;
                
        try
        {
            usuarioEN = usuarioCAD.getUsuarioByLogin(user, password);
        }
        catch (CADException ex)
        {
            throw ex;
        }
        
        return usuarioEN;
    }
    
    /**
     * Obtiene la lista de usuarios que no tienen ningún tipo de acceso a la
     * asignatura pasada por parámetro
     * 
     * @param asignaturaEN AsignaturaEN donde se mirará que los usuarios no
     * tengan acceso
     * @return ArrayList con la lista de usuarios
     * @throws CADException Error al acceder a la base de datos   
     */
    public ArrayList<UsuarioEN> getUsuariosSinAccesoAsignatura(AsignaturaEN asignaturaEN) throws CADException
    {   
        ArrayList<UsuarioEN> usuarios = null;
                
        try
        {
            usuarios = usuarioCAD.getUsuariosSinAccesoAsignatura(asignaturaEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
        
        return usuarios;
    }
    
    /**
     * Recupera la lista completa de usuarios del sistema
     * 
     * @return ArrayList con la lista completa de usuarios
     * @throws CADException Error al acceder a la base de datos   
     */
    public ArrayList<UsuarioEN> getUsuarios() throws CADException
    {   
        ArrayList<UsuarioEN> usuarios = null;
                
        try
        {
            usuarios = usuarioCAD.getUsuarios();
        }
        catch (CADException ex)
        {
            throw ex;
        }
        
        return usuarios;
    } 
    
    /**
     * Carga la lista de accesos del usuario
     * @param usuarioEN UsuarioEN en el que se rellenaran los datos
     * @throws CADException Si se produjo un error en el CAD  
     */
    public void rellenarAccesos(UsuarioEN usuarioEN) throws CADException
    {
        ArrayList<AccesoUsuarioAsignaturaEN> accesos;
        AccesoUsuarioAsignaturaCEN accesoUsuarioAsignaturaCEN = new AccesoUsuarioAsignaturaCEN();
        
        try
        {
            accesos = accesoUsuarioAsignaturaCEN.getAccesosByUsuario(usuarioEN);
            usuarioEN.setAccesosUsuarioAsignatura(accesos);
        }
        catch (CADException ex)
        {
            throw ex;
        }
    }
    
}
