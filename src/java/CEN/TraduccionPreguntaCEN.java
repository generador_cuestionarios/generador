/* 
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : TraduccionPreguntaCEN
 * Created on : 15-abr-2014, 00:46:00
 * Author     : Raúl Sempere Trujillo
 */

package CEN;

import clases_generador.CADException;
import CAD.TraduccionPreguntaCAD;
import EN.IdiomaEN;
import EN.PreguntaEN;
import EN.TraduccionPreguntaEN;
import java.util.ArrayList;

/**
 * Capa de Entidad de Negocio de la entidad TraduccionPreguntaCEN
 * 
 * @author Raúl Sempere Trujillo
 */
public class TraduccionPreguntaCEN
{
    
    /**
     * Variable privada: Clase CAD de la entidad traduccionPregunta
     */
    private final TraduccionPreguntaCAD traduccionPreguntaCAD;
    
    /**
     * Constructor por defecto. Instancia el CAD para que gestione el acceso 
     * a la base de datos.
     */
    public TraduccionPreguntaCEN()
    {
        traduccionPreguntaCAD = new TraduccionPreguntaCAD();
    }
    
    /**
     * Utiliza la capa CAD para insertar la traduccionPreguntaEN en la base de datos
     * 
     * @param traduccionPreguntaEN TraduccionPreguntaEN a crear en la base datos
     * @return true en caso de éxito, false si no se pudo modificar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean New (TraduccionPreguntaEN traduccionPreguntaEN) throws CADException
    {
        boolean insertado = false;
        
        try
        {
            insertado = traduccionPreguntaCAD.New(traduccionPreguntaEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
   
        return insertado;
    }
    
    /**
     * Utiliza la capa CAD para modificar el objeto traduccionPreguntaEN pasado
     * por parámetro en la base de datos con los nuevos datos que contiene
     * 
     * @param traduccionPreguntaEN TraduccionPreguntaEN a modificar en la base de datos
     * @return true en caso de éxito, false si no se pudo modificar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean Modify (TraduccionPreguntaEN traduccionPreguntaEN) throws CADException
    {
        boolean modificado = false;
        
        try
        {
            modificado = traduccionPreguntaCAD.Modify(traduccionPreguntaEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
   
        return modificado;
    }
    
    /**
     * Utiliza la capa CAD para elimina el objeto traduccionPreguntaEN pasado 
     * por parámetro de la base de datos
     * 
     * @param traduccionPreguntaEN TraduccionPreguntaEN a eliminar de la base de datos
     * @return true en caso de éxito, false si no se pudo eliminar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean Delete (TraduccionPreguntaEN traduccionPreguntaEN) throws CADException
    {
        boolean eliminado = false;
        
        try
        {
            eliminado = traduccionPreguntaCAD.Delete(traduccionPreguntaEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
   
        return eliminado;
    }
    
    /**
     * Obtiene todas las traducciones de una pregunta
     * 
     * @param preguntaEN PreguntaEN de la que se quieren recuperar las
     * traducciones
     * @return ArrayList con la lista de traducciones de la pregunta
     * @throws CADException Error en el acceso a la base de datos
     */
    public ArrayList<TraduccionPreguntaEN> getTraduccionesPreguntaByPregunta(PreguntaEN preguntaEN) throws CADException
    {
        ArrayList<TraduccionPreguntaEN> traduccionesPregunta = new ArrayList<TraduccionPreguntaEN>();
        
        try
        {
            traduccionesPregunta = traduccionPreguntaCAD.getTraduccionesPreguntaByPregunta(preguntaEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
        
        return traduccionesPregunta;
    }

    /**
     * Obtiene una traducción concreta de una pregunta
     * 
     * @param preguntaEN preguntaEN de la cuál obtendremos la traducción
     * @param idiomaEN IdiomaEN de la traducción
     * @return TraduccionPreguntaEN con la traducción de la pregunta
     * @throws CADException Error en el acceso a la base de datos
     */
    public TraduccionPreguntaEN getTraduccionPreguntaByPreguntaEIdioma(PreguntaEN preguntaEN, IdiomaEN idiomaEN) throws CADException
    {
        TraduccionPreguntaEN traduccionPreguntaEN = null;
        
        try
        {
            traduccionPreguntaEN = traduccionPreguntaCAD.getTraduccionPreguntaByPreguntaEIdioma(preguntaEN, idiomaEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
        
        return traduccionPreguntaEN;
    } 
    
    /**
     * Indica devuelve la traducción de una pregunta en el idioma pasado por
     * parámentro
     * 
     * @param lista ArrayList lista de las traducciones a buscar
     * @param idiomaEN IdiomaEN de la traducción que se está buscando
     * @return TraduccionPreguntaEN con la traducción.
     */
    public TraduccionPreguntaEN idiomaTraducidoEnLista(ArrayList<TraduccionPreguntaEN> lista, IdiomaEN idiomaEN)
    {
        TraduccionPreguntaEN TraduccionEncontrada = null;
        
        for (TraduccionPreguntaEN traduccion: lista)
        {
            if (traduccion.getIdioma().getCodigo() == idiomaEN.getCodigo())
            {
                TraduccionEncontrada = traduccion;
                break; //Optimizamos saliendo del bucle al encontrarla
            }
        }
        
        return TraduccionEncontrada;
    }
}
