/* 
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : PreparacionExamenIdiomaCEN
 * Created on : 15-abr-2014, 00:46:00
 * Author     : Raúl Sempere Trujillo
 */

package CEN;

import clases_generador.CADException;
import CAD.PreparacionExamenIdiomaCAD;
import EN.ExamenEN;
import EN.IdiomaEN;
import EN.PreparacionExamenIdiomaEN;
import java.util.ArrayList;

/**
 * Capa de Entidad de Negocio de la entidad PreparacionExamenIdiomaCEN
 * 
 * @author Raúl Sempere Trujillo
 */
public class PreparacionExamenIdiomaCEN
{
    
    /**
     * Variable privada: Clase CAD de la entidad preparacionExamenIdioma
     */
    private final PreparacionExamenIdiomaCAD preparacionExamenIdiomaCAD;
    
    /**
     * Constructor por defecto. Instancia el CAD para que gestione el acceso 
     * a la base de datos.
     */
    public PreparacionExamenIdiomaCEN()
    {
        preparacionExamenIdiomaCAD = new PreparacionExamenIdiomaCAD();
    }
    
    /**
     * Utiliza la capa CAD para insertar la preparacionExamenIdiomaEN en la base de datos
     * 
     * @param preparacionExamenIdiomaEN PreparacionExamenIdiomaEN a crear en la base datos
     * @return true en caso de éxito, false si no se pudo modificar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean New (PreparacionExamenIdiomaEN preparacionExamenIdiomaEN) throws CADException
    {
        boolean insertado = false;
        
        try
        {
            insertado = preparacionExamenIdiomaCAD.New(preparacionExamenIdiomaEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
   
        return insertado;
    }
    
    /**
     * Utiliza la capa CAD para modificar el objeto preparacionExamenIdiomaEN pasado
     * por parámetro en la base de datos con los nuevos datos que contiene
     * 
     * @param preparacionExamenIdiomaEN PreparacionExamenIdiomaEN a modificar en la base de datos
     * @return true en caso de éxito, false si no se pudo modificar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean Modify (PreparacionExamenIdiomaEN preparacionExamenIdiomaEN) throws CADException
    {
        boolean modificado = false;
        
        try
        {
            modificado = preparacionExamenIdiomaCAD.Modify(preparacionExamenIdiomaEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
   
        return modificado;
    }
    
    /**
     * Utiliza la capa CAD para elimina el objeto preparacionExamenIdiomaEN pasado 
     * por parámetro de la base de datos
     * 
     * @param preparacionExamenIdiomaEN PreparacionExamenIdiomaEN a eliminar de la base de datos
     * @return true en caso de éxito, false si no se pudo eliminar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean Delete (PreparacionExamenIdiomaEN preparacionExamenIdiomaEN) throws CADException
    {
        boolean eliminado = false;
        
        try
        {
            eliminado = preparacionExamenIdiomaCAD.Delete(preparacionExamenIdiomaEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
   
        return eliminado;
    }

    /**
     * Obtiene la preparación de un examen en un idioma pasado pro parámentro
     * 
     * @param examenEN ExamenEN del que se desea obtener la preparacion
     * @param idiomaEN IdiomaEN idioma de la preparacion
     * @return PreparacionExamenIdiomaEN con la preparación del examen
     * @throws CADException Error en el acceso a la base de datos
     */
    public PreparacionExamenIdiomaEN getPreparacionExamenByExamenEIdioma(ExamenEN examenEN, IdiomaEN idiomaEN) throws CADException
    {
        PreparacionExamenIdiomaEN preparacionExamenIdiomaEN = null;
        
        try
        {
            preparacionExamenIdiomaEN = preparacionExamenIdiomaCAD.getPreparacionExamenByExamenEIdioma(examenEN, idiomaEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
        
        return preparacionExamenIdiomaEN;
    }
    
    /**
     * Obtiene las preparaciones en todos los idiomas de un examen
     * pasado por parámentro
     * 
     * @param examenEN ExamenEN del que se desea obtener las preparaciones
     * @return ArrayList con las traducciones del examen
     * @throws CADException Error en el acceso a la base de datos
     */
    public ArrayList<PreparacionExamenIdiomaEN> getPreparacionesExamenByExamen(ExamenEN examenEN) throws CADException
    {
        ArrayList<PreparacionExamenIdiomaEN> preparaciones = new ArrayList<PreparacionExamenIdiomaEN>();
        
        try
        {
            preparaciones = preparacionExamenIdiomaCAD.getPreparacionesExamenByExamen(examenEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
        
        return preparaciones;
    }
    
    /**
     * Obtiene las preparaciones (solo de los idiomas actualmente configurados
     * en la asignatura) de un examen pasado por parámentro
     * 
     * @param examenEN ExamenEN del que se desea obtener las preparaciones
     * @return ArrayList con las traducciones del examen
     * @throws CADException Error en el acceso a la base de datos
     */
    public ArrayList<PreparacionExamenIdiomaEN> getPreparacionesDisponiblesByExamen(ExamenEN examenEN) throws CADException
    {
        ArrayList<PreparacionExamenIdiomaEN> preparaciones = new ArrayList<PreparacionExamenIdiomaEN>();
        
        try
        {
            preparaciones = preparacionExamenIdiomaCAD.getPreparacionesDisponiblesByExamen(examenEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
        
        return preparaciones;
    }
    
    /**
     * Indica si el idioma está preparado en una lista de preparaciones pasada
     * por parámentro
     * 
     * @param preparaciones ArrayList donde se comprobará si el idioma está
     * preparado
     * @param idiomaEN IdiomaEN a comprobar
     * @return true si se encuentra el idioma en la lista, false enc aso contrario
     */
    public boolean isIdiomaPreparado(ArrayList<PreparacionExamenIdiomaEN> preparaciones, IdiomaEN idiomaEN)
    {
        boolean preparado = false;
        
        for (PreparacionExamenIdiomaEN preparacion : preparaciones)
        {
            if (preparacion.getIdioma().getCodigo() == idiomaEN.getCodigo())
            {
                preparado = true;
                break; //Optimizamos saliendo cuando lo encuentra
            }
        }
        
        return preparado;
    }
    
}
