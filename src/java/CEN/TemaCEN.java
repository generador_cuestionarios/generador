/* 
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : TemaCEN
 * Created on : 15-abr-2014, 00:46:00
 * Author     : Raúl Sempere Trujillo
 */

package CEN;

import clases_generador.CADException;
import CAD.TemaCAD;
import EN.AsignaturaEN;
import EN.ClasificacionTemaEN;
import EN.KeywordEN;
import EN.TemaEN;
import java.util.ArrayList;


/**
 * Capa de Entidad de Negocio de la entidad TemaCEN
 * 
 * @author Raúl Sempere Trujillo
 */
public class TemaCEN
{
    
    /**
     * Variable privada: Clase CAD de la entidad tema
     */
    private final TemaCAD temaCAD;
    
    /**
     * Constructor por defecto. Instancia el usuarioCAD para que gestione el acceso 
     * a la base de datos.
     */
    public TemaCEN()
    {
        temaCAD = new TemaCAD();
    }
    
    /**
     * Utiliza la capa CAD para insertar la temaEN en la base de datos
     * 
     * @param temaEN TemaEN a crear en la base datos
     * @return true en caso de éxito, false si no se pudo modificar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean New (TemaEN temaEN) throws CADException
    {
        boolean insertado = false;
        
        try
        {
            insertado = temaCAD.New(temaEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
   
        return insertado;
    }
    
    /**
     * Utiliza la capa CAD para modificar el objeto temaEN pasado
     * por parámetro en la base de datos con los nuevos datos que contiene
     * 
     * @param temaEN TemaEN a modificar en la base de datos
     * @return true en caso de éxito, false si no se pudo modificar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean Modify (TemaEN temaEN) throws CADException
    {
        boolean modificado = false;
        
        try
        {
            modificado = temaCAD.Modify(temaEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
   
        return modificado;
    }
    
    /**
     * Utiliza la capa CAD para elimina el objeto temaEN pasado 
     * por parámetro de la base de datos
     * 
     * @param temaEN TemaEN a eliminar de la base de datos
     * @return true en caso de éxito, false si no se pudo eliminar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean Delete (TemaEN temaEN) throws CADException
    {
        boolean eliminado = false;
        
        try
        {
            eliminado = temaCAD.Delete(temaEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
   
        return eliminado;
    }
    
    /**
     * Recupera el tema de la asignatura y el código pasado por parámentro
     * 
     * @param asignaturaEN AsignaturaEN del tema a recuperar
     * @param codigo int con el código del tema a recuperar
     * @return TemaEN recuperado de la base de datos
     * @throws CADException Error en el acceso a la base de datos
     */
    public TemaEN getTemaByCodigo(AsignaturaEN asignaturaEN, int codigo) throws CADException
    {
        TemaEN temaEN = null;
        
        try
        {
            temaEN = temaCAD.getTemaByCodigo(asignaturaEN, codigo);
        }
        catch (CADException ex)
        {
            throw ex;
        }
        
        return temaEN;
    }
    
    /**
     * Recupera el la lista de temas de la asignatura pasada por parámentro
     * 
     * @param asignaturaEN AsignaturaEN de la cual se obtendrá la lista de temas
     * @return ArrayList con la lista de temas recuperados de la base de datos
     * @throws CADException Error en el acceso a la base de datos
     */
     public ArrayList<TemaEN> getTemasByAsignatura(AsignaturaEN asignaturaEN) throws CADException
    {
        ArrayList<TemaEN> temas = new ArrayList<TemaEN>();
        
        try
        {
            temas = temaCAD.getTemasByAsignatura(asignaturaEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
        
        return temas;
    
    }
    
    /**
     * Carga las keywords del tema
     * 
     * @param temaEN TemaEN en el que se rellenaran los datos
     * @throws CADException Si se produjo un error en el CAD  
     */
    public void rellenarKeywords(TemaEN temaEN) throws CADException
    {
        ArrayList<KeywordEN> keywords;
        KeywordCEN keywordCEN = new KeywordCEN();
        
        try
        {
            keywords = keywordCEN.getKeywordsByTema(temaEN);
            temaEN.setKeywords(keywords);
        }
        catch (CADException ex)
        {
            throw ex;
        }
    }
    
    /**
     * Carga las clasificaciones de preguntas del tema
     * 
     * @param temaEN TemaEN en el que se rellenaran los datos
     * @throws CADException Si se produjo un error en el CAD  
     */
    public void rellenarClasificaciones(TemaEN temaEN) throws CADException
    {
        ArrayList<ClasificacionTemaEN> clasificaciones;
        ClasificacionTemaCEN clasificacionTemaCEN = new ClasificacionTemaCEN();
        
        try
        {
            clasificaciones = clasificacionTemaCEN.getClasificacionesByTema(temaEN);
            temaEN.setClasificaciones(clasificaciones);
        }
        catch (CADException ex)
        {
            throw ex;
        }
    }
}
