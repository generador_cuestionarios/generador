/* 
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : ExamenPreguntaCEN
 * Created on : 15-abr-2014, 00:46:00
 * Author     : Raúl Sempere Trujillo
 */

package CEN;

import clases_generador.CADException;
import CAD.ExamenPreguntaCAD;
import EN.ExamenEN;
import EN.ExamenPreguntaEN;
import EN.IdiomaEN;
import EN.PreguntaEN;
import java.util.ArrayList;
import java.util.Calendar;


/**
 * Capa de Entidad de Negocio de la entidad ExamenPreguntaCEN
 * 
 * @author Raúl Sempere Trujillo
 */
public class ExamenPreguntaCEN
{
    
    /**
     * Variable privada: Clase CAD de la entidad examenPregunta
     */
    private final ExamenPreguntaCAD examenPreguntaCAD;
    
    /**
     * Constructor por defecto. Instancia el CAD para que gestione el acceso 
     * a la base de datos.
     */
    public ExamenPreguntaCEN()
    {
        examenPreguntaCAD = new ExamenPreguntaCAD();
    }

    /**
     * Utiliza la capa CAD para insertar la examenPreguntaEN en la base de datos
     * 
     * @param examenPreguntaEN ExamenPreguntaEN a crear en la base datos
     * @return true en caso de éxito, false si no se pudo modificar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean New (ExamenPreguntaEN examenPreguntaEN) throws CADException
    {
        boolean insertado = false;
        
        try
        {
            insertado = examenPreguntaCAD.New(examenPreguntaEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
   
        return insertado;
    }
    
    /**
     * Elimina el objeto examenEN pasado por parámetro de
     * la base de datos
     * 
     * @param examenEN ExamenEN a eliminar de la base de datos
     * @return true en caso de éxito, false si no se pudo eliminar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean eliminarPreguntasDeExamen (ExamenEN examenEN) throws CADException
    {
        boolean eliminadas = false;
        
        try
        {
            eliminadas = examenPreguntaCAD.eliminarPreguntasDeExamen(examenEN);
            
            //Ahora hay que recalcular la fecha de ultimo uso de las preguntas
            //Y restar las utilizaciones ya que se han eliminado de este examen
            ExamenCEN examenCEN = new ExamenCEN();
            PreguntaCEN preguntaCEN = new PreguntaCEN();
            
            for (ExamenPreguntaEN pregunta : examenEN.getPreguntas())
            {
                pregunta.getPregunta().setUtilizada(pregunta.getPregunta().getUtilizada()-1);
                
                Calendar cal = examenCEN.getUltimoUsoPreguntaByPregunta(pregunta.getPregunta());
                pregunta.getPregunta().setUltimoUso(cal);
                
                preguntaCEN.Modify(pregunta.getPregunta());
            }
        }
        catch (CADException ex)
        {
            throw ex;
        }
   
        return eliminadas;
    }
    
    /**
     * Devuelve las preguntas de un examen que estan sin revisar en el idioma
     * pasado por parámetro
     * 
     * @param examenEN ExamenEN de las preguntas
     * @param idiomaEN IdiomaEN en el que no estan revisadas
     * @return ArrayList con el listado de ExamenPreguntas
     * @throws CADException Error en el acceso a la base de datos
     */
    public ArrayList<ExamenPreguntaEN> getExamenesPreguntaSinRevisarByExamenEIdioma(ExamenEN examenEN, IdiomaEN idiomaEN) throws CADException
    {
        ArrayList<ExamenPreguntaEN> examenesPregunta = new ArrayList<ExamenPreguntaEN>();
        
        try
        {
            examenesPregunta = examenPreguntaCAD.getExamenesPreguntaSinRevisarByExamenEIdioma(examenEN, idiomaEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
        
        return examenesPregunta;
    }
    
    /**
     * Devuelve las preguntas de un examen pasado por parámentro
     * 
     * @param examenEN ExamenEN del que se quieren recuperar las preguntas
     * @return ArrayList con el listado de ExamenPreguntas
     * @throws CADException Error en el acceso a la base de datos
     */
    public ArrayList<ExamenPreguntaEN> getExamenPreguntasByExamen(ExamenEN examenEN) throws CADException
    {
        ArrayList<ExamenPreguntaEN> examenPreguntas = null;
        
        try
        {
            examenPreguntas = examenPreguntaCAD.getExamenPreguntasByExamen(examenEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
        
        return examenPreguntas;
    }

    /**
     * Devuelve todos los examenes donde aparece la pregunta pasada por
     * parámentro
     * 
     * @param preguntaEN PreguntaEN de la que queremos los ExamenesPregunta
     * @return ArrayList con el listado de ExamenPreguntas
     * @throws CADException Error en el acceso a la base de datos
     */
    public ArrayList<ExamenPreguntaEN> getExamenesPreguntaByPregunta(PreguntaEN preguntaEN) throws CADException
    {
        ArrayList<ExamenPreguntaEN> examenesPregunta = null;
        
        try
        {
            examenesPregunta = examenPreguntaCAD.getExamenesPreguntaByPregunta(preguntaEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
        
        return examenesPregunta;
    }
    
    /**
     * Sustituye en una lista de ExamenPregunta una preguntaEN por otra
     * 
     * @param lista ArrayList con las preguntaExamenEN
     * @param a_sustituir PreguntaEN a sustituir
     * @param sustituta PreguntaEN sustituta
     */
    public void sustituirEnLista(ArrayList<ExamenPreguntaEN> lista, PreguntaEN a_sustituir, PreguntaEN sustituta)
    { 
        for (int i = 0; i < lista.size(); i++)
        {
            if (lista.get(i).getPregunta().getCodigo() == a_sustituir.getCodigo())
            {
                ExamenPreguntaEN examenPreguntaEN = new ExamenPreguntaEN();
                examenPreguntaEN.setExamen(lista.get(i).getExamen());
                examenPreguntaEN.setPregunta(sustituta);

                lista.set(i, examenPreguntaEN);
                break;
            }
        }
    }
    
    /**
     * Sustituye las preguntas de una lista por todas las preguntas de otra lista
     * 
     * @param lista_sustituir lista de preguntas a sustituir
     * @param lista_sustitutas lista de preguntas sustitutas
     */
    public void sustituirEnLista(ArrayList<ExamenPreguntaEN> lista_sustituir, ArrayList<ExamenPreguntaEN> lista_sustitutas)
    { 
        int indice;
        
        for (ExamenPreguntaEN pregunta : lista_sustitutas)
        {
            indice = indiceEnLista(lista_sustituir, pregunta.getPregunta());
            
            if (indice != -1)
                lista_sustituir.set(indice, pregunta);
        }
    } 
   
    /**
     * Indica si una pregunta existe en una lista de ExamenPreguntaEN
     * 
     * @param lista ArrayList con la lista de ExamenPreguntasEN a comprobar
     * @param pregunta PreguntaEN a comprobar si existe
     * @return true si se encuentra en la lista, false en caso contrario
     */
    public boolean existeEnLista(ArrayList<ExamenPreguntaEN> lista, PreguntaEN pregunta)
    { 
        for (ExamenPreguntaEN p : lista)
        {
            if (p.getPregunta().getCodigo() == pregunta.getCodigo())
                return true;
        }
        
        return false;
    }
    
    /**
     * Devuelve el indice de una pregunta en caso de que se encuentre en una
     * lista de ExamenPreguntaEN
     * 
     * @param lista ArrayList con la lista de ExamenPreguntasEN a comprobar
     * @param pregunta PreguntaEN de la que se quiere el índice
     * @return -1 si la pregunta no se encuentra, indice de la pregunta en
     * caso contrario.
     */
    public int indiceEnLista(ArrayList<ExamenPreguntaEN> lista, PreguntaEN pregunta)
    { 
        for (int i = 0; i < lista.size(); i++)
        {
            if (lista.get(i).getPregunta().getCodigo() == pregunta.getCodigo())
                return i;
        }
        
        return -1;
    }
    
    /**
     * Elimina una pregunta de una lista de ExamenPreguntaEN
     * 
     * @param lista ArrayList con la lista de donde se eliminará la pregunta
     * @param pregunta PreguntaEN a liminar de la lista
     * 
     * @return true si la pregunta se encontró, false en caso contrario
     */
    public boolean borrarDeLista(ArrayList<ExamenPreguntaEN> lista, PreguntaEN pregunta)
    {
        boolean encontrado = false;
        
        for (int i = 0; i < lista.size(); i++)
        {
            if (lista.get(i).getPregunta().getCodigo() == pregunta.getCodigo())
            {
                lista.remove(i);
                encontrado = true;
                break;
            }
        }
        
        return encontrado;
    }
}
