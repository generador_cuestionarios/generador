/* 
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : IdiomaCEN
 * Created on : 15-abr-2014, 00:46:00
 * Author     : Raúl Sempere Trujillo
 */

package CEN;

import clases_generador.CADException;
import CAD.IdiomaCAD;
import EN.AsignaturaEN;
import EN.IdiomaEN;
import java.util.ArrayList;


/**
 * Capa de Entidad de Negocio de la entidad IdiomaCEN
 * 
 * @author Raúl Sempere Trujillo
 */
public class IdiomaCEN
{
    
    /**
     * Variable privada: Clase CAD de la entidad idioma
     */
    private final IdiomaCAD idiomaCAD;
    
    /**
     * Constructor por defecto. Instancia el CAD para que gestione el acceso 
     * a la base de datos.
     */
    public IdiomaCEN()
    {
        idiomaCAD = new IdiomaCAD();
    }
    
    /**
     * Utiliza la capa CAD para insertar la idiomaEN en la base de datos
     * 
     * @param idiomaEN IdiomaEN a crear en la base datos
     * @return true en caso de éxito, false si no se pudo modificar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean New (IdiomaEN idiomaEN) throws CADException
    {
        boolean insertado = false;
        
        try
        {
            insertado = idiomaCAD.New(idiomaEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
   
        return insertado;
    }
    
    /**
     * Utiliza la capa CAD para modificar el objeto idiomaEN pasado
     * por parámetro en la base de datos con los nuevos datos que contiene
     * 
     * @param idiomaEN IdiomaEN a modificar en la base de datos
     * @return true en caso de éxito, false si no se pudo modificar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean Modify (IdiomaEN idiomaEN) throws CADException
    {
        boolean modificado = false;
        
        try
        {
            modificado = idiomaCAD.Modify(idiomaEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
   
        return modificado;
    }
    
    /**
     * Utiliza la capa CAD para elimina el objeto idiomaEN pasado 
     * por parámetro de la base de datos
     * 
     * @param idiomaEN IdiomaEN a eliminar de la base de datos
     * @return true en caso de éxito, false si no se pudo eliminar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean Delete (IdiomaEN idiomaEN) throws CADException
    {
        boolean eliminado = false;
        
        try
        {
            eliminado = idiomaCAD.Delete(idiomaEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
   
        return eliminado;
    }
    
    /**
     * Recupera la lista de idiomas completa del sistema
     * 
     * @return ArrayList con la lista de idiomas dados de alta en el sistema
     * @throws CADException Error al acceder a la base de datos        
     */  
    public ArrayList<IdiomaEN> getIdiomas() throws CADException
    {   
        ArrayList<IdiomaEN> idiomas = null;
                
        try
        {
            idiomas = idiomaCAD.getIdiomas();
        }
        catch (CADException ex)
        {
            throw ex;
        }
        
        return idiomas;
    } 
    
    /**
     * Recupera un idioma de la base de datos a través de su código
     * 
     * @param codigo int con el código del idioma a obtener
     * @return IdiomaEN datos del idioma recuperados de la base de datos
     * @throws CADException Error al acceder a la base de datos        
     */
    public IdiomaEN getIdiomaByCodigo(int codigo) throws CADException
    {   
        IdiomaEN idiomaEN = null;
                
        try
        {
            idiomaEN = idiomaCAD.getIdiomaByCodigo(codigo);
        }
        catch (CADException ex)
        {
            throw ex;
        }
        
        return idiomaEN;
    }
    
    /**
     * Recupera un idioma de la base de datos a través de su nombre
     * 
     * @param nombre { con el nombre delidioma a obtener
     * @return IdiomaEN datos del idioma recuperados de la base de datos
     * @throws CADException Error al acceder a la base de datos        
     */
    public IdiomaEN getIdiomaByNombre(String nombre) throws CADException
    {   
        IdiomaEN idiomaEN = null;
                
        try
        {
            idiomaEN = idiomaCAD.getIdiomaByNombre(nombre);
        }
        catch (CADException ex)
        {
            throw ex;
        }
        
        return idiomaEN;
    }
    
    /**
     * Recupera la lista de idiomas que no estan configurados en
     * una asignatura
     * 
     * @param asignaturaEN AsignaturaEN donde los idiomas no estan configurados
     * @return ArrayList con la lista de idiomas que no estan configurados
     * @throws CADException Error al acceder a la base de datos        
     */  
    public ArrayList<IdiomaEN> getIdiomasSinConfigurarAsignatura(AsignaturaEN asignaturaEN) throws CADException
    {   
        ArrayList<IdiomaEN> idiomas = null;
                
        try
        {
            idiomas = idiomaCAD.getIdiomasSinConfigurarAsignatura(asignaturaEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
        
        return idiomas;
    }

}
