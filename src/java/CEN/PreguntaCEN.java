/* 
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : PreguntaCEN
 * Created on : 15-abr-2014, 00:46:00
 * Author     : Raúl Sempere Trujillo
 */

package CEN;

import clases_generador.CADException;
import CAD.PreguntaCAD;
import EN.AsignaturaEN;
import EN.ClasificacionTemaEN;
import EN.ExamenPreguntaEN;
import EN.IdiomaEN;
import EN.OpcionEN;
import EN.PreguntaEN;
import EN.TemaEN;
import EN.TraduccionPreguntaEN;
import clasificador.*;
import java.util.ArrayList;
import java.util.Calendar;


/**
 * Capa de Entidad de Negocio de la entidad PreguntaCEN
 * 
 * @author Raúl Sempere Trujillo
 */
public class PreguntaCEN
{
    
    /**
     * Variable privada: Clase CAD de la entidad Pregunta
     */
    private final PreguntaCAD preguntaCAD;
    
    /**
     * Constructor por defecto. Instancia el CAD para que gestione el acceso 
     * a la base de datos.
     */
    public PreguntaCEN()
    {
        preguntaCAD = new PreguntaCAD();
    }
    
    /**
     * Utiliza la capa CAD para insertar la preguntaEN en la base de datos
     * 
     * @param preguntaEN PreguntaEN a crear en la base datos
     * @return true en caso de éxito, false si no se pudo modificar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean New (PreguntaEN preguntaEN) throws CADException
    {
        boolean insertado = false;
        
        try
        {
            insertado = preguntaCAD.New(preguntaEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
   
        return insertado;
    }
    
    /**
     * Utiliza la capa CAD para modificar el objeto preguntaEN pasado
     * por parámetro en la base de datos con los nuevos datos que contiene
     * 
     * @param preguntaEN PreguntaEN a modificar en la base de datos
     * @return true en caso de éxito, false si no se pudo modificar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean Modify (PreguntaEN preguntaEN) throws CADException
    {
        boolean modificado = false;
        
        try
        {
            modificado = preguntaCAD.Modify(preguntaEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
   
        return modificado;
    }
    
    /**
     * Utiliza la capa CAD para elimina el objeto preguntaEN pasado 
     * por parámetro de la base de datos
     * 
     * @param preguntaEN PreguntaEN a eliminar de la base de datos
     * @return true en caso de éxito, false si no se pudo eliminar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean Delete (PreguntaEN preguntaEN) throws CADException
    {
        boolean eliminado = false;
        
        try
        {
            eliminado = preguntaCAD.Delete(preguntaEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
   
        return eliminado;
    }
    
    /**
     * Clasifica una pregunta pasada por parámetro dentro de los temas disponibles
     * en su asignatura.
     * 
     * @param preguntaEN PreguntaEN a clasificar
     * @return true si se le asignó algún tema, false si no se pudo clasificar
     * @throws CADException Si ocurre algun error de acceso a los datos.
     */
    public boolean clasificar(PreguntaEN preguntaEN) throws CADException
    {
        /**
         * Cambiar esta línea para utilizar otro clasificador 
         */
        Clasificador clasificador = new ClasificadorKeywords();
        
        ClasificacionTemaCEN clasificacionTemaCEN = new ClasificacionTemaCEN();
        ClasificacionTemaEN clasificacionTemaEN = new ClasificacionTemaEN();
        PreguntaCEN preguntaCEN = new PreguntaCEN();
        AsignaturaCEN asignaturaCEN = new AsignaturaCEN();
        TemaCEN temaCEN = new TemaCEN();
        ConfiguracionAsignaturaIdiomaCEN confAsigIdiomaCEN = new ConfiguracionAsignaturaIdiomaCEN();
        TraduccionOpcionCEN traOpcionCEN = new TraduccionOpcionCEN();
        TraduccionPreguntaCEN traPreguntaCEN = new TraduccionPreguntaCEN();
        TemaEN temaEN;
        
        if (preguntaEN.getAsignatura().getTemas() == null)
            asignaturaCEN.rellenarTemas(preguntaEN.getAsignatura());
        if (preguntaEN.getOpciones() == null)
            preguntaCEN.rellenarOpciones(preguntaEN);
        
        //Primero quitamos la clasificación actual
        this.rellenarClasificacion(preguntaEN);
        
        if (preguntaEN.getClasificacion() != null)
        {
            clasificacionTemaCEN.Delete(preguntaEN.getClasificacion());
            preguntaEN.setClasificacion(null);
        }

        //Vamos a recuperar el enunciado de la pregunta en el idioma de la asignatura
        //Idioma principal de la asignatura
        IdiomaEN idioma = confAsigIdiomaCEN.getConfiguracionIdiomaPrincipal(preguntaEN.getAsignatura()).getIdioma();
        
        //Obtenemos el enunciado y la opción correcta en el idioma principal
        //de la asignatura para analizarlos en busca de keywords
        String busqueda = traPreguntaCEN.getTraduccionPreguntaByPreguntaEIdioma(preguntaEN, idioma).getEnunciado();
        
        for (OpcionEN opcion : preguntaEN.getOpciones())
        {
            if (opcion.isCorrecta())
            {
                busqueda += " " + traOpcionCEN.getTraduccionOpcionByOpcionEIdioma(opcion, idioma).getEnunciado();
                break;
            }
        }
        
        busqueda = busqueda.toLowerCase();
        
        //Los temas de la asignatura deben tener los keywords cargados
        for (TemaEN t : preguntaEN.getAsignatura().getTemas())
        {
            temaCEN.rellenarKeywords(t);
        }
        
        temaEN = clasificador.clasificar(busqueda, preguntaEN.getAsignatura().getTemas());
        
        //Si se ha podido clasificar
        if (temaEN != null)
        {
            clasificacionTemaEN.setPregunta(preguntaEN);
            clasificacionTemaEN.setRevisada(false);
            clasificacionTemaEN.setTema(temaEN);
            
            //Añadimos la clasificación a la pregunta 
            preguntaEN.setClasificacion(clasificacionTemaEN);
            
            //y a la base de datos
            clasificacionTemaCEN.New(clasificacionTemaEN);
        }
        
        return (temaEN != null);
    }
    
    /**
     * Obtiene una pregunta por la asignatura y su código
     * 
     * @param asignaturaEN AsignaturaEN de la pregunta a recuperar
     * @param codigo int con el código de la pregunta a recuperar
     * @return PreguntaEn recuperada
     * @throws CADException Error en el acceso a la base de datos
     */
    public PreguntaEN getPreguntaByCodigo(AsignaturaEN asignaturaEN, int codigo) throws CADException
    {
        PreguntaEN pregunta = null;
        
        try
        {
            pregunta = preguntaCAD.getPreguntaByCodigo(asignaturaEN, codigo);
        }
        catch (CADException ex)
        {
            throw ex;
        }
        
        return pregunta;
    }
    
    /**
     * Obtiene todas las preguntas de la asignatura pasada por parámetro
     * 
     * @param asignaturaEN AsignaturaEN de la que se recuperarán las preguntas
     * @return ArrayList con la lista de preguntas de la asignatura
     * @throws CADException Error en el acceso a la base de datos
     */
    public ArrayList<PreguntaEN> getPreguntasByAsignatura(AsignaturaEN asignaturaEN) throws CADException
    {
        ArrayList<PreguntaEN> preguntas = null;
        
        try
        {
            preguntas = preguntaCAD.getPreguntasByAsignatura(asignaturaEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
        
        return preguntas;
    }
    
    /**
     * Obtiene solo las preguntas sin clasificar de la asignatura
     * pasada por parámetro
     * 
     * @param asignaturaEN AsignaturaEN de la que se recuperarán las preguntas
     * @return ArrayList con la lista de preguntas de la asignatura
     * @throws CADException Error en el acceso a la base de datos
     */
    public ArrayList<PreguntaEN> getPreguntasSinClasificarByAsignatura(AsignaturaEN asignaturaEN) throws CADException
    {
        ArrayList<PreguntaEN> preguntas = null;
        
        try
        {
            preguntas = preguntaCAD.getPreguntasSinClasificarByAsignatura(asignaturaEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
        
        return preguntas;
    }
        
    /**
     * Obtiene las preguntas marcadas con "utilizar en el próximo examen
     * de una asignatura pasada por parametro
     * 
     * @param asignaturaEN AsignaturaEN de la que se recuperarán las preguntas
     * @return ArrayList con la lista de preguntas de la asignatura
     * @throws CADException Error en el acceso a la base de datos
     */
    public ArrayList<PreguntaEN> getPreguntasProximoExamenByAsignatura(AsignaturaEN asignaturaEN) throws CADException
    {
        ArrayList<PreguntaEN> preguntas = null;
        
        try
        {
            preguntas = preguntaCAD.getPreguntasProximoExamenByAsignatura(asignaturaEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
        
        return preguntas;
    }
     
    /**
     * Obtiene las preguntas de una asignatura pasada por parametro que cumplan 
     * el fltro pasado por parámentro
     * 
     * @param asignaturaEN AsignaturaEN de la que se recuperarán las preguntas
     * @param filtro String del WHERE en formato SQL para filtrar las preguntas
     * @return ArrayList con la lista de preguntas de la asignatura
     * @throws CADException Error en el acceso a la base de datos
     */
    public ArrayList<PreguntaEN> getPreguntasByAsignaturaYFiltro(AsignaturaEN asignaturaEN, String filtro) throws CADException
    {
        ArrayList<PreguntaEN> preguntas = null;
        
        try
        {
            preguntas = preguntaCAD.getPreguntasByAsignaturaYFiltro(asignaturaEN, filtro);
        }
        catch (CADException ex)
        {
            throw ex;
        }
        
        return preguntas;
    }
    
    /**
     * Carga los datos de la clasificación de la pregunta
     * 
     * @param preguntaEN PreguntaEN en el que se rellenaran los datos
     * @throws CADException Si se produjo un error en el CAD  
     */
    public void rellenarClasificacion(PreguntaEN preguntaEN) throws CADException
    {
        ClasificacionTemaCEN clasificacionTemaCEN = new ClasificacionTemaCEN();
        ClasificacionTemaEN clasificacionTemaEN;
        
        try
        {
            clasificacionTemaEN = clasificacionTemaCEN.getClasificacionByPregunta(preguntaEN);
            preguntaEN.setClasificacion(clasificacionTemaEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
    } 
    
    /**
     * Carga las opciones de la pregunta en la estructura pasada por parámetro
     * 
     * @param preguntaEN PreguntaEN en el que se rellenaran los datos
     * @throws CADException Si se produjo un error en el CAD  
     */
    public void rellenarOpciones(PreguntaEN preguntaEN) throws CADException
    {
        OpcionCEN opcionCEN = new OpcionCEN();
        ArrayList<OpcionEN> opciones;
        
        try
        {
            opciones = opcionCEN.getOpcionesByPregunta(preguntaEN);
            preguntaEN.setOpciones(opciones);
        }
        catch (CADException ex)
        {
            throw ex;
        }
    }
    
    /**
     * Carga los examenes donde aparece la pregunta en la estructura
     * pasada por parámetro
     * 
     * @param preguntaEN PreguntaEN en el que se rellenaran los datos
     * @throws CADException Si se produjo un error en el CAD  
     */
    public void rellenarExamenes(PreguntaEN preguntaEN) throws CADException
    {
        ExamenPreguntaCEN examenPreguntaCEN = new ExamenPreguntaCEN();
        ArrayList<ExamenPreguntaEN> examenPreguntas;
        
        try
        {
            examenPreguntas = examenPreguntaCEN.getExamenesPreguntaByPregunta(preguntaEN);
            preguntaEN.setExamenes(examenPreguntas);
        }
        catch (CADException ex)
        {
            throw ex;
        }
    }

    /**
     * Carga las traducciones de la pregunta en la estructura pasada por parámetro
     * 
     * @param preguntaEN PreguntaEN en el que se rellenaran los datos
     * @throws CADException Si se produjo un error en el CAD  
     */
    public void rellenarTraducciones(PreguntaEN preguntaEN) throws CADException
    {
        TraduccionPreguntaCEN traduccionPreguntaCEN = new TraduccionPreguntaCEN();
        ArrayList<TraduccionPreguntaEN> traducciones;
        
        try
        {
            traducciones = traduccionPreguntaCEN.getTraduccionesPreguntaByPregunta(preguntaEN);
            preguntaEN.setTraducciones(traducciones);
        }
        catch (CADException ex)
        {
            throw ex;
        }
    } 
    
    /**
     * Indica si una preguntaEN existe en una lista
     * 
     * @param lista ArrayList donde se buscará la pregunta
     * @param pregunta PreguntaEN a buscar
     * @return true si se encuentra, false en caso contrario
     */
    public boolean existeEnLista(ArrayList<PreguntaEN> lista, PreguntaEN pregunta)
    { 
        for (PreguntaEN p : lista)
        {
            if (p.getCodigo() == pregunta.getCodigo())
                return true;
        }
        
        return false;
    }
    
    /**
     * Elimina una PreguntaEN de una lista de preguntas
     * 
     * @param lista ArrayList donde se eliminara la pregunta
     * @param pregunta PreguntaEN a eliminar
     */
    public void borrarDeLista(ArrayList<PreguntaEN> lista, PreguntaEN pregunta)
    { 
        for (int i = 0; i < lista.size(); i++)
        {
            if (lista.get(i).getCodigo() == pregunta.getCodigo())
            {
                lista.remove(i);
                break;
            }
        }
    }
    
    /**
     * Sustituye una pregunta por otra en una lista pasada por parámetro
     * 
     * @param lista ArrayList donde la pregunta se va a sustituir
     * @param a_sustituir PreguntaEN a sustituir
     * @param sustituta PreguntaEN sustituta
     */
    public void sustituirEnLista(ArrayList<PreguntaEN> lista, PreguntaEN a_sustituir, PreguntaEN sustituta)
    { 
        for (int i = 0; i < lista.size(); i++)
        {
            if (lista.get(i).getCodigo() == a_sustituir.getCodigo())
            {
                lista.set(i, sustituta);
                break;
            }
        }
    }
            
    /**
     * Obtiene solo las preguntas habilitadas de la asignatura 
     * pasada por parámetro
     * 
     * @param asignaturaEN AsignaturaEN de la que se recuperarán las preguntas
     * @return ArrayList con la lista de preguntas de la asignatura
     * @throws CADException Error en el acceso a la base de datos
     */
    public ArrayList<PreguntaEN> getPreguntasHabilitadasByAsignatura(AsignaturaEN asignaturaEN) throws CADException
    {
        ArrayList<PreguntaEN> preguntas = null;
        
        try
        {
            preguntas = preguntaCAD.getPreguntasHabilitadasByAsignatura(asignaturaEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
        
        return preguntas;
    }

    /**
     * Obtiene las preguntas habilitadas filtradas por fecha de último uso
     * de la asignatura pasada como parámentro
     * 
     * @param asignaturaEN AsignaturaEN de la que se recuperarán las preguntas
     * @param ultimoUso fecha de último uso máximo con el que se filtraran
     * las preguntas
     * @return ArrayList con la lista de preguntas de la asignatura
     * @throws CADException Error en el acceso a la base de datos
     */
    public ArrayList<PreguntaEN> getPreguntasHabilitadasByAsignaturaYUltimoUso(AsignaturaEN asignaturaEN, Calendar ultimoUso) throws CADException
    {
        ArrayList<PreguntaEN> preguntas = null;
        
        try
        {
            preguntas = preguntaCAD.getPreguntasHabilitadasByAsignaturaYUltimoUso(asignaturaEN, ultimoUso);
        }
        catch (CADException ex)
        {
            throw ex;
        }
        
        return preguntas;
    }
}
