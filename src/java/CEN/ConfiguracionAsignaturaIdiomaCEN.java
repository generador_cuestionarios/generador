/* 
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : ConfiguracionAsignaturaIdiomaCEN
 * Created on : 15-abr-2014, 00:46:00
 * Author     : Raúl Sempere Trujillo
 */

package CEN;

import clases_generador.CADException;
import CAD.ConfiguracionAsignaturaIdiomaCAD;
import EN.AsignaturaEN;
import EN.ConfiguracionAsignaturaIdiomaEN;
import EN.IdiomaEN;
import java.util.ArrayList;

/**
 * Capa de Entidad de Negocio de la entidad ConfiguracionAsignaturaIdiomaCEN
 * 
 * @author Raúl Sempere Trujillo
 */
public class ConfiguracionAsignaturaIdiomaCEN
{
    
    /**
     * Variable privada: Clase CAD de la entidad configuracionAsignaturaIdioma
     */
    private final ConfiguracionAsignaturaIdiomaCAD configuracionAsignaturaIdiomaCAD;
    
    /**
     * Constructor por defecto. Instancia el CAD para que gestione el acceso 
     * a la base de datos.
     */
    public ConfiguracionAsignaturaIdiomaCEN()
    {
        configuracionAsignaturaIdiomaCAD = new ConfiguracionAsignaturaIdiomaCAD();
    }
    
    /**
     * Utiliza la capa CAD para insertar la configuracionAsignaturaIdiomaEN en la base de datos
     * 
     * @param configuracionAsignaturaIdiomaEN ConfiguracionAsignaturaIdiomaEN a crear en la base datos
     * @return true en caso de éxito, false si no se pudo modificar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean New (ConfiguracionAsignaturaIdiomaEN configuracionAsignaturaIdiomaEN) throws CADException
    {
        boolean insertado = false;
        
        try
        {
            insertado = configuracionAsignaturaIdiomaCAD.New(configuracionAsignaturaIdiomaEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
   
        return insertado;
    }
    
    /**
     * Utiliza la capa CAD para modificar el objeto 
     * configuracionAsignaturaIdiomaEN pasado
     * por parámetro en la base de datos con los nuevos datos que contiene
     * 
     * @param configuracionAsignaturaIdiomaEN ConfiguracionAsignaturaIdiomaEN 
     * a modificar en la base de datos
     * @return true en caso de éxito, false si no se pudo modificar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean Modify (ConfiguracionAsignaturaIdiomaEN configuracionAsignaturaIdiomaEN) throws CADException
    {
        boolean modificado = false;
        
        try
        {
            modificado = configuracionAsignaturaIdiomaCAD.Modify(configuracionAsignaturaIdiomaEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
   
        return modificado;
    }
    
    /**
     * Utiliza la capa CAD para elimina el objeto 
     * configuracionAsignaturaIdiomaEN pasado 
     * por parámetro de la base de datos
     * 
     * @param configuracionAsignaturaIdiomaEN ConfiguracionAsignaturaIdiomaEN
     * a eliminar de la base de datos
     * @return true en caso de éxito, false si no se pudo eliminar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean Delete (ConfiguracionAsignaturaIdiomaEN configuracionAsignaturaIdiomaEN) throws CADException
    {
        boolean eliminado = false;
        
        try
        {
            eliminado = configuracionAsignaturaIdiomaCAD.Delete(configuracionAsignaturaIdiomaEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
   
        return eliminado;
    }
    
    /**
     * Obtiene la lista de configuraciones de una asignatura pasada por
     * parámetro
     * 
     * @param asignaturaEN AsignaturaEN de la que se quieren obtener las
     * configuraciones
     * @return ArrayList con la lista de configuraciones
     * @throws CADException Error en el acceso a la base de datos
     */
    public ArrayList<ConfiguracionAsignaturaIdiomaEN> getConfiguracionesAsignaturaIdiomaByAsignatura(AsignaturaEN asignaturaEN) throws CADException
    {
        ArrayList<ConfiguracionAsignaturaIdiomaEN> configuraciones = new ArrayList<ConfiguracionAsignaturaIdiomaEN>();
        
        try
        {
            configuraciones = configuracionAsignaturaIdiomaCAD.getConfiguracionesAsignaturaIdiomaByAsignatura(asignaturaEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
        
        return configuraciones;
    }
    
    /**
     * Obtiene la configuracion del idioma principal de una asignatura
     * pasada por parámetro
     * 
     * @param asignaturaEN AsignaturaEN de la que se quiere obtener la
     * configuracion
     * @return ConfiguracionAsignaturaIdiomaEN con la configuracion en el idioma.
     * @throws CADException Error en el acceso a la base de datos
     */
    public ConfiguracionAsignaturaIdiomaEN getConfiguracionIdiomaPrincipal(AsignaturaEN asignaturaEN) throws CADException
    {
        ConfiguracionAsignaturaIdiomaEN configuracionAsignaturaIdiomaEN = null;
        
        try
        {
            configuracionAsignaturaIdiomaEN = configuracionAsignaturaIdiomaCAD.getConfiguracionIdiomaPrincipal(asignaturaEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
        
        return configuracionAsignaturaIdiomaEN;
    } 
    
    /**
     * Obtiene la lista de configuraciones principales de todas las asignaturas
     * 
     * @return ArrayList con la lista de configuraciones obtenidas
     * @throws CADException Error en el acceso a la base de datos
     */
    public ArrayList<ConfiguracionAsignaturaIdiomaEN> getConfiguracionIdiomaPrincipalAsignaturas() throws CADException
    {
        ArrayList<ConfiguracionAsignaturaIdiomaEN> configuracionAsignaturas = null;
        
        try
        {
            configuracionAsignaturas = configuracionAsignaturaIdiomaCAD.getConfiguracionIdiomaPrincipalAsignaturas();
        }
        catch (CADException ex)
        {
            throw ex;
        }
        
        return configuracionAsignaturas;
    } 
    
    /**
     * Obtiene la lista de configuraciones de todas las asignaturas cuyo idioma
     * pasado por parámetro es el principal.
     * 
     * @param idiomaEN IdiomaEN del cual se obtendran las configuraciones
     * @return ArrayList con la lista de configuraciones obtenidas
     * @throws CADException Error en el acceso a la base de datos
     */
    public ArrayList<ConfiguracionAsignaturaIdiomaEN> getConfiguracionesPrincipalesByIdioma(IdiomaEN idiomaEN) throws CADException
    {
        ArrayList<ConfiguracionAsignaturaIdiomaEN> configuracionAsignaturas = null;
        
        try
        {
            configuracionAsignaturas = configuracionAsignaturaIdiomaCAD.getConfiguracionesPrincipalesByIdioma(idiomaEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
        
        return configuracionAsignaturas;
    }  
    
    /**
     * Obtiene la configuracion de una asignatura en un idioma pasado por
     * parámetro
     * 
     * @param asignaturaEN AsignaturaEN de la que se quiere obtener la
     * configuracion
     * @param idiomaEN IdiomaEN de la configuración a obtener
     * @return ConfiguracionAsignaturaIdiomaEN con laconfiguracion en el idioma.
     * @throws CADException Error en el acceso a la base de datos
     */ 
    public ConfiguracionAsignaturaIdiomaEN getConfiguracionAsignaturaIdiomaByAsignaturaEIdioma(AsignaturaEN asignaturaEN, IdiomaEN idiomaEN) throws CADException
    {
        ConfiguracionAsignaturaIdiomaEN configuracionAsignaturaIdiomaEN = null;
        
        try
        {
            configuracionAsignaturaIdiomaEN = configuracionAsignaturaIdiomaCAD.getConfiguracionesAsignaturaIdiomaByAsignaturaEIdioma(asignaturaEN, idiomaEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
        
        return configuracionAsignaturaIdiomaEN;
    }  
}
