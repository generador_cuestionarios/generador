/* 
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : TraduccionInstruccionCEN
 * Created on : 15-abr-2014, 00:46:00
 * Author     : Raúl Sempere Trujillo
 */

package CEN;

import clases_generador.CADException;
import CAD.TraduccionInstruccionCAD;
import EN.ExamenEN;
import EN.IdiomaEN;
import EN.InstruccionEN;
import EN.TraduccionInstruccionEN;
import java.util.ArrayList;

/**
 * Capa de Entidad de Negocio de la entidad TraduccionInstruccionCEN
 * 
 * @author Raúl Sempere Trujillo
 */
public class TraduccionInstruccionCEN
{
    
    /**
     * Variable privada: Clase CAD de la entidad traduccionInstruccion
     */
    private final TraduccionInstruccionCAD traduccionInstruccionCAD;
    
    /**
     * Constructor por defecto. Instancia el CAD para que gestione el acceso 
     * a la base de datos.
     */
    public TraduccionInstruccionCEN()
    {
        traduccionInstruccionCAD = new TraduccionInstruccionCAD();
    }
    
    /**
     * Utiliza la capa CAD para insertar la traduccionInstruccionEN en la base de datos
     * 
     * @param traduccionInstruccionEN TraduccionInstruccionEN a crear en la base datos
     * @return true en caso de éxito, false si no se pudo modificar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean New (TraduccionInstruccionEN traduccionInstruccionEN) throws CADException
    {
        boolean insertado = false;
        
        try
        {
            insertado = traduccionInstruccionCAD.New(traduccionInstruccionEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
   
        return insertado;
    }
    
    /**
     * Utiliza la capa CAD para modificar el objeto traduccionInstruccionEN pasado
     * por parámetro en la base de datos con los nuevos datos que contiene
     * 
     * @param traduccionInstruccionEN TraduccionInstruccionEN a modificar en la base de datos
     * @return true en caso de éxito, false si no se pudo modificar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean Modify (TraduccionInstruccionEN traduccionInstruccionEN) throws CADException
    {
        boolean modificado = false;
        
        try
        {
            modificado = traduccionInstruccionCAD.Modify(traduccionInstruccionEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
   
        return modificado;
    }
    
    /**
     * Utiliza la capa CAD para elimina el objeto traduccionInstruccionEN pasado 
     * por parámetro de la base de datos
     * 
     * @param traduccionInstruccionEN TraduccionInstruccionEN a eliminar de la base de datos
     * @return true en caso de éxito, false si no se pudo eliminar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean Delete (TraduccionInstruccionEN traduccionInstruccionEN) throws CADException
    {
        boolean eliminado = false;
        
        try
        {
            eliminado = traduccionInstruccionCAD.Delete(traduccionInstruccionEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
   
        return eliminado;
    }

    /**
     * Obtiene la traducción de una instrucción en un idioma concreto
     * 
     * @param instruccionEN InstruccionEN de la que se obtendrá la traducción
     * @param idiomaEN IdiomaEN de la traducción    
     * @return TraduccionInstruccionEN con la traducción de la instrucción
     * @throws CADException Error en el acceso a la base de datos
     */
    public TraduccionInstruccionEN getTraduccionInstruccionByInstruccionEIdioma(InstruccionEN instruccionEN, IdiomaEN idiomaEN) throws CADException
    {
        TraduccionInstruccionEN traduccionInstruccionEN = null;
        
        try
        {
            traduccionInstruccionEN = traduccionInstruccionCAD.getTraduccionInstruccionByInstruccionEIdioma(instruccionEN, idiomaEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
        
        return traduccionInstruccionEN;
    }
    
    /**
     * Obiene todas las traducciones de una instrucción
     * 
     * @param instruccionEN InstruccionEN de la que se obtendrán las traducciones
     * @return ArrayList con la lista de traducciones
     * @throws CADException Error en el acceso a la base de datos
     */
    public ArrayList<TraduccionInstruccionEN> getTraduccionesInstruccionByInstruccion(InstruccionEN instruccionEN) throws CADException
    {
        ArrayList<TraduccionInstruccionEN> traducciones = new ArrayList<TraduccionInstruccionEN>();
        
        try
        {
            traducciones = traduccionInstruccionCAD.getTraduccionesInstruccionByInstruccion(instruccionEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
        
        return traducciones;
    }
    
    /**
     * Obtiene las traducciones en un idioma pasado por parámetro de todas
     * las instrucciones de un examen
     * 
     * @param examenEN ExamenEN del que se obtendrán las instrucciones
     * @param idiomaEN IdiomaEN idioma de la traducción de las instrucciones
     * @return ArrayList con la traduccion de las instrucciones
     * @throws CADException Error en el acceso a la base de datos
     */
    public ArrayList<TraduccionInstruccionEN> getTraduccionesInstruccionByExamenEIdioma(ExamenEN examenEN, IdiomaEN idiomaEN) throws CADException
    {
        ArrayList<TraduccionInstruccionEN> traducciones = new ArrayList<TraduccionInstruccionEN>();
        
        try
        {
            traducciones = traduccionInstruccionCAD.getTraduccionesInstruccionByExamenEIdioma(examenEN, idiomaEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
        
        return traducciones;
    }

    /**
     * Elimina un enunciado de la lista y devuelve si se encontró
     * 
     * @param lista lista donde se buscara el enunciado de la traducción
     * @param enunciado enunciado a eliiminar.
     * 
     * @return true si se pudo encontrar, false en caso contrario
     */
    public boolean eliminaTraduccionEnListaByEnunciado(ArrayList<TraduccionInstruccionEN> lista, String enunciado)
    {
        boolean encontrada = false;
        
        for (int i = 0; i < lista.size(); i++)
        {
            if (lista.get(i).getEnunciado().equals(enunciado))
            {
                encontrada = true;
                lista.remove(i);
                break;
            }
        }
        
        return encontrada;
    }
}
