/* 
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : KeywordCEN
 * Created on : 15-abr-2014, 00:46:00
 * Author     : Raúl Sempere Trujillo
 */

package CEN;

import clases_generador.CADException;
import CAD.KeywordCAD;
import EN.KeywordEN;
import EN.TemaEN;
import java.util.ArrayList;

/**
 * Capa de Entidad de Negocio de la entidad KeywordCEN
 * 
 * @author Raúl Sempere Trujillo
 */
public class KeywordCEN
{
    
    /**
     * Variable privada: Clase CAD de la entidad keyword
     */
    private final KeywordCAD keywordCAD;
    
    /**
     * Constructor por defecto. Instancia el CAD para que gestione el acceso 
     * a la base de datos.
     */
    public KeywordCEN()
    {
        keywordCAD = new KeywordCAD();
    }
    
    /**
     * Utiliza la capa CAD para insertar la keywordEN en la base de datos
     * 
     * @param keywordEN KeywordEN a crear en la base datos
     * @return true en caso de éxito, false si no se pudo modificar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean New (KeywordEN keywordEN) throws CADException
    {
        boolean insertado = false;
        
        try
        {
            insertado = keywordCAD.New(keywordEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
   
        return insertado;
    }

    /**
     * Utiliza la capa CAD para elimina el objeto keywordEN pasado 
     * por parámetro de la base de datos
     * 
     * @param keywordEN KeywordEN a eliminar de la base de datos
     * @return true en caso de éxito, false si no se pudo eliminar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean Delete (KeywordEN keywordEN) throws CADException
    {
        boolean eliminado = false;
        
        try
        {
            eliminado = keywordCAD.Delete(keywordEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
   
        return eliminado;
    }

    /**
     * Obtiene un keyword concreto de un tema por su código dentro del tema
     * 
     * @param temaEN TemaEN del que se obtendrá el keyword
     * @param codigo int con el código del keyword a recuperar
     * @return KeywordEN keyword obtenido
     * @throws CADException Error en el acceso a la base de datos 
     */
    public KeywordEN getKeywordByCodigo(TemaEN temaEN, int codigo) throws CADException
    {
        KeywordEN keywordEN = null;
        
        try
        {
            keywordEN = keywordCAD.getKeywordByCodigo(temaEN, codigo);
        }
        catch (CADException ex)
        {
            throw ex;
        }
        
        return keywordEN;
    }
    
    /**
     * Devuelve la lista de keywords de un tema
     * 
     * @param temaEN TemaEN del que se obtendrán los keywords
     * @return ArrayList con la lista de keywords
     * @throws CADException Error en el acceso a la base de datos 
     */
    public ArrayList<KeywordEN> getKeywordsByTema(TemaEN temaEN) throws CADException
    {
        ArrayList<KeywordEN> keywords = new ArrayList<KeywordEN>();
        
        try
        {
            keywords = keywordCAD.getKeywordsByTema(temaEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
        
        return keywords;
    }
    
}
