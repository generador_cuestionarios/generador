/* 
 * Copyright (C) 2014 Raúl Sempere Trujillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : ExamenCEN
 * Created on : 15-abr-2014, 00:46:00
 * Author     : Raúl Sempere Trujillo
 */

package CEN;

import clases_generador.CADException;
import CAD.ExamenCAD;
import EN.AsignaturaEN;
import EN.ExamenEN;
import EN.ExamenPreguntaEN;
import EN.InstruccionEN;
import EN.PreguntaEN;
import EN.PreparacionExamenIdiomaEN;
import java.util.ArrayList;
import java.util.Calendar;


/**
 * Capa de Entidad de Negocio de la entidad ExamenCEN
 * 
 * @author Raúl Sempere Trujillo
 */
public class ExamenCEN
{
    
    /**
     * Variable privada: Clase CAD de la entidad examen
     */
    private final ExamenCAD examenCAD;
    
    /**
     * Constructor por defecto. Instancia el CAD para que gestione el acceso 
     * a la base de datos.
     */
    public ExamenCEN()
    {
        examenCAD = new ExamenCAD();
    }
    
    /**
     * Utiliza la capa CAD para insertar la examenEN en la base de datos
     * 
     * @param examenEN ExamenEN a crear en la base datos
     * @return true en caso de éxito, false si no se pudo modificar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean New (ExamenEN examenEN) throws CADException
    {
        boolean insertado = false;
        
        try
        {
            insertado = examenCAD.New(examenEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
   
        return insertado;
    }
    
    /**
     * Utiliza la capa CAD para modificar el objeto examenEN pasado
     * por parámetro en la base de datos con los nuevos datos que contiene
     * 
     * @param examenEN ExamenEN a modificar en la base de datos
     * @return true en caso de éxito, false si no se pudo modificar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean Modify (ExamenEN examenEN) throws CADException
    {
        boolean modificado = false;
        
        try
        {
            modificado = examenCAD.Modify(examenEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
   
        return modificado;
    }
    
    /**
     * Utiliza la capa CAD para elimina el objeto examenEN pasado 
     * por parámetro de la base de datos
     * 
     * @param examenEN ExamenEN a eliminar de la base de datos
     * @return true en caso de éxito, false si no se pudo eliminar
     * @throws CADException Error en el acceso a la base de datos
     */
    public boolean Delete (ExamenEN examenEN) throws CADException
    {
        boolean modificado = false;
        
        try
        {
            modificado = examenCAD.Delete(examenEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
   
        return modificado;
    }
    
    /**
     * Obtiene un examen de la base de datos y lo devuelve
     * 
     * @param asignaturaEN AsignaturaEN a la que pertenece el examen
     * @param codigo int. Código del examen a recuperar
     * @return ExamenEN con el examen recuperado
     * @throws CADException Error en el acceso a la base de datos
     */
    public ExamenEN getExamenByCodigo(AsignaturaEN asignaturaEN, int codigo) throws CADException
    {
        ExamenEN examen = null;
        
        try
        {
            examen = examenCAD.getExamenByCodigo(asignaturaEN, codigo);
        }
        catch (CADException ex)
        {
            throw ex;
        }
        
        return examen;
    }
    
    /**
     * Devuelve toda la lista de exámenes de la asignatura pasada por parámetro
     * 
     * @param asignaturaEN AsignaturaEN de la que se devolverán los exámenes
     * @return ArrayList con la lista de exámenes de la asignatura
     * @throws CADException Error en el acceso a la base de datos
     */
    public ArrayList<ExamenEN> getExamenesByAsignatura(AsignaturaEN asignaturaEN) throws CADException
    {
        ArrayList<ExamenEN> examenes = null;
        
        try
        {
            examenes = examenCAD.getExamenesByAsignatura(asignaturaEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
        
        return examenes;
    }
    
    /**
     * Obtiene la fecha de último uso de la pregunta pasada por parámetro
     * 
     * @param preguntaEN PreguntaEN de la que se quiere conocer el último uso
     * @return Calendar con la fecha de último uso
     * @throws CADException Error en el acceso a la base de datos
     */
    public Calendar getUltimoUsoPreguntaByPregunta(PreguntaEN preguntaEN) throws CADException
    {
        Calendar ultimoUso;
        
        try
        {
            ultimoUso = examenCAD.getUltimoUsoPreguntaByPregunta(preguntaEN);
        }
        catch (CADException ex)
        {
            throw ex;
        }
        
        return ultimoUso;
    }
    
    /**
     * Devuelve los examenes de la asignatura que cumplan con el filtro pasado
     * por parámetro
     * 
     * @param asignaturaEN AsignaturaEN de la que se devolverán los exámenes
     * @param filtro String del WHERE en formato SQL para filtrar los exámenes
     * @return ArrayList con la lista de exámenes de la asignatura
     * @throws CADException Error en el acceso a la base de datos
     */
    public ArrayList<ExamenEN> getExamenesByAsignaturaYFiltro(AsignaturaEN asignaturaEN, String filtro) throws CADException
    {
        ArrayList<ExamenEN> examenes = null;
        
        try
        {
            examenes = examenCAD.getExamenesByAsignaturaYFiltro(asignaturaEN, filtro);
        }
        catch (CADException ex)
        {
            throw ex;
        }
        
        return examenes;
    }
    
    /**
     * Rellena las preparaciones del examen en la estructura del ExamenEN
     * pasado por parámetro
     * 
     * @param examenEN ExamenEN en el que se rellenarán las preparaciónes
     * @throws CADException Error en el acceso a la base de datos
     */
    public void rellenarPreparacionIdiomas(ExamenEN examenEN) throws CADException
    {
        PreparacionExamenIdiomaCEN preparacionExamenIdiomaCEN = new PreparacionExamenIdiomaCEN();
        ArrayList<PreparacionExamenIdiomaEN> preparaciones;
        
        try
        {
            preparaciones = preparacionExamenIdiomaCEN.getPreparacionesExamenByExamen(examenEN);
            examenEN.setPreparacionIdiomas(preparaciones);
        }
        catch (CADException ex)
        {
            throw ex;
        }
    }

    /**
     * Rellena las preguntas del examen en la estructura del ExamenEN
     * pasado por parámetro
     * 
     * @param examenEN ExamenEN en el que se rellenarán las preguntas
     * @throws CADException Error en el acceso a la base de datos
     */
    public void rellenarPreguntas(ExamenEN examenEN) throws CADException
    {
        ExamenPreguntaCEN examenPreguntaCEN = new ExamenPreguntaCEN();
        ArrayList<ExamenPreguntaEN> examenPreguntas;
        
        try
        {
            examenPreguntas = examenPreguntaCEN.getExamenPreguntasByExamen(examenEN);
            examenEN.setPreguntas(examenPreguntas);
        }
        catch (CADException ex)
        {
            throw ex;
        }
    }

    /**
     * Rellena las instrucciones del examen en la estructura del ExamenEN
     * pasado por parámetro
     * 
     * @param examenEN ExamenEN en el que se rellenarán las instrucciones
     * @throws CADException Error en el acceso a la base de datos
     */
    public void rellenarInstrucciones(ExamenEN examenEN) throws CADException
    {
        InstruccionCEN instruccionCEN = new InstruccionCEN();
        ArrayList<InstruccionEN> instrucciones;
        
        try
        {
            instrucciones = instruccionCEN.getInstruccionesByExamen(examenEN);
            examenEN.setInstrucciones(instrucciones);
        }
        catch (CADException ex)
        {
            throw ex;
        }
    } 
    
}
